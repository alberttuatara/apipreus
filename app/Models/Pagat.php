<?php

namespace App\Models;

use Exception;
use APIException;


class Pagat extends Model
{   
	protected $table = 'DESCRIPCIO_PREUSPAGATS';
	protected $primaryKey = 'CODIPROD';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [    ];

    

}
