<?php

namespace App\Models;

use Exception;
use APIException;


class Percebut extends Model
{   
	protected $table = 'DESCRIPCIO_PREUSPERCEBUTS';
	protected $primaryKey = 'CODIPROD';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [    ];

    

}
