<?php

namespace App\Models;

use Exception;
use APIException;


class Territori extends Model
{   
	protected $table = 'TERRITORI';
	protected $primaryKey = 'CODITERR';
    public $incrementing = false;
	public $timestamps = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
	

    

}
