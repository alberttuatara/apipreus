<?php

namespace App\Models;

use Exception;
use APIException;


class Notapercebut extends Model
{   
	protected $table = 'NOTES_PREUSPERCEBUTS';
	protected $primaryKey = array('CODIPROD', 'CODITERR', 'ANY', 'CODITERR','MES','SEQ');
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [    ];
	
	protected $dates = [
        'DATACOMENTARI'
    ];

    

}
