<?php

namespace App\Models;

use Exception;
use APIException;


class Aplicacio extends Model
{   
	protected $table = 'APLICACIO';
	protected $primaryKey = 'IDAPP';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [ ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [ 'ULTIM_CANVI','LOG_CANVIS'   ];

    

}
