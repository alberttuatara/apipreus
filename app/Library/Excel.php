<?php
/*
 * 
 * Producte        Qfisiques00       QF�siques05           �ndex00    �ndex05
 * Segalls                  N                N                 N         N
 * Suro                     S                S                 S         N
 * Prods forestals          S                S                 S         N
 * Bestiar Vida             S                S                 N         N
 * Base00 -- > 071600,071700,071800 --> N
*  Base05 -- > 071200,071300,071400 --> N
 * 
 */
// include_once "excelwriter.inc.php";	
// include_once "class.Prices.php";
/*
 * clase Excel
*/
namespace App\Library {
	
	class Excel {	
		var $conn;
		var $baseTable;
		var $base;
		var $qFisiquesRed;					  
		var $valorConsist;					
		var $codisGrupsPer;
		var $AgrupacionsGrups;
		var $AgrupacionsGrupsPer;                                                       
		var $AgrupacionsGrupsPag; 	                              	                        
		var $valorConsistWithBestiarVida;		
		var	$a_minuscules;
		var $preus_base_anual_any_2000_red;	
		var $preus_base_anual_any_2005_red;	
		var $indexGreen;
		var $indexBlue;
		var $indexGroups;
		var $indexAll;
		var $allCodes;
		var $allCodesWithBestiarVida;					
		var $estructuraSortidaPer;
		var $estructuraSortidaPag;									
		var $estructuraSortidaPagSalaris;														
		var $sortidaPercent = false;
		var $toHTML_format_ZERO_especial = true;
		
		/**
		 * Funci� Excel:
		* @param - $conn	 
		*/
		function __construct($conn,$pagatspercebuts,$base) 
		{
			$this->conn = $conn;
			$this->baseTable = "base".$pagatspercebuts.$base;
			$this->base = $base;
			
			
		}
		
		function EsGrup($codi)
		{   
			return array_key_exists($codi,$this->AgrupacionsGrups);
		}
		
		/**
		 * Funci� entradaPreusAnyT:
		* @param - $year: 	
		* @param - $bestiarvida=false: 		
		*/	
		function entradaPreusAnyT($year,$bestiarvida=false) {
			$entradaPreusAnyT=array();
			// Inicialitzem el valor del preu
			if ($this->base=='00') 
			{ $codifora1='071600'; $codifora2='071700'; $codifora3='071800'; 
			}
			else 
			{ $codifora1='071200'; $codifora2='071300'; $codifora3='071400'; 
			}
			if (!$bestiarvida) $sql = strtoupper("SELECT CodiProd, NomProd FROM descripcio_preuspercebuts WHERE CodiProd NOT IN " .
								"(SELECT CodiProd FROM descripcio_preuspercebuts WHERE 
								 Grup = 'BESTIAR PER VIDA') AND " .
								"CodiProd <> '".$codifora1."' AND CodiProd <> '".$codifora2."' AND CodiProd <> '".$codifora3."' and ACTIU".$this->base."='s'");	
								// treiem "BESTIAR PER VIDA" i Oli d'oliva
			else $sql = strtoupper("SELECT CodiProd, NomProd FROM descripcio_preuspercebuts WHERE ".
								"CodiProd <> '".$codifora1."' AND CodiProd <> '".$codifora2."' AND CodiProd <> '".$codifora3."' and ACTIU".$this->base."='s' ");							
			$rows = $this->conn->select($sql);
			if (count($rows)>0) {
				foreach($rows as $rowOBJ){  
					$row = ((array) $rowOBJ); 
					$entradaPreusAnyT[$row['CODIPROD']]['DESCRIPCIO'] = $row['NOMPROD'];
					
					for ($i = 1; $i<13; $i++) {
						$entradaPreusAnyT[$row['CODIPROD']]['MES' .$i] = 0;
					}
				}
			}
			else 
				return null;	
			
			// suma de preu * qfisica any base
			for ($i = 1; $i<13; $i++) 
			{
				if (!$bestiarvida) $sql = strtoupper("SELECT preuspercebuts.CodiProd, SUM(preuspercebuts.FinPreu*basepercebuts".$this->base.".MES" .$i .") AS C1 
					   FROM preuspercebuts, basepercebuts".$this->base." WHERE " .
					   "preuspercebuts.ANY = " .$year ." AND preuspercebuts.Mes = " .$i ." 
					   AND preuspercebuts.CodiProd " .
					   "NOT IN (SELECT CodiProd FROM descripcio_preuspercebuts WHERE Grup = 'BESTIAR PER VIDA') AND " .
					   "preuspercebuts.CodiProd <> '".$codifora1."' AND preuspercebuts.CodiProd <> '".$codifora2."' AND " .
					   "preuspercebuts.CodiProd <> '".$codifora3."' AND preuspercebuts.CodiProd = basepercebuts".$this->base.".CodiProd AND " .
					   "preuspercebuts.CodiTerr = basepercebuts".$this->base.".CodiTerr GROUP BY preuspercebuts.CodiProd  ");
				if ($bestiarvida) $sql = strtoupper("SELECT preuspercebuts.CodiProd, SUM(preuspercebuts.FinPreu*basepercebuts".$this->base.".MES" .$i .") AS C1
					   FROM preuspercebuts, basepercebuts".$this->base." WHERE " .
					   "preuspercebuts.ANY = " .$year ." AND preuspercebuts.Mes = " .$i ."  				   
					   AND preuspercebuts.CodiProd <> '".$codifora1."' AND preuspercebuts.CodiProd <> '".$codifora2."' AND " .
					   "preuspercebuts.CodiProd <> '".$codifora3."' AND preuspercebuts.CodiProd = basepercebuts".$this->base.".CodiProd AND " .
					   "preuspercebuts.CodiTerr = basepercebuts".$this->base.".CodiTerr GROUP BY preuspercebuts.CodiProd  ");
				
				$rows = $this->conn->select($sql);
				if (count($rows)>0) { 
					foreach($rows as $rowOBJ){  
						$row = ((array) $rowOBJ);
						
						  
						//for products with codes like ^21 (excepte "Pi insigne Radiata trit") and 221000 the rounding is to 0 places after .
						if ((preg_match('/^21/', $row['CODIPROD']) or $row['C1'] == '221000') and $row['CODIPROD'] != '211120') 
						{
							$entradaPreusAnyT[$row['CODIPROD']]['MES' .$i] = round($row['C1'], 0);
							
						}
						else {
							$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i] = round($row['C1'], 2);
							$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i] = $row['C1'];
						}
						$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i] = $row['C1'];
						//echo $row['CODIPROD'].'-'.'MES'.$i.'-'.$row['C1'].'<br>';
						//if ($row['CODIPROD']=='123300') echo 'SUM(preuspercebuts.FinPreu*basepercebuts00.MES'.$i.') '.$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i].'<br>';
					}
				}
				else 
				{  //return $entradaPreusAnyT; 
				}
			}
			
			//if ($year==2005) { echo "123<hr>"; print_r($entradaPreusAnyT); die();  }	 
			/*
			Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 180927.6 [MES2] => 119555.84 [MES3] => 88643.84 [MES4] => 90510.66 [MES5] => 29274 [MES6] => 251552.85 [MES7] => 1049749.02 [MES8] => 476993.48 [MES9] => 253804.99 [MES10] => 223912.18 [MES11] => 168274.74 [MES12] => 176036.29 ) 
			[002100] => Array ( [DESCRIPCIO] => Arr�s closca rod� i semillarg [MES1] => 649984.45 [MES2] => 337232.98 [MES3] => 346603.9 [MES4] => 346603.9 [MES5] => 346603.9 [MES6] => 339395.5 [MES7] => 339395.5 [MES8] => 0 [MES9] => 0 [MES10] => 67525.05 [MES11] => 374030 [MES12] => 350949.75 ) 
			[002200] => Array ( [DESCRIPCIO] => Arr�s closca llarg [MES1] => 30415 [MES2] => 30415 [MES3] => 30415 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 31735 [MES12] => 31735 ) 
			[003300] => Array ( [DESCRIPCIO] => Ordi per a pinso [MES1] => 229419.18 [MES2] => 229419.18 [MES3] => 226787.22 [MES4] => 63409.5 [MES5] => 61275.72 [MES6] => 415612.41 [MES7] => 1553390.01 [MES8] => 900177.6 [MES9] => 783329.96 [MES10] => 578757.1 [MES11] => 515541.18 [MES12] => 398335.29 ) 
			[003400] => Array ( [DESCRIPCIO] => Ordi per a malta [MES1] => 3725.67 [MES2] => 3725.67 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 8156.16 [MES7] => 81963 [MES8] => 31626 [MES9] => 19132.2 [MES10] => 16298.1 [MES11] => 8788.5 [MES12] => 8186.88 ) 
			[004000] => Array ( [DESCRIPCIO] => Civada [MES1] => 1519.83 [MES2] => . . . . . . . . . . ..................
			.
			*/	
			// Total producci� segona la base
			$sql = strtoupper("SELECT CodiProd, 
				SUM(MES1) AS T1, 
				SUM(MES2) AS T2, 
				SUM(MES3) AS T3, 
				SUM(MES4) AS T4, 
				SUM(MES5) AS T5, 
				SUM(MES6) AS T6, 
				SUM(MES7) AS T7, 
				SUM(MES8) AS T8, 
				SUM(MES9) AS T9,
				SUM(MES10) AS T10,
				SUM(MES11) AS T11,
				SUM(MES12) AS T12
				FROM  basepercebuts".$this->base."
				GROUP BY CodiProd
				ORDER BY CodiProd ");
			$rows = $this->conn->select($sql);
			reset($entradaPreusAnyT);
			$elscodis = array_keys($entradaPreusAnyT);
			// Array ( [0] => 001100 [1] => 002100 [2] => 002200 [3] => 003300 [4] => 003400 [5] => 004000 [6] => 005000 [7] => 006000 [8] => 007000 [9] => 011000 [10] => 013000 [11] => 014000 [12] => 016000 [13] => 021010 [14] => 021020 [15] => 021030 [16] => 021040 [17] => 023000 [18] => 041000 [19] => 042000 [20] => 051110 [21] => 051120 [22] => 051170 [23] => 051180 [24] => 051212 [25] => 051223 [26] => 051224 [27] => 051242 [28] => 052110 [29] => 052120 [30] => 052135 [31] => 052160 [32] => 052209 [33] => 052210 [34] => 052220 [35] => 052230 [36] => 052231 [37] => 052240 [38] => 052250 [39] => 052325 [40] => 052401 [41] => 052402 [42] => 052403 [43] => 052404 [44] => 052405 [45] => 052406 [46] => 052410 [47] => 052490 [48] => 052530 [49] => 052540 [50] => 052610 [51] => 052620 [52] => 052630 [53] => 052900 [54] => 053110 [55] => 053120 [56] => 053130 [57] => 053150 [58] => 053200 [59] => 054011 [60] => 054012 [61] => 054015 [62] => 054020 [63] => 054031 [64] => 054032 [65] => 054033 [66] => 054035 [67] => 054044 [68] => 054045 [69] => 054046 [70] => 054047 [71] => 054050 [72] => 054061 [73] => 054071 [74] => 054072 [75] => 054080 [76] => 054085 [77] => 054091 [78] => 054092 [79] => 054093 [80] => 054101 [81] => 054103 [82] => 054111 [83] => 054120 [84] => 054130 [85] => 054135 [86] => 054140 [87] => 054150 [88] => 054161 [89] => 054162 [90] => 054163 [91] => 054170 [92] => 054190 [93] => 061000 [94] => 062110 [95] => 062120 [96] => 062210 [97] => 062220 [98] => 062610 [99] => 062620 [100] => 063000 [101] => 071200 [102] => 071300 [103] => 071400 [104] => 072000 [105] => 091100 [106] => 091200 [107] => 092100 [108] => 092200 [109] => 092300 [110] => 093400 [111] => 111100 [112] => 111200 [113] => 111300 [114] => 111400 [115] => 112100 [116] => 112200 [117] => 112300 [118] => 112400 [119] => 113100 [120] => 113300 [121] => 114200 [122] => 114300 [123] => 115100 [124] => 115200 [125] => 116000 [126] => 122500 [127] => 122600 [128] => 122700 [129] => 122800 [130] => 131100 [131] => 131200 [132] => 131500 [133] => 131600 [134] => 132100 [135] => 132200 [136] => 133000 [137] => 134000 [138] => 135100 [139] => 135200 [140] => 121100 [141] => 121300 [142] => 123200 [143] => 123300 [144] => 211110 [145] => 211120 [146] => 211210 [147] => 211220 [148] => 211230 [149] => 211240 [150] => 211250 [151] => 211260 [152] => 211280 [153] => 212110 [154] => 212210 [155] => 212220 [156] => 212230 [157] => 212240 [158] => 221000 [159] => 033000 ) 
			if (count($rows)>0) { 
				  foreach($rows as $rowOBJ){  
					 $row = ((array) $rowOBJ);
				     // Array ( [CODIPROD] => 001100 [T1] => 12321 [T2] => 8122 [T3] => 6022 [T4] => 6022 [T5] => 2100 [T6] => 19469 [T7] => 83664 [T8] => 37105 [T9] => 18995 [T10] => 16619 [T11] => 12044 [T12] => 12321 )
					  // echo $row['CODIPROD'].'****';
					  if (in_array($row['CODIPROD'], $elscodis)) {
						  for ($i = 1; $i<13; $i++) 
						  {   //if ($row['CODIPROD']=='123300') echo 'Year:'.$year.'. '.'MES'.$i.':::'.$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i].'/'.$row['T'.$i].'-------------';
							  if ($row['T'.$i]>0) $entradaPreusAnyT[$row['CODIPROD']]['MES'.$i] 
								   = str_replace(',','.',$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i])/$row['T'.$i];
							  else $entradaPreusAnyT[$row['CODIPROD']]['MES'.$i] = null;
							  //if ($row['CODIPROD']=='123300') echo $entradaPreusAnyT[$row['CODIPROD']]['MES'.$i].'<br>';
						  }
					  }
				   }
				}
				/*
				Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 14.6844898953 [MES2] => 14.72 [MES3] => 14.72 [MES4] => 15.03 [MES5] => 13.94 [MES6] => 12.9206867328 [MES7] => 12.5472009466 [MES8] => 12.8552346045 [MES9] => 13.3616735983 [MES10] => 13.473264336 [MES11] => 13.9716655596 [MES12] => 14.2875002029 ) 
				[002100] => Array ( [DESCRIPCIO] => Arr�s closca rod� i semillarg [MES1] => 27.05 [MES2] => 28.07 [MES3] => 28.85 [MES4] => 28.85 [MES5] => 28.85 [MES6] => 28.25 [MES7] => 28.25 [MES8] => [MES9] => [MES10] => 27.55 [MES11] => 28.25 [MES12] => 28.25 ) 
				[002200] => Array ( [DESCRIPCIO] => Arr�s closca llarg [MES1] => 27.65 [MES2] => 27.65 [MES3] => 27.65 [MES4] => [MES5] => [MES6] => [MES7] => [MES8] => [MES9] => [MES10] => [MES11] => 28.85 [MES12] => 28.85 ) 
				[003300] => Array ( [DESCRIPCIO] => Ordi per a pinso [MES1] => 15.69 [MES2] => 15.69 [MES3] => 15.51 [MES4] => 15.75 [MES5] => 15.22 [MES6] => 15.69 [MES7] => 13.6796267007 [MES8] => 14.2894405994 [MES9] => 15.3917033777 [MES10] => 15.3728511475 [MES11] => 15.5452050416 [MES12] => 15.7962997184 )
				[003400] => Array ( [DESCRIPCIO] => Ordi per a malta [MES1] => 15.99 [MES2] => 15.99 [MES3] => [MES4] => [MES5] => [MES6] => 15.93 [MES7] => 15.75 [MES8] => 15.75 [MES9] => 16.2 [MES10] => 16.38 [MES11] => 15.75 [MES12] => 15.99 ) 
				[004000] => Array ( [DESCRIPCIO] => Civada [MES1] => 12.99 [MES2] => 13.59 [MES3] => 14.18 [MES4] => 14.47 [MES5] => .......................
									.
				.
				.
				*/		
			if (!$bestiarvida) $sql = strtoupper("SELECT CodiProd, SUM(MES1) as MES1, " .
				   "SUM(MES2) as MES2, SUM(MES3) as MES3, " .
				   "SUM(MES4) as MES4, SUM(MES5) as MES5, " .
				   "SUM(MES6) as MES6, SUM(MES7) as MES7, " .
				   "SUM(MES8) as MES8, SUM(MES9) as MES9, " .
				   "SUM(MES10) as MES10, SUM(MES11) as MES11, " .
				   "SUM(MES12) as MES12 FROM basepercebuts".$this->base." WHERE CodiProd " .
				   "NOT IN (SELECT CodiProd FROM descripcio_preuspercebuts WHERE Grup = 'BESTIAR PER VIDA') AND " .
				   "CodiProd <> '".$codifora1."' AND CodiProd <> '".$codifora2."' AND CodiProd <> '".$codifora3."' GROUP BY CodiProd");
			if ($bestiarvida) $sql = strtoupper("SELECT CodiProd, SUM(MES1) as MES1, " .
				   "SUM(MES2) as MES2, SUM(MES3) as MES3, " .
				   "SUM(MES4) as MES4, SUM(MES5) as MES5, " .
				   "SUM(MES6) as MES6, SUM(MES7) as MES7, " .
				   "SUM(MES8) as MES8, SUM(MES9) as MES9, " .
				   "SUM(MES10) as MES10, SUM(MES11) as MES11, " .
				   "SUM(MES12) as MES12 FROM basepercebuts".$this->base." WHERE  " .
				   "CodiProd <> '".$codifora1."' AND CodiProd <> '".$codifora2."' AND CodiProd <> '".$codifora3."' GROUP BY CodiProd");
			
			unset($result);
			$rows = $this->conn->select($sql);		   
				
			if (count($rows)>0) { 
				foreach($rows as $rowOBJ){  
					$row = ((array) $rowOBJ);
					
					// Array ( [CODIPROD] => 003400 [MES1] => 233 [MES2] => 233 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 512 [MES7] => 5204 [MES8] => 2008 [MES9] => 1181 [MES10] => 995 [MES11] => 558 [MES12] => 512 ) 
					$sum = 0;
					if (!isset($entradaPreusAnyT[$row['CODIPROD']]['ANUAL'])) { $entradaPreusAnyT[$row['CODIPROD']]['ANUAL']=0; }	
					for ($i=1; $i<13; $i++) 
					{
						if (!isset($entradaPreusAnyT[$row['CODIPROD']]['MES'.$i])) { $entradaPreusAnyT[$row['CODIPROD']]['MES'.$i]=0; }
						$entradaPreusAnyT[$row['CODIPROD']]['ANUAL'] += $row['MES'.$i]*$entradaPreusAnyT[$row['CODIPROD']]['MES'.$i];				
						$sum = $sum + $row['MES'.$i];
					}
					$entradaPreusAnyT[$row['CODIPROD']]['ANUAL'] = $entradaPreusAnyT[$row['CODIPROD']]['ANUAL']/$sum;			    
									
					/* (2008/04/19)
					
					//for products with codes like ^21 and 221000 the rounding is to 0 places after .
					if ((ereg('^21', $row['CODIPROD']) or $row['CODIPROD'] == '221000') and $row['CODIPROD'] != '211120')
						$entradaPreusAnyT[$row['CODIPROD']]['ANUAL'] = round($entradaPreusAnyT[$row['CODIPROD']]['ANUAL'], 0);
					else
						$entradaPreusAnyT[$row['CODIPROD']]['ANUAL'] = round($entradaPreusAnyT[$row['CODIPROD']]['ANUAL'], 2);
					//if ($row['CODIPROD']=='054032')  echo 	'///////////'.$entradaPreusAnyT[$row['CODIPROD']]['ANUAL'].'///////////////////<br>';

					*/				
				}
			}
			else 
				{  return null;	}	
			
			/*
			print_r($entradaPreusAnyT);
			Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
						[MES1] => 17.9 [MES2] => 17.74 [MES3] => 17.22 [MES4] => 17.36 [MES5] => 18.08 [MES6] => 14.12 [MES7] => 13.32 [MES8] => 13.44 [MES9] => 13.23 [MES10] => 13.09 [MES11] => 13.28 [MES12] => 13.56 [ANUAL] => 14.03 ) 
					   [002100] => Array ( [DESCRIPCIO] => Arr�s closca rod� i semillarg 
					   [MES1] => 27 [MES2] => 27 [MES3] => 27 [MES4] => 27 [MES5] => 27 [MES6] => 27 [MES7] => 27 [MES8] => 0 [MES9] => 0 [MES10] => 21 [MES11] => 21 [MES12] => 21 [ANUAL] => 25.64 ) 
					  [002200] => Array ( [DESCRIPCIO] => Arr�s closca llarg 
					  [MES1] => 27.5 [MES2] => 27.5 [MES3] => 27.5 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 21 [MES12] => 21 [ANUAL] => 24.9 ) 
					   [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
					   [MES1] => 15.99 [MES2] => 16.25 [MES3] => 15.92 [MES4] => 16 [MES5] => 15 [MES6] => 12.86 [MES7] => 12.41 [MES8] => 12.67 [MES9] => 12.67 [MES10] => 12.36 [MES11] => 12.53 [MES12] => 12.61 [ANUAL] => 12.99 ) 
					   �
					   �
					   �				   
			*/
			
			return $entradaPreusAnyT; 
		}
		
		/**
		* Funci� addQFisiquesSumBasePercebuts: Retorna la suma de les Quantitats F�siques de tots els territoris, d'un producte concret.
		* @param - &$table: 	
		* @param - $basepercebuts: 		
		* @param - $codi: 
		* @param - $descripcio: 
		* 
		*/
		function addQFisiquesSumBasePercebuts(&$table, $basepercebuts, $codi, $descripcio) {
			$sql = strtoupper("SELECT SUM(MES1), SUM(MES2), SUM(MES3), SUM(MES4), SUM(MES5), SUM(MES6), SUM(MES7), SUM(MES8), "
				   ."SUM(MES9), SUM(MES10), SUM(MES11), SUM(MES12) FROM " .$basepercebuts ." WHERE CodiProd = '" 
				   .$codi ."'");
			//if ($codi=='051130') echo $sql.'<br>';
			$rows = $this->conn->select($sql);  
			$num = count($rows);
			
			if ($num>0) { 
					$table[$codi] = ((array)$rows[0]); 
					//if ($codi=='051130') { print_r($table[$codi]);  die();}
					$table[$codi]['DESCRIPCIO'] = $descripcio;
					$table[$codi]['ANUAL'] = 0;
					/*
					 * // Arreglem unes ponderacions especials. Aix� s'havia fet inicialment, per� es decideix treure (2009-06-10)
					if ($codi == '111100') {
						for ($i=1; $i<13; $i++) {
							$table[$codi]['SUM(MES' .$i .')'] = $table[$codi]['SUM(MES' .$i .')']*10;
						}
					}
					
					if ($codi == '116000') {
						for ($i=1; $i<13; $i++) {
							$table[$codi]['SUM(MES' .$i .')'] = $table[$codi]['SUM(MES' .$i .')']/2;
						}
					}
					*/	
					for ($i=1; $i<13; $i++) {
						$table[$codi]['ANUAL'] = $table[$codi]['ANUAL'] + 
												 $table[$codi]['SUM(MES' .$i .')'];
					}
					
			}
			else 
				return null;
		}
		
		/**
		 * Funci� QFisiquesSumBasePercebuts:
		* @param - $basepercebuts: 	
		* @param - $codi: 		
		* @param - $descripcio: 
		*/
		function QFisiquesSumBasePercebuts($basepercebuts, $codi, $descripcio) {
			$sql = strtoupper("SELECT SUM(MES1), SUM(MES2), SUM(MES3), SUM(MES4), SUM(MES5), SUM(MES6), SUM(MES7), SUM(MES8), "
				   ."SUM(MES9), SUM(MES10), SUM(MES11), SUM(MES12) FROM " .$basepercebuts ." WHERE CodiProd = '" 
				   .$codi ."'");
			//echo $sql;  
			$rows = $this->conn->select($sql);  
			$num = count($rows);
			$table=array();
			if ($num>0) { 
					$table[$codi] = ((array)$rows[0]); 
					$table[$codi]['DESCRIPCIO'] = $descripcio;
					$table[$codi]['ANUAL'] = 0;
					/*
					// Arreglem unes ponderacions especials. Aix� s'havia fet inicialment, per� es decideix treure (2009-06-10)
					if ($codi == '111100') {
						for ($i=1; $i<13; $i++) {
							$table[$codi]['SUM(MES' .$i .')'] = $table[$codi]['SUM(MES' .$i .')']*10;
						}
					}
					
					if ($codi == '116000') {
						for ($i=1; $i<13; $i++) {
							$table[$codi]['SUM(MES' .$i .')'] = $table[$codi]['SUM(MES' .$i .')']/2;
						}
					}
					*/
					for ($i=1; $i<13; $i++) {
						$table[$codi]['ANUAL'] = $table[$codi]['ANUAL'] + 
												 $table[$codi]['SUM(MES' .$i .')'];
					}
					return $table;
			}
			else 
				return null;
		}
		
		/**
		 * Funci� QFisiquesProd:
		* @param - $basepercebuts: 	
		* @param - $codi: 		
		*/
		function QFisiquesProd($basepercebuts, $codi) {
			$sql = strtoupper("SELECT * FROM " .$basepercebuts ." WHERE CodiProd = '" 
				   .$codi ."' ORDER BY CODITERR;");
			
			$rows = $this->conn->select($sql);  
			$num = count($rows);
			$table=array();
			if ($num>0) { 
				foreach($rows as $rowOBJ){                                                                          
					$fila = ((array) $rowOBJ);        
					$table[$fila['CODITERR']] = $fila;
					$table[$fila['CODITERR']]['ANUAL'] = 0;
					// Arreglem unes ponderacions especials. Aix� s'havia fet inicialment, per� es decideix treure (2009-06-10)
					/*
					if ($codi == '111100') {
						for ($i=1; $i<13; $i++) {
							$table[$fila['CODITERR']]['MES' .$i .''] = $table[$fila['CODITERR']]['MES' .$i .'']*10;
						}
					}
					
					if ($codi == '116000') {
						for ($i=1; $i<13; $i++) {
							$table[$fila['CODITERR']]['MES' .$i .''] = $table[$fila['CODITERR']]['MES' .$i .'']/2;
						}
					}
					*/
					for ($i=1; $i<13; $i++) {
						$table[$fila['CODITERR']]['ANUAL'] = $table[$fila['CODITERR']]['ANUAL'] + 
												 $table[$fila['CODITERR']]['MES' .$i .''];
					}
				}
				return $table;
			}
			else 
				return null;
		}
		
		
		/**
		 * Funci� addQFisiquesSumBasePercebutsRed:
		* @param - &$table:
		* @param - $basepercebuts: 	
		* @param - $codiGrup: 		
		* @param - $like: 	
		* @param - $notLike: 
		* @param - $descripcio: 
		*/
		function addQFisiquesSumBasePercebutsRed(&$table, $basepercebuts, $codiGrup, $like, $notLike, $descripcio){
			$sql = strtoupper("SELECT SUM(MES1), SUM(MES2), SUM(MES3), SUM(MES4), SUM(MES5), SUM(MES6), SUM(MES7), SUM(MES8), "
				   ."SUM(MES9), SUM(MES10), SUM(MES11), SUM(MES12) FROM " .$basepercebuts ." WHERE CodiProd LIKE '" 
				   .$like ."'");
	   
			if ($notLike != null) {
				
				$sql = strtoupper("SELECT SUM(MES1), SUM(MES2), SUM(MES3), SUM(MES4), SUM(MES5), SUM(MES6), SUM(MES7), SUM(MES8), "
				   ."SUM(MES9), SUM(MES10), SUM(MES11), SUM(MES12) FROM " .$basepercebuts ." WHERE CodiProd LIKE '" 
				   .$like ."'");
				   
				foreach($notLike as $value) {
					$sql = $sql ." AND CodiProd != '" .$value ."'";
				}
				
				$sql = strtoupper($sql);
			}

			$rows = $this->conn->select($sql);  
			$num = count($rows);	
			//echo '<br>'.$sql.'<br>';
			if ($num>0) { 
					$table[$codiGrup] = ((array)$rows[0]); 
					$table[$codiGrup]['DESCRIPCIO'] = $descripcio;
					$table[$codiGrup]['ANUAL'] = 0;
						
					for ($i=1; $i<13; $i++) {
						$table[$codiGrup]['ANUAL'] = $table[$codiGrup]['ANUAL'] + 
													 $table[$codiGrup]['SUM(MES' .$i .')'];
					}
					/*
												::: $table ::::
					 Array ( [001000] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 
															[SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 
															[SUM(MES12)] => 12321 
															[DESCRIPCIO] => Blat 
															[ANUAL] => 234804 ) ) 
					 */
			}
			else 
				return null;
		}
		
		/**
		 * Funci� addQFisiquesConsistRed:
		* @param - &$table:
		* @param - $basepercebuts: 	
		* @param - $codiGrup: 		
		* @param - $like: 	
		* @param - $notLike: 
		*/
		function addQFisiquesConsistRed(&$table, $basepercebuts, $codiGrup, $like, $notLike) {
			$sql = strtoupper("SELECT DISTINCT CodiProd FROM " .$basepercebuts ." WHERE CodiProd LIKE '" .$like ."'");
			
			if ($notLike != null) {
				$sql = strtoupper("SELECT DISTINCT CodiProd FROM " .$basepercebuts ." WHERE CodiProd LIKE '" .$like ."'");
				
				foreach($notLike as $value) {
					$sql = $sql ." AND CodiProd != '" .$value ."'";
				}
				
				$sql = strtoupper($sql);
			}

			$rows = $this->conn->select($sql);  
			$num = count($rows);
			
			if ($num>0) { 
				$i=0;
				foreach($rows as $rowOBJ){                                                                          
						$row = ((array) $rowOBJ);   
						$table[$codiGrup]['consist'][$i] = $row['CODIPROD'];
						$i++;
				}
			}
			else 
				return null;
		}
		
		/**
		 * Funci� addQFisiquesPreusAnual:
		* @param - &$table:
		* @param - $codi: 	
		* @param - $entradaPreusAnyT2000: 		 
		*/
		function addQFisiquesPreusAnual(&$table, $codi, $entradaPreusAnyT2000) 
		{   //echo '--> '.$codi.'<br>';
			
			if (!isset($table[$codi]['PREUS_ANUAL'])) {  $table[$codi]['PREUS_ANUAL']=0; }
			for ($i=1; $i<13; $i++) {
				if (!isset($entradaPreusAnyT2000[$codi])) { $entradaPreusAnyT2000[$codi]=array(); }
				if (!isset($entradaPreusAnyT2000[$codi]['MES' .$i])) {$entradaPreusAnyT2000[$codi]['MES' .$i]=0; }
				$table[$codi]['PREUS_ANUAL'] = $table[$codi]['PREUS_ANUAL'] + 
						$table[$codi]['SUM(MES' .$i .')']*$entradaPreusAnyT2000[$codi]['MES' .$i];	
			}
			
			if (preg_match("/^21/", $codi)) 
			{   if ($table[$codi]['ANUAL']==0)
				{   echo "<br>Alerta. Divisi� per zero. Preu anual del ".$codi." �s zero. (addQFisiquesPreusAnual)";
				}
				else $table[$codi]['PREUS_ANUAL'] = round($table[$codi]['PREUS_ANUAL']/$table[$codi]['ANUAL'], 0);
			}
			else
			{   // echo $codi.'<br>';
				
				if ($table[$codi]['ANUAL']==0)
				{
				  // echo "Divisi� x zero:".$codi.'<br>';
				  //print_r($table[$codi]); die();
				}
				
				//$table[$codi]['PREUS_ANUAL'] = round($table[$codi]['PREUS_ANUAL']/$table[$codi]['ANUAL'], 2);
				if ($table[$codi]['ANUAL']!=0) {
					$table[$codi]['PREUS_ANUAL'] = round($table[$codi]['PREUS_ANUAL']/$table[$codi]['ANUAL'], 12);  // !!! Pels c�lculs amb l'Excel de la Base00 estava retallat a 2 decimals. 
				} else {
					$table[$codi]['PREUS_ANUAL'] = '?';
				}

			}
			//if ($codi=='054032') echo '******('.$table[$codi]['PREUS_ANUAL'].')******'.($table[$codi]['PREUS_ANUAL']/$table[$codi]['ANUAL']).'************<br>';
			
		}
			
		/**
		 * Funci� addQFisiquesPreusAnual:
		* @param - $basepercebuts:
		* @param - $entradaPreusAnyT: 		 
		*/	
		function qFisiques ($basepercebuts, $entradaPreusAnyT) 
		{    
			
			//print_r($entradaPreusAnyT); // preus any 2000
			/*
			Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
												  [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 
												  [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
					   [002100] => Array ( [DESCRIPCIO] => Arr�s closca rod� i semillarg 
												   [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 
												   [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
					   [002200] => Array ( [DESCRIPCIO] => Arr�s closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 
												   [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
					   [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
													[MES1] => 13.83 [MES2] => 13.37 [MES3] => 12.71 [MES4] => 12.3 [MES5] => 12.3 [MES6] => 12.28 [MES7] => 12.1 [MES8] => 12.52 
												   [MES9] => 13.62 [MES10] => 14.69 [MES11] => 15.67 [MES12] => 15.94 [ANUAL] => 13.28 ) 
						�
						�
						�
			*/		
			//print_r($entradaPreusAnyT) die();
			$qFisiques = array();
			$entradaPreusAnyT2000 = $this->entradaPreusAnyT('20'.$this->base);
			// esborrem els que no tenen info
			/*
				[113200] => Array
					(
						[DESCRIPCIO] => Segalls (bestiar per a carn)
						[MES1] => 0
						[MES2] => 0
						[MES3] => 0
						[MES4] => 0
						[MES5] => 0
						[MES6] => 0
						[MES7] => 0
						[MES8] => 0
						[MES9] => 0
						[MES10] => 0
						[MES11] => 0
						[MES12] => 0

			*/		
			$entradaPreusAnyT2000_INI = $entradaPreusAnyT2000;
			$entradaPreusAnyT2000 = array();
			while (list($elcodi, $val) = each($entradaPreusAnyT2000_INI)) 
			{  //print_r($val);
			   if ($val['MES1']+$val['MES2']+$val['MES3']+$val['MES4']+$val['MES5']+$val['MES6']+
				  $val['MES7']+$val['MES8']+$val['MES9']+$val['MES10']+$val['MES11']+$val['MES12']>0)
					   $entradaPreusAnyT2000[$elcodi] = $val;
			}
			
			// ULL .. base00 .. es feina aix�? .. no era pas preus_base_anual_any_2000_red?
			//    [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 14.6844898953 [MES2] => 14.72 [MES3] => 14.72 [MES4] => 15.03 [MES5] => 13.94 [MES6] => 12.9206867328 [MES7] => 12.5472009466 [MES8] => 12.8552346045 [MES9] => 13.3616735983 [MES10] => 13.473264336 [MES11] => 13.9716655596 [MES12] => 14.2875002029 [ANUAL] => 13.2418335718 )
			$preus = new Prices('percebuts',(2000+$this->base),1,8,$this->base,$this->conn);	
			// calculem els preus dels agregats del 2000, els farem servir com a preus Base any 2000
			reset($this->estructuraSortidaPer);  // en aquest array hi tenim definides les agregacions
			while (list($elcodi, $val) = each($this->estructuraSortidaPer)) 
				{  if ($preus->es_agregacio($this,$elcodi)) 
				   {    
					   $preusAgr = $preus->pricesAgrAll($elcodi,$preus->setAgregacio($elcodi,$this));		 		  	       
					   /*
					   Array ( 
					   [2000] => Array ( 
						   [99] => Array ( [1] => 14.6844898953 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 13.94 [6] => 12.9206867328 [7] => 12.5472009466 [8] => 12.8552346045 [9] => 13.3616735983 [10] => 13.473264336 [11] => 13.9716655596 [12] => 14.2875002029 [2000] => 13.2418335718 ) 
						   [8] => Array ( [2000] => 12.5480023375 [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 0 [7] => 12.38 [8] => 12.56 [9] => 12.86 [10] => 13.04 [11] => 0 [12] => 0 ) 
						   [17] => Array ( [2000] => 12.408005395 [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 12.43 [7] => 12.32 [8] => 12.98 [9] => 0 [10] => 0 [11] => 0 [12] => 0 ) 
						   [25] => Array ( [2000] => 13.9031974662 [1] => 14.72 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 13.94 [6] => 13.34 [7] => 13.22 [8] => 13.28 [9] => 13.7 [10] => 13.94 [11] => 14.12 [12] => 14.36 ) 
						   [43] => Array ( [2000] => 14.025 [1] => 14.48 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 0 [6] => 0 [7] => 0 [8] => 0 [9] => 13.25 [10] => 13.46 [11] => 13.63 [12] => 13.87 ) ) 
					   [2001] => ...........
						*/
					   for ($m=1;$m<=12;$m++)
					   { 
						 $entradaPreusAnyT2000[$elcodi]['MES'.$m]=$preusAgr['20'.$this->base][99][$m];
					   }
		
					   $entradaPreusAnyT2000[$elcodi]['ANUAL']=$preusAgr['20'.$this->base][99]['20'.$this->base];
					   //if ($elcodi=='001000') { echo "** Agregaci�: ".$elcodi."**  "; print_r($entradaPreusAnyT2000[$elcodi]); die(); }
				   }
				}
			 
			//blackValues		
			foreach (array_keys($entradaPreusAnyT) as $codi) {		
				
				if (!isset($entradaPreusAnyT[$codi]['DESCRIPCIO'])) { $entradaPreusAnyT[$codi]['DESCRIPCIO']=''; }
				$this->addQFisiquesSumBasePercebuts($qFisiques, $basepercebuts, $codi, 
													$entradaPreusAnyT[$codi]['DESCRIPCIO']);
				$qFisiques[$codi]['consist'] = $codi;  
				//if ($codi=='113200') { print_r($qFisiques); echo "<br>"; die(); }			
				$this->addQFisiquesPreusAnual($qFisiques, $codi, $entradaPreusAnyT2000);  // !!!!! Els preus anunals perden els decimals
				
				//echo $codi.';'.$qFisiques[$codi]['PREUS_ANUAL'].';'.$entradaPreusAnyT2000[$codi]['ANUAL'].'<br>';
				
			}
			foreach(array_keys($this->qFisiquesRed) as $codiGrup) {
				$this->addQFisiquesSumBasePercebutsRed($qFisiques, $basepercebuts, $codiGrup, 
													   $this->qFisiquesRed[$codiGrup]['like'], 
													   $this->qFisiquesRed[$codiGrup]['notLike'],
													   $this->qFisiquesRed[$codiGrup]['DESCRIPCIO']);
				$this->addQFisiquesConsistRed($qFisiques, $basepercebuts, $codiGrup, 
											  $this->qFisiquesRed[$codiGrup]['like'], 
											  $this->qFisiquesRed[$codiGrup]['notLike']);
				//$qFisiques[$codiGrup]['PREUS_ANUAL'] = $this->preus_base_anual_any_2000_red[$codiGrup];			
				//echo $codiGrup.';'.$qFisiques[$codiGrup]['PREUS_ANUAL'].';'.$entradaPreusAnyT2000[$codiGrup]['ANUAL'].'<br>';
				$qFisiques[$codiGrup]['PREUS_ANUAL'] = $entradaPreusAnyT2000[$codiGrup]['ANUAL'];
				//if ($codiGrup=='001000') { echo "** Codi grup: ".$elcodi."**  "; print_r($qFisiques[$codiGrup]); die(); }
			}
			if ($this->base=='00')
			{
			  $qFisiques['221000']['PREUS_ANUAL'] = 129.36;   // SURO
			  $qFisiques['212110']['PREUS_ANUAL'] = 2477.00;  // Eucaliptus per triturar
			  $qFisiques['221000']['PREUS_ANUAL'] = $entradaPreusAnyT2000['221000']['ANUAL'];
			  $qFisiques['212110']['PREUS_ANUAL'] = $entradaPreusAnyT2000['212110']['ANUAL'];
			}
			//echo '221000'.';'.$qFisiques['221000']['PREUS_ANUAL'].';'.$entradaPreusAnyT2000['221000']['ANUAL'].'<br>';
			//echo '212110'.';'.$qFisiques['212110']['PREUS_ANUAL'].';'.$entradaPreusAnyT2000['212110']['ANUAL'].'<br>';
			// print_r($qFisiques); die();
			return $qFisiques; 
		}
		
		/**
		 * Funci� preusProductesAnyT:
		* @param - $qFisiques:
		* @param - $entradaPreusAnyT: 		 
		*/
		function preusProductesAnyT($entradaPreusAnyT, $qFisiques) 
		{
			
			/*
			print_r($entradaPreusAnyT);
			Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
								 [MES1] => 17.9 [MES2] => 17.74 [MES3] => 17.22 [MES4] => 17.36 [MES5] => 18.08 [MES6] => 14.12 [MES7] => 13.32 [MES8] => 13.44 [MES9] => 13.23 [MES10] => 13.09 [MES11] => 13.28 [MES12] => 13.56 [ANUAL] => 14.03 ) 
					   [002100] => Array ( [DESCRIPCIO] => Arr�s closca rod� i semillarg 
					   [MES1] => 27 [MES2] => 27 [MES3] => 27 [MES4] => 27 [MES5] => 27 [MES6] => 27 [MES7] => 27 [MES8] => 0 [MES9] => 0 [MES10] => 21 [MES11] => 21 [MES12] => 21 [ANUAL] => 25.64 ) 
					  [002200] => Array ( [DESCRIPCIO] => Arr�s closca llarg 
					  [MES1] => 27.5 [MES2] => 27.5 [MES3] => 27.5 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 21 [MES12] => 21 [ANUAL] => 24.9 ) 
				 [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
					   [MES1] => 15.99 [MES2] => 16.25 [MES3] => 15.92 [MES4] => 16 [MES5] => 15 [MES6] => 12.86 [MES7] => 12.41 [MES8] => 12.67 [MES9] => 12.67 [MES10] => 12.36 [MES11] => 12.53 [MES12] => 12.61 [ANUAL] => 12.99 ) 
					   �
					   �
					   �				   
			*/
			/*
			print_r($qFisiques);
			Array ( [001100] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 
								  [DESCRIPCIO] => Blat tou o semidur [ANUAL] => 234804 [consist] => 001100 [PREUS_ANUAL] => 13.24 ) 
						 [002100] => Array ( [SUM(MES1)] => 24029 [SUM(MES2)] => 12014 [SUM(MES3)] => 12014 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 13240 [SUM(MES12)] => 12423 
								  [DESCRIPCIO] => Arr�s closca rod� i semillarg [ANUAL] => 124227 [consist] => 002100 [PREUS_ANUAL] => 28.16 ) 
				   [002200] => Array ( [SUM(MES1)] => 1100 [SUM(MES2)] => 1100 [SUM(MES3)] => 1100 [SUM(MES4)] => 0 [SUM(MES5)] => 0 [SUM(MES6)] => 0 [SUM(MES7)] => 0 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 0 [SUM(MES11)] => 1100 [SUM(MES12)] => 1100 
								  [DESCRIPCIO] => Arr�s closca llarg [ANUAL] => 5500 [consist] => 002200 [PREUS_ANUAL] => 28.13 ) 
					   �
					   �
			*/
			foreach (array_keys($entradaPreusAnyT) as $codi) {
				if (!isset($entradaPreusAnyT[$codi]['DESCRIPCIO'])) { $entradaPreusAnyT[$codi]['DESCRIPCIO']='(?)'; }
				$preusProductesAnyT[$codi]['DESCRIPCIO'] = $entradaPreusAnyT[$codi]['DESCRIPCIO'];
				
				for ($i=1; $i<13; $i++) {
					$preusProductesAnyT[$codi]['MES' .$i] = $entradaPreusAnyT[$codi]['MES' .$i];
				}
				$preusProductesAnyT[$codi]['ANUAL'] = $entradaPreusAnyT[$codi]['ANUAL'];
			}
			
			foreach (array_keys($this->qFisiquesRed) as $codiGrup) {
				$preusProductesAnyT[$codiGrup]['DESCRIPCIO'] = $this->qFisiquesRed[$codiGrup]['DESCRIPCIO'];
				
				for ($i=1; $i<13; $i++) {
					if ($qFisiques[$codiGrup]['SUM(MES' .$i .')'] != 0) {
						if (!isset($qFisiques[$codiGrup]['consist'])) { } //  echo "No existeix la dada <i>consist</i> per ".$codiGrup.".<br>";
						else
						{
							foreach($qFisiques[$codiGrup]['consist'] as $value) {

								if (!isset($qFisiques[$value])) { $qFisiques[$value]=array(); }
								if (!isset($qFisiques[$value]['SUM(MES' .$i .')'])) { $qFisiques[$value]['SUM(MES' .$i .')']=0; }
								if (!isset($entradaPreusAnyT[$value])) { $entradaPreusAnyT[$value]=array(); }
								if (!isset($entradaPreusAnyT[$value]['MES' .$i])) { $entradaPreusAnyT[$value]['MES' .$i]=0; }
								
								if (!isset($preusProductesAnyT[$codiGrup]['MES' .$i])) { $preusProductesAnyT[$codiGrup]['MES' .$i]=0; }
								$preusProductesAnyT[$codiGrup]['MES' .$i] = $preusProductesAnyT[$codiGrup]['MES' .$i] 
												+ $qFisiques[$value]['SUM(MES' .$i .')']*$entradaPreusAnyT[$value]['MES' .$i];
							}
							$preusProductesAnyT[$codiGrup]['MES' .$i] = $preusProductesAnyT[$codiGrup]['MES' .$i]
																		 /$qFisiques[$codiGrup]['SUM(MES' .$i .')'];
						}
					}
					else
						$preusProductesAnyT[$codiGrup]['MES' .$i] = 0;
				}
				
				if ($qFisiques[$codiGrup]['ANUAL'] != 0) {
					if (!isset($qFisiques[$codiGrup]['consist'])) {} //  echo "No existeix la dada <i>consist</i> per ".$codiGrup.".<br>";
					else
					{
						foreach($qFisiques[$codiGrup]['consist'] as $value) {
							if (!isset($preusProductesAnyT[$codiGrup]['ANUAL'])) { $preusProductesAnyT[$codiGrup]['ANUAL']=0; }
							if (!isset($qFisiques[$value]['ANUAL'])) { $qFisiques[$value]['ANUAL']=0; }
							if (!isset($entradaPreusAnyT[$value]['ANUAL'])) { $entradaPreusAnyT[$value]['ANUAL']=0; }
							$preusProductesAnyT[$codiGrup]['ANUAL'] = $preusProductesAnyT[$codiGrup]['ANUAL'] 
											+ $qFisiques[$value]['ANUAL']*$entradaPreusAnyT[$value]['ANUAL'];
						}
					}
					$preusProductesAnyT[$codiGrup]['ANUAL'] = $preusProductesAnyT[$codiGrup]['ANUAL']
															 /$qFisiques[$codiGrup]['ANUAL'];
				}
				else
					$preusProductesAnyT[$codiGrup]['ANUAL'] = 0;
			}
			
			return $preusProductesAnyT;
		}
		
		/**
		 * Funci� indexSimpleAnyT:
		* @param - $qFisiques:
		* @param - $entradaPreusAnyT: 		 
		*/
		function indexSimpleAnyT($qfisiques, $preusProductesAnyT) {
			foreach (array_keys($qfisiques) as $codeProd) 
			{   //if ($codeProd=='110000') echo '['.$codeProd.']';
				if (isset($preusProductesAnyT[$codeProd])) { 
				    if (!isset($preusProductesAnyT[$codeProd]['DESCRIPCIO'])) {$preusProductesAnyT[$codeProd]['DESCRIPCIO'] = '(?)'; }
					$indexSimple[$codeProd]['DESCRIPCIO'] = $preusProductesAnyT[$codeProd]['DESCRIPCIO'];
					
					for($i=1; $i<13; $i++ ) 
					{
						if ($preusProductesAnyT[$codeProd]['MES' .$i] != 0) 
						{   if ($qfisiques[$codeProd]['PREUS_ANUAL']==0)
							{
							  echo $codeProd; 					  
							  echo "<br>";
							  die('. Div zero al calcular el MES'.$i);
							}
							$indexSimple[$codeProd]['MES' .$i] = round(($preusProductesAnyT[$codeProd]['MES'.$i]
																 /$qfisiques[$codeProd]['PREUS_ANUAL'])*100, 12);
																 
							//if ($codeProd=='110000') echo 	'MES' .$i.'**(*)**('.$codeProd.') '.$i.'::'.$preusProductesAnyT[$codeProd]['MES'.$i].' div '.$qfisiques[$codeProd]['PREUS_ANUAL'].' = '.$indexSimple[$codeProd]['MES' .$i]."::::::<br>";            					 				
							
						}
						else
							$indexSimple[$codeProd]['MES' .$i] = 0;
						//if ($codeProd=='110000') echo 'MES' .$i.':indexSimple'.'($codeProd]):'.$indexSimple[$codeProd]['MES' .$i].')<br>';	
					}			
					if ($preusProductesAnyT[$codeProd]['ANUAL'] != 0) 
					{      if ($qfisiques[$codeProd]['PREUS_ANUAL']==0)
							{
							  echo $codeProd; die('. Div zero al calcular el preu ANUAL.');
							}
							$indexSimple[$codeProd]['ANUAL'] = round(($preusProductesAnyT[$codeProd]['ANUAL']
																 /$qfisiques[$codeProd]['PREUS_ANUAL'])*100, 12);
						
					}
					else
							$indexSimple[$codeProd]['ANUAL'] = 0;
				}
			}
		
			return $indexSimple; 
		}

		/**
		 * Funci� wValorAll:
		* @param - $qFisiques:		 
		*/
		function wValorAll($qfisiques) {
			$elscodis = array_keys($qfisiques);
			sort($elscodis);
			$wValor = array();
			$mensual_addition = array('MES1' => 0,'MES2' => 0,'MES3' => 0,'MES4' => 0,'MES5' => 0,'MES6' => 0,'MES7' => 0,'MES8' => 0,'MES9' => 0,'MES10' => 0,'MES11' => 0,'MES12' => 0);
			$TOTAL_PRODS = 0;
			$elems_in_estructuraSortidaPer = array_keys($this->estructuraSortidaPer); // tenim el problema de troba si ISSET per� IS_NULL
			foreach($elscodis as $res => $code) {	
				if (!preg_match('/^13/', $code)) 
				{
					$data = $qfisiques[$code];	
					/*
						[SUM(MES1)] => 16912
						[SUM(MES2)] => 13314
						[SUM(MES3)] => 0
						[SUM(MES4)] => 0
						[SUM(MES5)] => 0
						[SUM(MES6)] => 0
						[SUM(MES7)] => 90984
						[SUM(MES8)] => 63280
						[SUM(MES9)] => 46599
						[SUM(MES10)] => 39404
						[SUM(MES11)] => 36883
						[SUM(MES12)] => 25474
						[DESCRIPCIO] => Blat
						[ANUAL] => 332850
						[consist] => Array([0] => 001100)
						[PREUS_ANUAL] => 17.437127835361
					*/
					for($i=1; $i<13; $i++) {
						$wValor[$code]['MES' .$i] = 
									$qfisiques[$code]['SUM(MES' .$i .')']*$qfisiques[$code]['PREUS_ANUAL'];
						if ($code=='001100')
						{
							//echo "//".$qfisiques[$code]['SUM(MES' .$i .')']."*".$qfisiques[$code]['PREUS_ANUAL']."//".$wValor[$code]['MES' .$i]."<br>";							
						}
						if (in_array($code,array('211110','211120','211210','211220','211230','211240','211250','211260',
												  '211280','212110','212210','212220','212230','212240')))	
						{ $wValor[$code]['MES' .$i]=$wValor[$code]['MES' .$i]/1000;				  
						}		
						if (preg_match('/^062/', $code)) $wValor[$code]['MES' .$i]=$wValor[$code]['MES' .$i]/10;
						if (in_array($code,$elems_in_estructuraSortidaPer)) {						
							if (!is_array($this->estructuraSortidaPer[$code])) 
							{ 
							  {
								$mensual_addition['MES' .$i] += $wValor[$code]['MES' .$i];
								//if ($i==10) echo $code.":".$wValor[$code]['MES' .$i].":".$mensual_addition['MES' .$i]."<br>";
								//if ($i==1) echo str_replace('.',',',$wValor[$code]['MES' .$i])."\r\n";
							  }
							} else {
	
								
							}
						} else {
 
							
						}							
					}		

					$wValor[$code]['ANUAL'] = 
												$qfisiques[$code]['ANUAL']*$qfisiques[$code]['PREUS_ANUAL'];
																					
					//$wValor[$code]['MEurosAnual'] = $wValor[$code]['ANUAL']/100000;
					$TOTAL_PRODS++;
				}
			}
			$wValor['TOTAL_MES'] = $mensual_addition;
			//print_r($mensual_addition);
			$wValor['TOTAL_PRODS'] = $TOTAL_PRODS;
			return $wValor; 
		}	
		
		/**
		 * Funci� wValorAll:
		* @param - $qFisiques:		 
		*/
		function wValor($qfisiques) {
			$wValor=array();
			foreach(array_keys($this->valorConsist) as $code) {
				foreach($this->valorConsist[$code]['consist'] as $consist) {
					if (!isset($wValor[$consist])) { $wValor[$consist]=array(); }
					if (!isset($qfisiques[$consist])) { $qfisiques[$consist] = array(); }
					if (!isset($qfisiques[$consist]['DESCRIPCIO'])) { $qfisiques[$consist]['DESCRIPCIO'] = '?'; }
					$wValor[$consist]['DESCRIPCIO'] = $qfisiques[$consist]['DESCRIPCIO'];
					//if ($code=='113000') { print_r($qfisiques); die(); }
					for($i=1; $i<13; $i++) {
						/*
						if ($consist=='111100') {  // Vedella b.carn  ull!! .. base 00?  !!!!!!!!!!!!!!!!!!!!!
							$wValor[$consist]['MES' .$i] = 
									($qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'])/10;
						}
						 */
						 /*
						else if ($consist=='116000') {  // Conills b.carn  ull!! .. base 00?  !!!!!!!!!!!!!!!!!!!!!
							$wValor[$consist]['MES' .$i] = 
									($qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'])*2;
						}
											  else 
											*/
						if (preg_match("/^062/", $consist)) { // Vi
							$wValor[$consist]['MES' .$i] = 
									($qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'])/10;
						}
						else if ((preg_match("/^211/", $consist)) or (preg_match("/^212/", $consist))) {  // Fustes
							$wValor[$consist]['MES' .$i] = 
									($qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'])/1000;
						}
						else {
							if (!isset($qfisiques[$consist]['SUM(MES' .$i .')'])) 	{  $qfisiques[$consist]['SUM(MES' .$i .')']=0;  }
							if (!isset($qfisiques[$consist]['PREUS_ANUAL'])) 		{  $qfisiques[$consist]['PREUS_ANUAL']=0;       }
							//echo "wValor[".$consist."][".'MES' .$i."] = (".$qfisiques[$consist]['SUM(MES' .$i .')'].")(".$qfisiques[$consist]['PREUS_ANUAL'].")\r\n";
							$wValor[$consist]['MES' .$i] = 
									$qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'];	
						}
					}
					
					$wValor[$consist]['ANUAL'] = 0;
					
					/*if ($consist=='111100') {  // Vedella b.carn  ull!! .. base 00?  !!!!!!!!!!!!!!!!!!!!!
							$wValor[$consist]['MES' .$i] = 
									($qfisiques[$consist]['SUM(MES' .$i .')']*$qfisiques[$consist]['PREUS_ANUAL'])/10;
						}
					else
					*/ 
					
					if (preg_match("/^062/", $consist)) {
						$wValor[$consist]['ANUAL'] = 
												$qfisiques[$consist]['ANUAL']*$qfisiques[$consist]['PREUS_ANUAL']/10;
					}
					else if ((preg_match("/^211/", $consist)) or (preg_match("/^212/", $consist))) {
						$wValor[$consist]['ANUAL'] = 
												$qfisiques[$consist]['ANUAL']*$qfisiques[$consist]['PREUS_ANUAL']/1000;
					}
					else
					{
						if (!isset($qfisiques[$consist]['ANUAL'])) 		        {  $qfisiques[$consist]['ANUAL']=0;        }
						if (!isset($qfisiques[$consist]['PREUS_ANUAL'])) 		{  $qfisiques[$consist]['PREUS_ANUAL']=0;  }
						$wValor[$consist]['ANUAL'] = 
												$qfisiques[$consist]['ANUAL']*$qfisiques[$consist]['PREUS_ANUAL'];
					}
												
					$wValor[$consist]['MEurosAnual'] = $wValor[$consist]['ANUAL']/100000;
				}
			}
			foreach(array_keys($this->valorConsist) as $code) {
				
				$wValor[$code]['DESCRIPCIO'] =  $code.$this->valorConsist[$code]['name'];
				
				for($i=1; $i<13; $i++){
					if ($code != '116000' and $code != '122000' and $code != '123000') {
						$wValor[$code]['MES' .$i] = 0;
					
						foreach($this->valorConsist[$code]['consist'] as $consist) {
							if (!isset($wValor[$consist])) { $wValor[$consist]=array(); }
							if (!isset($wValor[$consist]['MES' .$i])) { $wValor[$consist]['MES' .$i]=0; }
							$wValor[$code]['MES' .$i] = $wValor[$code]['MES' .$i] + $wValor[$consist]['MES' .$i];
						}
					}
				}	
			}
			foreach(array_keys($this->valorConsist) as $code) {
				if (!isset($wValor[$code]['ANUAL'])) { $wValor[$code]['ANUAL']=0; }
				if ($code != '116000' and $code != '122000' and $code != '123000') {	
					foreach($this->valorConsist[$code]['consist'] as $consist) {
						if (!isset($wValor[$consist]['ANUAL'])) { $wValor[$consist]['ANUAL']=0; }
						$wValor[$code]['ANUAL'] = $wValor[$code]['ANUAL'] + $wValor[$consist]['ANUAL'];
					}
				}
				$wValor[$code]['MEurosAnual'] = $wValor[$code]['ANUAL']/100000;
			}
			
			foreach(array_keys($this->valorConsist) as $code) {
				foreach($this->valorConsist[$code]['consist'] as $consist) 
				{
					if (!isset($wValor[$consist]['MEurosAnual'])) { $wValor[$consist]['MEurosAnual']=0; }
					if ($wValor['TOTAL']['MEurosAnual']==0) { $wValor[$consist]['base2000'] = 0; } //echo "Error. Divisi� per zero. Falten dades. Codi: $consist. Grup: $code.<br>";			
					if ($wValor['TOTAL']['MEurosAnual']>0) { $wValor[$consist]['base2000'] = 100*$wValor[$consist]['MEurosAnual']/$wValor['TOTAL']['MEurosAnual']; } 
					else  { $wValor[$consist]['base2000'] = 0; }
				}
				if ($wValor['TOTAL']['MEurosAnual']>0) { $wValor[$code]['base2000'] = 100*$wValor[$code]['MEurosAnual']/$wValor['TOTAL']['MEurosAnual']; }
				else { $wValor[$code]['base2000'] = 0; }
			}
			//print_r($wValor); die();
			return $wValor; 
		}
		
		/**
		* Funci� indexCompostAnyT:
		* @param - $indexSimple:		 
		* @param - $wValor:	
		*/	
		function indexCompostAnyT($indexSimple, $wValor) 
		{   // print_r($this->valorConsist);  die();
			foreach(array_keys($this->valorConsist) as $code) 
			{
				foreach($this->valorConsist[$code]['consist'] as $consist) 
				{
					if (!isset($indexSimple[$consist])) { $indexSimple[$consist]=array();}
					if (!isset($indexSimple[$consist]['DESCRIPCIO'])) { $indexSimple[$consist]['DESCRIPCIO']=''; }
					$indexCompost[$consist]['DESCRIPCIO'] = $indexSimple[$consist]['DESCRIPCIO']; 
					
					for($i=1; $i<13; $i++) {
						if (!isset($indexSimple[$consist]['MES' .$i])) { $indexSimple[$consist]['MES' .$i]=0; }
						$indexCompost[$consist]['MES' .$i] = $indexSimple[$consist]['MES' .$i];
				}
				if (!isset($indexSimple[$consist]['ANUAL'])) { $indexSimple[$consist]['ANUAL']=0; }
				$indexCompost[$consist]['ANUAL'] = $indexSimple[$consist]['ANUAL'];
					
				}
			}
			
			/*
			Array ( 
			[001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 102.039274924 [MES2] => 101.435045317 [MES3] => 102.114803625 [MES4] => 101.963746224 [MES5] => 103.32326284 [MES6] => 102.114803625 [MES7] => 100.906344411 [MES8] => 99.16918429 [MES9] => 100.151057402 [MES10] => 100.453172205 [MES11] => 103.549848943 [MES12] => 105.664652568 [ANUAL] => 101.208459215 ) 
			[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 74.5772320637 [MES2] => 74.5772320637 [MES3] => 74.5772320637 [MES4] => 81.324695917 [MES5] => 81.324695917 [MES6] => 81.324695917 [MES7] => 81.324695917 [MES8] => 0 [MES9] => 0 [MES10] => 80.9695662406 [MES11] => 84.8704453286 [MES12] => 85.1006253986 [ANUAL] => 79.4169128615 ) 
			[003000] => Array ( [DESCRIPCIO] => Ordi [MES1] => 83.0100188444 [MES2] => 83.1161856768 [MES3] => 83.3034271764 [MES4] => 86.1991809101 [MES5] => 86.1991809101 [MES6] => 90.1800862708 [MES7] => 89.6130227758 [MES8] => 88.6131315929 [MES9] => 89.4928841356 [MES10] => 89.887446097 [MES11] => 92.4754621149 [MES12] => 93.9771413026 [ANUAL] => 89.2352107937 ) 
			[004000] => Array ( [DESCRIPCIO] => Civada .......................
			*/
			foreach(array_keys($this->valorConsist) as $code) 
			{   
				$indexCompost[$code]['DESCRIPCIO'] = $this->valorConsist[$code]['name'];
				
				for($i=1; $i<13; $i++)
				{   //if ($code=='113000') echo "<br>** Mes $i **<br>";
					$total['MES' .$i]=0;
					if ($code != '116000' and $code != '122000' and $code != '123000')
					//  116000,122000 i 123000 -> ells mateixos son l'agrupaci�. Consist == ells mateixos IndxSimple = IndexCompost
					{
						if ($wValor[$code]['MES' .$i] != 0) {
							// Compost de CODE = sumatori de [ Compost CONSIST * Pes / totalPES ]
							foreach($this->valorConsist[$code]['consist'] as $consist) 
							{   
								$total['MES' .$i] += $wValor[$consist]['MES' .$i];
								if (!isset($indexCompost[$code]['MES' .$i])) { $indexCompost[$code]['MES' .$i]=0; }
								$indexCompost[$code]['MES' .$i] = 
									$indexCompost[$code]['MES' .$i] +
													$indexCompost[$consist]['MES' .$i]*$wValor[$consist]['MES' .$i];
								  //if ($code=='113000') echo "($consist) += ".$wValor[$consist]['MES' .$i].". Total: ".$total['MES' .$i].'<br>';
								  //if ($code=='113000') echo "($consist) indexCompost: ".$indexCompost[$code]['MES' .$i].'<br>';
							}
							 // if (($code=='TOTAL') and ($i==1)) echo "<br>***************************<br>";
							 // if ($code=='113000') echo "-($code)-> ".$indexCompost[$code]['MES' .$i]."/".$total['MES' .$i]." --> ";
							 $indexCompost[$code]['MES' .$i] = $indexCompost[$code]['MES' .$i]/$total['MES' .$i];
							 // if ($code=='113000') echo $indexCompost[$code]['MES' .$i].'<br>';
							 // if (($code=='TOTAL') and ($i==1)) echo "<br>*********".$total['MES' .$i]."////".$wValor[$code]['MES' .$i]."******************<br>";
						}
						else
							$indexCompost[$code]['MES' .$i] = 0;
					}
				}
				// == ANUAL ===
				if ($code != '116000' and $code != '122000' and $code != '123000') 
				{
					if ($wValor[$code]['ANUAL'] != 0) {
						foreach($this->valorConsist[$code]['consist'] as $consist) {
							if (!isset($indexCompost[$code]['ANUAL'])) { $indexCompost[$code]['ANUAL']=0; }
							$indexCompost[$code]['ANUAL'] = $indexCompost[$code]['ANUAL'] +
												$indexCompost[$consist]['ANUAL']*$wValor[$consist]['ANUAL'];
							//if ($code=='113000') echo "$code: -($consist)-> ".
							//                          $indexCompost[$code]['ANUAL']." + ".
							//                          $indexCompost[$consist]['ANUAL']." * ".$wValor[$consist]['ANUAL'].'<br>';					
						}
						$indexCompost[$code]['ANUAL'] = $indexCompost[$code]['ANUAL']/$wValor[$code]['ANUAL'];
					}
					else
						$indexCompost[$code]['ANUAL'] = 0;
				}
				//if (($code=='TOTAL')) echo "<br>0000000000000000000000000<br>";
			}
			/*
			Array ( 
			[001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 102.039274924 [MES2] => 101.435045317 [MES3] => 102.114803625 [MES4] => 101.963746224 [MES5] => 103.32326284 [MES6] => 102.114803625 [MES7] => 100.906344411 [MES8] => 99.16918429 [MES9] => 100.151057402 [MES10] => 100.453172205 [MES11] => 103.549848943 [MES12] => 105.664652568 [ANUAL] => 101.208459215 ) 
			[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 74.5772320637 [MES2] => 74.5772320637 [MES3] => 74.5772320637 [MES4] => 81.324695917 [MES5] => 81.324695917 [MES6] => 81.324695917 [MES7] => 81.324695917 [MES8] => 0 [MES9] => 0 [MES10] => 80.9695662406 [MES11] => 84.8704453286 [MES12] => 85.1006253986 [ANUAL] => 79.4169128615 ) 
			[003000] => Array ( [DESCRIPCIO] => Ordi [MES1] => 83.0100188444 [MES2] => 83.1161856768 [MES3] => 83.3034271764 [MES4] => 86.1991809101 [MES5] => 86.1991809101 [MES6] => 90.1800862708 [MES7] => 89.6130227758 [MES8] => 88.6131315929 [MES9] => 89.4928841356 [MES10] => 89.887446097 [MES11] => 92.4754621149 [MES12] => 93.9771413026 [ANUAL] => 89.2352107937 ) 
			[004000] => Array ( [DESCRIPCIO] => Civada [MES1] => 98.4251968504 [MES2] => 98.4251968504 [MES3] => 101.023622047 [MES4] => 102.362204724 [MES5] => 102.362204724 [MES6] => 0 [MES7] => 106.141732283 [MES8] => 106.220472441 [MES9] => 107.795275591 [MES10] => 106.299212598 [MES11] => 107.874015748 [MES12] => 107.874015748 [ANUAL] => 105.748031496 ) 
			[005000] => Array ( [DESCRIPCIO] => S�gol ............

			[000000] => Array ( [DESCRIPCIO] => CEREALS [MES1] => 82.9358525819 [MES2] => 83.0482690659 [MES3] => 82.4444175072 [MES4] => 87.0214380021 [MES5] => 83.5108955386 [MES6] => 90.2609651736 [MES7] => 92.75789302 [MES8] => 92.3836517988 [MES9] => 94.8685990593 [MES10] => 96.07412147 [MES11] => 95.1689385326 [MES12] => 96.445423746 [ANUAL] => 91.8237356081 ) 
			[010000] => Array ( [DESCRIPCIO] => LLEGUMINOSES [MES1] => 87.4240845469 [MES2] => 87.1188428056 [MES3] => 80.5875089361 [MES4] => 0 [MES5] => 90.2321655343 [MES6] => 115.880312928 [MES7] => 93.2111826858 [MES8] => 91.1857977534 [MES9] => 89.0260915498 [MES10] => 89.9720744252 [MES11] => 87.3618095121 [MES12] => 94.5354556199 [ANUAL] => 90.813286211 ) 
			[020000] => Array ( [DESCRIPCIO] => TUBERCLES [MES1] => 131.786681446 [MES2] => 118.387535546 [MES3] => 166.02764332 [MES4] => 146.437071137 [MES5] => 135.080490924 [MES6] => 90.1877335456 [MES7] => 83.1013856995 [MES8] => 87.0828504523 [MES9] => 92.2143369726 [MES10] => 130.274574441 [MES11] => 133.002880401 [MES12] => 126.288594743 [ANUAL] => 104.338669628 ) 
			[030000] => Array ( [DESCRIPCIO] => CONREUS INDUSTRIALS [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 135.97733711 [MES9] => 140.113314448 [MES10] => 138.470254958 [MES11] => 143.399433428 [MES12] => 144.702549575 [ANUAL] => 140.226628895 ) 
			[040000] => Array ( [DESCRIPCIO] => FARRATGES ....................
			*/
			return $indexCompost;
		}
		
		/**
		* Funci� indexCompostAnyTGrups:
		* Retorna l'�ndex Compost, nom�s dels GRUPS de productes.
		* @param - $indexSimple:		 
		* @param - $wValor:	
		*/
		function indexCompostAnyTGrups($indexCompost, $wValor) 
		{   // 
			//$indexCompost=$this->indexCompostAnyT($indexSimple, $wValor);
			// Agafem nom�s els consists
			$indexCompost2=array();
			foreach(array_keys($this->valorConsist) as $code) {
			   $indexCompost2[$code]=$indexCompost[$code];
			}
			return $indexCompost2;
		}

		/**
		 * Funci� indexAnualMobilSimpleAnyT:
		* @param - $preusProductesAnyT:		 
		* @param - $preusProductesAnyT_1:	
		* @param - $qFisiques:
		*/
		function indexAnualMobilSimpleAnyT($preusProductesAnyT, $preusProductesAnyT_1, $qFisiques) {
			foreach(array_keys($qFisiques) as $code) {
				if (isset( $preusProductesAnyT[$code])) {
					$indexAnualMobilSimpleAnyT[$code]['DESCRIPCIO'] = $preusProductesAnyT[$code]['DESCRIPCIO'];
					
					for($i=1; $i<13; $i++) {
						for ($j=$i; $j>0; $j--) {
							if (!isset($indexAnualMobilSimpleAnyT[$code]['MES' .$i])) { $indexAnualMobilSimpleAnyT[$code]['MES' .$i]=0; }
							$indexAnualMobilSimpleAnyT[$code]['MES' .$i] = $indexAnualMobilSimpleAnyT[$code]['MES' .$i] +
										$preusProductesAnyT[$code]['MES' .$j]*$qFisiques[$code]['SUM(MES' .$j .')'];							
						}
						// if ($code=='052300') echo "$code: (mes $i)".$indexAnualMobilSimpleAnyT[$code]['MES' .$i].'<br>';
						for($j=$i+1; $j<13; $j++) {
							$indexAnualMobilSimpleAnyT[$code]['MES' .$i] = $indexAnualMobilSimpleAnyT[$code]['MES' .$i] +
										$preusProductesAnyT_1[$code]['MES' .$j]*$qFisiques[$code]['SUM(MES' .$j .')'];
							//if ($code=='052300') echo "***$code (mes $j)".$preusProductesAnyT_1[$code]['MES' .$j].' * '.
							  //                        $qFisiques[$code]['SUM(MES' .$j .')'].'<br>';			
						}
						
						//if ($code=='052300') echo ">> $code: (mes $i)".$indexAnualMobilSimpleAnyT[$code]['MES' .$i].'<br>';
						if ($qFisiques[$code]['PREUS_ANUAL']>0 )
							  $indexAnualMobilSimpleAnyT[$code]['MES' .$i] = 100.0*$indexAnualMobilSimpleAnyT[$code]['MES' .$i]/
												 ($qFisiques[$code]['ANUAL']*$qFisiques[$code]['PREUS_ANUAL']);
						else $indexAnualMobilSimpleAnyT[$code]['MES' .$i] = 0;
					}
					
				}	
			}
			
			return $indexAnualMobilSimpleAnyT;
		}
		
		/**
		 * Funci� indexAnualMobilCompostAnyT:
		* @param - $indexAnualMobilSimpleAnyT:		 
		* @param - $wValor:	
		*/
		function indexAnualMobilCompostAnyT($indexAnualMobilSimpleAnyT, $wValor) {
			//should we keep those values?
			foreach(array_keys($this->valorConsist) as $code) {
				foreach($this->valorConsist[$code]['consist'] as $consist) {
					if (!isset($wValor[$consist]['DESCRIPCIO'])) {  $wValor[$consist]['DESCRIPCIO']='(?)'; }
					$indexAnualMobilCompostAnyT[$consist]['DESCRIPCIO'] = $wValor[$consist]['DESCRIPCIO'];
					
					for ($i=1; $i<13; $i++) {
						if (!isset($indexAnualMobilSimpleAnyT[$consist])) {  $indexAnualMobilSimpleAnyT[$consist] = array(); }
						if (!isset($indexAnualMobilSimpleAnyT[$consist]['MES'.$i])) {  $indexAnualMobilSimpleAnyT[$consist]['MES'.$i] = 0; }
						$indexAnualMobilCompostAnyT[$consist]['MES'.$i] = $indexAnualMobilSimpleAnyT[$consist]['MES'.$i];
					}
				}
			}
			
			foreach(array_keys($this->valorConsist) as $code) {
				$indexAnualMobilCompostAnyT[$code]['DESCRIPCIO'] = $wValor[$code]['DESCRIPCIO'];
			
				if ($code != '110000' and $code != '120000' and $code != 'PROD. AGR�COLES' and 
					$code != 'PROD. RAMADERS' and $code != 'PROD. FORESTALS' and $code != 'TOTAL') {
					
					for ($i=1; $i<13; $i++) {
						foreach($this->valorConsist[$code]['consist'] as $consist) {
							$indexAnualMobilCompostAnyT[$code]['MES' .$i] = $indexAnualMobilCompostAnyT[$code]['MES' .$i] 
										+($indexAnualMobilSimpleAnyT[$consist]['MES' .$i]*$wValor[$consist]['ANUAL']);
						}
						if ($wValor[$code]['ANUAL']!=0)
						  $indexAnualMobilCompostAnyT[$code]['MES' .$i] = $indexAnualMobilCompostAnyT[$code]['MES' .$i]/
																							$wValor[$code]['ANUAL'];	
					}
				}
			}
			
			for ($i=1; $i<13; $i++) {
				foreach($this->valorConsist['110000']['consist'] as $consist) {
					$indexAnualMobilCompostAnyT['110000']['MES' .$i] = $indexAnualMobilCompostAnyT['110000']['MES' .$i]+ 
								$indexAnualMobilCompostAnyT[$consist]['MES' .$i]*$wValor[$consist]['ANUAL'];
				}
				if ($wValor['110000']['ANUAL']>0) 	{
					$indexAnualMobilCompostAnyT['110000']['MES' .$i] = $indexAnualMobilCompostAnyT['110000']['MES' .$i] /
																						$wValor['110000']['ANUAL'];	
				} else { $indexAnualMobilCompostAnyT['110000']['MES' .$i] = '-'; }
			}
			
			for ($i=1; $i<13; $i++) {
				foreach($this->valorConsist['120000']['consist'] as $consist) {
					$indexAnualMobilCompostAnyT['120000']['MES' .$i] = $indexAnualMobilCompostAnyT['120000']['MES' .$i] + 
								$indexAnualMobilCompostAnyT[$consist]['MES' .$i]*$wValor[$consist]['ANUAL'];
				}
					
				if ($wValor['120000']['ANUAL']>0) 	{
					$indexAnualMobilCompostAnyT['120000']['MES' .$i] = $indexAnualMobilCompostAnyT['120000']['MES' .$i] /
																						$wValor['120000']['ANUAL'];	
				} else { $indexAnualMobilCompostAnyT['120000']['MES' .$i] = '-'; }
			}
			
			foreach($this->indexGreen as $code) {
				for ($i=1; $i<13; $i++) {
					if (is_array($this->valorConsist[$code]['consist']))
					{
					  foreach($this->valorConsist[$code]['consist'] as $consist) 
					  {
						$indexAnualMobilCompostAnyT[$code]['MES' .$i] = $indexAnualMobilCompostAnyT[$code]['MES' .$i] + 
									$indexAnualMobilCompostAnyT[$consist]['MES' .$i]*$wValor[$consist]['ANUAL'];
					  }
					}
					if ($wValor[$code]['ANUAL']>0.0) $indexAnualMobilCompostAnyT[$code]['MES' .$i] = $indexAnualMobilCompostAnyT[$code]['MES' .$i] /
																							$wValor[$code]['ANUAL'];	
				}
			}
			
			for ($i=1; $i<13; $i++) {
				$indexAnualMobilCompostAnyT['TOTAL']['MES' .$i]=0;
				foreach($this->valorConsist['TOTAL']['consist'] as $consist) {
					$indexAnualMobilCompostAnyT['TOTAL']['MES' .$i] = $indexAnualMobilCompostAnyT['TOTAL']['MES' .$i] + 
								$indexAnualMobilCompostAnyT[$consist]['MES' .$i]*$wValor[$consist]['ANUAL'];
				}
					
				if ($wValor['TOTAL']['ANUAL']>0.0) {
					 $indexAnualMobilCompostAnyT['TOTAL']['MES' .$i] = $indexAnualMobilCompostAnyT['TOTAL']['MES' .$i] /
																						$wValor['TOTAL']['ANUAL'];	
				} else { $indexAnualMobilCompostAnyT['TOTAL']['MES' .$i] = '-'; }
			}
			
			return $indexAnualMobilCompostAnyT;
		}
		
		/**
		 * Funci� accumulatAnyT:
		* @param - $wValor:		 
		* @param - $indexCompostAnyT:	
		*/
		function accumulatAnyT($wValor, $indexCompostAnyT) {
			foreach($this->indexGroups as $code) {
				$accumulatAnyT[$code]['DESCRIPCIO'] = $indexCompostAnyT[$code]['DESCRIPCIO'];
				
				$sumProduct = 0;
				$sumWValor = 0;
				
				for ($i=1; $i<13; $i++) 
				{
					//echo $code.':(index compost)'.$indexCompostAnyT[$code]['MES' .$i].':(valor)'.$wValor[$code]['MES' .$i].'<br>';
					$sumProduct = $sumProduct + $indexCompostAnyT[$code]['MES' .$i]*$wValor[$code]['MES' .$i];
					$sumWValor = $sumWValor + $wValor[$code]['MES' .$i];
					
					if ($sumWValor != 0)
						$accumulatAnyT[$code]['MES' .$i] = $sumProduct/$sumWValor;
					else
						$accumulatAnyT[$code]['MES' .$i] = 0;
				}
				$accumulatAnyT[$code]['INDEX ANUAL any t'] = $indexCompostAnyT[$code]['ANUAL'];
				$accumulatAnyT[$code]['Test (O-P)=0'] = $accumulatAnyT[$code]['MES12'] - $indexCompostAnyT[$code]['ANUAL'];
			}
			
			return $accumulatAnyT;
		}
		
		/**
		 * Funci� varACiMENanyT:
		* @param - $accumulatAnyT:		 
		* @param - $indexCompostAnyT:
		* @param - $indexCompostAnyT_1:		 
		* @param - $indexAnualMobilCompostAnyT:
		* @param - $indexAnualMobilCompostAnyT_1:	
		*/
		function varACiMENanyT($accumulatAnyT, $indexCompostAnyT, $indexCompostAnyT_1, 
							   $indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1) {
			foreach ($this->indexGroups as $code) {
				$varACiMENanyT[$code]['DESCRIPCIO'] = $accumulatAnyT[$code]['DESCRIPCIO'];
				
				for($i=1; $i<13; $i++) {
					if ($indexCompostAnyT_1[$code]['ANUAL']==0) {} //  
					else
				   {				
						$varACiMENanyT[$code]['MES' .$i]['Acum'] = 100*(($accumulatAnyT[$code]['MES' .$i]/
																				$indexCompostAnyT_1[$code]['ANUAL'])-1);
						if ($i == 1)
							$varACiMENanyT[$code]['MES' .$i]['Men'] = $varACiMENanyT[$code]['MES' .$i]['Acum'];
						else
							$varACiMENanyT[$code]['MES' .$i]['Men'] = $varACiMENanyT[$code]['MES' .$i]['Acum'] - 
																				$varACiMENanyT[$code]['MES' .($i-1)]['Acum'];
					}
				}	
				if ($indexCompostAnyT_1[$code]['ANUAL']==0) {} //  
				else {	$varACiMENanyT[$code]['anual1'] = 100*(($indexCompostAnyT[$code]['ANUAL']/$indexCompostAnyT_1[$code]['ANUAL'])-1); }
				if ($indexAnualMobilCompostAnyT[$code]['MES12']==0) {} //  
				else { $varACiMENanyT[$code]['anual2'] = 100*(($indexAnualMobilCompostAnyT[$code]['MES12']/$indexAnualMobilCompostAnyT_1[$code]['MES12'])-1); }
			}
			
			return $varACiMENanyT;
		}
		
		/**
		 * Funci� contribucioAnyT:
		* @param - $indexAnualMobilCompostAnyT:		 
		* @param - $indexAnualMobilCompostAnyT_1:
		* @param - $indexAnualMobilSimpleAnyT:		 
		* @param - $indexAnualMobilSimpleAnyT_1:
		* @param - $wValor:	
		*/
		function contribucioAnyT($indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1, 
								 $indexAnualMobilSimpleAnyT, $indexAnualMobilSimpleAnyT_1, $wValor) 
			{
			  foreach(array_keys($this->valorConsist) as $code) 
				{
				  foreach($this->valorConsist[$code]['consist'] as $consist) 
					{
					  $contribucioAnyT[$consist]['DESCRIPCIO'] = $indexAnualMobilCompostAnyT[$consist]['DESCRIPCIO'];
					
					  for($i=1; $i<13; $i++) 
					  {
						  if ($indexAnualMobilCompostAnyT_1['TOTAL']['MES' .$i] != 0) {
							  $contribucioAnyT[$consist]['MES' .$i] = $wValor[$consist]['base2000']*
																(($indexAnualMobilSimpleAnyT[$consist]['MES' .$i]-
																				$indexAnualMobilSimpleAnyT_1[$consist]['MES' .$i])/
																$indexAnualMobilCompostAnyT_1['TOTAL']['MES' .$i]);
						  } else {
							  $contribucioAnyT[$consist]['MES' .$i] = '(?)';
						  }
					  }
					}
			   }
			
			  foreach(array_keys($this->valorConsist) as $code) {
				$contribucioAnyT[$code]['DESCRIPCIO'] = $indexAnualMobilCompostAnyT[$code]['DESCRIPCIO'];
				
				for($i=1; $i<13; $i++) {
					if ($indexAnualMobilCompostAnyT_1['TOTAL']['MES' .$i] != 0) {
						$contribucioAnyT[$code]['MES' .$i] = $wValor[$code]['base2000']*(($indexAnualMobilCompostAnyT[$code]['MES' .$i]-
																				$indexAnualMobilCompostAnyT_1[$code]['MES' .$i])/
																$indexAnualMobilCompostAnyT_1['TOTAL']['MES' .$i]);
					} else {
						$contribucioAnyT[$code]['MES' .$i] = '(?)';
					}
				}
			}
			
			return $contribucioAnyT;
		}
		
		/**
		 * Funci� taxaVariacioInteranualAnyT:
		* @param - $indexAnualMobilCompostAnyT:		 
		* @param - $indexAnualMobilCompostAnyT_1:
		* @param - $indexAnualMobilSimpleAnyT:		 
		* @param - $indexAnualMobilSimpleAnyT_1:	
		*/
		function taxaVariacioInteranualAnyT($indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1, 
											$indexAnualMobilSimpleAnyT, $indexAnualMobilSimpleAnyT_1) {
			foreach(array_keys($this->valorConsist) as $code) 
			{   
				foreach($this->valorConsist[$code]['consist'] as $consist) 
					{
					
					$taxaVariacioInteranualAnyT[$consist]['DESCRIPCIO'] = $indexAnualMobilCompostAnyT[$consist]['DESCRIPCIO'];
					
					if ($code != '110000' and $code != '120000' and $code != 'PROD. AGR�COLES' and 
					$code != 'PROD. RAMADERS' and $code != 'PROD. FORESTALS' and $code != 'TOTAL') {
						for($i=1; $i<13; $i++) 
						{   //if ($consist=='052300') 
							//{
							//   echo $code.": mes $i)".
							//        $indexAnualMobilCompostAnyT[$consist]['MES' .$i].'/'.
							//        $indexAnualMobilCompostAnyT_1[$consist]['MES' .$i].'-1<br>'; 
							// }  
							if ($indexAnualMobilCompostAnyT_1[$consist]['MES' .$i]==0) {} // 
							else $taxaVariacioInteranualAnyT[$consist]['MES' .$i] = 100*(($indexAnualMobilCompostAnyT[$consist]['MES' .$i]/
																				$indexAnualMobilCompostAnyT_1[$consist]['MES' .$i])-1);
						}
					}	
				}
			}		
			foreach(array_keys($this->valorConsist) as $code) {
				$taxaVariacioInteranualAnyT[$code]['DESCRIPCIO'] = $indexAnualMobilCompostAnyT[$code]['DESCRIPCIO'];
				
				for($i=1; $i<13; $i++) 
				{   
					if ($indexAnualMobilCompostAnyT_1[$code]['MES' .$i]==0) {} // 
					else $taxaVariacioInteranualAnyT[$code]['MES' .$i] = 100*(($indexAnualMobilCompostAnyT[$code]['MES' .$i]/
																	 $indexAnualMobilCompostAnyT_1[$code]['MES' .$i])-1);
				}
			}
			
			return $taxaVariacioInteranualAnyT;
		}
		
		
		/**
		* Funci� escapeCharacters: we could avoid using the escape function if we'd not store the products names (descripcion). right now it's stored only in one table. also we would have to make sure that all the column names in all the parsed tables are free of '\n' and ',' and "". function to escape "" and , and /n characters
		* @param - $string:		 	
		*/
		function escapeCharacters($string) {
			
			$string = str_replace('"', '""', $string);
			if (preg_match("/,/", $string) or preg_match("/\n/", $string))
				return '"' .$string .'"';
			else
				return $string;
		}

		/**
		 * Funci� toCSV:
		* @param - $toExport:		 
		*/
		function toCSV ($toExport) {
			$csv = 'CODI';
		
			foreach (array_keys($toExport) as $code) {
				foreach (array_keys($toExport[$code]) as $name) {
					$csv = $csv .',' .$this->escapeCharacters($name);
				}
				$csv = $csv ."\n";
				break;
			}
		
			foreach ($this->allCodes as $code) {
				if ($toExport[$code] != NULL) {
					$csv = $csv .$this->escapeCharacters($code);
		
					foreach ($toExport[$code] as $field) {
						$csv = $csv .',' .$this->escapeCharacters($field);
					}
					$csv = $csv ."\n";
				}
			}
		
			return $csv;
		}
		
		/**
		 * Funci� toHTML:
		* @param - $toExport:
		* @param - $filtre='':	
		*/
		function toHTML ($toExport,$filtre='',$decimals=0) {			
			/*
			print_r($toExport); 
			Array ( [001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[003000] => Array ( [DESCRIPCIO] => Ordi [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[004000] => Array ( [DESCRIPCIO] => Civada [MES1] => 105.937517048 [MES2] => 106.133912822 [MES3] => 106.341765016 [MES4] => 106.521794475 [MES5] => 106.849120765 [MES6] => 106.849120765 [MES7] => 104.313600967 [MES8] => 101.892785253 [MES9] => 101.980296076 [MES10] => 102.278163 [MES11] => 102.576029924 [MES12] => 102.890263162 ) 
			*/		
			
			if ($filtre!='')
			{  $valorsNoFiltrats=array();
				while (list ($codiP, $infoP) = each ($this->valorConsist)) 
				 {    if ($codiP==$filtre)
					   { $nom = $infoP['name'];
						  $elsconsit = $infoP['consist'];
						  $elsconsit_arr = array();
						  while (list ($ix, $codiPc) = each ($elsconsit)) 
						  {  $elsconsit_arr[] = $codiPc;
						  }	
						  /*
						  print_r($elsconsit_arr);	
						  Array ( [0] => 011000 [1] => 013000 [2] => 014000 [3] => 016000 ) 
						  */
					   }
				 }
			
			}
			$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
					<title></title>
					<link href="_css/gene_estils.css" rel="stylesheet" type="text/css" />
					<style type="text/css">
					body {
						margin-left: 0px;
						margin-top: 0px;
					}
					
					</style>
					<link href="_css/generalitat_estils.css" rel="stylesheet" type="text/css" />
					<style type="text/css">
					<!--
					.panel {border: 1px dashed; background-color: white; padding: 5px; overflow: auto;}
					-->
					</style>
					<link href="_css/revisio_preus.css" rel="stylesheet" type="text/css" />
					<link href="_css/historic.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
			';
			$html .= '<table><tr><th><div class="h_taula_001">CODI</div>';
			foreach (array_keys($toExport) as $code) {
				foreach (array_keys($toExport[$code]) as $name) {
					$html = $html .'</th><th><div class="h_taula_001">' .$this->escapeCharacters($name)."</div>";
				}
				break;
			}
			
			$html = $html .'</th></tr>';
			// print_r($toExport); echo "<hr>";
			foreach ($this->allCodes as $code) 
			{   // echo '**> '.$code.'<br>';
				$fer = false;
				if (isset($toExport[$code])) {
					if ($toExport[$code] != NULL) 
					{   // echo "(1)";
						if ($filtre=='') { $fer = true;  } 
						else // hi ha filtre
						{  // echo "(3)";
						   if (in_array($code,$elsconsit_arr)) $fer=true;
						}
					}else
					{
					  // echo "(4)(".$code.") val:".$toExport[$code].'<br>';
					}
				}
				
				if ($fer) 
				{   // echo '*****> '.$code.' (fet)<br>';
					$html = $html .'<tr><td>';
					$html = $html .$this->escapeCharacters($code);
		
					foreach ($toExport[$code] as $field) {
						
						if (is_numeric($field)) 
						{  if ($field==0) { 
											if ($this->toHTML_format_ZERO_especial) $fieldFormat="--"; 
											else $fieldFormat = "<div class='h_taula_004_bis'><i>0</i></div>";
										  }
							else { if ($this->sortidaPercent)
										 { $decs = ($decimals==0)?(3):($decimals);
										   //if ($field>=0) $field=number_format($field, $decs, ',', '');
										   //else  $field='-&nbsp;'.number_format(abs($field), $decs, ',', '');				
										   $field=number_format($field, $decs, ',', '');		                
										   $fieldFormat = "<div class='h_taula_004_bis'><i>".$field."</i>%</div>";
										 }
									  else 
										{  $decs = ($decimals==0)?(2):($decimals);
										   $field=number_format($field, $decs, ',', '');  
										   $fieldFormat = "<div class='h_taula_004_bis'><i>".$field."</i></div>";
										}								  
									}
						}
						else { $fieldFormat = $field; }
						$html = $html ."</td><td>" .($fieldFormat)."";
					}
					$html = $html ."</td></tr>";
				}
			}
			
			$html = $html .'</table>';
			
			return $html;
		}

		/**
		 * Funci� toArray:
		* @param - $toExport:
		* @param - $filtre='':
		* @param - $bestiarvida=false:	
		*/
		function toArray ($toExport,$filtre='',$bestiarvida=false) 
		{   
			/*
			print_r($toExport); 
			Array ( [001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[003000] => Array ( [DESCRIPCIO] => Ordi [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 ) 
						[004000] => Array ( [DESCRIPCIO] => Civada [MES1] => . . . . . . .   [MES9] => 101.980296076 [MES10] => 102.278163 [MES11] => 102.576029924 [MES12] => 102.890263162 ) 
			*/
			if ($filtre!='')  
			{   $valorsNoFiltrats=array();
				if ($bestiarvida) $valorConsist = $this->valorConsistWithBestiarVida;
				else $valorConsist = $this->valorConsist;
				
				while (list ($codiP, $infoP) = each ($valorConsist)) 
				 {    if ($codiP==$filtre)
					   { $nom = $infoP['name'];
						  $elsconsit = $infoP['consist'];
						  $elsconsit_arr = array();
						  while (list ($ix, $codiPc) = each ($elsconsit)) 
						  {  $elsconsit_arr[] = $codiPc;
						  }	
						  /*
															  print_r($elsconsit_arr);	
							Array ( [0] => 011000 [1] => 013000 [2] => 014000 [3] => 016000 ) 
						  */
					   }
				 }
			
			}
			$xAgregats = array();
			$linies = array();
			if ($bestiarvida) $allcodes =$this->allCodesWithBestiarVida;
			else $allcodes =$this->allCodes;
			foreach ($allcodes as $code) 
			{
				$linia = array();
				$fer = false;
				if ($toExport[$code] != NULL) 
				{  if ($filtre=='') $fer = true; 
					else // hi ha filtre
					{  if (in_array($code,$elsconsit_arr)) $fer=true;
					}
				}
				
				if ($fer) {
					
					array_push($linia,$code);
					// Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 17.48 [MES2] => 17.44 [MES3] => 17.42 [MES4] => 17.57 [MES5] => 18.18 [MES6] => 17.12 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => ) 
					while (list($index, $field) = each($toExport[$code])) 
					{		if ($index=='DESCRIPCIO') {}
							elseif ($index=='ANUAL') {}						
							else
							{ // �s un mes MES_
							  $month=str_replace('MES','',$index);
							  
							  if (array_key_exists($code,$this->qFisiquesRed)) $esagregat = true;
							  else $esagregat = false;
							  if ($esagregat)
								 { // if ($code=='052400') echo "Agregat: (".$code.")<br>";
								   // if ($code=='052400') echo "Pr�ssec"."<br>";
								   // if ($code=='052400') echo "Mes:".$month."<br>";
								   $BaseZero = true;
								   $condicio=$this->qFisiquesRed[$code]['like'];
								   // 002___
								   $condicio=str_replace('_','',$condicio);
								   // mirem tots els codis anteriors que compleixen la condici�, a veure si algun t� pres --> l'agregat tindr� preu tamb�.
								   // if ($code=='052400') print_r($xAgregats);
								   // if ($code=='052400') echo "<br>";
								   reset($xAgregats);
								   while (list($elcodi, $sino) = each($xAgregats))
								   { 
									 preg_match('/^'.$condicio.'/',$elcodi, $matches);
									 if (count($matches)>0)
									 {  // if ($code=='052400') echo "�s pr�ssec: ".$elcodi."<br>";
										$esundelsbuscats = true;
										$condicioNO=$this->qFisiquesRed[$code]['notLike'];
										if (is_array($condicioNO))  // aquesta part no �s necessaria perqu� els codis ja venen ordenats, per� no fa cap mal!
										{  while (list(, $elcodiNO) = each($condicioNO))
										   {  if ($elcodi==$elcodiNO) { $esundelsbuscats = false;  }
										   }
										}
										if ($esundelsbuscats)
										{ 
										  // �s un dels productes de l'agregat
										  if ($xAgregats[$elcodi][$month]) { $BaseZero = false;  } // S� que t� preu
										}
									 }
								   }
								   // if ($code=='052400') echo "BaseZero (".$BaseZero.")<br>";
								   if (!$BaseZero)
								   {  // T� preu perqu� algun dels productes de l'agregat t� preu
									  if (is_numeric($field)) 
										{  if ($field==0) {  }
										   else { $field=number_format($field, 2, '.', '');  }
										}
								   }
								   else $field='--';							   
								 }
							  else
								{ $xAgregats[$code][$month]=true;  // suposem que el codi t� preu aquest mes
								  if (Prices::BaseZeroTerritoriMesCat($code,$month,$this->baseTable,$this->conn)) 
									 { $field='--';
									   $xAgregats[$code][$month]=false;  // el codi NO t� preu aquest mes
									 } 
								  // echo "Codi:".$code."Mes:".$month." Preus: ".$xAgregats[$code]."<br>";	 
								}							  		
							  if (is_numeric($field)) 
								{  if ($field==0) {  }
								   else { $field=number_format($field, 2, '.', '');  }
								}
							}
							array_push($linia,$field);	
					} // fi del mentre que itera sobre els elements d'un producte (codi, descripci�, preus ....)
					if ($esagregat) { $xAgregats=array(); // hem acabat la iteraci� de les dades d'un producte agregat d'altres. Podem fer un 'reset' dels codis.
									  $linia[1] = '* '.$linia[1]; // marquem l'agregat.
									 }
					// if ($code=='052400') die();
				}
				// $linia:  Array ( [0] => 001100 [1] => Blat tou o semidur [2] => 13.96 [3] => 13.82 [4] => 13.55 [5] => 13.42 [6] => 13.78 [7] => 13.35 [8] => 13.09 [9] => 13.69 [10] => 14.99 [11] => 16.31 [12] => 17.05 [13] => 17.26 [14] => 14.11 ) 
				if (count($linia)>0) $linies[] = $linia;
			}
			return $linies;
		}
		
		/**
		 * Funci� toCSVbyMonth:
		* @param - $toExport:
		*/	
		function toCSVbyMonth($toExport)
		{   
			$excel=new ExcelWriter(); 
			header("Content-Disposition: attachment; filename=GraficaPreus.xls"); 
			header("Pragma: no-cache"); 
			header("Expires: 0"); 
			

					/*
			
			Array ( [000000] => Array ( [DESCRIPCIO] => CEREALS [MES1] => 91.9290515095 [MES2] => 91.0576002958 [MES3] => 88.8592295603 [MES4] => 88.8970285712 [MES5] => 86.2360139289 [MES6] => 88.2479006848 [MES7] => 87.9681841066 [MES8] => 90.8622041478 [MES9] => 101.73304957 [MES10] => 110.724879535 [MES11] => 109.606225927 [MES12] => 111.99276458 ) 
			[010000] => Array ( [DESCRIPCIO] => LLEGUMINOSES [MES1] => 94.4074356133 [MES2] => 93.6237260354 [MES3] => 90.3359979203 [MES4] => 0 [MES5] => 89.9461508605 [MES6] => 117.961859094 [MES7] => 89.8665181141 [MES8] => 94.2185925651 [MES9] => 94.8081015693 [MES10] => 99.0052561994 [MES11] => 92.7794192525 [MES12] => 97.1737686082 ) 
			[020000] => Array ( [DESCRIPCIO] => TUBERCLES ................. 
			
			*/
			$output = "";
			$elscodis = array_keys($toExport);
			/*
			Array
				(
					[0] => 000000
					[1] => 010000
					[2] => 020000
					.
					.
					.
					[11] => 090000
					[12] => PROD. AGR�COLES
					[13] => 111000
					[14] => 112000
					.
					.

			*/
			$mesosAvr = array("","gen","feb","mar","abr","mai","jun","jul","ago","set","oct","nov","des");
			$arreglar_dec=true;
			$output='';
			$filaix=0;
			$quants = count($toExport);
			for ($m=1;$m<=12;$m++)
			{  for ($n=0; $n<$quants; $n++)
			   {
				 $filaix++;
				 if ($toExport[$elscodis[$n]]['DESCRIPCIO']=='TOTAL') { $toExport[$elscodis[$n]]['DESCRIPCIO'] = 'IPPA GENERAL'; $elcodi = "'++"; }
				 else if ($toExport[$elscodis[$n]]['DESCRIPCIO']!=$elscodis[$n]) { $elcodi = "'".$elscodis[$n]; }
				 else {  $elcodi = "'+"; }
				 $lafila = array($filaix,$elcodi,$toExport[$elscodis[$n]]['DESCRIPCIO'],2006,$m,$mesosAvr[$m],number_format($toExport[$elscodis[$n]]['MES'.$m],2,'.',','));
				 $output .= $excel->ferCSV($lafila,$arreglar_dec);
				 
			   }
			}
			
			echo $output;
			die();
		
		}
		
		/**
		 * Funci� toCSVqFisiques:
		* @param - $toExport:
		*/
		function toCSVqFisiques ($toExport) {
			$csv = "CODI,DESCRIPCIO,MES1,MES2,MES3,MES4,MES5,MES6,MES7,MES8,MES9,MES10,MES11,MES12," .
				   "ANUAL,PREU BASE anual any 2000 \n";
														
			foreach ($this->indexAll as $code) {
				if ($toExport[$code] != NULL) {
					$csv = $csv .$this->escapeCharacters($code) .',' .$toExport[$code]['DESCRIPCIO'];				
					for ($i=1; $i<13; $i++) {
						$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['SUM(MES' .$i .')']);
					}		
					$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['ANUAL']);
					$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['PREUS_ANUAL']) ."\n";
				}
			}
			return $csv;
		}
		/**
		 * Funci� toHTMLqFisiques:
		* @param - $toExport:
		*/
		function toHTMLqFisiques ($toExport) {
			$html = '<table><tr><th>CODI</th><th>DESCRIPCIO</th><th>MES1</th><th>MES2</th><th>MES3</th>' .
					'<th>MES4</th><th>MES5</th><th>MES6</th><th>MES7</th><th>MES8</th><th>MES9</th>' .
					'<th>MES10</th><th>MES11</th><th>MES12</th><th>ANUAL</th><th>PREU BASE anual any 2000</th></tr>';
					
			foreach ($this->indexAll as $code) {
				if ($toExport[$code] != NULL) {
					$html = $html .'<tr><td>';
					$html = $html .$this->escapeCharacters($code) .'</td><td>' .$toExport[$code]['DESCRIPCIO'];
					
					for ($i=1; $i<13; $i++) {
						$html = $html .'</td><td>' .$this->escapeCharacters($toExport[$code]['SUM(MES' .$i .')']);
					}
			
					$html = $html .'</td><td>' .$this->escapeCharacters($toExport[$code]['ANUAL']);
					$html = $html .'</td><td>' .$this->escapeCharacters($toExport[$code]['PREUS_ANUAL']) .'</td></tr>';
				}
			}
			
			$html = $html .'</table></body></html>';
		
			return $html;
		}

		/**
		 * Funci� toCSVvarACiMENanyT:
		* @param - $toExport:
		*/
		function toCSVvarACiMENanyT ($toExport) {
			$csv = "CODI,DESCRIPCIO,,MES1,MES2,MES3,MES4,MES5,MES6,MES7,MES8,MES9,MES10,MES11,MES12," .
				   "IPPA anual t / IPPA anual t-1,IPPA anual mòbil m12 / IPPA mòbil m12-12 \n";
																	
			foreach ($this->indexGroups as $code) {
				$csv = $csv .$this->escapeCharacters($code) .',' .$this->escapeCharacters($toExport[$code]['DESCRIPCIO']) .',Acum';
			
				for ($i=1; $i<13; $i++) {
					$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['MES' .$i]['Acum']);
				}
			
				$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['anual1']);
				$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['anual2']);
				$csv = $csv ."\n";
				
				$csv = $csv .',,Men.';
				
				for ($i=1; $i<13; $i++) {
					$csv = $csv .',' .$this->escapeCharacters($toExport[$code]['MES' .$i]['Men']);
				}
				
				$csv = $csv ."\n";
			}
			
			return $csv;
		}
		
		/**
		 * Funci� toHTMLvarACiMENanyT:
		* @param - $toExport:
		*/
		function toHTMLvarACiMENanyT ($toExport,$precis=false) {
			$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
					<title></title>
					<link href="_css/gene_estils.css" rel="stylesheet" type="text/css" />
					<style type="text/css">
					body {
						margin-left: 0px;
						margin-top: 0px;
					}
					
					</style>
					<link href="_css/generalitat_estils.css" rel="stylesheet" type="text/css" />
					<style type="text/css">
					<!--
					.panel {border: 1px dashed; background-color: white; padding: 5px; overflow: auto;}
					-->
					</style>
					<link href="_css/revisio_preus.css" rel="stylesheet" type="text/css" />
					<link href="_css/historic.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
			';
			$html .= '<table><tr><th><div class="h_taula_001">CODI</div><th><div class="h_taula_001">DESCRIPCIO</div></th><th></th>
					 <th><div class="h_taula_001">MES1</div></th><th><div class="h_taula_001">MES2</div></th>' .
					'<th><div class="h_taula_001">MES3</div></th><th><div class="h_taula_001">MES4</div></th><th><div class="h_taula_001">MES5</div></th>
					 <th><div class="h_taula_001">MES6</div></th><th><div class="h_taula_001">MES7</div></th><th><div class="h_taula_001">MES8</div></th>' .
					'<th><div class="h_taula_001">MES9</div></th><th><div class="h_taula_001">MES10</div></th><th><div class="h_taula_001">MES11</div></th>
					 <th><div class="h_taula_001">MES12</div></th>';
			//	'<th><div class="h_taula_001">IPPA anual t / IPPA anual t-1</div></th>
			//		 <th><div class="h_taula_001">IPPA anual m�bil m12 / IPPA m�bil m12-12</div></th></tr>';
					
			foreach ($this->indexGroups as $code) {
				$html = $html .'<tr><td>' .$this->escapeCharacters($code) .'</td><td>' .$this->escapeCharacters($toExport[$code]['DESCRIPCIO']) .'</td><td>Acum';
			
				for ($i=1; $i<13; $i++) {
					if (isset($toExport[$code]['MES'.$i])) {$html = $html .'</td><td><div class="h_taula_004_bis" align="right">' .(number_format($toExport[$code]['MES'.$i]['Acum'], (($precis)?10:2), ',', '')).'&nbsp;</div>';
					}
				}
				//$html = $html .'</td><td><div class="h_taula_004_bis">' .(number_format($toExport[$code]['anual1'], 2, ',', ''));
				//$html = $html .'</div></td><td><div class="h_taula_004_bis">' .(number_format($toExport[$code]['anual2'], 2, ',', ''));
				$html = $html .'</div>';
				$html = $html .'</td></tr>';
				
				$html = $html .'<tr><td></td><td></td><td>Men.';
				
				for ($i=1; $i<13; $i++) {
					if (isset($toExport[$code]['MES'.$i])) {
						$html = $html .'</td><td><div  align="right">' .(number_format($toExport[$code]['MES' .$i]['Men'], 
					  (($precis)?10:2), ',', '')).'&nbsp;</div>';
					}
				}
				
				$html = $html .'</td></tr>';
			}
			
			return $html;
		}
		/*
		 * Funci� PesAnual:
		 * @param codiprod	 
		 * @param mes	 
		 */
		function PesAnual($codiprod,$mes)
		{   $anyBase = '20'.$this->base;
			$avgCat[$anyBase] = $this->entradaPreusAnyT((2000+$this->base),true);			
			$qFisiques = $this->qFisiques("basepercebuts".$this->base, $avgCat[$anyBase]);
			$wValorAll = $this->wValorAll($qFisiques);
			$elscodis = array_keys($qFisiques);
			sort($elscodis);
			
			$totalValor = 0;
			foreach($elscodis as $res => $code) {	
			if (!is_array($this->estructuraSortidaPer[$code])) 
							   { $data = $wValorAll[$code];	
								 if (in_array($code,array('211110','211120','211210','211220','211230','211240','211250','211260',
														  '211280','212110','212210','212220','212230','212240')))
										 $dataANUAL=($data['ANUAL']/1000);
								 elseif (eregi("^062",$code)) $dataANUAL=($data['ANUAL']/10);      
								 else $dataANUAL=$data['ANUAL'];
								 $totalValor+=$dataANUAL;
								 //echo str_replace('.',',',$dataANUAL)."<br>";
								}
							}   
			$pes_any = $wValorAll[$codiprod]['MES'.$mes]/$totalValor*10000;	
			return $pes_any;
		}
		/*
		 * Funci� estructuraSortida:
		 * @param tipusPreus	  
		 */	
		function estructuraSortida($tipusPreus)
		{ if ($tipusPreus=='percebuts') return $this->estructuraSortidaPer;
		  if ($tipusPreus=='pagats') return $this->estructuraSortidaPag;	  
		  if ($tipusPreus=='salaris') return $this->estructuraSortidaPagSalaris;
		  return null;
		}
		
		function codis_grups($nom_grup)
		{
		  $lesparaules_arr = split(' ',$nom_grup);
		  //print_r($lesparaules_arr);  //
		  $lesprimereslletres = "";
		  while (list($key, $value) = each($lesparaules_arr)) {
			if (strlen($value)>3) $lesprimereslletres .= substr(ereg_replace("[^A-Za-z0-9]", "",$value),0,2);
		  }
		  $lesprimereslletres = substr($lesprimereslletres,0,6);
		  if ($nom_grup=='Blat') $elcodidelgrup = '001000';
		  elseif ($nom_grup=='Arr�s') $elcodidelgrup = '002000';
		  elseif ($nom_grup=='Ordi') $elcodidelgrup = '003000';
		  elseif ($nom_grup=='Patata') $elcodidelgrup = '021000';
		  elseif ($nom_grup=='Taronja') $elcodidelgrup = '051100';
		  elseif ($nom_grup=='Mandarina') $elcodidelgrup = '051200';
		  elseif ($nom_grup=='Poma') $elcodidelgrup = '052100';
		  elseif ($nom_grup=='Pera') $elcodidelgrup = '052200';
		  elseif ($nom_grup=='Albercoc') $elcodidelgrup = '052300';
		  elseif ($nom_grup=='Pr�ssec') $elcodidelgrup = '052400';
		  elseif ($nom_grup=='Cirera') $elcodidelgrup = '052500';
		  elseif ($nom_grup=='Pruna') $elcodidelgrup = '052600';
		  elseif ($nom_grup=='Ametlla') $elcodidelgrup = '053100';
		  elseif ($nom_grup=='Enciam') $elcodidelgrup = '054010';
		  elseif ($nom_grup=='Col cabdellada') $elcodidelgrup = '054030';
		  elseif ($nom_grup=='Tom�quet') $elcodidelgrup = '054040';
		  elseif ($nom_grup=='P�sol') $elcodidelgrup = '054060';
		  elseif ($nom_grup=='Ceba') $elcodidelgrup = '054070';
		  elseif ($nom_grup=='Pebrot') $elcodidelgrup = '054090';
		  elseif ($nom_grup=='Carxofa') $elcodidelgrup = '054100';
		  elseif ($nom_grup=='Esp�rrec') $elcodidelgrup = '054110';
		  elseif ($nom_grup=='Mongeta tendra') $elcodidelgrup = '054160';
		  elseif ($nom_grup=='Vi Blanc') $elcodidelgrup = '062100';
		  elseif ($nom_grup=='Vi Negre') $elcodidelgrup = '062200';
		  elseif ($nom_grup=='Vi Rosat i Claret') $elcodidelgrup = '062600';
		  elseif ($nom_grup=="Oli d'Oliva") $elcodidelgrup = '071001';
		  elseif ($nom_grup=='Ous') $elcodidelgrup = '122000';
		  elseif ($nom_grup=='Llana') $elcodidelgrup = '123000';
		  else $elcodidelgrup = $lesprimereslletres;
		  return ($elcodidelgrup);
		}
	}

}
?>