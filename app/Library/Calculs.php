<?php 
use Illuminate\Database\DatabaseManager as DB;

namespace App\Library {
    /*
	* clase Calculs
	*/
	class Calculs {
		 var $v1;
		 var $v2;
		 
		 /**
		 * Funció SumaVector:
		* @param - $v:
		*/
		 static function SumaVector($v)
		 {  $valor=0;
			for ($i=0;$i<count($v);$i++)
			   {  
				  $valor += $v[$i];
			   }
			return $valor;
		 }
		/**
		 * Funció SumaProducteVect:
		* @param - $v1:
		* @param - $v2:
		* @param - $error:
		*/
		 static function SumaProducteVect($v1,$v2)
		 {  
			if (count($v1)!=count($v2))
			{ 
			   return null;
			}
			else
			{  $valor = 0;
			   for ($i=0;$i<count($v2);$i++)
			   {  
				  $valor += $v1[$i]*$v2[$i];
			   }
			   return $valor;
			}
		 } 
		/**
		 * Funció mitjanaPonderada:
		* @param - $ponderacio:
		* @param - $valor:
		*/
		 static function mitjanaPonderada($ponderacio,$valor)
		 {
			$SumProd = Calculs::SumaProducteVect($ponderacio,$valor);
			$SumaVect = Calculs::SumaVector($ponderacio);
			if ($SumaVect>0) $valor = $SumProd / $SumaVect;
			else $valor = 0; // ????
			return ($valor);
		 }
	}                                                                                                               
}  // namespace                                                                                                                 
?>