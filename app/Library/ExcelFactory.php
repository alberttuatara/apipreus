<?php

namespace App\Library {
	
    class ExcelFactory {

        public $simpleFactory;

        public function __construct(SimpleFactory $simpleFactory) {
            $this->simpleFactory = $simpleFactory;
        }

        public function creaExcel($conn,$pagatspercebuts,$base) {
            $excel = null;

            $excel = $this->simpleFactory->creaExcel($conn,$pagatspercebuts,$base);
			/*
            $excel->prepare();
            $excel->package();
            $excel->label();
			*/

            return $excel;
        }
    }
}