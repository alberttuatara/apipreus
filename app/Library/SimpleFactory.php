<?php
// https://www.binpress.com/tutorial/the-factory-design-pattern-explained-by-example/142

namespace App\Library {
	
    class SimpleFactory {
        public function creaExcel($conn,$pagatspercebuts,$base) {
            $excelOBJ = null;

            if (10==$base) {
                $excelOBJ = new Excel10($conn,$pagatspercebuts,$base);
            } else if (05==$base) {
                $excelOBJ = new Excel5($conn,$pagatspercebuts,$base);
            }

            return $excelOBJ;
        }
    }
}
