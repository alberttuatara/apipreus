<?php 
use Illuminate\Database\DatabaseManager as DB;

namespace App\Library {
    class Prices {
		var $idapp='ppp';
		var $conn;
		var $cal_arrelgar_comes;
		var $baseTable;
		var $base;
		var $descripcioTable;
		var $preusTable;
		var $pagPer;
		var $notesTable;
		var $year;
		var $month;
		var $codiTerr;
		var $base_intervals;
		var $TerritoriesCodes;
		var $TerritorisLletra;
		var $TerritorisNoms;
		var $mesos;
		var $demesos;
		var $mesosAvr;
		var $ValorxBuit;
		var $iconesEstat;

		
		/**
		* Constructor
		*/
		function __construct($pagatspercebut,$any,$mes,$codiTerr, $base, $conn) {
			
			
			
			
			$this->conn   			= $conn;
			$this->cal_arrelgar_comes = false;
			$this->base_intervals = array();
			$this->base_intervals['00'] = array(0,2008);
			$this->base_intervals['05'] = array(2005,2015);
			$this->base_intervals['10'] = array(2010,0);
			// (??) s'ha de fer dinàmic 
			$pagPer 				= strtolower($pagatspercebut);
			$this->pagPer 			= $pagatspercebut;
			$this->base   			= $base;
			$this->baseTable 		= strtoupper('base' .$pagPer ."" .$base);
			$this->descripcioTable 	= strtoupper('descripcio_preus' .$pagPer);
			$this->notesTable 		= strtoupper('notes_preus' .$pagPer);
			$this->preusTable 		= strtoupper('preus' .$pagPer);
			$this->year 			= $any;
			$this->month 			= $mes;
			$this->codiTerr			= $codiTerr;
			$this->TerritorisLletra = array('8'=>'B','17'=>'G','25'=>'L','43'=>'T');
			$this->TerritorisNoms 	= array('8'=>'Barcelona','17'=>'Girona','25'=>'Lleida','43'=>'Tarragona');
			$this->TerritorisNomsTots 	= array('8'=>'Barcelona','17'=>'Girona','25'=>'Lleida','43'=>'Tarragona','99'=>'Catalunya');
			$this->mesos			= array("","gener","febrer","març","abril","maig","juny","juliol","agost","setembre","octubre","novembre","desembre");
			$this->demesos 			= array("","de ","de ","de ","d'","de ","de ","de ","d'","de ","d'","de ","de ");
			$this->mesosAvr 		= array("","gen","feb","mar","abr","mai","jun","jul","ago","set","oct","nov","des");
			$this->iconesEstat 		= array('entrada'=>'b_edit.png','revisant'=>'b_revisio.png','validat'=>'b_validat.png');
			
			$sql = strtoupper("SELECT CodiTerr FROM territori");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				foreach($rows as $rowOBJ){  
					$row = ((array) $rowOBJ); 
					$this->TerritoriesCodes[] = $row['CODITERR'];
				}
			}
                                                                                                                    
			                                                                                                      
			                                                                                                        
			                                                                                                        
		}   

		/**** 
		*  Private functions 
		**/		                                                                                                            
        private function is_ok() {                                                                                   
            return 'OK';                          
        }                                                                                                           
		    
		private function arreglar($val)
		{  
			// return str_replace(',','.',$val);   
			return $val;
		}
		
		
		
		/**** 
		*  Public functions 
		**/	
		public function number_format_O($dada,$digits,$charDEC,$charMIL)
		{  
		   if ($this->cal_arrelgar_comes)
		   {  $dada = str_replace(',','.',$dada);
		   }
		   return number_format($dada,$digits,$charDEC,$charMIL);

		}
		
		function quants_preus_entrats_HiHa($codiprod,$PagatsPercebuts,$conn,$base)
		{   $sql = strtoupper("select count(*) as quants  from preus".$PagatsPercebuts." 
					 where  CODIPROD = '".$codiprod."' ");
			$rows = $this->conn->select($sql); 
			$num = count($rows);
			$row_q = ((array)$rows[0]); 
			return $row_q['QUANTS'];	
		}
		
		
		
		/**                                                                                                         
		* Validació que hi ha preus en aquest internval de temsp any+mes                                            
		*                                                                                                           
		***/                                                                                                        
		public function limits_no_correctes($any,$mes) {                                                            
			return false;                                                                                           
		}                                                                                                           
		                                                                                                            
		/*                                                                                                          
		*                                                                                                           
		*/                                                                                                          
		public function limits_anys_preus($priceType,$conn,$base) { //                                              
			$anyMIN = $base;                                                                                        
			$base_intervals=$this->base_intervals;                                                                  
			$anyMAX =  $base_intervals[$base][1];                                                                   
			$sqlmaxmin= "SELECT MAX(PREUS".strtoupper($priceType).".ANY) AS MAX, MIN(PREUS".strtoupper($priceType).".ANY) AS MIN 
						 FROM PREUS".strtoupper($priceType).                                                        
						 ' WHERE PREUS'.strtoupper($priceType).".ANY >= ".$anyMIN."  ";		                        
			if ($anyMAX>0)	                                                                                        
			{ $sqlmaxmin .= 'AND PREUS'.strtoupper($priceType).".ANY <= ".$anyMAX."  "; }                           
		                                                                                                            
			$rows = $conn->select($sqlmaxmin);                                                                      
			if (count($rows)==0) { $row_maxmin = array('MAX'=>0, 'MIN'=>0);                                         
			} else { $row_maxmin = ((array) $rows[0]); }                                                            
			$sqlmaxmin = "SELECT MIN(PREUS".strtoupper($priceType).".MES) as MMIN  FROM PREUS".strtoupper($priceType)." WHERE PREUS".strtoupper($priceType).".ANY = ".$row_maxmin['MIN']."  ";
			$rows = $conn->select($sqlmaxmin);                                                                      
			$row_maxmindetall = ((array) $rows[0]);                                                                 
			$row_maxmin['MMIN']=$row_maxmindetall['MMIN'];                                                          
			$sqlmaxmin= "SELECT MAX(PREUS".strtoupper($priceType).".MES) AS MMAX FROM PREUS".strtoupper($priceType)." WHERE PREUS".strtoupper($priceType).".ANY = ".$row_maxmin['MAX']."  ";
			$rows = $conn->select($sqlmaxmin);                                                                      
			$row_maxmindetall = ((array) $rows[0]);                                                                 
			$row_maxmin['MMAX']=$row_maxmindetall['MMAX'];                                                          
			return $row_maxmin;  // "MAX":2017,"MIN":2010,"MMIN":1,"MMAX":                                          
			                                                                                                        
        }       

		/**
		 * Funció quants_preus_entrats_App: 
		 Retorna el número de preus que hi ha entrats en total, preus pagats i percebuts junts, donat una data concreta (any i mes)
		* @param - $any
		* @param - $mes
		* @param - $conn
		 */
		function quants_preus_entrats_App($any,$mes,$conn,$base)
		{   $sql = strtoupper("select count(*) as quants  from preuspercebuts where  ANY=".$any." and mes = ".$mes." ");
			$rows = $conn->select($sql);  
			$row_q = ((array)$rows[0]);  // FetchRow();
			return $row_q['QUANTS'];	
		}		
		                                                                                                            
		/**
		 * Funció nom
		 * @param - $code:
		 */	
		function nom($code) {
			$sql = strtoupper("SELECT NomProd FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows);

			if ($num>0) { 
				$row = ((array)$rows[0]); 
				return $row['NOMPROD'];
			}
			else
			return null;
		}
		
		function units($code) {
			$sql = strtoupper("SELECT Unitat FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows);

			if ($num>0) { 
				$row = ((array)$rows[0]); 
				return $row['UNITAT'];
			}
			else
			return null;
		}
		
		/**
		 * Funció infoProd
		 * @param - $code:
		 */
		function infoProd($code)
		{  $sql = strtoupper("SELECT CodiProd,NomProd,Unitat,Subgrup,Grup,FaseIntercanvi,CondicionsComercialitzacio
				   FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");
				   
		$rows = $this->conn->select($sql); 
		$num = count($rows);		
		if ($num>0) { 
			$row = ((array)$rows[0]); 
			return $row;
		}
		else
		return null;

		}
		
		/*
		 * Funció es_agregacio
		 * @param CalculsExcel
		 * @param code
		 */
		function es_agregacio($CalculsExcel,$code)  // (¿?)
		{   
			// if (!is_object($CalculsExcel)) $CalculsExcel = new Excel($this->conn,'percebuts',$this->base);
			$est =  $CalculsExcel->estructuraSortida('percebuts');
			$est = $est+$CalculsExcel->estructuraSortida('pagats');
			$est = $est+$CalculsExcel->estructuraSortida('salaris');
			/*
			 * Array ( [001100] => [001000] => Array ( [0] => 001100 ) [002100] => [002200] => [002000] => Array ( [0] => 002100 [1] => 002200 ) ..........
			 */
			return is_array($est[$code]);
			
		}
		
		/*
		 * Funció pertany_agregacio
		 * @param CalculsExcel
		 * @param code
		 */
		function pertany_agregacio($CalculsExcel,$code)
		{   //if (!is_object($CalculsExcel)) $CalculsExcel = new Excel($this->conn);
			$est =  $CalculsExcel->estructuraSortida('percebuts');
			$est = $est+$CalculsExcel->estructuraSortida('pagats');
			$est = $est+$CalculsExcel->estructuraSortida('salaris');
			/*
			 * Array ( [001100] => [001000] => Array ( [0] => 001100 ) [002100] => [002200] => [002000] => Array ( [0] => 002100 [1] => 002200 ) ..........
			 */
			 if (!is_array($est)) { return null; }
			reset($est);
			$elsAgr = array();
			while (list($ccodi, $vval) = each($est)) 
				{ if (is_array($vval))
					{ while (list(, $Det_codi) = each($vval)) 
						{
						   $elsAgr[$Det_codi] = $ccodi;
						}
						
					}
				}
			// Array ( [001100] => 001000 [002100] => 002000 [002200] => 002000 [003300] => 003000 ............
			if (array_key_exists($code,$elsAgr))
			{ 
			  return $elsAgr[$code];
			}
			else return null;		
		}
		
		
		// ===================================================================
		
		public function priceTerr($code) {                                                                          
			                                                                                                        
			$sql = strtoupper("SELECT " .$this->preusTable .".FinPreu  AS preu, " .                                 
					"territori.CodiTerr  FROM " .$this->preusTable .", " .$this->baseTable .", territori " .        
					"WHERE " .$this->preusTable .".CodiProd = " .$this->baseTable .".CodiProd AND " .$this->preusTable .
					".CodiTerr = " .$this->baseTable .".CodiTerr AND " .$this->preusTable .".CodiProd = '" .$code . 
					"' AND " .$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".Mes = '" . 
					"" .$this->month ."' AND " .$this->preusTable .".CodiTerr IN (SELECT CodiTerr FROM " .          
					"territori) AND territori.CodiTerr = " .$this->preusTable .".CodiTerr;");                       
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);                                                                                    
			$ret = null;                                                                                            
			if ($num>0) {                                                                                           
				$ret = array();                                                                                     
				foreach($rows as $rowOBJ){                
				    $row = ((array) $rowOBJ);
					$ret[$row['CODITERR']] = $this->arreglar($row['PREU']);			
				}                                                                                                   
			}                                                                                                       
			return $ret;                                                                                            
		}       

		/**
		 * Funció priceTerritori: 
		 * @param - $code: 
		 * @param - $territ: 
		 */
		
		function priceTerritori($code,$territ) {
			$sql = strtoupper("SELECT FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
				   " CodiTerr = '" .$territ ."' AND ANY = '" .$this->year ."' AND mes = '" .$this->month ."'");
			
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);                                                                                    
			$ret = null;                                                                                            
			if ($num>0) { 
				$row = ((array)$rows[0]); 
				return $this->arreglar($row['FINPREU']);
			}
			else
			return null;
		}		
		
		/**
		 * Funció priceTerritoriPeriode: 
		 * @param - $code: 
		 * @param - $territ: 
		 * @param - $mes: 
		 * @param - $any: 
		 */
		 
		function priceTerritoriPeriode($code,$territ,$mes,$any) {
			$sql = strtoupper("SELECT FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
				   " CodiTerr = '" .$territ ."' AND ANY = '" .$any ."' AND mes = '" .$mes ."'");
			
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			if ($num>0) {	
				$row = ((array)$rows[0]);  // FetchRow();
				return $this->arreglar($row['FINPREU']);
			}
			else
			return null;
		}
		
		/**
		 * Funció priceDate
		 * @param - $code: 
		 * @param - $year: 
		 * @param - $month:
		 */
		
		function priceDate($code, $year, $month) {
			$sql = strtoupper("SELECT FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
			" CodiTerr = '" .$this->codiTerr ."' AND ANY = '" .$year ."' AND mes = '" .$month ."'");
			$rows = $this->conn->select($sql);
			$num = count($rows);        	
			if ($num>0) {				
				$fila = ((array)$rows[0]);  // FetchRow();
				return $this->arreglar($fila['FINPREU']);
			}
			else
			return null;
		
		}
		
		/**
		* Funció is_priceAuto: 
		* @param code
		* @param territ
		* @param year
		* @param month
		*/
		function is_priceAuto($code, $territ, $year, $month) 
		{
			$sql = strtoupper("SELECT CodiProd FROM " .$this->preusTable ." WHERE autom='s' AND CodiProd = '" .$code ."' AND" .
			" CodiTerr = '" .$territ ."' AND ANY = '" .$year ."' AND mes = '" .$month ."'");
			$rows = $this->conn->select($sql);
			$num = count($rows);        	
			if ($num>0) {			
				return true;			
			}
			else
			return false;
		}
		
		/*
		 * Funció espotmodif_preu: Inspecciona l'estat de l'aplicació, i retorna si està permès modficar el preu.
		 * @param code: 
		 * @param territori
		 * @param perfil
		 */
		
		function espotmodif_preu($code,$territori,$perfil)
		{   
			$estat = $this->estatAplicatiu($territori,$perfil);		
			if ($perfil=='SC')
			{  return true;
			}
			else
			{  
			   if ($estat=='validat') return false;
			   elseif ($estat=='revisant')
			   {  
				  return $this->hasNotes($code,$territori,$this->year,$this->month);
			   }
			   else  // entrant dades
			   {  return true;
			   }
			}
		  
		}
		
		
		function estatAplicatiu($territori,$perfil)   // ST: entrada,revisant,validat        SC:historic,actual
		{   
			$sql = "SELECT IDAPP,ESTAT,BASE,ENCURS_ANY_DADES,ENCURS_MES_DADES FROM APLICACIO WHERE IDAPP = '".$this->idapp."'";
			$rows = $this->conn->select($sql);
			$num = count($rows);        	
			if ($num>0) {	
				$fila = ((array)$rows[0]);  // FetchRow();
				$anyinc = $fila['ENCURS_ANY_DADES'];
				$mesinc = $fila['ENCURS_MES_DADES'];
				if ($perfil=='ST')
				{   //echo $month."!=".$mesinc." :: ".$year."!=".$anyinc;
					if ( ($this->month!=$mesinc) or ($this->year!=$anyinc) )  { return 'validat'; }
					else
					{
						$sql = strtoupper("SELECT estat FROM territori WHERE CodiTerr = ".$territori);
						$rows = $this->conn->select($sql);
						if ($num>0) {
							$row = ((array)$rows[0]);  // FetchRow();
							return $row['ESTAT'];
						}
						else	return false;
					}
				}
				else // SC
				{  if ( ($this->month!=$mesinc) or ($this->year!=$anyinc) )  { return 'historic'; }
				else { return 'actual'; }
				}
			} else	return false;
		}
	
		/**
		 * Funció baseCalMesosPreuSino: 
		 * @param - $code:
		 */
		function baseCalMesosPreuSino($code)
		{    $quinsMesosSql = strtoupper("SELECT 
				CASE WHEN (max(MES1)>0) THEN 1 ELSE 0 END as m1,
				CASE WHEN (max(MES2)>0) THEN 1 ELSE 0 END as m2,
				CASE WHEN (max(MES3)>0) THEN 1 ELSE 0 END as m3,
				CASE WHEN (max(MES4)>0) THEN 1 ELSE 0 END as m4,
				CASE WHEN (max(MES5)>0) THEN 1 ELSE 0 END as m5,
				CASE WHEN (max(MES6)>0) THEN 1 ELSE 0 END as m6,
				CASE WHEN (max(MES7)>0) THEN 1 ELSE 0 END as m7,
				CASE WHEN (max(MES8)>0) THEN 1 ELSE 0 END as m8,
				CASE WHEN (max(MES9)>0) THEN 1 ELSE 0 END as m9,
				CASE WHEN (max(MES10)>0) THEN 1 ELSE 0 END as m10,
				CASE WHEN (max(MES11)>0) THEN 1 ELSE 0 END as m11,
				CASE WHEN (max(MES12)>0) THEN 1 ELSE 0 END as m12
								FROM ".$this->baseTable."
								WHERE CodiProd= '".$code."'");
		
		$rows = $this->conn->select($quinsMesosSql);   
		$row_sino = ((array)$rows[0]); 

		return $row_sino;
		}

		
		/**
		 * Funció baseCalHistoricMesTerrit: 
		 * @param - $code:
		 * @param - $territori
		 */

		function baseCalHistoricMesTerrit($code,$territori) {
			$sql = strtoupper("SELECT MES1, MES2, MES3, MES4, MES5, MES6, MES7, MES8, MES9, MES10, MES11, MES12 FROM " .
					"" .$this->baseTable ." WHERE CodiProd = '" .$code ."' AND CodiTerr = '" .$territori ."'");
			
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows); 
			$sum = 0;
			$ret = null;
			if ($num>0) {  
				$table = ((array)$rows[0]);  // FetchRow()
					
				for ($i = 1; $i < 13; $i++) {
					$ret[$i] = $table["MES" .$i];
					
				}
			}		
			return $ret;

		}
		
		/**
		 * Funció baseCalHistoricMesTerritPerc: 
		 * @param - $code:
		 * @param - $territori
		 */
		function baseCalHistoricMesTerritPerc($code,$territori) {
			$sql = strtoupper("SELECT MES1, MES2, MES3, MES4, MES5, MES6, MES7, MES8, MES9, MES10, MES11, MES12 FROM " .
					"" .$this->baseTable ." WHERE CodiProd = '" .$code ."' AND CodiTerr = '" .$territori ."'");
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows); 
			$sum = 0;
			if ($num>0) {   
				$table = ((array)$rows[0]);  // FetchRow()
					
				for ($i = 1; $i < 13; $i++) {
					$sum = $sum + $table["MES" .$i];
				}
			}
			else { return null; }
				
			//checks if the sum is not null (null+null = null) or sm not 0
			if (($sum == null) || ($sum == 0)) {
				return null;
			}

			$sql = strtoupper("SELECT MES1, MES2, MES3, MES4, MES5, MES6, MES7, MES8, MES9, MES10, MES11, MES12 FROM " .
			"" .$this->baseTable ." WHERE CodiProd = '" .$code ."' AND CodiTerr = '".$territori ."'"); 

			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows); 
			$ret = null;
			if ($num>0) {  
				$ret = ((array)$rows[0]);
					
				for ($i = 1; $i < 13; $i++) {
					$ret[$i] = $ret["MES" .$i] * 100/$sum;
					$ret[$i] = intval($ret[$i]*100)/100;
					unset($ret["MES" .$i]);
				}
			}
			return $ret;
		}
		
		public function baseCalPercentageAllTerr($code,$highPrecision=false) {                                      
			//gets the sum of quantities for all territories                                                                                                                                                 
			$sql = strtoupper("SELECT SUM(MES" .$this->month .") AS suma FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .
					"AND CodiTerr IN (SELECT CodiTerr FROM Territori)");	                                        
			//echo $sql;		                                                                                    
			/*                                                                                                      
			 * SQL WITHOUT SUBQUERIES                                                                               
			 *                                                                                                      
			$sql = "SELECT SUM(MES" .$this->month .") FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .
					"AND CodiTerr IN ('8', '17', '25', '43')";                                                      
			 */		                                                                                                
			                                                                                                        
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);                                                                                    
			$ret = null;                                                                                            
			if ($num>0) {                                                                                           
				foreach($rows as $rowOBJ){                                                                          
					$row = ((array) $rowOBJ);                                                                       
					$sum = $row['SUMA'];                                                                            
				}                                                                                                   
			}                                                                                                       
			else { return null; }                                                                                   
			//checks if the sum is not null (null+null = null) or sm not 0                                          
			if (($sum == null) || ($sum == 0)) {                                                                    
				return null;                                                                                        
			}                                                                                                       
			//gets the array of quantities for all territories                                                      
			//if for one territory it's null ,then % is null for this territory as well                             
			$sql = strtoupper("SELECT " .$this->baseTable .".MES" .$this->month ." AS mes, territori.CodiTerr, territori.NomTerr AS terrNom FROM "
			.$this->baseTable .", territori WHERE " .$this->baseTable .".CodiProd = '" .$code ."' " .               
					"AND " .$this->baseTable .".CodiTerr IN (SELECT CODITERR FROM TERRITORI) AND " .                
					"territori.CodiTerr = " .$this->baseTable .".CodiTerr");                                        
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);                                                                                    
			if ($num>0) {                                                                                           
				foreach($rows as $rowOBJ){                                                                          
					$row = ((array) $rowOBJ);                                                                       
					$c=$row['CODITERR'];                                                                            
					$t=$this->TerritorisLletra[$c];                                                                 
					if ($highPrecision)                                                                             
					{                                                                                               
						$ret[$t] = ($row['MES'] / $sum)*100;                                                        
					}                                                                                               
					else                                                                                            
					{                                                                                               
					 $ret[$t] = (($row['MES'])*100 / $sum);                                                         
					 $ret[$t] = intval($ret[$t]*100)/100;                                                           
					}                                                                                               
				}                                                                                                   
				return $ret;                                                                                        
			}                                                                                                       
			else { return null; }                                                                                   
		}                                                                                                           
		                                                                                                            
		public function baseCalPercentageAllTerrMonth($code,$mes,$highPrecision=false) { 
			$ret = null;  
			//gets the sum of quantities for all territories                                                        
			$sql = strtoupper("SELECT SUM(MES" .$mes .") AS q FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .
					"AND CodiTerr IN (SELECT CodiTerr FROM Territori)");							                
			/*                                                                                                      
			 * SQL WITHOUT SUBQUERIES                                                                               
                                                                                                                    
			$sql = "SELECT SUM(MES" .$mes .") FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .        
					"AND CodiTerr IN ('8', '17', '25', '43')";                                                      
			 */     
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);                                                                                    
			if ($num>0) {                                                                                           
				foreach($rows as $rowOBJ){                                                                          
					$row = ((array) $rowOBJ);                                                                       
					$sum = $row['Q'];                                                                               
				}                                                                                                   
			}          
			if (!(($sum == null) || ($sum == 0))) {                                                                    
				//gets the array of quantities for all territories                                                  
				//if for one territory it's null ,then % is null for this territory as well                                 
				$sql = strtoupper("SELECT " .$this->baseTable .".MES" .$mes ." AS mes, territori.NomTerr AS terrNom, territori.CodiTerr FROM "
				.$this->baseTable .", territori WHERE " .$this->baseTable .".CodiProd = '" .$code ."' " .                   
						"AND " .$this->baseTable .".CodiTerr IN (SELECT CODITERR FROM TERRITORI) AND " .                    
						"territori.CodiTerr = " .$this->baseTable .".CodiTerr");                                            
				$rows = $this->conn->select($sql);                                                                          
				$num = count($rows);                                                                                                                                                                        
				if ($num>0) {                                                                                               
					foreach($rows as $rowOBJ){                                                                              
						$row = ((array) $rowOBJ);                                                                           
						$t=$row['CODITERR'];                                                                                
						$t=$this->TerritorisLletra[$t];                                                                     
						if ($highPrecision)                                                                                 
						{                                                                                                   
							$ret[$t] = ($row['MES'] / $sum)*100;                                                            
						}                                                                                                   
						else                                                                                                
						{                                                                                                   
						 $ret[$t] = ($row['MES']*100 / $sum);                                                               
						 $ret[$t] = intval($ret[$t]*100)/100;                                                               
						}                                                                                                   
					}                                                                                                                                                                                                  
				}                                                                                                           
			}
			return $ret; 
		}   // function       

		/**
		 * Funció baseCalHistoricMesPerc: 
		 * @param - $code:
		 */
		
		public function baseCalHistoricMesPerc($code) {
			$sql = strtoupper("SELECT MES1, MES2, MES3, MES4, MES5, MES6, MES7, MES8, MES9, MES10, MES11, MES12 FROM " .
					"" .$this->baseTable ." WHERE CodiProd = '" .$code ."' AND CodiTerr = '" .$this->codiTerr ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows);  
			$sum = 0;
			if ($num>0) {  
				$table = ((array)$rows[0]);  // FetchRow()
				for ($i = 1; $i < 13; $i++) {
					$sum = $sum + $table["MES" .$i];
				}
			}
			else { return null; }
			//checks if the sum is not null (null+null = null) or sm not 0
			if (($sum == null) || ($sum == 0)) {
				return null;
			}

			$sql = strtoupper("SELECT MES1, MES2, MES3, MES4, MES5, MES6, MES7, MES8, MES9, MES10, MES11, MES12 FROM " .
			"" .$this->baseTable ." WHERE CodiProd = '" .$code ."' AND CodiTerr = '".$this->codiTerr ."'"); 
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			if ($num>0) { 
				$ret = ((array)$rows[0]);  // FetchRow()
					
				for ($i = 1; $i < 13; $i++) {
					$ret[$i] = $ret["MES" .$i] * 100/$sum;
					$ret[$i] = intval($ret[$i]*100)/100;
					unset($ret["MES" .$i]);
				}
				return $ret;
			}
			else
			return null;
		}
		
		/**
		 * Funció descPrices
		 * @param - $code: 
		 */	
		function descPrices($code) {
			$sql = strtoupper("SELECT DescPreu1,DescPreu2,DescPreu3  FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."' ");
			
			$rows = $this->conn->select($sql); 
			$num = count($rows);
			if ($num>0) {  
				$fila = ((array)$rows[0]); 	
				return $fila;
			}
			else return null;
		}
		
		/**
		 * Funció maxPrices
		 * @param - $code: 
		 */
		
		function maxPrices($code) {
			$sql = strtoupper("SELECT MaxPreus FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");

			$rows = $this->conn->select($sql); 
			$num = count($rows);
			
			if ($num>0) {  
				$fila = ((array)$rows[0]); 	
				return $fila['MAXPREUS'];
			}
			else
			return null;
		}
	
		public function pricesYearBefore($code) {
			$ret = null; 
			$sql = strtoupper("SELECT CodiTerr,FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
			"  ANY = '" .($this->year-1) ."' AND mes = '" .$this->month ."' ");
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);     
			if ($num>0) { 
				foreach($rows as $rowOBJ){   
					$row = ((array) $rowOBJ);    
					$ret[$row['CODITERR']] = $this->arreglar($row['FINPREU']);
				}
			}
			return $ret; 
		}
		
		
		public function pricesMonthBefore($code) {
			$ret = null;
			$mesAnt = $this->month-1;
			$anymesAnt = $this->year;
			if ($mesAnt<1) { $mesAnt=12; $anymesAnt -=1;}
			$sql = strtoupper("SELECT CodiTerr,FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
			"  ANY = '" .($anymesAnt) ."' AND mes = '" .($mesAnt) ."'");
			$rows = $this->conn->select($sql);                                                                      
			$num = count($rows);   
			if ($num>0) { 
				foreach($rows as $rowOBJ){  
					$row = ((array) $rowOBJ);   
					$ret[$row['CODITERR']] = $this->arreglar($row['FINPREU']);
				}
			}
			return $ret;
		}

		/**
		 * Funció priceCat: 
		 * @param - $code:
		 */
		public function priceCat($code) 
		{
			$elsPreus=$this->priceTerr($code);		
			if (is_array($elsPreus))
			{
				$elsPercentsTmp=$this->baseCalPercentageAllTerr($code,true);  //   valuePercentageTerr with high precision			
				// Array ( [B] => 39.2857142857 [G] => 19.6428571429 [L] => 26.7857142857 [T] => 14.2857142857 ) 
				$elsPercents = array();
				if (is_array($elsPercentsTmp))
				{
				 while (list ($TLletra, $TVal) = each ($elsPercentsTmp)) {
					$elsPercents[array_search($TLletra,$this->TerritorisLletra)] = $TVal;
				 }
				}			
				// Array ( [8] => 0 [17] => 0 [25] => 85.204123042 [43] => 14.795876958 ) 
				reset($elsPreus);
				reset($elsPercents);
				$preuCat=0;
				$totalPercent=0;
				while (list ($Tkey, $TVal) = each ($elsPercents)) {
					if (isset($elsPreus[$Tkey])){
						if ($elsPreus[$Tkey]>0) 
						{ $preuCat+=$elsPreus[$Tkey]*$elsPercents[$Tkey];
						  $totalPercent += $elsPercents[$Tkey];
						}	
					}						
				}
				if ($totalPercent>0) $preuCat=$preuCat/$totalPercent;
				else $preuCat=null;
				return $preuCat;
			}
			else { return null; }
		}
		
		/**
		* Funció priceCatMonthBefore: 
		* @param - $code:
		*/
		public function priceCatMonthBefore($code) {
			$mesAct = $this->month;
			$anyAct = $this->year;
			$mesAnt = $this->month-1;
			$anymesAnt = $this->year;
			if ($mesAnt<1) { $mesAnt=12; $anymesAnt -=1;}
			$this->month=$mesAnt;
			$this->year=$anymesAnt;
			
			$preuCatMesAnt = $this->priceCat($code);
			$this->month=$mesAct; // (¿?) és correcte?, actualitzar l'objecte preus?
			$this->year=$anyAct;
			return $preuCatMesAnt;
		}
		
		/**
		 * Funció priceCatYearBefore: 
		 * @param - $code:
		 */
		function priceCatYearBefore($code) {
			$mesAct = $this->month;
			$anyAct = $this->year;
			$anyAnt = $this->year-1;
				
			$this->year=$anyAnt;

			$preuCatMesAnt = $this->priceCat($code);
			 
			$this->year=$anyAct;
			return $preuCatMesAnt;

		}

		
		/**
		 * Funció baseCalAllTerr: returns array with values from base calendar for all territories. If the number of territories will change it will be ok
		 * @param - $code:
		 */
		public function baseCalAllTerr($code) {
			$ret = null;
			$sql = strtoupper("SELECT " .$this->baseTable .".MES" .$this->month ." AS mes, territori.NomTerr AS terrNom FROM "
				.$this->baseTable .", territori WHERE " .$this->baseTable .".CodiProd = '" .$code ."' " .
					"AND " .$this->baseTable .".CodiTerr IN (SELECT CodiTerr FROM territori) AND " .
					"territori.CodiTerr = " .$this->baseTable .".CodiTerr");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 

			if ($num>0) { 
				foreach($rows as $rowOBJ){  
					$row = ((array) $rowOBJ); 
					$ret[$row['TERRNOM']] = $row['MES'];
				}
			}
			return $ret;
		}
		
		/**
		 * Funció priceCatYearBefore: s-provided needed, s-provided not needed, x-not provided needed, o-not provided not needed, for example if the string is OO. means that only 2 prices were needed, the third one, marked with "." is more than maxpreus. we have to check somehow that max is >= than nec!!!!
		 * @param - $code:
		 */
		function estat($code,$territ=null) {
			
			if (is_null($territ)) $territ=$this->codiTerr;
			if (!$this->ifBaseZeroTerritori($code,$territ))
			{
				$sql = strtoupper("SELECT " .$this->preusTable .".Preu1, " .$this->preusTable .".Preu2, " .$this->preusTable .
						".Preu3, " .$this->descripcioTable .".NecPreus, " .$this->descripcioTable .".MaxPreus 
						, " .$this->descripcioTable .".Calcul 
						FROM " .
						"" .$this->preusTable .", " .$this->descripcioTable ." WHERE " .$this->preusTable .
						".CodiProd = " .$this->descripcioTable .".CodiProd AND " .$this->preusTable .".CodiProd = '" .
						"" .$code ."' AND " .$this->preusTable .".CodiTerr = '" .$territ ."' AND " .
						"" .$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
						"" .$this->month ."'");
				$rows = $this->conn->select($sql); 
				$num = count($rows); 
				$string = "";	
				if ($num>0) { 
					$ret = ((array)$rows[0]); 		
					// Array ( [PREU1] => 55 [PREU2] => 0 [PREU3] => 0 [NECPREUS] => 2 [MAXPREUS] => 2 [CALCUL] => S ) 
					if ($ret["NECPREUS"] > $ret["MAXPREUS"]) {
						echo 'Number of necessary prices (descripcio table->NecPreus) > max number' .
							 'of prices (descripcio table->MaxPreus), exiting function';
						exit;
					}

					for ($i = 0; $i<$ret["NECPREUS"]; $i++) {
						if (floatval($this->arreglar($ret["PREU" .($i+1)])) != 0) {
							$string = $string ."s";
						}
						else
						$string = $string ."n";
					}

					for ($i = $ret["NECPREUS"]; $i < $ret["MAXPREUS"]; $i++) {
						if ($ret["PREU" .($i+1)] != null) {
							$string = $string ."s";
						}
						else
						$string = $string ."0";
					}

					for ($i = $ret["MAXPREUS"]; $i<3; $i++) {
						$string = $string .".";
					}
					if ($string=='s..')  $string='.s.';
					// si el càlcul és "SUMA" .. un únic preu --> tots entrats
					if (strtoupper($ret["CALCUL"])=='S') 
					{ 				
					  $string=str_replace('n','s',$string);
					}
					return $string;
				}
				else
				{  	$sql = strtoupper("SELECT " .$this->descripcioTable .".NecPreus, " .$this->descripcioTable .".MaxPreus FROM " .
					$this->descripcioTable ." WHERE " .$this->descripcioTable .".CodiProd = " .$code."  ");
					$rows = $this->conn->select($sql); 
					$num = count($rows); 
					$ret = ((array)$rows[0]); 
					for ($i = 0; $i<$ret["NECPREUS"]; $i++) {  $string = $string ."n";    }
					for ($i = $ret["NECPREUS"]; $i < $ret["MAXPREUS"]; $i++) {  $string = $string ."0";    }
					return $string;
				}
			}
			else // no s'ha d'entrar preu
			{  return "-";
			}
		}
		
		/**
		 * Funció BaseZeroTerritoriMesCat
		 * @param - $code:
		 * @param - $month:
		 * @param - $baseTable:
		 * @param - $conn:	 
		 */	
		
		function BaseZeroTerritoriMesCat($code,$month,$baseTable,$conn) {
			$sql = strtoupper("SELECT sum(MES" .$month .") as total FROM " .$baseTable ." WHERE CodiProd = '" .$code ."' ");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num > 0) {
				$fila = ((array)$rows[0]); 	
				//  Array ( [SUM(MES1)] => 12321 ) 
				$ret = $fila['TOTAL'];
				if ($ret == 0)
				return true;
				else
				{
					return false;
				}
			}
			else
			return true;
		}


		public function ifBaseZeroTerritoriAnyMonth($code,$terr) {
			$retn = null;
			$sql = strtoupper("SELECT  (MES1+MES2+MES3+MES4+MES5+MES6+MES7+MES8+MES9+MES10+MES11+MES12) AS SUMA
					FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .
					"AND CodiTerr = '" .$terr ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				$fila = ((array) $rows[0]); 
				$ret = $fila['SUMA'];
				if ($ret == 0) {
					$retn=true;
				}
				else
				{
					$retn=false;
				}
			}
			return $retn;
		}
		
		/**
		 * Funció ifBaseZeroTerritori
		 * @param - $code:
		 * @param - $code:	 
		 */	
		public function ifBaseZeroTerritori($code,$terr,$month=0) {
			$retn = true;
			if ($month<1) $month=$this->month;
			$sql = strtoupper("SELECT MES" .$month ." AS mes FROM " .$this->baseTable ." WHERE CodiProd = '" .$code ."' " .
					"AND CodiTerr = '" .$terr ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				$fila = ((array) $rows[0]); 
				$ret = $fila['MES'];
				if ($ret == 0) {
					$retn=true;
				}
				else
				{
					$retn=false;
				}
			}
			return $retn;
		}
			
		
		/**
		 * Funció stateAllTerritories: 
		* @param - $codiProd
		* @param - $year
		* @param - $month
		* @param - $priceType
		 */
		public function stateAllTerritories($codiProd, $year, $month, $priceType) {
			$preusTable = 'preus' .strtolower($priceType);
			$descripcioTable = 'descripcio_preus' .strtolower($priceType);
			$sqlOK =  strtoupper("SELECT " .$preusTable .".CodiTerr,NecPreus,Preu1,Preu2,Preu3,Calcul FROM " .$descripcioTable .", ".$preusTable .
			" WHERE (".$preusTable .".CodiProd = '" .$codiProd ."' AND ".$preusTable .".ANY = '" .$year ."' AND " .$preusTable .".mes = '" .
			"" .$month ."' AND ".$descripcioTable .".CodiProd = " .$preusTable .".CodiProd) AND " .
			"((" .$descripcioTable .".NecPreus = '1' AND " .$preusTable .".Preu1 <> '0') OR 
			 (" .$descripcioTable .".NecPreus = '2' AND (" .$preusTable .".Preu1 <> '0' OR ".$preusTable .".Preu2 <> '0') ) OR 
			 (" .$descripcioTable.".NecPreus = '3' AND (".$preusTable.".Preu1 <> '0' OR ".$preusTable.".Preu2 <> '0' OR ".$preusTable.".Preu3 <> '0') ))");
			//if ($codiProd=='703114') echo $sqlOK."<br>";
			/*
			 // Mirem si hi ha algun preu entrat ... amb el retorn de la query, ja mirarem si estan TOTS entrats, 
				o solament alguns i, per tant, en falten.
			 SELECT preuspercebuts.CodiTerr
			 FROM descripcio_preuspercebuts, preuspercebuts
			 WHERE (preuspercebuts.CodiProd = '001100'
			 AND preuspercebuts.any = '2007'
			 AND preuspercebuts.mes = '1'
			 AND descripcio_preuspercebuts.CodiProd = preuspercebuts.CodiProd)
			 AND (   (descripcio_preuspercebuts.NecPreus = '1' AND preuspercebuts.Preu1 != '0')
			 OR (descripcio_preuspercebuts.NecPreus = '2' AND ( preuspercebuts.Preu1 != '0' OR preuspercebuts.Preu2 != '0'))
			 OR (descripcio_preuspercebuts.NecPreus = '3' AND ( preuspercebuts.Preu1 != '0' OR preuspercebuts.Preu2 != '0' OR preuspercebuts.Preu3 != '0') ) );

			 */
			//if ($codiProd=='001100') { echo "<br>Preus OK:".$sqlOK; echo "<br>";   }
			// Primer posem tots els territoris a "RES" == no s'ha d'entrar preus
			$ret=array();
			for ($i = 0; $i < count($this->TerritoriesCodes); $i++) {
				$ret[$this->TerritoriesCodes[$i]] = 'res';
			}
			// Seguim posant a notOK. Pels que s'ha d'entrar preu, suposem que no estan entrats .. i si detectem que sí que ho estant, els posarem a OK
			$sqlCalEntrar = strtoupper("SELECT CodiTerr FROM ".$this->baseTable."	WHERE (CodiProd = '" .$codiProd ."' AND  MES".$month." <>0) ");		
			$results = $this->conn->select($sqlCalEntrar); 
			$nr = count($results); 
			foreach($results as $rowOBJ){ 
				$row = ((array) $rowOBJ); 
				$ret[$row['CODITERR']] = 'notOK';
			}
			unset($result);
			// Seguim posant a OK els que s'han entrat tots els preus
			$results = $this->conn->select($sqlOK); 
			$nr = count($results);
			if ($nr>0) { 
				foreach($results as $rowOBJ){ 
					$row = ((array) $rowOBJ); 
					// Array ( [CODITERR] => 8 [NECPREUS] => 1 [PREU1] => 1 [PREU2] => 0 [PREU3] => 0 [CALCUL] => M ) 
					if ( (strtoupper($row['CALCUL'])=='S') and ($row['PREU1']==0 or $row['PREU2']==0 or $row['PREU3']==0) ) 
						   $ret[$row['CODITERR']] = 'OK';  // els que son suma .. n'hi ha prou amb un únic preu. 
					elseif (  (($row['NECPREUS']==3) and ($row['PREU1']==0 or $row['PREU2']==0 or $row['PREU3']==0)) or
					(($row['NECPREUS']==2) and ($row['PREU1']==0 or $row['PREU2']==0)) ) $ret[$row['CODITERR']] = 'quasiOK';
					else $ret[$row['CODITERR']] = 'OK';
				}
			}
			// Seguim posant a OKv els validats
			//  CodiProd  CodiTerr  Any  Mes  FinPreu  Preu1  Preu2  Preu3  DataPreu1  DataPreu2  DataPreu3  validat
			$sqlValidats = strtoupper("SELECT CodiTerr FROM ".$preusTable."
				  WHERE CodiProd = '" .$codiProd ."' AND  ANY=".$year." AND Mes=".$month."  AND Validat = 's'");
			//echo $sqlValidats."<br>";
			unset($result);
			$results = $this->conn->select($sqlValidats); 
			$nr = count($results);
			if ($nr>0) { 
				foreach($results as $rowOBJ){ 
					$row = ((array) $rowOBJ); 
					$ret[$row['CODITERR']] = 'OKv';
					//echo $codiProd."::".mysql_result($result, $i)."**";
				}
			}
			// Array ( [8] => res [17] => res [25] => OK [43] => quasiOK )
			return $ret;
		}


		/**
		 * Funciò get_ponderacio_mensual:
		 * @param - $codiprod
		 * @param - $mes
		 */
		function get_ponderacio_mensual($codiprod,$mes)
		{
			$sql = strtoupper("SELECT M".$mes." FROM PONDERACIO_MENSUAL".$this->base." WHERE CODIPROD = '".$codiprod."' ");
			$rows = $this->conn->select($sql);
			$num = count($rows);
			if ($num>0) {
				$fila = ((array) $rows[0]);
				if($fila) return $fila['M' . $mes];
				else return false;
			}
			return false;
		}

		/*
		* Funció get_rang_posicio_mes_conn: 
		* @param codiprod
		* @param mes
		* @param Conn
		*/
		function get_rang_posicio_mes_conn ($codiprod,$mes,$Conn,$base)
		{
			$sql = strtoupper("SELECT RM".$mes." AS POSICIOMES FROM RANG_PONDERACIO_MENSUAL".$base." WHERE CODI = '".$codiprod."'");
			$rows = $this->conn->select($sql);
			$num = count($rows);
			if ($num>0) {
				$row = ((array) $rows[0]);
				return $row['POSICIOMES'];
			}
			return false;
		}
		
		/**
		 * Funció get_rang_posicio_mes: 
		* @param - $codiprod
		* @param - $mes
		*/
		function get_rang_posicio_mes($codiprod,$mes)
		{
			
			return $this->get_rang_posicio_mes_conn($codiprod,$mes,$this->conn,$this->base);
		}
		
		/**
		* Funció get_rang_posicio_anual: 
		* @param - $codiprod
		* @param - $mes
		*/
		function get_rang_posicio_anual($codiprod,$mes)
		{
			return $this->get_rang_posicio_anual_conn($codiprod,$mes,$this->conn,$this->base);
		}
	
		/**
		* Funció get_rang_posicio_anual_conn: 
		* @param - $codiprod
		* @param - $mes
		*/
		function get_rang_posicio_anual_conn($codiprod,$mes,$Conn,$base)
		{
			$sql = strtoupper("SELECT POSICIO FROM RANG_PONDERACIO_ANUAL".$base." WHERE CODI = '".$codiprod."' AND MES = '".$mes."'");
			$rows = $this->conn->select($sql);
			$num = count($rows);
			if ($num>0) {
				$row = ((array) $rows[0]);
				return $row['POSICIO'];
			}
			return false;
		}
	
	
		/**
		 * Funció get_ponderacio_mensual:
		 * @param - $codiprod
		 * @param - $mes
		 */
		function get_ponderacio_anual($codiprod,$mes)
		{
			$sql = strtoupper("SELECT M".$mes." FROM PONDERACIO_ANUAL".$this->base." WHERE CODIPROD = '".$codiprod."' ");
			$rows = $this->conn->select($sql);
			$num = count($rows);
			if ($num>0) {
				$fila = ((array) $rows[0]);
				if($fila) return $fila['M' . $mes];
				else return false;
			}
			return false;

		}

		/**
		* Funció get_rang_quantitat_productes_mes:
		* @param - $mes	 
		*/	
		function get_rang_total_productes_mes($mes)
		{
			$sql = strtoupper("SELECT MAX(RM".$mes.") AS MAXIM FROM RANG_PONDERACIO_MENSUAL".$this->base);
			$rows = $this->conn->select($sql);
			$fila = ((array) $rows[0]);
			return intval($fila['MAXIM']);
		}
		
		
		/**
		 * Funció get_rang_quantitat_productes_anual: 
		*/
		function get_rang_quantitat_productes_anual()
		{
			$sql = strtoupper("SELECT COUNT(*) AS POSICIOANUAL FROM RANG_PONDERACIO_ANUAL".$this->base);
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			if ($num>0) { 
				$row = ((array)$rows[0]); 
				return $row['POSICIOANUAL'];
			}
			return false;		
		}
		
		/*
		* Funció BuscaGestor() 
		* 
		* @param $CodiProd
		*/
		function BuscaGestor($CodiProd) 
		{
			$gestor = array();
			$gestor_lletra = array();
			
			$sql = strtoupper("SELECT gestor FROM ".$this->descripcioTable." WHERE CodiProd ='".$CodiProd."' ");
			$rows = $this->conn->select($sql);
			$num = count($rows);
			if ($num>0) {
					$row = ((array) $rows[0]);						
					$gestor_lletra=explode(",",$row['GESTOR']);
					
					for ($i=0;$i<count($gestor_lletra);$i++)
						$gestor[$i] = array_search($gestor_lletra[$i],$this->TerritorisLletra);
			}
			return $gestor;
		}
		
		/**
		 * Funció enteredPrices
		 * @param - $code: 
		 */		
		function enteredPrices($code)
		{
			
			$sql = strtoupper("SELECT " .$this->preusTable .".Preu1, " .$this->preusTable .".Preu2, " .$this->preusTable .
				".Preu3, " .$this->preusTable .".qui, ".$this->descripcioTable .".NecPreus, " .$this->descripcioTable .".MaxPreus FROM " .
				"" .$this->preusTable .", " .$this->descripcioTable ." WHERE " .$this->preusTable .
				".CodiProd = " .$this->descripcioTable .".CodiProd AND " .$this->preusTable .".CodiProd = '" .
				"" .$code ."' AND " .$this->preusTable .".CodiTerr = '" .$this->codiTerr ."' AND " .
				"" .$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
				"" .$this->month ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			$ret = null;
			if ($num>0) { 
				$ret = $this->arreglar((array)$rows[0]);  // FetchRow()
			}
			else
			{
				$sql = strtoupper("SELECT NecPreus,MaxPreus FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."' ");
				$rows = $this->conn->select($sql);
				$num = count($rows); 
				if ($num>0) { 
					$ret = (array) $rows[0];
				}
				else {
				}
			}
			return $ret;
		}
		
		/**
		 * Funció enteredPricesTerritori
		 * @param - $code:
		 * @param - $territ: 	 
		 */
		
		function enteredPricesTerritori($code,$territ) {
			$sql = strtoupper("SELECT " .$this->preusTable .".Preu1, " .$this->preusTable .".Preu2, " .$this->preusTable .
					".Preu3, " .$this->descripcioTable .".NecPreus, " .$this->descripcioTable .".MaxPreus FROM " .
					"" .$this->preusTable .", " .$this->descripcioTable ." WHERE " .$this->preusTable .
					".CodiProd = " .$this->descripcioTable .".CodiProd AND " .$this->preusTable .".CodiProd = '" .
					"" .$code ."' AND " .$this->preusTable .".CodiTerr = '" .$territ ."' AND " .
					"" .$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
					"" .$this->month ."'");	

			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			$ret = null;
			if ($num>0) { 
				$ret = $this->arreglar((array)$rows[0]);  // FetchRow()
			}
			else
			{
				 
				$sql = strtoupper("SELECT NecPreus,MaxPreus FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");
				$rows = $this->conn->select($sql);
				$num = count($rows); 
				if ($num>0) { 
					$ret = (array) $rows[0];
				}
			}
			return $ret;
				
		}
		
		/*
		* Funció TerritsDonenPreuUnProdUnMes() 
		* @param $codiprod
		* @param $mes
		*/
		function TerritsDonenPreuUnProdUnMes($codiprod,$mes=0)
		{   if ($mes==0) $mes = $this->month;
			$sql = strtoupper("select codiprod,coditerr 
				   from " .$this->baseTable ."
				   where (mes".$mes." > 0)
					 and codiprod = '".$codiprod."'
				   order by codiprod,coditerr");
			$ret = null;	   
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				$ret = array();	
				foreach($rows as $rowOBJ){ 
					$row = ((array) $rowOBJ); 	
					if (!isset($ret[$row['CODIPROD']])) $ret[$row['CODIPROD']] = $row['CODITERR'];
					else $ret[$row['CODIPROD']] .= ','.$row['CODITERR'];
					
				}
			}
			// Array ( [001100] => 8,17,25,43 
			return $ret[$codiprod];
		}
	
		/**
	 * Funció priceTerritoriValidat: 
	 * @param - $code: 
	 * @param - $territ: 
	 */

		function priceTerritoriValidat($code,$territ)
		{  
			$sql =  strtoupper("SELECT (CASE WHEN validat='s' THEN 1 ELSE 0 END) AS PREGUNTA FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
			" CodiTerr = '" .$territ ."' AND ANY = '" .$this->year ."' AND mes = '" .$this->month ."'");
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				$fila = $this->arreglar((array)$rows[0]);  // FetchRow()
				$ret = $fila['PREGUNTA'];
				return ($ret==1);
			}
			else
				return false;
		}
	
		/**
		 * Funció methodPrices
		 * @param - $code: 
		 */
		function methodPrices($code) {
			$sql = strtoupper("SELECT Calcul FROM " .$this->descripcioTable ." WHERE CodiProd = '" .$code ."'");
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			if ($num>0) {
				$fila = $this->arreglar((array)$rows[0]);  // FetchRow()
				$ret = $fila['CALCUL'];
				return $ret;
			}
			else
			return null;
		}
		
			/**
		 * Funció PriceDateTime
		 * @param - $CodiProd: 
		 * @param - $territori: 
		 */
		function PriceDateTime($CodiProd,$territori)
		{  
			$sql = strtoupper("SELECT DataPreu1,HoraPreu1  FROM " .$this->preusTable ." "
				." WHERE ".$this->preusTable .".CodiProd = '".$CodiProd ."' AND "
				.$this->preusTable .".CodiTerr = '" .$territori ."' AND "
				.$this->preusTable .".ANY = '" .$this->year ."' AND "
				.$this->preusTable .".mes = '".$this->month ."'");
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			if ($num>0) {
				$ret = $this->arreglar((array)$rows[0]);  // FetchRow()
				// Array ( [DATAPREU1] => 2008-01-01 16:52:38 [HORAPREU1] => 2008-01-01 16:52:38 )
				/*
				 * 
				 * TODO: Revisar el format de les dates
				 * 
				 */
				if ($ret['DATAPREU1']=='0000-00-00') return array('','');
				if ($ret['DATAPREU1']=='') return array('','');
				$ladata = substr(trim($ret['DATAPREU1']),8,2)."/".$this->mesosAvr[intval(substr(trim($ret['DATAPREU1']),5,2))]."/".substr(trim($ret['DATAPREU1']),0,4);
				list($any, $mes, $dia,$hora, $minut, $segon) = sscanf($ret['HORAPREU1'], "%d-%d-%d %d:%d:%d");
				//echo $ret['HORAPREU1']."::";
				//echo $any."/".$mes."/".$dia." ".$hora.":".$minut.":".$segon."::";		
				$lahora = $hora.":".$minut.":".$segon;
				return array($ladata,$lahora);
			}
			else return null;
		}

		/*
		 * Funció price
		 * @param code
		 */
		function price($code,$territ=null) {
			if (is_null($territ)) $territ=$this->codiTerr;
			$sql = strtoupper("SELECT FinPreu FROM " .$this->preusTable ." WHERE CodiProd = '" .$code ."' AND" .
			" CodiTerr = '" .$territ."' AND ANY = '" .$this->year ."' AND mes = '" .$this->month ."'");	
			$rows = $this->conn->select($sql); 
			$num = count($rows); 
			if ($num>0) { 
				$fila = (array) $rows[0];
				return $this->arreglar($fila['FINPREU']);
			}
			else
			return null;
		}
	
		/*
		 * Funció setAgregacio
		 * @param code
		 * @param CalculsExcel
		 */
		function setAgregacio($code,$CalculsExcel)
		{   
			if (!is_object($CalculsExcel)) $CalculsExcel = new Excel($this->conn);
			$est =  $CalculsExcel->estructuraSortida('percebuts');
			$est = $est+$CalculsExcel->estructuraSortida('pagats');
			$est = $est+$CalculsExcel->estructuraSortida('salaris');	   
			return $est[$code];
		}
		
		/*
		 * Funció pricesAgrAll
		 * @param code
		 * @param arrSet
		 */
		function pricesAgrAll($code,$arrSet)
		{   
			
			$preusTots=array();
			$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
			$CalculsExcel 	= $excelfactory->creaExcel($this->conn,$this->pagPer,$this->base);
			// $arrSet: Array ( [0] => 054071 [1] => 054070 )
			
			if (!is_array($arrSet))
			{  // Busquem el conjunt que forma l'agrupació ...
			  
			}		
			// Inicialització de la suma de les Quantitats Físiques segona la Base
			$sumaqf=array();
			for ($m=1;$m<=12;$m++)
				{  $sumaqf[$m]=0;
				}
			 
			// Array de preus i quantitas físiques dels productes que composen l'agregació		
			// print_r($arrSet);
			// Array ( [0] => 702101 [1] => 702102 [2] => ..............702210 [22] => 702211 [23] => 702212 [24] => 702213 [25] => 702310 ) 
			while (list ( ,$uncodi) = each ($arrSet)) 
			{ 	  
			  $preusTots[$uncodi]['anual'] = $this->mitjanaAnual($uncodi);  // Tots els preus d'aquest producte.
			  
			  $limits_anys_preus=Prices::limits_anys_preus($this->pagPer,$this->conn,$this->base);
			  // Array ( [MAX] => 2007 [MIN] => 2000 [MMIN] => 1 [MMAX] => 10 )
			  $preusTots[$uncodi]['mensuals'] 
				 = $this->HistoricPrices2Arr($uncodi,$limits_anys_preus['MIN'],$limits_anys_preus['MAX'],1,12,$this->TerritoriesCodes,true);	
			  $descripcio='';				 
			  $preusTots[$uncodi]['qfisiques']['totals'] = $CalculsExcel->QFisiquesSumBasePercebuts($CalculsExcel->baseTable, $uncodi, $descripcio);
			  //print_r($preusTots[$uncodi]['qfisiques']['totals']); die();
			  $preusTots[$uncodi]['qfisiques']['territoris'] = $CalculsExcel->QFisiquesProd($CalculsExcel->baseTable, $uncodi);	  

			  /*
			  [021010] 
			  => Array ( 
				 [anual] => Array ( 
				   [2000] => Array ( [Cat] => 36.514134981 [8] => -- [17] => 36.5134714446 [25] => -- [43] => 36.5178616352 ) 
				   [2001] => Array ( [Cat] => 39.415256654 [8] => -- [17] => 40.3644232923 [25] => -- [43] => 34.0844025157 ) 
				   [2002] => Array ( [Cat] => 29.3386026616 [8] => -- [17] => 30.321343785 [25] => -- [43] => 23.8191823899 ) 
				   [2003] => Array ( [Cat] => 17.8402614068 [8] => -- [17] => 18.33 [25] => -- [43] => 15.0897169811 ) 
				   [2004] => Array ( [Cat] => 33.1117680608 [8] => -- [17] => 35 [25] => -- [43] => 22.5067924528 ) 
				   [2005] => Array ( [Cat] => 31.2762357414 [8] => -- [17] => 31.8595744681 [25] => -- [43] => 28 ) 
				   [2006] => Array ( [Cat] => 28.4505703422 [8] => -- [17] => 28.4501679731 [25] => -- [43] => 28.4528301887 ) ) 
				 [mensuals] => Array ( 
				   [0] => Array ( [0] => Any [1] => Mes [2] => Barcelona [3] => Girona [4] => Lleida [5] => Tarragona ) 
				   [1] => Array ( [0] => 2000 [1] => 1 [2] => [3] => [4] => [5] => ) 
				   [2] => Array ( [0] => 2000 [1] => 2 [2] => [3] => 45.16 [4] => [5] => 45.16 ) 
				   [3] => Array ( [0] => 2000 [1] => 3 [2] => [3] => 36.06 [4] => [5] => 36.06 ) 
				   [4] => Array ( [0] => 2000 [1] => 4 [2] => [3] => 36.06 [4] => [5] => 36.06 ) 
				   [5] => Array ( [0] => 2000 [1] => 5 [2] => [3] => 36.06 [4] => [5] => 36.06 ) 
				   [6] => Array ( [0] => 2000 [1] => 6 [2] => [3] => [4] => [5] => ) 
				   [7] => Array ( [0] => 2000 [1] => 7 [2] => [3] => [4] => [5] => ) 
				   [8] => Array ( [0] => 2000 [1] => 8 [2] => [3] => [4] => [5] => ) 
				   [9] => Array ( [0] => 2000 [1] => 9 [2] => [3] => [4] => [5] => ) 
				   [10] => Array ( [0] => 2000 [1] => 10 [2] => [3] => [4] => [5] => ) 
				   [11] => Array ( [0] => 2000 [1] => 11 [2] => [3] => [4] => [5] => ) 
				   [12] => Array ( [0] => 2000 [1] => 12 [2] => [3] => [4] => [5] => ) 
				   [13] => Array ( [0] => 2001 [1] => 1 [2] => [3] => [4] => [5] => ) 
				   [14] => Array ( [0] => 2001 [1] => 2 [2] => [3] => 42.07 [4] => [5] => 42.07 ) 
				   [15] => Array ( [0] => 2001 [1] => 3 [2] => [3] => 31.98 [4] => [5] => 28.7 )
					.
					.
					.  
				   
				 [qfisiques] 
					 => Array ( 
					   [totals] 
							=> Array ( [021010] => Array ( [SUM(MES1)] => 0 [SUM(MES2)] => 105 [SUM(MES3)] => 631 [SUM(MES4)] => 1052 [SUM(MES5)] => 316 [SUM(MES6)] => 0 [SUM(MES7)] => 0 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 0 [SUM(MES11)] => 0 [SUM(MES12)] => 0 [DESCRIPCIO] => [ANUAL] => 2104 ) ) 
					   [territoris] 
							=> Array ( [8] => Array ( [CODIPROD] => 021010 [CODITERR] => 8 [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => 0 ) 
									   [17] => Array ( [CODIPROD] => 021010 [CODITERR] => 17 [MES1] => 0 [MES2] => 89 [MES3] => 536 [MES4] => 893 [MES5] => 268 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => 1786 ) 
									   [25] => Array ( [CODIPROD] => 021010 [CODITERR] => 25 [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => 0 ) 
									   [43] => Array ( [CODIPROD] => 021010 [CODITERR] => 43 [MES1] => 0 [MES2] => 16 [MES3] => 95 [MES4] => 159 [MES5] => 48 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => 318 ) )
			   */
			}    

			// Càlculs
			$preusFinals = array();
			for ($any=$limits_anys_preus['MIN']; $any<=$limits_anys_preus['MAX']; $any++ )
			{
				$preusFinals[$any] = array();	
				$preusFinals[$any][99]=array(); // Cat
				for ($m=1;$m<=12;$m++)
				{
				  $sumaQF_x_preus[$any][99][$m]=0;
				  $sumaQF[$any][99][$m]=0;
				}
				$preuAnyCAT = 0;
				$totalQF_anyCAT = 0;
				reset($this->TerritoriesCodes);
				// Array ( [0] => 8 [1] => 17 [2] => 25 [3] => 43 )
				while (list(, $TCodi) = each($this->TerritoriesCodes)) 
				{  $TerrIx = intval(key($this->TerritoriesCodes));
				   if ($TerrIx == 0) $TerrIx = count($this->TerritoriesCodes);    // 1->2->3->4
				   $preusFinals[$any][$TCodi] = array();
				   $totalQF_any=0;
				   $preuAny = 0;
				   $preusFinals[$any][$TCodi][$any]=0; // Anual
				   for ($m=1;$m<=12;$m++)
				   {  $preusFinals[$any][$TCodi][$m]=0;
					  
					  reset($arrSet);
					  $preu = 0;
					  $totalQF=0;
					  if (is_array($arrSet))
					  {
						  while (list ( ,$uncodi) = each ($arrSet))  	
						  {  
							 if (isset($preusTots[$uncodi]['qfisiques']['territoris'][$TCodi])) {
								 $preu += $preusTots[$uncodi]['mensuals'][(($any-$limits_anys_preus['MIN'])*12+$m)][$TerrIx+1]*$preusTots[$uncodi]['qfisiques']['territoris'][$TCodi]['MES'.$m];  // preu * Qf
								 $totalQF += $preusTots[$uncodi]['qfisiques']['territoris'][$TCodi]['MES'.$m]; 
							 }
							 
							 //echo $uncodi.'-) Preu: '.$TerrIx.'::'.$TCodi.'::'.$any.'::'.$m.'::('.$preusTots[$uncodi]['mensuals'][$m][$TerrIx+1].')::['.$preusTots[$uncodi]['qfisiques']['territoris'][$TCodi]['MES'.$m].']::'.$preu.".<br>";
						  }				   	 
					  } 
					  if ($totalQF>0) $preuM = $preu/$totalQF;
					  else $preuM = 0;
					  //echo "------>[$TCodi]$any)($m) suma(Preu+QF)/TotalQF:".$preu.'/'.$totalQF.'='.$preuM.'<br>';
					  $preusFinals[$any][$TCodi][$m]=$preuM;
					  $preuAny += $preu;
					  $totalQF_any += $totalQF;
					  $sumaQF_x_preus[$any][99][$m]+=$preu;
					  $sumaQF[$any][99][$m]+=$totalQF;
					  $preuAnyCAT += $preu;
					  $totalQF_anyCAT += $totalQF;
					  //echo $any.'::'.$m.'::'.$TCodi.'::'.$preusFinals[$any][$TCodi][$m].'<br>';
					  
				   }
				   // hem acabat els mesos .. ara toc el preu anual			   
				   if ($totalQF_any>0) $preusFinals[$any][$TCodi][$any] = $preuAny/$totalQF_any;
				   else $preusFinals[$any][$TCodi][$any] = 0;
				   //echo $any.'//'.$TCodi.'//'.$preusFinals[$any][$TCodi][$any]."**";
				}
				// Hem acabat els territoris, ara fem el CAT
				for ($m=1;$m<=12;$m++)
				  {
					 if ($sumaQF[$any][99][$m]!=0) $preusFinals[$any][99][$m]=$sumaQF_x_preus[$any][99][$m]/$sumaQF[$any][99][$m];
					 else $preusFinals[$any][99][$m]=0;
					 //echo $m.'**'.$preusFinals[$any][99][$m]."<br>";
					 //echo "------>[CAT]($any)($m) suma(Preu+QF)/TotalQF:".$sumaQF_x_preus[$any][99][$m].'/'.$sumaQF[$any][99][$m].'='.$preusFinals[$any][99][$m].'<hr>';
				  }
				// Anual i CAT
				if ($totalQF_anyCAT>0) $preusFinals[$any][99][$any] = $preuAnyCAT/$totalQF_anyCAT;
				else $preusFinals[$any][99][$any] = 0; 
				
			}
			/*
			 Array ( 
			  [2000] => Array ( 
			  [8]  => Array ( [1] => 14.58 [2] => 10.75 [3] => 0 [4] => 0 [5] => 15.86 [6] => 18.977986459 [7] => 19.740869897 [8] => 20.97 [9] => 18.35 [10] => 18.7543354839 [11] => 16.85 [12] => 15.86 ) 
			  [17] => Array ( [1] => 0 [2] => 34.6093989071 [3] => 33.0743809524 [4] => 33.9447619048 [5] => 31.4007747748 [6] => 24.04 [7] => 22.1022961609 [8] => 20.31 [9] => 21.04 [10] => 21.03 [11] => 35.2 [12] => 36.06 ) 
			  [25] => Array ( [1] => 11.2302254545 [2] => 4.74 [3] => 0 [4] => 0 [5] => 0 [6] => 17.7 [7] => 19.47 [8] => 19.68 [9] => 18.81 [10] => 28.1020765832 [11] => 24.1358924636 [12] => 20.8835236004 ) [43] => Array ( [1] => 0 [2] => 45.16 [3] => 36.06 [4] => 36.06 [5] => 21.7907220387 [6] => 16.9647385957 [7] => 17.61 [8] => 17.8 [9] => 0 [10] => 0 [11] => 27.58 [12] => 25.96 ) ) [2001] => Array ( [8] => Array ( [1] => 14.58 [2] => 10.75 [3] => 0 [4] => 0 [5] => 15.86 [6] => 18.977986459 [7] => 19.740869897 [8] => 20.97 [9] => 18.35 [10] => 18.7543354839 [11] => 16.85 [12] => 15.86 ) [17] => Array ( [1] => 0 [2] => 34.6093989071 [3] => 33.0743809524 [4] => 33.9447619048 [5] => 31.4007747748 [6] => 24.04 [7] => 22.1022961609 [8] => 20.31 [9] => 21.04 [10] => 21.03 [11] => 35.2 [12] => 36.06 ) [25] => Array ( [1] => 11.2302254545 [2] => 4.74 [3] => 0 [4] => 0 [5] => 0 [6] => 17.7 [7] => 19.47 [8] => 19.68 [9] => 18.81 [10] => 28.1020765832 [11] => 24.1358924636 [12] => 20.8835236004 ) 
			  [43] => Array ( [1] => 0 [2] => 45.16 [3] => 36.06 [4] => 36.06 [5] => 21.7907220387 ...........................
			 */
			
			return $preusFinals;   
				
		}
		
		/*
		* Funció esBestiarPerVida: 
		* @param code
		*/
		function esBestiarPerVida($code)
		{
			return (preg_match('/^131/', $code,$matches));
		}
		
		/**
		* Funció HistoricPrices2Arr: 
		* @param - $code:
		* @param - $fromYear=2001: Any d'inici de les dades històriques.
		* @param - $toYear=2006: Any de finalització de les dades històriques.
		* @param - $firstMonth=1: Mes d'inici de les dades històriques.
		* @param - $lastMonth=12: Mes de finalització de les dades històriques.
		* @param - $territVeure: Territoris que es volen tenir dades.
		* @param - $dadesmes=0: Indica SI/NO es volen les dades parcials de cada mes. 
		* @param - $preusAnuals: Preus anuals, per si l'opció `AnualVeure` està activa.	
		* @param - $AnualVeure=0: Indica SI/NO es volen les dades anuals (mitjanes ponderades)  de cada mes
		 */
		
		function HistoricPrices2Arr($code,$fromYear=2001,$toYear=2006,$firstMonth=1,$lastMonth=12,$territVeure=null,$dadesmes=0,$preusAnuals=null,$AnualVeure=0)
		{   
			if (!is_array($territVeure)) $territVeure=$this->TerritoriesCodes;		
			$arrayPrices = $this->HistoricPrices($code); 	
			
			/*
			 Array ( [8] => Array ( 	[2000] => Array ( [1] => 151.76 [2] => 134.33 [7] => 138.23 [8] => 138.23 [9] => 148.63 [10] => 135.11 [11] => 135.11 [12] => 142.32 )
			 [2001] => Array ( [1] => 142.98 [2] => 145.14 [7] => 136.67 [8] => 133.67 [9] => 134.33 [10] => 134.33 [11] => 139.74 [12] => 148.03 )
			 [2002] => Array ( [1] => 147.67 [2] => 149.92 [7] => 119.56 [8] => 121.52 [9] => 133.85 [10] => 126.56 [11] => 127.4 [12] => 127.21 )
			 [2003] => Array ( [1] => 122.06 [2] => 125.25 [7] => 123.31 [8] => 126.96 [9] => 124.24 [10] => 129.6 [11] => 134.03 [12] => 134.6 )
			 [2004] => Array ( [1] => 126.15 [2] => 125.21 [7] => 107.24 [8] => 107.03 [9] => 104.97 [10] => 109.26 [11] => 106 [12] => 107.08 )
			 [2005] => Array ( [1] => 109.35 [2] => 111.07 [7] => 122.09 [8] => 123.15 [9] => 121.48 [10] => 121.48 [11] => 124.17 [12] => 123.91 )
			 [2006] => Array ( [1] => 122.32 [2] => 123.66 [7] => 117.34 [8] => 130.8 [9] => 130.8 [10] => 136.04 [11] => 124.29 [12] => 120.46 )
			 [2007] => Array ( [1] => 125.333333333 ) )
			 [17] => Array ( [2000] => Array ( [1] => 42.67 [2] => 41.74 [3] => 38.99 [4] => 90.15 [11] => 210.35 [12] => 221.58 )
			 [2001] => Array ( [1] => 51.51 [2] => 52.65 [3] => 50.41 [4] => 108.87 [11] => 180.3 [12] => 191 )
			 [2002] => Array ( [1] => 180 [2] => 180 [3] => 180 [4] => 180 [11] => 147 [12] => 147 )
			 [2003] => Array ( [1] => 147 [2] => 151 [3] => 151 [4] => 155 [11] => 154 [12] => 154 )
			 [2004] => Array ( [1] => 154 [2] => 154 [3] => 154 [4] => 154 [11] => 125 [12] => 125 )
			 [2005] => Array ( [1] => 125 [2] => 126 [3] => 124 [11] => 131.25 [12] => 133.8 )
			 [2006] => Array ( [1] => 135 [2] => 135 [3] => 139 [11] => 145.7 [12] => 147.8 )
			 [2007] => Array ( [1] => 134 ) )
			 [43] => Array ( [2000] => Array ( [1] => 276.47 [2] => 270.46 [12] => 270.46 ) ..................
			 */
			$arrayTerritoriesNoms = $this->TerritorisNoms;
			$arrayTerritoriesNoms[99] = "Catalunya";

			$ht = array();
			if(is_array($arrayPrices))
			{
				$ht[0] = array("Any","Mes");
				// Years' heather
				foreach($territVeure as $tcode)
				{
					array_push($ht[0],$arrayTerritoriesNoms[$tcode]);
				}
				// Array ( [0] => Array ( [0] => Any [1] => Mes [2] => Barcelona [3] => Girona [4] => Lleida [5] => Tarragona [6] => Catalunya ) )
				$acabat = false;
				$any = $fromYear;
				$mes = $firstMonth;
				$ix=0;
				while (!$acabat)
				{   
					$ix++;
					if ($dadesmes==1) $ht[$ix] = array($any,$mes);

					// Years' heather
					reset($territVeure);
					foreach($territVeure as $tcode)
					{
						if ($tcode!=99)
						{   if (isset($arrayPrices[$tcode][$any][$mes])) $elvalor = $arrayPrices[$tcode][$any][$mes];
							else { $elvalor=$this->ValorxBuit; }
						if ($elvalor=='') { $elvalor=$this->ValorxBuit;  }
						}
						else  // Catalunya
						{   $preuCat=0;
						$elsPercents = $this->baseCalPercentageAllTerrMonth($code,$mes,true);					
						if (is_array($elsPercents))
						{  // Array ( [B] => 25 [G] => 25 [L] => 25 [T] => 25 )
							reset($elsPercents);
							while (list ($Tkey, $TVal) = each ($elsPercents))
							{  $TCodi = array_search($Tkey,$this->TerritorisLletra);
								if (isset($arrayPrices[$TCodi][$any][$mes])) { $preuCat+=($arrayPrices[$TCodi][$any][$mes])*$elsPercents[$Tkey]/100; }
							}
							$elvalor=$preuCat;
							//$elvalor= number_format($elvalor,2, '.', '');

							if ($elvalor==0) { $elvalor=$this->ValorxBuit; }
						}
						else { $elvalor=$this->ValorxBuit; }
						}
						if ($dadesmes==1)
						{ array_push($ht[$ix],$elvalor);
							
						}
					}
					$mes++;
					if ($mes>12)
					{ if (strtolower($AnualVeure))
						{ 	$ix++;
							$ht[$ix] = array($any,'Anual');
							reset($territVeure);
							foreach($territVeure as $tcode)
							{ if ($tcode==99) $tcode='Cat';
								//$elvalor = number_format($preusAnuals[$any][$tcode],2, '.', '');
								$elvalor = $preusAnuals[$any][$tcode];
								if ($elvalor==0) $elvalor = null;
								array_push($ht[$ix],$elvalor);
							}
						}
						$any++;
						$mes=1;
					}
					if (  ($any>$toYear) or (($any==$toYear) and ($mes>$lastMonth)) )  { $acabat=true; }
						
				}
			}
			
			return $ht;
		}

		
		/**
		 * Funció HistoricPrices2ArrCat
		 * @param - $code: Codi del producte.
		 * @param - $fromYear: Any d'inici de les dades històriques.
		 * @param - $toYear: Any de finalització de les dades històriques.
		 * @param - $firstMonth: Mes d'inici de les dades històriques.
		 * @param - $lastMonth: Mes de finalització de les dades històriques.
		 * @param - $territVeure: Territoris que es volen tenir dades.
		 * @param - $dadesmes: Indica SI/NO es volen les dades parcials de cada mes.
		 * @param - $preusAnuals: Preus anuals, per si l'opció `AnualVeure` està activa.
		 * @param - $AnualVeure:: Indica SI/NO es volen les dades anuals (mitjanes ponderades)  de cada mes.
		 * @return - $Array amb els preus demanats, en format HTML.
		 */
		
		function HistoricPrices2ArrCat($code,$fromYear=2001,$toYear=2006,$firstMonth=1,$lastMonth=12)
		{  $arr=$this->HistoricPrices2Arr($code,$fromYear,$toYear,$firstMonth,$lastMonth,array(99),1,null,0);
		   
		   return $arr;
		}

	
		/**
		 * Funció HistoricPrices_ambFiltre: 
		 * @param - $code: 
		 * @param - $filtre: 
		 */
		function HistoricPrices_ambFiltre($code,$filtre) 
		{       /*
						   $filtre :  
							Array
								   (  [1] => Array
										(   [B] => 53.39
											[L] => 0
											[T] => 46.6
										)
									[2] => Array
										(   [B] => 32.01
											[L] => 40.04
											[T] => 27.94
										)
						  */
						  
				$sql = strtoupper("SELECT FINPREU,CODITERR ,".strtoupper($this->preusTable) .".ANY,MES
						   FROM " .strtoupper($this->preusTable) ." WHERE CODIPROD = '" .$code ."' ORDER BY CODITERR, ".strtoupper($this->preusTable) .".ANY, MES");	   
				$rows = $this->conn->select($sql);
			    $num = count($rows); 
				$ret = array();
				if ($num>0) {
					foreach($rows as $rowOBJ){ 				
						$row = ((array) $rowOBJ); 		
						if (isset($this->TerritorisLletra[$row['CODITERR']])){
							if (isset($filtre[$row['MES']][$this->TerritorisLletra[$row['CODITERR']]])) {      
								if ($filtre[$row['MES']][$this->TerritorisLletra[$row['CODITERR']]]>0) {
									$ret[$row['CODITERR']][$row['ANY']][$row['MES']] = $this->arreglar($row['FINPREU']);
								}
							}
						}
					}
				}
				return $ret;
		}
		
		/**
		 * Funció HistoricPrices: 
		 * @param - $code: 
		 */
	 
		function HistoricPrices($code) 
		{   
			$sql = strtoupper("SELECT FINPREU,CODITERR ,".strtoupper($this->preusTable) .".ANY,MES
					   FROM " .strtoupper($this->preusTable) ." WHERE CODIPROD = '" .$code ."' ORDER BY CODITERR, ".strtoupper($this->preusTable) .".ANY, MES");
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			$ret = array();
			if ($num>0) {
				foreach($rows as $rowOBJ){ 
					$row = ((array) $rowOBJ); 				
					$ret[$row['CODITERR']][$row['ANY']][$row['MES']] = $this->arreglar($row['FINPREU']);
				}
			}
			return $ret;
		}
	
		/**
		* Funció mitjanaAnual: 
		* @param - $codi
		* @param - $yeari
		* @param - $yearf
		 */
		 function mitjanaAnual($codiProd, $yeari=0, $yearf=0) {
			$AnualPreu=0;
			// (Suma de QFísiques*preu)/Qfísiques
			for ($m=1;$m<=12; $m++)
			{   $qfisiques[$m]=$this->baseCalAllTerrAny($codiProd,$m);
				
				$qfisiquesCAT[$m]=0;
				if (is_array($qfisiques[$m]))
				{
					while (list ($Tkey, $Qf) = each ($qfisiques[$m]))
					$qfisiquesCAT[$m] += $Qf;
				}
			}
			/*
			 $qfisiques
			 Array ( [1] => Array ( [Barcelona] => 39 [Girona] => 15 [Lleida] => 0 [Tarragona] => 11 )
			 [2] => Array ( [Barcelona] => 39 [Girona] => 10 [Lleida] => 0 [Tarragona] => 11 )
			 [3] => Array ( [Barcelona] => 0 [Girona] => 5 [Lleida] => 0 [Tarragona] => 0 )
			 [4] => Array ( [Barcelona] => 0 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [5] => Array ( [Barcelona] => 0 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [6] => Array ( [Barcelona] => 0 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [7] => Array ( [Barcelona] => 58 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [8] => Array ( [Barcelona] => 78 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [9] => Array ( [Barcelona] => 58 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [10] => Array ( [Barcelona] => 39 [Girona] => 0 [Lleida] => 0 [Tarragona] => 0 )
			 [11] => Array ( [Barcelona] => 39 [Girona] => 40 [Lleida] => 0 [Tarragona] => 0 )
			 [12] => Array ( [Barcelona] => 39 [Girona] => 30 [Lleida] => 0 [Tarragona] => 14 ) )

			 $qfisiquesCAT
			 Array ( [1] => 99.99 [2] => 99.99 [3] => 100 [4] => 0 [5] => 0 [6] => 0 [7] => 100 [8] => 100 [9] => 100 [10] => 100 [11] => 99.99 [12] => 99.98 )
			 */
			$elsPreusHist = $this->HistoricPrices($codiProd);
			/*
			 Array ( [8] => Array (      [2000] => Array ( [1] => 151.76 [2] => 134.33 [7] => 138.23 [8] => 138.23 [9] => 148.63 [10] => 135.11 [11] => 135.11 [12] => 142.32 )
			 [2001] => Array ( [1] => 142.98 [2] => 145.14 [7] => 136.67 [8] => 133.67 [9] => 134.33 [10] => 134.33 [11] => 139.74 [12] => 148.03 )
			 [2002] => Array ( [1] => 147.67 [2] => 149.92 [7] => 119.56 [8] => 121.52 [9] => 133.85 [10] => 126.56 [11] => 127.4 [12] => 127.21 )
			 [2003] => Array ( [1] => 122.06 [2] => 125.25 [7] => 123.31 [8] => 126.96 [9] => 124.24 [10] => 129.6 [11] => 134.03 [12] => 134.6 )
			 ......
			 ..............
			 */
			$AnualPreu=array();
			
			reset($this->TerritoriesCodes);	
			
			$limits_anys_preus = \App\Library\Prices::limits_anys_preus($this->pagPer,$this->conn,$this->base);		
			// Array ( [max] => 2007 [min] => 2000 [mmin] => 1 [mmax] => 6 )
			if ($yeari==0) $yeari=$limits_anys_preus['MIN'];
			if ($yearf==0) $yearf=$limits_anys_preus['MAX'];
			if ($yearf<$limits_anys_preus['MAX']) $yearff = $yearf;
			elseif ($limits_anys_preus['MMAX']==12) $yearff = $yearf;
			else $yearff = $yearf-1; // l'últim any no està acabat -> no hi ha preu anual
			for ($y=$yeari; $y<=$yearff; $y++)
			{   $AnualPreu[$y] = array();
				$AnualPreu[$y]['Cat'] = 0.0;			
				reset($this->TerritoriesCodes);
				while (list (,$Tkey) = each($this->TerritoriesCodes))
				{   
					$AnualPreu[$y][$Tkey] = 0.0;
					$SumaQf[$Tkey] = 0.0;
					for ($m=1;$m<=12; $m++)
					{  
					   if (!isset($elsPreusHist[$Tkey][$y][$m])) { $elsPreusHist[$Tkey][$y][$m]=0;  } // (Albert 2017-07-11) 
					   if (isset($qfisiques[$m][$this->TerritorisNoms[$Tkey]])) { 
								$AnualPreu[$y][$Tkey] += $elsPreusHist[$Tkey][$y][$m]*$qfisiques[$m][$this->TerritorisNoms[$Tkey]]; 
								$SumaQf[$Tkey] +=  $qfisiques[$m][$this->TerritorisNoms[$Tkey]];
						}
					}
					if ($SumaQf[$Tkey]>0) $AnualPreu[$y][$Tkey] = $AnualPreu[$y][$Tkey]/$SumaQf[$Tkey];
					else $AnualPreu[$y][$Tkey] = '--';
				}
				/*  AnualPreu
				 Array ( [2005] => Array ( [8] => 119.98402324 [17] => 127.433242826 [25] => -- [43] => 205 )
				 [2006] => Array ( [8] => 126.612265748 [17] => 141.200145695 [25] => -- [43] => 205 ) )
				 */
				$sumaQfAny = 0.0;
				$AnualPreuCatAny = 0.0;
				for ($m=1;$m<=12; $m++)
				{  	$AnualPreuCat[$m] = 0.0;
					$sumaQf[$m]=0.0;
					reset($this->TerritoriesCodes);
					while (list (,$Tkey) = each($this->TerritoriesCodes))
					{  
						if (isset($qfisiques[$m][$this->TerritorisNoms[$Tkey]])) {  
								$AnualPreuCat[$m] += $elsPreusHist[$Tkey][$y][$m]*$qfisiques[$m][$this->TerritorisNoms[$Tkey]];
								$sumaQf[$m] += $qfisiques[$m][$this->TerritorisNoms[$Tkey]];
						}
					}
					if ($sumaQf[$m]>0) $AnualPreuCat[$m] = $AnualPreuCat[$m] / $sumaQf[$m];
					$AnualPreuCatAny +=  $AnualPreuCat[$m]*$sumaQf[$m];
					$sumaQfAny += $sumaQf[$m];
				}
				if ($sumaQfAny)  $AnualPreuCatAny = $AnualPreuCatAny / $sumaQfAny;
				else $AnualPreuCatAny = '--';
				/*
				 :: AnualPreuCat ::
				 Array ( [1] => 139.238153846 [2] => 140.462333333 [3] => 139 [4] => 0 [5] => 0 [6] => 0 [7] => 117.34 [8] => 130.8 [9] => 130.8 [10] => 136.04 [11] => 135.130506329 [12] => 144.601686747 )
		
				 :: AnualPreuCatAny ::
				 */
				$AnualPreu[$y]['Cat'] = $AnualPreuCatAny;
			}
			// Array ( [2005] => Array ( [Cat] => 127.887580952 [8] => 120.15933162 [17] => 130.19 [25] => -- [43] => 205 )
			//
			
			return $AnualPreu;
		}
		
		/**
		* Funció CoefVariacio: 
		* @param - $codi
		* @param - $year
		* @param - $month
		* @param - $priceType
		 */
		
		function CoefVariacio($codi, $year, $month, $priceType)
		{           
			
			$EstatCat=$this->stateAllTerritories($codi, $year, $month, $priceType);
			//if ($codi=='221000') print_r($EstatCat);
			// Array ( [8] => res [17] => notOK [25] => OK [43]
			$quantTerr=0;
			$sumaPreus=0;
			while (list($terr,$esstatt)=each($EstatCat))
			{  if (($esstatt!='res') and ($esstatt!='notOK'))
				{   $quantTerr++;
					$sumaPreus+=($this->priceTerritoriPeriode($codi,$terr,$month,$year));
							//echo $this->priceTerritoriPeriode($codi,$terr,$month,$year).'::';
				}
			}
			//if ($codi=='221000') echo $quantTerr;
			if ($quantTerr>0) $mitjPreus=$sumaPreus/$quantTerr;
			else 
			{ 
			   $mitjPreus=0;
			   // no hi ha cap Territori amb preu
			   // hi ha algun territori que dóna preu?
			   $recompte = array_count_values($EstatCat);
			   // Array ( [notOK] => 2 [res] => 2 ) 
			   if (!isset($recompte['res'])) { return '?'; }
			   else {
				   if ($recompte['res']==count($this->TerritorisLletra)) return '';
					else return '?';  // no hi ha cap preu, però hi ha algun territori que ha de donar preu.	
			   }
			   
			}			
			$SumaQuadDiffs=0;
			reset($EstatCat);
			while (list($terr,$esstatt)=each($EstatCat))
			{  if (($esstatt!='res') and ($esstatt!='notOK'))
				{
					$diff = ($this->priceTerritoriPeriode($codi,$terr,$month,$year))-$mitjPreus;				
					$SumaQuadDiffs += ($diff*$diff);
			
				}
			}	    
			if ($quantTerr>0) $SumaQuadDiffsMitj = $SumaQuadDiffs/$quantTerr;
			else $SumaQuadDiffsMitj =0;
			$DesvEst  = sqrt ($SumaQuadDiffsMitj);
			if ($mitjPreus>0)
				{	$cv=($DesvEst/$mitjPreus)*100;			    
					//$cv=number_format($cv*100, 2, ',', '').'%';
				}
			else $cv='';
			//if ($quantTerr==0) $cv='?'; 
			
			return $cv;
		}
		/**
		 * Funció baseCalAllTerrAny
		 * @param - $code:
		 * @param - $mes:
		 */
		function baseCalAllTerrAny($code,$mes) {
			$sql = strtoupper("SELECT " .$this->baseTable .".MES" .$mes ." AS mes, territori.NomTerr AS terrNom FROM "
			.$this->baseTable .", territori WHERE " .$this->baseTable .".CodiProd = '" .$code ."' " .
					"AND " .$this->baseTable .".CodiTerr IN (SELECT CodiTerr FROM territori) AND " .
					"territori.CodiTerr = " .$this->baseTable .".CodiTerr");
			$rows = $this->conn->select($sql);
			$num = count($rows);       
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$row = ((array) $rowOBJ); 
					$ret[$row['TERRNOM']] = $row['MES'];
				}
				return $ret;
			}
			else
			return null;
		}
	
	
		/**
		 * Funció HistoricPrices3ToHTML
		 * @param - $code: Codi del producte.
		 * @param - $fromYear: Any d'inici de les dades històriques.
		 * @param - $toYear: Any de finalització de les dades històriques.
		 * @param - $firstMonth: Mes d'inici de les dades històriques.
		 * @param - $lastMonth: Mes de finalització de les dades històriques.
		 * @param - $territVeure: Territoris que es volen tenir dades.
		 * @param - $dadesmes: Indica SI/NO es volen les dades parcials de cada mes.
		 * @param - $preusAnuals: Preus anuals, per si l'opció `AnualVeure` està activa.
		 * @param - $AnualVeure:: Indica SI/NO es volen les dades anuals (mitjanes ponderades)  de cada mes.
		 * @return - $Array amb els preus demanats, en format HTML.
		 */
		function HistoricPrices3ToHTML($code,$fromYear=2001,$toYear=2006,$firstMonth=1,$lastMonth=12,$territVeure,$dadesmes=0,$preusAnuals,$AnualVeure=0)
		{   // $preusAnuals		
			$arrayPrices = $this->HistoricPrices($code);
			
			/*
			 Array ( [8] => Array ( 	[2000] => Array ( [1] => 151.76 [2] => 134.33 [7] => 138.23 [8] => 138.23 [9] => 148.63 [10] => 135.11 [11] => 135.11 [12] => 142.32 )
			 [2001] => Array ( [1] => 142.98 [2] => 145.14 [7] => 136.67 [8] => 133.67 [9] => 134.33 [10] => 134.33 [11] => 139.74 [12] => 148.03 )
			 [2002] => Array ( [1] => 147.67 [2] => 149.92 [7] => 119.56 [8] => 121.52 [9] => 133.85 [10] => 126.56 [11] => 127.4 [12] => 127.21 )
			 [2003] => Array ( [1] => 122.06 [2] => 125.25 [7] => 123.31 [8] => 126.96 [9] => 124.24 [10] => 129.6 [11] => 134.03 [12] => 134.6 )
			 [2004] => Array ( [1] => 126.15 [2] => 125.21 [7] => 107.24 [8] => 107.03 [9] => 104.97 [10] => 109.26 [11] => 106 [12] => 107.08 )
			 [2005] => Array ( [1] => 109.35 [2] => 111.07 [7] => 122.09 [8] => 123.15 [9] => 121.48 [10] => 121.48 [11] => 124.17 [12] => 123.91 )
			 [2006] => Array ( [1] => 122.32 [2] => 123.66 [7] => 117.34 [8] => 130.8 [9] => 130.8 [10] => 136.04 [11] => 124.29 [12] => 120.46 )
			 [2007] => Array ( [1] => 125.333333333 ) )
			 [17] => Array ( [2000] => Array ( [1] => 42.67 [2] => 41.74 [3] => 38.99 [4] => 90.15 [11] => 210.35 [12] => 221.58 )
			 [2001] => Array ( [1] => 51.51 [2] => 52.65 [3] => 50.41 [4] => 108.87 [11] => 180.3 [12] => 191 )
			 [2002] => Array ( [1] => 180 [2] => 180 [3] => 180 [4] => 180 [11] => 147 [12] => 147 )
			 [2003] => Array ( [1] => 147 [2] => 151 [3] => 151 [4] => 155 [11] => 154 [12] => 154 )
			 [2004] => Array ( [1] => 154 [2] => 154 [3] => 154 [4] => 154 [11] => 125 [12] => 125 )
			 [2005] => Array ( [1] => 125 [2] => 126 [3] => 124 [11] => 131.25 [12] => 133.8 )
			 [2006] => Array ( [1] => 135 [2] => 135 [3] => 139 [11] => 145.7 [12] => 147.8 )
			 [2007] => Array ( [1] => 134 ) )
			 [43] => Array ( [2000] => Array ( [1] => 276.47 [2] => 270.46 [12] => 270.46 ) ..................
			 */
			$arrayTerritoriesNoms = $this->TerritorisNoms;
			$arrayTerritoriesNoms[99] = "Catalunya";

			/*
			 Estructura per cada fila ...................................
			 <div class="h_taula_004">2005 </div>
			 <div class="h_taula_005">01 </div>
			 <div class="h_taula_006">125 </div>
			 <div class="h_taula_006">200 </div>
			 <div class="h_taula_006">-- </div>
			 <div class="h_taula_006">134 </div>
			 <div class="h_taula_006">-- </div>
			 <div class="h_espai_01"> </div>
			 <div class="h_espai_01"> </div>

			 */
			$ht = "";
			if(is_array($arrayPrices))
			{    
				$ht = "";
				// Years' heather
				
				$acabat = false;
				$any = $fromYear;
				$mes = $firstMonth;
				$infoPreus = array();
				$infoPreus['valors'] = array();
				$infoPreus['territorisCodis'] = $territVeure;
				$infoPreus['territorisNoms'] = array();
				foreach($territVeure as $tcode) {
					$infoPreus['territorisNoms'][] = $this->TerritorisNomsTots[$tcode];
				}
				while (!$acabat)
				{   
					$filaInfoPreus['any']=$any;
					$filaInfoPreus['mes']=$mes;
					if ($dadesmes==1)
					{ $ht .= "\r\n";
						$ht .= "<div class='h_taula_004'>".$any." </div>\r\n";
						$ht .= "<div class='h_taula_005'>".$mes." </div>\r\n";
					}
					// Years' heather
					reset($territVeure);			
					$filaInfoPreus['PreusTerritoris']=array();
					foreach($territVeure as $tcode)
					{   $elsPercents = $this->baseCalPercentageAllTerrMonth($code,$mes,true);
						// Array ( [B] => 63.1875881523 [L] => 0 [T] => 36.8124118477 ) 
						if ($tcode!=99)
						{   if (!is_array($elsPercents)) $elvalor=' -- ';
							else
							 if (array_key_exists($this->TerritorisLletra[$tcode],$elsPercents))
							 {
							   if (!isset($arrayPrices[$tcode][$any])) {  $arrayPrices[$tcode][$any] = array(); }  
							   if (!isset($arrayPrices[$tcode][$any][$mes])) {  $arrayPrices[$tcode][$any][$mes] = 0; }
							   $elvalor = $arrayPrices[$tcode][$any][$mes];
							   if ($elvalor=='') {$elvalor=' -- ';  }
							   else $elvalor= number_format($elvalor, 2, ',', '');
							 } else $elvalor=' -- ';
							
						}
						else  // Catalunya, el calculem
						{   $preuCat=0;					    
							if (is_array($elsPercents))
							{   
								reset($elsPercents);
								while (list ($Tkey, $TVal) = each ($elsPercents))
								{   $TCodi = array_search($Tkey,$this->TerritorisLletra);
								$preuCat+=($arrayPrices[$TCodi][$any][$mes]*$elsPercents[$Tkey]/100.00);
								}
								$elvalor=$preuCat;
				
								$elvalor= number_format($elvalor, 2, ',', '');
								if ($elvalor==0) { $elvalor=' -- '; }
							}
							else { $elvalor=' -- '; }
						}
						if ($dadesmes==1) $ht .= "<div class='h_taula_006'>".$elvalor." </div>\r\n";
						$filaInfoPreus['PreusTerritoris'][]=$elvalor;
					} 
					if ($dadesmes==1) $ht .= "<div class='h_espai_01'> </div><div class='h_espai_01'> </div>\r\n";
					
					$mes++;
					if ($mes>12)
					{  if (strtolower($AnualVeure))
						{ 	$ht .= "\r\n";
							$ht .= "<div class='h_taula_004'>".$any." </div>\r\n";
							$ht .= "<div class='h_taula_005'><b> Anual </b></div>\r\n";
							reset($territVeure);
							foreach($territVeure as $tcode)
							{ 	if ($tcode==99) $tcode='Cat';
							    if (!isset($preusAnuals[$any])) { $preusAnuals[$any]=array(); }
								if (!isset($preusAnuals[$any][$tcode])) { $preusAnuals[$any][$tcode]=0; }
								$elvalor = number_format(floatval($preusAnuals[$any][$tcode]),2, '.', '');
								if ($elvalor==0) $elvalor = '--';
								$ht .= "<div class='h_taula_006'><b>".$elvalor." </b></div>\r\n";
							}
							$ht .= "<div class='h_espai_01'> </div><div class='h_espai_01'> </div>\r\n";
							$ht .= "\r\n";
					  }
					  $any++;
					  $mes=1;
				  }
				  if (  ($any>$toYear) or (($any==$toYear) and ($mes>$lastMonth)) )  { $acabat=true; }
				$infoPreus['valors'][]=$filaInfoPreus;
				}
				if ($dadesmes==1) $ht .= "\r\n";
			}
			return $infoPreus; //$ht;
		}
		
		
		// ==================
		
		/*
		* Selecciona tots els productes i els retorna en un array
		*
		*/
		function arrayOfAllProducts()	{
			$sql = strtoupper("SELECT * FROM ".$this->descripcioTable."
								   where actiu".$this->base." = 's' order by CODIPROD ASC");
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);	
					$ret[$fila['CODIPROD']]['NOMPROD'] = $fila['NOMPROD'];
					$ret[$fila['CODIPROD']]['UNITAT']= $fila['UNITAT'];
					$ret[$fila['CODIPROD']]['SUBGRUP']= $fila['SUBGRUP'];
					$ret[$fila['CODIPROD']]['GRUP']= $fila['GRUP'];
					$ret[$fila['CODIPROD']]['FASEINTERCANVI']= $fila['FASEINTERCANVI'];
					$ret[$fila['CODIPROD']]['CONDICIONSCOMERCIALITZACIO']= $fila['CONDICIONSCOMERCIALITZACIO'];
					$ret[$fila['CODIPROD']]['NECPREUS']= $fila['NECPREUS'];
					$ret[$fila['CODIPROD']]['MAXPREUS']= $fila['MAXPREUS'];
					$ret[$fila['CODIPROD']]['CALCUL']= $fila['CALCUL'];
					$ret[$fila['CODIPROD']]['DESCPREU1']= $fila['DESCPREU1'];
					$ret[$fila['CODIPROD']]['DESCPREU2']= $fila['DESCPREU2'];
					$ret[$fila['CODIPROD']]['DESCPREU3']= $fila['DESCPREU3'];
					$ret[$fila['CODIPROD']]['PUBLICAR']= $fila['PUBLICAR'];
					$ret[$fila['CODIPROD']]['TIPUS']= $fila['TIPUS'];
					$ret[$fila['CODIPROD']]['GESTOR']= $fila['GESTOR'];
				}
			}
			return $ret;
		}
		
		/**
		 * Funció baseCalAllTerrAny
		 * @param - $code:
		 * @param - $mes:
		 */
		function arrayOfProductsToDoCat() {
			
			$sql  = strtoupper("SELECT CodiProd FROM " .$this->baseTable ." WHERE  MES" .$this->month ." <>0   ");
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = "cal entrar";
				}
				/*
				 Array ( [001100] => cal entrar [002100] => cal entrar [003300] => cal entrar [003400] => cal entrar [014000] => cal entrar [016000] => cal entrar [021020] => cal entrar [021030] => cal entrar [041000] => cal entrar [042000] => cal entrar [052120] => cal entrar [052209] => cal entrar [052231] => cal entrar [052250] => cal entrar [052325] => cal entrar [052401] => cal entrar [052402] => cal entrar [052403] => cal entrar [052410] => cal entrar [052530] => cal entrar [052540] => cal entrar [052620] => cal entrar [052630] => cal entrar [052900] => cal entrar [053110] => cal entrar [053120] => cal entrar [053130] => cal entrar [054011] => cal entrar [054012] => cal entrar [054031] => cal entrar [054046] => cal entrar [054050] => cal entrar [054061] => cal entrar [054071] => cal entrar [054072] => cal entrar [054080] => cal entrar [054091] => cal entrar [054092] => cal entrar [054101] => cal entrar [054103] => cal entrar [054111] => cal entrar [054135] => cal entrar [054140] => cal entrar [054150] => cal entrar [054161] => cal entrar [054162] => cal entrar [054163] => cal entrar [054170] => cal entrar [054190] => cal entrar [062110] => cal entrar [062120] => cal entrar [062210] => cal entrar [062220] => cal entrar [062610] => cal entrar [071200] => cal entrar [071300] => cal entrar [071400] => cal entrar [071600] => cal entrar [071700] => cal entrar [071800] => cal entrar [091100] => cal entrar [091200] => cal entrar [092100] => cal entrar [092200] => cal entrar [092300] => cal entrar [111100] => cal entrar [111200] => cal entrar [111300] => cal entrar [111400] => cal entrar [112100] => cal entrar [112200] => cal entrar [112300] => cal entrar [112400] => cal entrar [113100] => cal entrar [113300] => cal entrar [114200] => cal entrar [114300] => cal entrar [115100] => cal entrar [115200] => cal entrar [116000] => cal entrar [121100] => cal entrar [121300] => cal entrar [122500] => cal entrar [122600] => cal entrar [122700] => cal entrar [122800] => cal entrar [123200] => cal entrar [123300] => cal entrar [131100] => cal entrar [131200] => cal entrar [131500] => cal entrar [131600] => cal entrar [132100] => cal entrar [132200] => cal entrar [133000] => cal entrar [134000] => cal entrar [135100] => cal entrar [135200] => cal entrar [211110] => cal entrar [211120] => cal entrar [211210] => cal entrar [211220] => cal entrar [211230] => cal entrar [211240] => cal entrar [211250] => cal entrar [211260] => cal entrar [211280] => cal entrar [212110] => cal entrar [212210] => cal entrar [212220] => cal entrar [212230] => cal entrar [212240] => cal entrar [221000] => cal entrar )
				 */
			}
			return $ret;
			
		}
		
		/**
		 * Funció arrayOfProductsToDo() 
		 *  Retorna un array amb els codis dels productes que, en la taula BASE, tenen un valor no zero assignat en el mes de treball.
		 *  La consulta està filtrada per codi de territori.
		 *  Això correspon als productes que el calendari marca que cal entrar preu
		 */
		function arrayOfProductsToDo() {
			$sql = strtoupper("SELECT CodiProd FROM " .$this->baseTable ." WHERE  MES" .$this->month ." <>0  AND CodiTerr = '".$this->codiTerr ."' ".
					" AND CodiProd IN (SELECT codiprod   FROM ".$this->descripcioTable."   where actiu".$this->base." = 's')");	
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);	
					$ret[$fila['CODIPROD']] = "cal entrar";
				}
			}
			return $ret;
		}
	
		/**
		 * Funció arrayOfProductsDoneCat: 
		 */
		
		function arrayOfProductsDoneCat() 
		{
			$sql = strtoupper("SELECT " .$this->descripcioTable .".NomProd, ".$this->descripcioTable .".CodiProd FROM " .$this->descripcioTable .", "
				.$this->preusTable ." WHERE ( ".$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
				"" .$this->month ."' AND ".$this->descripcioTable .".CodiProd = " .$this->preusTable .".CodiProd) AND " .
				"((" .$this->descripcioTable .".NecPreus = '1' AND " .$this->preusTable .".Preu1 != '0') OR " .
				"(" .$this->descripcioTable .".NecPreus = '2' AND " .$this->preusTable .".Preu1 != '0' AND " 
				.$this->preusTable .".Preu2 != '0') OR (" .$this->descripcioTable .".NecPreus = '3' " .
				"AND " .$this->preusTable .".Preu1 != '0' AND " .$this->preusTable .".Preu2 != '0' AND " .
				"" .$this->preusTable .".Preu3 != '0'))
				 ORDER BY   ".$this->preusTable .".CodiProd, ".$this->preusTable .".coditerr");

			//echo $sql;
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					if (isset($ret[$fila['CODIPROD']])) { $ret[$fila['CODIPROD']]++; }
					else $ret[$fila['CODIPROD']] = 1; // (¿?) això no hi era inicialment, però donava un error de variable no inicialitzada
				}
				return $ret;
			}
			else
			return null;
		}

		/**
	 * Funció arrayOfProductsDone: 
	 */
	function arrayOfProductsDone() {
		$sql = strtoupper("SELECT " .$this->descripcioTable .".NomProd, ".$this->descripcioTable .".CodiProd FROM " .$this->descripcioTable .", "
		.$this->preusTable ." WHERE (".$this->preusTable .".CodiTerr = '" .$this->codiTerr ."' AND ".$this->preusTable .".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
		"" .$this->month ."' AND ".$this->descripcioTable .".CodiProd = " .$this->preusTable .".CodiProd) AND " .
		" ".$this->descripcioTable .".actiu".$this->base." = 's'  AND ".
		"((" .$this->descripcioTable .".NecPreus = '1' AND " .$this->preusTable .".Preu1 <> '0') OR " .
		"(" .$this->descripcioTable .".NecPreus = '2' AND " .$this->preusTable .".Preu1 <> '0' AND " 
		.$this->preusTable .".Preu2 <> '0') OR 
		(" .$this->descripcioTable .".NecPreus = '3' " .
		"AND " .$this->preusTable .".Preu1 <> '0' AND " .$this->preusTable .".Preu2 <> '0' AND " .
		"" .$this->preusTable .".Preu3 <> '0')
		OR (" .$this->descripcioTable .".Calcul='S'  AND " .$this->preusTable .".FinPreu > 0)
		)");
		
		$rows = $this->conn->select($sql);
		$num = count($rows);  
		$ret=null;
		if ($num>0) {
			$ret = array();
			foreach($rows as $rowOBJ){ 
				$fila = ((array) $rowOBJ);
				$ret[$fila['CODIPROD']] = $fila['NOMPROD'];
			}
			//var_dump($ret);
			return $ret;
		}
		else
		return null;
	}
		
		
		/**
		 * Funció arrayOfProductsIniciatsInacabatsCat: 
		 */
		function arrayOfProductsIniciatsInacabatsCat() 
		{
			/*
			 $sql = "SELECT " .$this->descripcioTable .".NomProd, ".$this->descripcioTable .".CodiProd FROM " .$this->descripcioTable .", "
			.$this->preusTable ." WHERE ( ".$this->preusTable .".any = '" .$this->year ."' AND " .$this->preusTable .".mes = '" .
			"" .$this->month ."' AND ".$this->descripcioTable .".CodiProd = " .$this->preusTable .".CodiProd) AND " .
			"((" .$this->descripcioTable .".NecPreus = '1' AND " .$this->preusTable .".Preu1 = '0') OR " .
			"(" .$this->descripcioTable .".NecPreus = '2' AND (" .$this->preusTable .".Preu1 = '0' OR " 
			.$this->preusTable .".Preu2 = '0')) OR (" .$this->descripcioTable .".NecPreus = '3' " .
			"AND (" .$this->preusTable .".Preu1 = '0' OR " .$this->preusTable .".Preu2 = '0' OR " .
			"" .$this->preusTable .".Preu3 = '0')));";		
			//echo "arrayOfProductsInacabatsCat->".$sql;
			$result = mysql_query($sql, $this->conn);
			$num = mysql_numrows($result);
			echo $sql;
			if ($result && mysql_numrows($result) != 0){
				for ($i = 0; $i<$num; $i++) {
					$fila = mysql_fetch_assoc($result);
					$ret[$fila['CODIPROD']] = $fila['NomProd'];
				}
				//var_dump($ret);
				return $ret;
			}
			else
			return null;
			*/

			$p1 = $this->preusTable.".preu1";
			$p2 = $this->preusTable.".preu2";
			$p3 = $this->preusTable.".preu3";
			
			$sql = strtoupper("SELECT " .$this->descripcioTable .".NomProd, ".$this->descripcioTable .".CodiProd ".
				   " FROM " .$this->descripcioTable .", ".$this->preusTable ." ".
				   " WHERE ( ".$this->preusTable .
					 ".ANY = '" .$this->year ."' AND " .$this->preusTable .".mes = '"."" .$this->month .
					 "' AND ".$this->descripcioTable .".CodiProd = " .$this->preusTable .".CodiProd) AND " .
					"( " .
					"(".$this->descripcioTable.".NecPreus='2') AND (".$p1."*".$p2."=0) AND (".$p1."+".$p2.">0) AND (Calcul ='M')  OR ".
					"(".$this->descripcioTable.".NecPreus='3') AND (".$p1."*".$p2."*".$p3."=0) AND (".$p1."+".$p2."+".$p3.">0) AND (Calcul ='M') ".		
					"     ". 
					")");	
			//echo $sql;
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = $fila['NOMPROD'];
				}
				//var_dump($ret);
				return $ret;
			}
			else
			return array();
		}
		
		
		/**
		* Funció NotValidated: 
		*/
		function arrayOfHowManyTerritsHavePrice()
		{
			$sql = strtoupper("SELECT count(*)as Q,CodiProd FROM " .$this->baseTable ." WHERE  MES" .$this->month ." <>0  ".
					" AND CodiProd IN (SELECT codiprod   FROM ".$this->descripcioTable."   where actiu".$this->base." = 's') ".
					" group by CodiProd order by codiprod ");
					
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = $fila['Q'];
				}
				
				return $ret;
			}
			else return null;
		
		}
		
		/**
		 * Funció arrayOfProductsValidats: 
		 */
		function arrayOfHowManyTerrValidats()
		{  $sql = strtoupper("SELECT count(*) as Q, ".$this->descripcioTable .".CodiProd
					 FROM " .$this->descripcioTable .", ".$this->preusTable
			." WHERE ".$this->descripcioTable .".CodiProd = " .$this->preusTable .".CodiProd AND "
			.$this->preusTable .".mes = '".$this->month ."' AND "
			." validat='s' AND ".$this->preusTable .".ANY = '" .$this->year ."' ".
			" GROUP BY " .$this->descripcioTable .".CodiProd ORDER BY " .$this->descripcioTable .".CodiProd");	
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;

			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = $fila['Q'];
				}
				return $ret;
			}
			else return array();
		}

		
		/**
		 * Funció arrayOfProductsWithNotes: 
		 */
		function arrayOfProductsWithNotes()
		{  $sql = strtoupper("SELECT count( CodiProd ) as q, CodiProd FROM ".$this->notesTable."
						WHERE CodiTerr  = ".$this->codiTerr." AND MES = ".$this->month." AND ANY = ".$this->year." 
						GROUP BY CodiProd  ");
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = $fila['Q'];
				}
			}
			return $ret;
		}
		
		/**
		 * Funció arrayOfProductsWithLastNoteSC: 
		 */
		function arrayOfProductsWithLastNoteSC()
			{  $sql = strtoupper("SELECT DataComentari as datac, qui, CodiProd FROM ".$this->notesTable."
							WHERE CodiTerr  = ".$this->codiTerr." AND MES = ".$this->month." AND ANY = ".$this->year." 					 
							ORDER BY CodiProd, DataComentari  ");
			// Posem en un array tots els comentaris ordenats primer per codi de prod, i després per data,
			//   i sobreescribim els del mateix índex (CodiProc), d'aquesta manera en l'array queda únicament l'últim comentari.
			$rows = $this->conn->select($sql);
			$num = count($rows); 
			$retPre = array();
			if ($num>0) {
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$retPre[$fila['CODIPROD']] = array('qui'=>$fila['QUI'],'datac'=>$fila['DATAC']);
				}
				// Array ( [014000] => Array ( [qui] => SC [datac] => 2007-11-03 19:10:16 ) [042000] => Array ( [qui] => SC [datac] => 2007-11-04 10:16:18 ) ...........
				// Construim un array només amb els que han quedat 'SC' que corresponen amb els que l'últim comentari és 'SC'
				$ret = array();
				while (list ($CodiProdKey,$DadesArr) = each($retPre))
				{   if ($DadesArr['qui']=='SC') {  $ret[$CodiProdKey]=$DadesArr['datac'];  }
				}
				// Array ( [014000] => 2007-11-03 19:10:16 [042000] => 2007-11-04 10:16:18 )
				return $ret;
			}
			else return array();
		}

		/**
		 * Funció arrayOfProductsWithNotesCat: 
		 */
		function arrayOfProductsWithNotesCat()
		{  $sql = strtoupper("SELECT count( CodiProd ) as q, CodiProd FROM notes_preus".$this->pagPer.
					 "   WHERE MES = ".$this->month." AND ANY = ".$this->year." 
						GROUP BY CodiProd  ");
			$rows = $this->conn->select($sql);
			$num = count($rows);  
			$ret=null;
			if ($num>0) {
				$ret = array();
				foreach($rows as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = $fila['Q'];
				}
				return $ret;
			}
			else return null;
		}
		
		/**
		 * Funció arrayOfProductsWithLastNoteSC_Cat: 
		 */
		function arrayOfProductsWithLastNoteSC_Cat()
		{  $sql = strtoupper("SELECT DataComentari as datac, qui, CodiProd,CodiTerr FROM ".$this->notesTable."
						WHERE  MES = ".$this->month." AND ANY = ".$this->year." 					 
						ORDER BY CodiProd, DataComentari  ");	
		// Posem en un array tots els comentaris ordenats primer per codi de prod, i després per data,
		//   i sobreescribim els del mateix índex (CodiProc), d'aquesta manera en l'array queda únicament l'últim comentari.
		$rows = $this->conn->select($sql);
		$num = count($rows);  
		$retPre = array();
		if ($num>0)
		{
			foreach($rows as $rowOBJ){ 
				$fila = ((array) $rowOBJ);
				$retPre[$fila['CODIPROD']][$fila['CODITERR']] = array('qui'=>$fila['QUI'],'datac'=>$fila['DATAC']);
			}
			/*
				Array (
				[001100] => Array ( [17] => Array ( [qui] => ST [datac] => 2007-11-04 16:27:23 ) [25] => Array ( [qui] => SC [datac] => 2007-10-20 22:04:07 ) )
				[014000] => Array ( [8] => Array ( [qui] => SC [datac] => 2007-11-03 19:10:16 ) [17] => Array ( [qui] => ST [datac] => 2007-10-29 20:27:19 ) )
				[041000] => Array ( [17] => Array ( [qui] => SC [datac] => 2007-11-04 16:24:12 ) )
				[042000] => Array ( [8] => Array ( [qui] => SC [datac] => 2007-11-04 10:16:18 ) )
				[054190] => Array ( [25] => Array ( [qui] => SC [datac] => 2007-11-04 10:20:15 ) [17] => Array ( [qui] => SC [datac] => 2007-11-04 10:20:15 ) [8] => Array ( [qui] => SC [datac] => 2007-11-04 10:47:04 ) )
				[123200] => Array ( [8] => Array ( [qui] => ST [datac] => 2007-10-02 00:00:00 ) )
				[212110] => Array ( [8] => Array ( [qui] => ST [datac] => 2007-10-02 00:00:00 ) ) )
				*/
			// Construim un array només amb els que han quedat 'SC' que corresponen amb els que l'últim comentari és 'SC'
			$ret = array();
			while (list ($CodiProdKey,$DadesArr) = each($retPre))
			{   // Array ( [17] => Array ( [qui] => ST [datac] => 2007-11-04 16:27:23 ) [25] => Array ( [qui] => SC [datac] => 2007-10-20 22:04:07 ) )
				$tempArr = $DadesArr;  // no sé ben bé perquè, però si no poso això .. es penja
				while (list($TerrKey,$MesDadesArr) = each($DadesArr))
				{ // Array ( [qui] => ST [datac] => 2007-11-04 16:27:23 )
					if ($MesDadesArr['qui']=='SC') {  $ret[$CodiProdKey][$TerrKey]=$MesDadesArr['datac'];  }
				}
			}
			/*
			 Array ( [001100] => Array ( [25] => 2007-10-20 22:04:07 )
			 [014000] => Array ( [8] => 2007-11-03 19:10:16 )
			 [041000] => Array ( [17] => 2007-11-04 16:24:12 )
			 [042000] => Array ( [8] => 2007-11-04 10:16:18 )
			 [054190] => Array ( [25] => 2007-11-04 10:20:15 [17] => 2007-11-04 10:20:15 [8] => 2007-11-04 10:47:04 ) )
				*/
			return $ret;
		}
		else return null;
	}
	
		/**
		 * Funció arrayOfProductsToDoWithPricesTypes: 
		 *
		 * Si es tipus prov mira si el connectat és un gestor i li toca entrar
		 * Si es tipus Cat mira si és el gestor i si hi ha algun territori que ha d'entrar	
		 */
		 
		function arrayOfProductsToDoWithPricesTypes($territori) 
		{
			$territori_lletra = $this->TerritorisLletra[$territori];
			$sqlCAT = strtoupper("SELECT DISTINCT CodiProd FROM " .$this->baseTable ." WHERE MES" .$this->month ." <>0 ".
				   " AND CodiProd IN (SELECT codiprod   FROM ".$this->descripcioTable."   where actiu".$this->base." = 's' AND tipus = 'Cat' AND gestor = '".$territori_lletra."')");
			$sqlProv = strtoupper("SELECT CodiProd FROM ".$this->baseTable ." WHERE MES" .$this->month ." <>0 "."AND Coditerr = '".$territori.
						"' AND CodiProd IN (SELECT codiprod FROM ".$this->descripcioTable." WHERE Actiu".$this->base." = 's' AND Tipus = 'Prov' AND gestor like '%".$territori_lletra."%')");	

			$rowsProv = $this->conn->select($sqlProv);
			$numProv = count($rowsProv);
			
			$rowsCAT = $this->conn->select($sqlCAT);
			$numCAT = count($rowsCAT);
			
			if ($numCAT>0) {
				foreach($rowsCAT as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = "cal entrar";
				}
			}
			if ($numProv>0) {
				foreach($rowsProv as $rowOBJ){ 
					$fila = ((array) $rowOBJ);
					$ret[$fila['CODIPROD']] = "cal entrar";
				}
			}
			
			if (($numCAT+$numProv)>0){
				return $ret;
			}
			else
			return null;
		}
		
		/*
		* Funció TenenPreu() 
		* 
		* @param $elsCalFer
		*/
		function TenenPreu($elsCalFer)
		{  // Per cada producte de $elsCalFer, els que tenen preu final
		   // $elsCalFer --> Array ( [011000] => cal entrar [021010] => cal entrar [021040] => cal entrar .............
		   $ret=array(); 
		   while (list ($codi,) = each ($elsCalFer))
		   {
			if ($this->TipusPreu($codi)=='Prov') 
			{ $preu=$this->price($codi);  }
			else 
			{ // els prod de tipus Cat, si un territori té preu .. els altres també, perquè es propaga.
			  $donenPreu = $this->TerritsDonenPreuUnProdUnMes($codi);  // 8,17,43
			  $donenPreu_arr = explode(",",$donenPreu);  // Array ( [0] => 8 [1] => 17 [2] => 43 ) 
			  
			  $preu=$this->price($codi,$donenPreu_arr[0]);       }
			  if ($preu>0.0) { $ret[$codi]=$this->tots[$codi];   }
		   }
		   return $ret;
		}
    
		
		/*
		* Funció TenenPreuCat() 
		* 
		* @param $elsCalFer
		*/
		function TenenPreuCat($elsCalFer)
		{  // Per cada producte de $elsCalFer, els que tenen preu final
		   // $elsCalFer --> Array ( [011000] => cal entrar [021010] => cal entrar [021040] => cal entrar .............
		   $ret=array(); 
		   while (list ($codi,) = each ($elsCalFer))
		   {  
			  $preu=$this->priceCat($codi);
			  //if ($codi=='052110') echo 'o)'.$preu;
			  if ($preu>0.0) { $ret[$codi]=$this->tots[$codi];   }
		   }
		   return $ret;
		}
		
		/*
		* Funció TenenPreuNoFinalitzatCat() 
		* 
		*/
		function TenenPreuNoFinalitzatCat()
		{  // $elsTenenPreu-> Array ( [011000] => Array ( [NOMPROD] => Mongetes seques [UNITAT] => eur/100 kg [SUBGRUP] => Mongetes seques
			$ret=array(); 
			$ProductsIniciatsInacabatsCat=$this->arrayOfProductsIniciatsInacabatsCat();    	
			while (list ($codi,) = each ($ProductsIniciatsInacabatsCat))
			{
				$ret[$codi] = "Inacabat";
			}
			// Array ( [054015] => Escarola ) 
			$elsAcabats=$this->arrayOfProductsDoneCat();
			// Array ( [001100] => 1 [002100] => 1 [003300] => 2 [004000] => 1 [006000] => 1 [011000] => 1 [021040] => 1
			$arrayOfHowManyTerritsHavePrice = $this->arrayOfHowManyTerritsHavePrice();    	
			while (list ($codi,) = each ($arrayOfHowManyTerritsHavePrice))
			{  if (isset($elsAcabats[$codi]))
				{  if (($arrayOfHowManyTerritsHavePrice[$codi]-$elsAcabats[$codi])>0) $ret[$codi] = "Algun territori no té preu"; }    		
			}
			return $ret;
		}
		
		
		/*
		* Funció TenenPreuNoFinalitzat() 
		* 
		*/
		function TenenPreuNoFinalitzat($elsTenenPreu)
		{  // $elsTenenPreu-> Array ( [011000] => Array ( [NOMPROD] => Mongetes seques [UNITAT] => eur/100 kg [SUBGRUP] => Mongetes seques
			$ret=array(); 
			while (list ($codi,) = each ($elsTenenPreu))
			{  
			  if ($this->TipusPreu($codi)=='Prov') $enteredPrices=$this->enteredPrices($codi); // Array ( [PREU1] => 59.84 [PREU2] => 0 [PREU3] => 59.84 [NECPREUS] => 3 [MAXPREUS] => 3 )
			  else 
			   {  $donenPreu = $this->TerritsDonenPreuUnProdUnMes($codi); // 8,17,43
				  $donenPreu_arr = explode(",",$donenPreu);  // Array ( [0] => 8 [1] => 17 [2] => 43 ) 
				  $enteredPrices=$this->enteredPricesTerritori($codi,$donenPreu_arr[0]);
			   }
			  $quantsPreusHiHa=0; 
			  for ($i=1;$i<=$enteredPrices['NECPREUS']; $i++)
			  { $quantsPreusHiHa += ($enteredPrices['PREU'.$i]>0);	      	
			  }
			  if ($enteredPrices['NECPREUS']>$quantsPreusHiHa) $ret[$codi]=$quantsPreusHiHa;
			  
			}
			return $ret;
		}
    
	    /*
		* Funció TenenPreuFinalitzatNoValidat() 
		* 
		* @param $elsTenenPreuFinalitzat
		*/
		function TenenPreuFinalitzatNoValidat($elsTenenPreuFinalitzat)
		{ // $elsTenenPreuFinalitzat --> Array ( [011000] => Array ( [NOMPROD] => Mongetes seques [UNITAT] => eur/100 kg [SUBGRUP] => 
		  $ret=array();
		  //print_r($elsTenenPreuFinalitzat);
		  while (list ($codi,) = each ($elsTenenPreuFinalitzat))
			{  
			   if ($this->TipusPreu($codi)=='Prov') $validat=$this->priceTerritoriValidat($codi,$this->codiTerr);
			   else
			   {
				  $donenPreu = $this->TerritsDonenPreuUnProdUnMes($codi); // 8,17,43
				  //if ($codi=='011000') echo $donenPreu;
				  $donenPreu_arr = explode(",",$donenPreu);  // Array ( [0] => 8 [1] => 17 [2] => 43 ) 
				  $validat=$this->priceTerritoriValidat($codi,$donenPreu_arr[0]);
			   }

			   if (!$validat) $ret[$codi]='No Validat';
				 
			}
		   return $ret; 
		}
		
	
		/*
		* Funció TenenPreuFinalitzatNoValidatCat() 
		* 
		* @param $elsTenenPreuFinalitzat
		*/
		function TenenPreuFinalitzatNoValidatCat($elsTenenPreuFinalitzat)
		{   $ret=array(); 
			$validatsArr = $this->arrayOfHowManyTerrValidats();
			// Array ( [011000] => 1 [052120] => 3 [054061] => 1 [054150] => 1 ) 
			$arrayOfHowManyTerritsHavePrice = $this->arrayOfHowManyTerritsHavePrice();   
			// Array ( [001100] => 2 [002100] => 1 [002200] => 1 [003300] => 2 [004000] => 1 ....
			while (list ($codi,) = each ($elsTenenPreuFinalitzat))
			{  if (($arrayOfHowManyTerritsHavePrice[$codi]-$validatsArr[$codi])>0)  $ret[$codi] = "Falta algun territori per validar";
			}
			return $ret; 
			
			
		}
    
		/*
		* Funció TipusPreu() 
		* Funció que retorna el tipus del producte cat o prov
		* @param $CodiProd
		*/
		function TipusPreu($CodiProd)
		{
			$sql = strtoupper("SELECT tipus FROM ".$this->descripcioTable." WHERE CodiProd ='".$CodiProd."' ");
			//echo $sql;
			$rows = $this->conn->select($sql);
		    $num = count($rows);  
			if ($num>0) {
				$row = (array) $rows[0];
				// Array ( [TIPUS] => Cat )
				return $row['TIPUS'];
			}	
			else
				return false;
		}

		
    } // class                                                                                                               
}  // namespace                                                                                                                 
?>