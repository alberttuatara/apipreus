<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/ca', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| This route group applies for the API.
|
*/

Route::group(['prefix' => 'api'], function () {

    Route::get('language', 'API\UsersController@getLanguage');
	Route::any('appinfo', 'API\UsersController@getAppInfo');
	Route::get('dadesinici', 'API\UsersController@getIniciData');
	Route::get('testpreus', 'API\UsersController@testpreus');
	Route::get('testexcel', 'API\UsersController@testexcel');  
	// usuaris
	Route::get('users/info',  'API\UsersController@info');
	// aplicacio
	Route::get('aplicacio/estat',  'API\AplicacioController@obtenirestat');
	Route::get('aplicacio/nouperiode',  'API\AplicacioController@nouperiode');
	Route::get('aplicacio/validaciomassiva',  'API\AplicacioController@validaciomassiva');
	Route::post('aplicacio/ponderacio-quantitativa',  'API\AplicacioController@ponderacio_quantitativa');
	// Producte
	Route::get('fitxa/{id}', 'API\ProducteController@getinforevisio')->where('id', '[0-9]+');
	Route::get('producte/preus/{codiprod}/{any}/{mes}',  'API\ProducteController@obtenirpreus')
			->where(['codiprod','[0-9]+','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*','opcio'=>'[0-9,a-z,A-Z]+']);
	Route::get('producte/tipus/{codiprod}',  'API\ProducteController@obtenirTipus')->where('codiprod','[0-9]+');	
	Route::get('producte/comentaris/{codiprod}/{any}/{mes}',  'API\ProducteController@obtenircomentaris')
			->where(['codiprod','[0-9]+','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*','opcio'=>'[0-9,a-z,A-Z]+']);
	Route::post('producte/comentaris/afegircomentari','API\ProducteController@afegircomentari');
	Route::get('producte/validapreu/{codiprod}/{coditerr}/{any}/{mes}',  'API\ProducteController@validapreu')
		->where(['codiprod'=>'[0-9]+','coditerr', '8|17|25|43|99','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
	Route::get('producte/eliminapreu/{codiprod}/{coditerr}/{any}/{mes}',  'API\ProducteController@eliminapreu')
		->where(['codiprod'=>'[0-9]+','coditerr', '8|17|25|43|99','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
	Route::get('producte/invalidapreu/{codiprod}/{coditerr}/{any}/{mes}',  'API\ProducteController@invalidapreu')
		->where(['codiprod'=>'[0-9]+','coditerr', '8|17|25|43|99','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
	Route::get('producte/autopreu/{codiprod}/{coditerr}/{any}/{mes}',  'API\ProducteController@autopreu')
		->where(['codiprod'=>'[0-9]+','coditerr', '8|17|25|43|99','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
	// territoris
	Route::get('territoris/info',  'API\TerritorisController@info');
	Route::get('territoris/infoUn/{coditerr}',  'API\TerritorisController@infoUn')->where('coditerr', '8|17|25|43|99');
	Route::get('territoris/estatpreusUn/{coditerr}',  'API\TerritorisController@estatpreusUn')->where('coditerr', '8|17|25|43|99');
	Route::post('territoris/estatpreusUn/{coditerr}', 'API\TerritorisController@actualitzarestatpreusUn')->where('coditerr', '8|17|25|43|99');
	// revisió
	Route::get('grafiques/importanciarelativa',  'API\GrafiquesController@importanciarelativa');
	// pàgines
	    Route::get('pagina/status',  'API\PaginesController@status');
		// Llistat de preus
		Route::get('dadespercebuts', 'API\PaginesController@getProductesLlista');
		Route::get('dadespagats', 'API\PaginesController@getProductesLlista');
		Route::get('productes/{base}/{pagatspercebuts}/{any}/{mes}/{opcio}',      'API\PaginesController@getProductesData')
			->where(['base' => '2[0-9]{3}','pagatspercebuts' => 'pagats|percebuts','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*','opcio'=>'[0-9,a-z,A-Z]+']);
		
		// revisió
		Route::get('pagina/revisio/{base}/{pagatspercebuts}/{codiprod}/{any}/{mes}',  'API\PaginesController@revisio')
			->where(['base' => '2[0-9]{3}','pagatspercebuts' => 'pagats|percebuts','codiprod'=>'[0-9]+','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
		// entrada
		Route::get('pagina/entrada/{base}/{pagatspercebuts}/{codiprod}/{any}/{mes}',  'API\PaginesController@entrada')
			->where(['base' => '2[0-9]{3}','pagatspercebuts' => 'pagats|percebuts','codiprod'=>'[0-9]+','any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);	
		Route::post('pagina/entrada',  'API\PaginesController@entrada_preu');	
		// consulta
		Route::get('pagina/consulta/{base}/{pagatspercebuts}/{codiprod}',  'API\PaginesController@consulta')
			->where(['base' => '2[0-9]{3}','pagatspercebuts' => 'pagats|percebuts','codiprod'=>'[0-9]+']);	
		Route::post('pagina/consulta_refresc',  'API\PaginesController@consulta_refresc');
		Route::post('pagina/consulta_arbreCodis',  'API\PaginesController@consulta_arbreCodis');
		// Exportacions
		Route::get('exportacio/marm/{any}/{mes}',  'API\ExportacioController@marm')
			->where(['any' => '2[0-9]{3}','mes' => '[1-9][0-2]*']);
		Route::get('exportacio/excel_historicTots/{anyinici}/{anyfi}/{tipuspreus}/{actuals}/{format}',  'API\ExportacioController@excel_historicTots')
			->where(['anyinici' => '2[0-9]{3}','anyfi' => '2[0-9]{3}','tipuspreus' => 'pagats|percebuts|salaris','actuals' => 'si|no','format'=> 'csv|xls']);
		Route::get('exportacio/excel_indexCompMensAnual/{anyinici}/{anyfi}/{actuals}/{format}',  'API\ExportacioController@excel_indexCompMensAnual')
			->where(['anyinici' => '2[0-9]{3}','anyfi' => '2[0-9]{3}','actuals' => 'si|no','format'=> 'csv|xls']);
		Route::get('exportacio/indexCompAnualMobil/{anyinici}/{anyfi}/{actuals}/{format}',  'API\ExportacioController@indexCompAnualMobil')
			->where(['anyinici' => '2[0-9]{3}','anyfi' => '2[0-9]{3}','actuals' => 'si|no','format'=> 'csv|xls']);
		Route::get('exportacio/exportacio_preus_TOTS/{opcio}',  'API\ExportacioController@exportacio_preus_TOTS')
			->where(['opcio' => 'percebuts|pagats']);
		Route::get('exportacio/exportacio_ponde_TOTES/{opcio}',  'API\ExportacioController@exportacio_ponde_TOTES')
			->where(['opcio' => 'm|a']);
		Route::get('exportacio/exportacioQFisiques/{format}',  'API\ExportacioController@exportacioQFisiques')
			->where(['format' => 'web|csv']);
		Route::get('exportacio/exportacioPreusBase/{format}',  'API\ExportacioController@exportacioPreusBase')
			->where(['format' => 'web|csv']);
		Route::get('exportacio/exportacioPonderValor/{format}',  'API\ExportacioController@exportacioPonderValor')
			->where(['format' => 'web|csv']);
		// Calculs
		Route::get('resultatsCalculs/{opcio}/{any}/{format}/{precisio}',  'API\CalculsController@resultatsCalculs')
			->where(['opcio'=>'[a-z,A-Z]+','any' => '2[0-9]{3}','format' => 'web|csv','precisio' => 'true|false']);
		
		
    /**
     * Authentication routes
     */
    Route::group(['prefix' => 'auth'], function() {
    	Route::post('login',         'API\UsersController@login');
    	Route::post('logout',        'API\UsersController@logout');
    	Route::post('register',      'API\UsersController@register');
    	Route::get('refresh-token',  'API\UsersController@refreshToken');
		
    });

    /**
     * Authenticated routes
     */
    Route::group(['middleware' => ['jwt.auth']], function() {
    	Route::get('protected', 'API\UsersController@getProtected');
    });
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
