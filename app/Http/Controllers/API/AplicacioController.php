<?php 

namespace App\Http\Controllers\API;

use DateTime;
use JWTAuth;
use Auth;
use APIException;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Input;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

use App\Models\Aplicacio;


class AplicacioController extends APIController
{
	
	public function __construct() {
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$this->timestart = new DateTime( "now" );
	}
	
	public function obtenirestat() {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		
		$aplicacio = Aplicacio::where('IDAPP', 'ppp')->get();
		$result['estat'] = true;
		$result['informacio'] = $aplicacio;
		
		
		return $result;  
	}
	
	public function validacioMassiva() {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		$ok = false;
		$msg = '';
		
		$rol = $this->user->rol;
		$result['informacio']['rol'] = $rol;
		
		if ($rol != 'sc') {
			$ok = false;
			$msg = 'Error de permisos.';
		} else {
		    $Conn = DB::connection(); 
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$anycurs = $aplicacio->ENCURS_ANY_DADES;
			$mescurs = $aplicacio->ENCURS_MES_DADES;
			$base = config('ppp.base')-2000;
			
			$pagatspercebuts = "percebuts";
			$where = "((FINPREU>0) and (validat='n') and (ANY=".$anycurs.") and (mes=".$mescurs."))";
			$selSQL = strtoupper("select * from preus".$pagatspercebuts." where ".$where." ");
			$rows_elsProds = DB::connection()->select($selSQL);  
			if (count($rows_elsProds) > 0) {
				 $elsCodisProd = array();
				 foreach($rows_elsProds as $rowOBJ){  
				    $row_elProd = ((array) $rowOBJ); 
					$elsCodisProd[]=$row_elProd['CODIPROD'];
				 }
				 if (count($elsCodisProd)>0) {
						$actualitzaSQL = strtoupper("UPDATE preus".$pagatspercebuts." 
										SET validat='s'
										where ".$where);		
										
						$affected = DB::update($actualitzaSQL);
						if ($affected>0)  // Tot OK
						{ 
							$ok = true;
							$result["informacio"][$pagatspercebuts]= " - ".$affected." preus ".$pagatspercebuts." actualitzats."; 
						}
				 } else {
					 $msg = "No s'han trobat preus percebuts per validar.";
				 }
			} else {
				$msg = "No s'han trobat preus percebuts per validar.";
			}
			
			$pagatspercebuts = "pagats";
			$where = "((FINPREU>0)  and (validat='n') and (ANY=".$anycurs.") and (mes=".$mescurs."))";
			$selSQL = strtoupper("select * from preus".$pagatspercebuts." where ".$where." ");
			$rows_elsProds = DB::connection()->select($selSQL);  
			if (count($rows_elsProds) > 0) {
				 $elsCodisProd = array();
				 foreach($rows_elsProds as $rowOBJ){  
				    $row_elProd = ((array) $rowOBJ); 
					$elsCodisProd[]=$row_elProd['CODIPROD'];
				 }
				 if (count($elsCodisProd)>0) {
						$actualitzaSQL = strtoupper("UPDATE preus".$pagatspercebuts." 
										SET validat='s'
										where ".$where);		
										
						$affected = DB::update($actualitzaSQL);
						if ($affected>0)  // Tot OK
						{ 
							$ok = true;
							$result["informacio"][$pagatspercebuts]= " - ".$affected." preus ".$pagatspercebuts." actualitzats."; 
						}
				 } else {
					 $msg = "No s'han trobat preus percebuts per validar.";
				 }
			} else {
				$msg = "No s'han trobat preus percebuts per validar.";
			}
			
			
		}
		
		$result['estat']=$ok;
		$result['informacio']['msg']=$msg;
			
		return $result;
	}
	
	public function nouperiode() {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		$ok = false;
		$msg = '';
		
		$rol = $this->user->rol;
		$result['informacio']['rol'] = $rol;
		
		if ($rol != 'sc') {
			$ok = false;
			$msg = 'Error de permisos.';
		} else {
		    $Conn = DB::connection();  
			$anycurs = 2017;
			$mescurs = 2;
			$base=10;
		    // Hi ha tots els preus entrats?
		    $calendari_ple=false;
		    // els preus del calendari entrats
			// els preus del calendari entrats
		    $elsEntrats_Pagats = strtoupper("
					select * 
					from preuspagats, basepagats".$base.", descripcio_preuspagats 
					where preuspagats.CODIPROD = basepagats".$base.".CODIPROD
					and preuspagats.CODITERR = basepagats".$base.".CODITERR
					and descripcio_preuspagats.CODIPROD=basepagats".$base.".CODIPROD
					and descripcio_preuspagats.CODIPROD=preuspagats.CODIPROD
					and basepagats".$base.".MES".$mescurs.">0
					and descripcio_preuspagats.ACTIU".$base."='s'
					and preuspagats.ANY=".$anycurs."
					and descripcio_preuspagats.MAPA = 's' 
					and preuspagats.MES=".$mescurs."
					and preuspagats.FINPREU>0"); 
			$RECS_elsEntrats_Pagats = $Conn->select($elsEntrats_Pagats); 
			$q_elsEntrats_Pagats = count($RECS_elsEntrats_Pagats);   					
		    $elsEntrats_Percebuts = strtoupper("
					select * 
					from preuspercebuts, basepercebuts".$base.", descripcio_preuspercebuts 
					where preuspercebuts.CODIPROD = basepercebuts".$base.".CODIPROD
					and preuspercebuts.CODITERR = basepercebuts".$base.".CODITERR
					and descripcio_preuspercebuts.CODIPROD=basepercebuts".$base.".CODIPROD
					and descripcio_preuspercebuts.CODIPROD=preuspercebuts.CODIPROD
					and basepercebuts".$base.".MES".$mescurs.">0
					and descripcio_preuspercebuts.ACTIU".$base."='s'
					and preuspercebuts.ANY=".$anycurs."
					and descripcio_preuspercebuts.MAPA = 's' 
					and preuspercebuts.MES=".$mescurs."
					and preuspercebuts.FINPREU>0");
		    $RECS_elsEntrats_Percebuts = $Conn->select($elsEntrats_Percebuts); 
			$q_elsEntrats_Percebuts = count($RECS_elsEntrats_Percebuts); 
			// els preus que s'han d'entrar
			$elsCalEntrar_Pagats = strtoupper("select * 
			from basepagats".$base.",descripcio_preuspagats
			where
			descripcio_preuspagats.CODIPROD=basepagats".$base.".CODIPROD
			and descripcio_preuspagats.ACTIU".$base."='s' 
			and descripcio_preuspagats.MAPA = 's'
			and basepagats".$base.".MES".$mescurs.">0 ");
			$RECS_elsEntrats_Pagats = $Conn->select($elsCalEntrar_Pagats); 
			$q_elsCalEntrats_Pagats = count($RECS_elsEntrats_Pagats); 
			$elsCalEntrar_Percebuts = strtoupper("select * 
			from basePercebuts".$base.",descripcio_preusPercebuts
			where
			descripcio_preusPercebuts.CODIPROD=basePercebuts".$base.".CODIPROD
			and descripcio_preusPercebuts.ACTIU".$base."='s'
			and descripcio_preusPercebuts.MAPA = 's'
			and basePercebuts".$base.".MES".$mescurs.">0 ");
			$RECS_elsEntrats_Percebuts = $Conn->select($elsCalEntrar_Percebuts); 
			$q_elsCalEntrats_Percebuts = count($RECS_elsEntrats_Percebuts); 
		   	
			$calendari_ple=(($q_elsCalEntrats_Pagats+$q_elsCalEntrats_Percebuts)==($q_elsEntrats_Pagats+$q_elsEntrats_Percebuts));
			$result['informacio']['q_elsCalEntrats_Pagats']=$q_elsCalEntrats_Pagats;
			$result['informacio']['q_elsCalEntrats_Percebuts']=$q_elsCalEntrats_Percebuts;
			$result['informacio']['q_elsEntrats_Pagats']=$q_elsEntrats_Pagats;
			$result['informacio']['q_elsEntrats_Percebuts']=$q_elsEntrats_Percebuts;
			if (!$calendari_ple)
			{
			   $ok = false;
			   $msg = 'No estan tots els preus entrats.';
			   
			} else {
				// Tots els preus estan validats?
				$result['informacio']['calendari_ple']=$calendari_ple;
				// Tots els preus estan validats?
				$totValidat = strtoupper("select * from preuspagats where ANY = ".$anycurs." AND MES = ".$mescurs." AND validat <> 's'");
				$RECS_totValidat = $Conn->select($totValidat); 
			    $num1 = count($RECS_elsEntrats_Percebuts); 
				$totValidat = strtoupper("select * from preuspercebuts where ANY = ".$anycurs." AND MES = ".$mescurs." AND validat <> 's'");
				$RECS_totValidat = $Conn->select($totValidat); 
			    $num2 = count($RECS_totValidat); 
				$result['informacio']['validatspagats']=$num1;
			    $result['informacio']['validatspercebuts']=$num2;
				if ($num1+$num2 != 0)  // No hi ha preus sense validar
				{
					$ok = false;
					$msg = "Error. Hi ha preus sense validar.";
				} else {
					// No hi ha preus sense validar
					// estan tots els territoris en estat validar
					$estatTerrits = strtoupper("select estat from territori where estat = 'validat'");
					$resultRECS = $Conn->select($estatTerrits);
					$qvalidats = count($resultRECS); 
					$elsTerrits =  strtoupper("select estat from territori");
					$resultRECS = $Conn->select($elsTerrits);
					$qterrits = count($resultRECS);
					$result['informacio']['qvalidats']=$qvalidats;
					$result['informacio']['qterrits']=$qterrits;
					if ($qvalidats==$qterrits) {
						$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
						$encurs_mes_dades = $aplicacio->ENCURS_MES_DADES;
						$encurs_mes_dades = $encurs_mes_dades+1;
						if ($encurs_mes_dades>12) {
							$encurs_mes_dades = 1;
							$encurs_any_dades = $aplicacio->ENCURS_ANY_DADES;
							$encurs_any_dades = $encurs_any_dades+1;
						}
						$aplicacio->ENCURS_MES_DADES = $encurs_mes_dades;
						$aplicacio->ENCURS_ANY_DADES = $encurs_any_dades;
						$saveresult = $aplicacio->save();
						$result['informacio']['saveresult']=$saveresult;
						$ok = true;
					}
				}					
   
			}
			
			
		}
			
		$result['estat']=$ok;
		$result['informacio']['msg']=$msg;
			
		return $result;
	}
	
	public function ponderacio_quantitativa(Request $request) {
		$result = array('estat'=>false, 'informacio'=>array());   
		
		$result['informacio']['path']=$request->path();                 
		$result['informacio']['params']=$request->all(); 
		
		$estatOK = true;
		$msg = '';
		
		$result['informacio']['msg'] = $msg;
		$result['estat']['OK']=$estatOK;
		return $result; 
		
	}
}                                                                  
                                                                   
 ?>                                                                