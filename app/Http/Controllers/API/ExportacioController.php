<?php 

namespace App\Http\Controllers\API;

use DateTime;
use DatePeriod;
use DateInterval;
use JWTAuth;
use Auth;
use App;
use APIException;
use DB;
use App\Models\Territori;
use App\Models\Aplicacio;
use App\Library\Preus;
use Illuminate\Http\Request;
use Illuminate\Http\Input;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;
use Response;
use ExcelMaat;
use PHPExcel; 
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_Drawing;


class ExportacioController extends APIController
{
	
	public function __construct() {
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$this->rol = $user->rol;
		$this->timestart = new DateTime( "now" );

		$this->familyfont = 'Arial';
			
		$this->callback = function()
			{
				$FH = fopen('php://output', 'w');
				foreach ($this->list as $row) { 
					fputcsv($FH, $row);
				}
				fclose($FH);
			};
		
		
		
		
	}
	
	private function configurarestilsexcel() {
		// 
		$familyfont = 'Arial';  // Helvetica
		$this->general = array(
				'font' => array(
					'family'     => $familyfont,
					'size'       => '10',
					'bold'       =>  false,
				),
				
		);
		$this->titleFormat = array(
					'fontfamily'     => $familyfont,
					'size'       => '14',
					'bold'       =>  true
		);
		$this->titleFormat1 = array(
					'fontfamily'     => $familyfont,
					'size'       => '12',
					'bold'       =>  true
		);
		$this->titleFormat2 = array(
					'fontfamily'     => $familyfont,
					'size'       => '8',
					'bold'       =>  false
		);
		$this->titleFormat3 = array(
					'fontfamily'     => $familyfont,
					'size'       => '12',
					'bold'       =>  false
		);
		$this->titleFormat4 = array(
					'family'     => $familyfont,
					'size'       => '8',
					'bold'       =>  false
		);
		$this->inf = array(
					'family'     => $familyfont,
					'size'       => '10',
					'bold'       =>  false
		);
		$this->inf1 = array(
					'family'     => $familyfont,
					'size'       => '12',
					'bold'       =>  true
		);
		$this->inf2 = array(
					'family'     => $familyfont,
					'size'       => '10',
					'bold'       =>  true
		);
		$this->FormatRatllaDaltBold = array(
					'family'     => $familyfont,
					'size'       => '8',
					'bold'       =>  true
		);
		$this->FormatRatllaDaltBold1 = array(
					'family'     => $familyfont,
					'size'       => '8',
					'bold'       =>  true
		);
		$this->FormatRatllaDalt = array(
					'family'     => $familyfont,
					'size'       => '8',
					'bold'       =>  false
		);
		$this->FormatRatllaSenseRes = array(
					'family'     => $familyfont,
					'size'       => '8',
					'bold'       =>  false
		);
				
	}
	
	public function marm($any, $mes){
		
		if ($this->rol == 'sc') {
			$headers = [
					'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
				,   'Content-type'        => 'text/csv'
				,   'Content-Disposition' => 'attachment; filename=marm.csv'
				,   'Expires'             => '0'
				,   'Pragma'              => 'public'
			];

			
			$pagatspercebuts = "percebuts";
			$base = 2010-2000; // (??)
			$territori=99;
			$preus = new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base ,DB::connection());
	        $elslimits_1=$preus->limits_anys_preus("Percebuts",DB::connection(),$base);
			$elslimits_2=$preus->limits_anys_preus("Pagats",DB::connection(),$base);
			// Array ( [MAX] => 2009 [MIN] => 2005 [MMIN] => 1 [MMAX] => 1 ) 
			// Array ( [MAX] => 2009 [MIN] => 2005 [MMIN] => 1 [MMAX] => 1 )
			$any_curs = max($elslimits_1['MAX'],$elslimits_2['MAX']);
			$mes_curs = max($elslimits_1['MMAX'],$elslimits_2['MMAX']);
			// Estem situats en un període nou, sense preus?
			$PeriodeBuit=false;
			if ($preus->quants_preus_entrats_App($any,$mes,DB::connection(),$base)==0)
			 {  // No hi ha preus,  és que acabaem d'obrir un nou període.
				$PeriodeBuit=true;
			 }
			//
			// Tots els territoris validats?
			$actEstatSQL = strtoupper("select * from territori order by  CodiTerr ");
			$rows = DB::connection()->select($actEstatSQL); 
			$Estat_Act_Terrs = array();
			$totValidat=true;
			if (!$PeriodeBuit) {  ; } // ok
			else
			{ 
				if (count($rows)!=0) {
						$totValidat=true;
						foreach($rows as $rowOBJ){  
							$row = ((array) $rowOBJ); 
							if ($row['ESTAT']!='validat') { $totValidat=false; }  
						}
				}
			}
			if (!$totValidat)  
			{
			 return response()->json(['error' => 'El calendari no està complert.'],403);
			 die();
			} 
			
			$output = array();
			for ($p=1;$p<=2;$p++)
			{  
				if ($p==1) $taulapeus = 'preuspercebuts';
				else  $taulapeus = 'preuspagats';
				for ($t=1;$t<=4;$t++)
				{   if ($t==1) $territori = 8;
					if ($t==2) $territori = 17;
					if ($t==3) $territori = 25;
					if ($t==4) $territori = 43;
					
					$quin_prods_SQL  = strtoupper("SELECT CODIPROD FROM DESCRIPCIO_".$taulapeus." WHERE actiu".$base."='s' AND MAPA='s' ORDER BY CODIPROD");
					$rows_quin_prods = DB::connection()->select($quin_prods_SQL); 
					//echo "Error: ".$Conn->ErrorMsg()."<br>";
					$conjunt = "('999'";
					$quants = count($rows_quin_prods);
					foreach($rows_quin_prods as $rowOBJ){  
						$row_quin_prods = ((array) $rowOBJ);
					    $conjunt = $conjunt.",'".$row_quin_prods['CODIPROD']."'";
						
					}
					$conjunt .= ')';
					$sql_exp = strtoupper("select ".$taulapeus.".*, ROUND(".$taulapeus.".FINPREU*100) as FINPREUX100 from ".$taulapeus."  
					   where ANY = ".$any." and mes = ".$mes." and CodiTerr='".$territori."'  
					   and  codiprod in ".$conjunt."				   
					   order by CodiProd ");
					
					$rows_exp = DB::connection()->select($sql_exp);
					if (count($rows_exp)>0) {
						foreach($rows_exp as $rowOBJ){  
							$row_elProd = ((array) $rowOBJ);
							$fila = array("".sprintf('%02d',$row_elProd['CODITERR']),
											($row_elProd['ANY']),
											sprintf('%02d',$row_elProd['MES']),
											("".$row_elProd['CODIPROD']),
											($row_elProd['FINPREUX100']));
							$output[]=$fila;
						}
					}
					
					
					
					
				}
		
			}
			$list = $output;

			# add headers for each column in the CSV download
			array_unshift($list, array_keys($list[0]));

		   

			return Response::stream($this->callback, 200, $headers);
		} else {
			return response()->json(['error' => 'Not authorized.'],403);
		}
	}
	
	public function excel_historicTots($anyinici, $anyfi, $tipuspreus, $actuals='no',$format) {
                                //excel_historicTots/{anyinici}/{anyfi}/{tipuspreus}/{actuals}/{format}
								// 2016**2017**percebuts**si**xls**

			$format='xls';
			if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
			} else {
				
				
				$mimetype = ($format=='xls')?'application/xls':'text/csv';
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => $mimetype
					,   'Content-Disposition' => 'attachment; filename=excel_historicTots.'.$format
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				
				
			
			$Inc_Ara = strtolower($actuals[0]); 
			$base = config('ppp.base')-2000;
			$salaris = false;
			$pagatspercebuts=$tipuspreus;
			if ($pagatspercebuts=='salaris')
			{
				$pagatspercebuts = 'pagats';
				$salaris = true;
			}
			
			
			
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$any = $aplicacio->ENCURS_ANY_DADES;
			$mes = $aplicacio->ENCURS_MES_DADES;
			$preus = new \App\Library\Prices($pagatspercebuts,$anyinici, 1, 8, $base ,DB::connection());
			$limits_anys_preus=$preus->limits_anys_preus($tipuspreus,DB::connection(),$base);
			$anyiniciAbs=$limits_anys_preus['MIN'];
			$mesiniciAbs=$limits_anys_preus['MMIN'];
			$anyfiAbs=$limits_anys_preus['MAX'];
			$mesfiAbs=$limits_anys_preus['MMAX'];
			$mesinici=1;
			$mesfi=12;
			if ($anyinici<$anyiniciAbs) $anyinici=$anyiniciAbs;
			if ($anyfi>$anyfiAbs) $anyfi=$anyfiAbs;
			
			if (($anyfi>$any) or ( ($anyfi==$any) and ($mesfi>=$mes) ) )
			{ // Final d'exportació major o igual al "Període de treball actual"
				if ($Inc_Ara)
				{  $mesfi=$mes;
				$anyfi=$any;
				}
				else
				{
					$mesfi=$mes-1;
					if ($mesfi==0) { $anyfi=$any-1; $mesfi=12; }
					else $anyfi=$any;
				}
			}
			$excel=new \App\Library\ExcelWriter();
			$linia=array("Codi","Grup","Producte","Unitats","Any","Gen","Feb","Mar","Abr","Mai","Jun","Jul","Ago","Set","Oct","Nov","Des","Anual");
			//$output = $excel->ferCSV($linia);
			$output = array();
			$output[]=$linia;
			$preus = new \App\Library\Prices($pagatspercebuts,$any,$mes,99,$base,DB::connection());
			if (!$salaris) $SQL_elsProds = "SELECT *  FROM ".$preus->descripcioTable." where GRUP<>'SALARIS' and actiu".$base." = 's' and publicar = 's'";
			else $SQL_elsProds = "SELECT *  FROM ".$preus->descripcioTable."  where GRUP='SALARIS' and actiu".$base." = 's'  ORDER BY CodiProd";
			$SQL_elsProds = strtoupper($SQL_elsProds);
			$rows_elsProds = DB::connection()->select($SQL_elsProds);     
			$elsProdsTots=array();
			foreach($rows_elsProds as $rowOBJ){                
				    $row_elsProds = ((array) $rowOBJ);
					$elsProdsTots[$row_elsProds['CODIPROD']]=$row_elsProds;
					$elsProdsTots[$row_elsProds['CODIPROD']]['nivell']=0;
			}
			/*
			 Array ( [001100] => Array ( [CodiProd] => 001100 [NomProd] => Blat tou o semidur [Unitat] => €/100 kg [Subgrup] => Blat [Grup] => CEREALS [FaseIntercanvi] => De productor a majorista [CondicionsComercialitzacio] => A granel, s/magatzem agricultor [NecPreus] => 3 [MaxPreus] => 3 [Calcul] => M [DescPreu1] => al dia 5 [DescPreu2] => al dia 15 [DescPreu3] => al dia 25 [nivell] => 0 )
			 [002100] => Array ( [CodiProd] => 002100 [NomProd] => Arròs closca rodó i semillarg [Unitat] => €/100 kg [Subgrup] => Arròs [Grup] => CEREALS [FaseIntercanvi] => De productor a indústria [CondicionsComercialitzacio] => A granel, s/magatzem agricultor [NecPreus] => 3 [MaxPreus] => 3 [Calcul] => M [DescPreu1] => al dia 5 [DescPreu2] => al dia 15 [DescPreu3] => al dia 25 [nivell] => 0 )
			 [002200] => Array ( [CodiProd] => 002200 [NomProd] => Arròs closca llarg [Unitat] => €/100 kg [Subgrup] => Arròs [Grup] => CEREALS [FaseIntercanvi] => De productor a indústria [CondicionsComercialitzacio] => A granel, s/magatzem agricultor [NecPreus] => 3 [MaxPreus] => 3 [Calcul] => M [DescPreu1] => al dia 5 [DescPreu2] => al dia 15 [DescPreu3] => al dia 25 [nivell] => 0 )
			 [003300] => Array ( [CodiProd] => 003300 [NomProd] => Ordi per a pinso [Unitat] => €/100 kg [Subgrup] => Ordi [Grup] => CEREALS [FaseIntercanvi] => De productor a majorista [CondicionsComercialitzacio] => A granel, s/magatzem agricultor [NecPreus] => 3 [MaxPreus] => 3 [Calcul] => M [DescPreu1] => al dia 5 [DescPreu2] => al dia 15 [DescPreu3] => al dia 25 [nivell] => 0 )
			 .
			 .
			 .
			 */
			$CalculsExcel = new \App\Library\Excel10(DB::connection(),$pagatspercebuts,$base);
			// -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------  -----------
			$array_x_excel = array();
			
			if ($pagatspercebuts=='percebuts') $estructuraSortida=$CalculsExcel->estructuraSortidaPerPreus;
			else // pagats
			{
				if (!$salaris) $estructuraSortida=$result = array_diff_key(($CalculsExcel->estructuraSortidaPag),
				($CalculsExcel->estructuraSortidaPagSalaris));
				else $estructuraSortida=$CalculsExcel->estructuraSortidaPagSalaris;
			}
			/*
			 Array ( [001100] =>
			 [001000] => Array ( [0] => 001100 )
			 [002100] =>
			 [002200] =>
			 [002000] => Array ( [0] => 002100 [1] => 002200 )
			 .
			 .
			 .
			 */
			// echo "Inici loop estructuraSortida ".date("H:i:s")."\r\n";  
			while (list ($CodiProd, $infoagregat) = each ($estructuraSortida))
			{  
				// echo "Inici iteració estructuraSortida ".date("H:i:s")."\r\n";  
				if (!isset($CalculsExcel->qFisiquesRed[$CodiProd])) {  $CalculsExcel->qFisiquesRed[$CodiProd] = array('like'=>null, 'notLike'=>null, 'DESCRIPCIO'=>'?'); }
				if (($pagatspercebuts=='pagats') and (!array_key_exists($CodiProd,$elsProdsTots))) continue;
				// els pagats es regeixen pel SELECT, els percebuts no, perquè hi ha agrupacions ....
				if (!is_array($infoagregat))
				{    // producte NO agregat
					$baseCalMesosPreuSino=$preus->baseCalMesosPreuSino($CodiProd);  // quins mesos hi ha preu
					// Array ( [m1] => 1 [m2] => 1 [m3] => 1 [m4] => 1 [m5] => 1 [m7] => 1 [m8] => 1 [m9] => 1 [m10] => 1 [m11] => 1 [m12] => 1 )

					$preusAnuals=$preus->mitjanaAnual($CodiProd,$anyinici,$anyfi);

					// [2004] => Array ( [Cat] => 40.311666666667 [8] => 42.29 [17] => 31.033333333333 [25] => 42.65 [43] => 45.273333333333 )
					// [2005] => Array ( [Cat] => 42.55 [8] => 43.6 [17] => 37.6 [25] => 42 [43] => 47 ) [2006] => Array ( [Cat] => 40.5775 [8] => 44.06 [17] => 31.25 [25] => 42 [43] => 45 ) )
					
					$infoProd=$elsProdsTots[$CodiProd];

					// Array ( [NomProd] => Arròs closca rodó i semillarg [Unitat] => €/100 kg [Subgrup] => Arròs [Grup] => CEREALS [FaseIntercanvi] => De productor a indústria [CondicionsComercialitzacio] => A granel, s/magatzem agricultor )
					//$elsPreusHist = $preus->HistoricPrices2Arr($CodiProd,$anyinici,$anyfi,$mesinici,$mesfi,$territVeure,$dadesmes,$preusAnuals,$anuals);

					$elsPreusHist = $preus->HistoricPrices2ArrCat($CodiProd,$anyinici,$anyfi,$mesinici,$mesfi);
					//if ($CodiProd=='071600') { print_r($elsPreusHist); die(); }
					/*
					 $elsPreusHist:
					 -------------
					 Array (
					 [0] => Array ( [0] => Any [1] => Mes [2] => Catalunya )
					 [1] => Array ( [0] => 2001 [1] => 1 [2] => 14.30 )
					 [2] => Array ( [0] => 2001 [1] => 2 [2] => 14.09 )
					 [3] => Array ( [0] => 2001 [1] => 3 [2] => 13.78 )
					 .
					 .
					 .
					 [23] => Array ( [0] => 2002 [1] => 11 [2] => 13.06 )
					 [24] => Array ( [0] => 2002 [1] => 12 [2] => 13.26 )
					 [25] => Array ( [0] => 2003 [1] => 1 [2] => 13.43 )
					 .
					 .
					 .
					 [35] => Array ( [0] => 2003 [1] => 11 [2] => 17.22 )
					 [36] => Array ( [0] => 2003 [1] => 12 [2] => 17.52 )
					 .
					 .
					 )
					 */
					$elsPreusHist_bis=array();
					reset($elsPreusHist);
					list ($lin, $arrPreusAny) = each ($elsPreusHist); // capçalera

					while (list ($lin, $arrPreusAny) = each ($elsPreusHist))
					{ $elsPreusHist_bis[$arrPreusAny[0]][$arrPreusAny[1]]=$arrPreusAny[2];
					}

					/*
					 Array ( [2000] => Array ( [1] => 14.68 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 13.94 [6] => 12.92 [7] => 12.54 [8] => 12.85 [9] => 13.36 [10] => 13.47 [11] => 13.97 [12] => 14.29 )
					 [2001] => Array ( [1] => 14.30 [2] => 14.09 [3] => 13.78 [4] => 13.55 [5] => 13.99 [6] => 14.79 [7] => 14.41 [8] => 14.84 [9] => 15.31 [10] => 14.96 [11] => 15.35 [12] => 15.73 )
					 [2002] => Array ( [1] => 16.15 [2] => 15.40 [3] => 15.05 [4] => 14.25 [5] => 14.32 [6] => 12.87 [7] => 12.47 [8] => 12.49 [9] => 12.78 [10] => 12.72 [11] => 13.06 [12] => 13.26 )
					 .
					 .
					 */
					for ($lany=$anyinici;$lany<=$anyfi;$lany++)
					{   
						// echo "Inici iteració any ($lany) per array_x_excel ".date("H:i:s")."\r\n";   
						$linia=array("".$CodiProd,$infoProd['GRUP'],$infoProd['NOMPROD'],$infoProd['UNITAT'],$lany);
						for ($m=1;$m<=12;$m++)
						{       
								if ( ($lany==$anyfiAbs) and ($m>$mesfiAbs) ) $elsPreusHist_bis[$lany][$m]='';
								elseif ( ($lany==$anyfiAbs) and ($m>$mesfi) ) $elsPreusHist_bis[$lany][$m]='';
								elseif ( ($lany==$anyiniciAbs) and ($m<$mesiniciAbs) ) $elsPreusHist_bis[$lany][$m]='';
								else 
								{
									if ($elsPreusHist_bis[$lany][$m]==0)
									{ // no hi ha preu
										if ($baseCalMesosPreuSino['M'.$m]==1) $elsPreusHist_bis[$lany][$m]='?';
										else $elsPreusHist_bis[$lany][$m]='-';
									}
									else
									{  // Tot ok
									}
								}

								array_push($linia,$elsPreusHist_bis[$lany][$m]);
								array_merge(array($CodiProd),$linia);
						 } 
						/*
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2000 [4] => 14.68 [5] => 14.72 [6] => 14.72 [7] => 15.03 [8] => 13.94 [9] => 12.92 [10] => 12.54 [11] => 12.85 [12] => 13.36 [13] => 13.47 [14] => 13.97 [15] => 14.29 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2001 [4] => 14.30 [5] => 14.09 [6] => 13.78 [7] => 13.55 [8] => 13.99 [9] => 14.79 [10] => 14.41 [11] => 14.84 [12] => 15.31 [13] => 14.96 [14] => 15.35 [15] => 15.73 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2002 [4] => 16.15 [5] => 15.40 [6] => 15.05 [7] => 14.25 [8] => 14.32 [9] => 12.87 [10] => 12.47 [11] => 12.49 [12] => 12.78 [13] => 12.72 [14] => 13.06 [15] => 13.26 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2003 [4] => 13.43 [5] => 13.15 [6] => 12.94 [7] => 12.99 [8] => 13.68 [9] => 13.05 [10] => 12.97 [11] => 13.47 [12] => 14.76 [13] => 15.25 [14] => 17.22 [15] => 17.52 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2004 [4] => 17.90 [5] => 17.74 [6] => 17.22 [7] => 17.36 [8] => 18.08 [9] => 14.12 [10] => 13.31 [11] => 13.44 [12] => 13.23 [13] => 13.08 [14] => 13.28 [15] => 13.56 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2005 [4] => 13.50 [5] => 13.43 [6] => 13.52 [7] => 13.49 [8] => 13.68 [9] => 13.52 [10] => 13.35 [11] => 13.13 [12] => 13.26 [13] => 13.30 [14] => 13.71 [15] => 13.99 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2006 [4] => 13.96 [5] => 13.82 [6] => 13.55 [7] => 13.41 [8] => 13.78 [9] => 13.35 [10] => 13.09 [11] => 13.69 [12] => 14.99 [13] => 16.31 [14] => 17.05 [15] => 17.26 )
						 Array ( [0] => Cereals [1] => Blat [2] => €/100Kg [3] => 2007 [4] => 17.48 [5] => 17.44 [6] => 17.41 [7] => 17.57 [8] => 18.18 [9] => [10] => [11] => [12] => [13] => [14] => [15] => )
						 */

						// durant l'any en curs ... no hi ha preu CAT
						if ( ($lany<$anyfiAbs) ) array_push($linia,number_format($preusAnuals[$lany]['Cat'],10, '.', ''));
						else array_push($linia,'');
						//$output .= $excel->ferCSV($linia,true);
						$output[]=$linia;
						$recorda_ho[$CodiProd][$lany]=$linia;
						/*
						 */
						$array_x_excel[]=$linia;
						// echo "FI iteració any ($lany) per array_x_excel ".date("H:i:s")."\r\n";  
					}
				}
				else
				{  // producte SI agregat

					/*
					 $CodiProd:'002000')
					 $recorda_ho		 :
					 Array (
					 [002100] => Array (
					 [2000] => Array ( [0] => CEREALS [1] => Arròs closca rodó i semillarg [2] => €/100 kg [3] => 2000 [4] => 27.05 [5] => 28.07 [6] => 28.85 [7] => 28.85 [8] => 28.85 [9] => 28.25 [10] => 28.25 [11] => - [12] => - [13] => 27.55 [14] => 28.25 [15] => 28.25 [16] => 28.16 )
					 [2001] => Array ( [0] => CEREALS [1] => Arròs closca rodó i semillarg [2] => €/100 kg [3] => 2001 [4] => 28.85 [5] => 28.85 [6] => 28.85 [7] => 28.55 [8] => 28.55 [9] => 28.55 [10] => 28.55 [11] => - [12] => - [13] => 28.56 [14] => 29.45 [15] => 29.45 [16] => 28.85 )
					 [2002] => Array ( [0] => CEREALS [1] => Arròs closca rodó i semillarg [2] => €/100 kg [3] => 2002 [4] => 29.45 [5] => 29.45 [6] => 29.45 [7] => 29.45 [8] => 29.45 [9] => 29.45 [10] => 29.45 [11] => - [12] => - [13] => 29.00 [14] => 28.31 [15] => 28.26 [16] => 29.20 )
					 .
					 .
					 )
					 [002200] => Array (
					 [2000] => Array ( [0] => CEREALS [1] => Arròs closca llarg [2] => €/100 kg [3] => 2000 [4] => 27.65 [5] => 27.65 [6] => 27.65 [7] => - [8] => - [9] => - [10] => - [11] => - [12] => - [13] => - [14] => 28.85 [15] => 28.85 [16] => 28.13 )
					 [2001] => Array ( [0] => CEREALS [1] => Arròs closca llarg [2] => €/100 kg [3] => 2001 [4] => 30.05 [5] => 30.05 [6] => 30.05 [7] => - [8] => - [9] => - [10] => - [11] => - [12] => - [13] => - [14] => 30.05 [15] => 30.05 [16] => 30.05 )
					 [2002] => Array ( [0] => CEREALS [1] => Arròs closca llarg [2] => €/100 kg [3] => 2002 [4] => 30.05 [5] => 30.05 [6] => 30.05 [7] => - [8] => - [9] => - [10] => - [11] => - [12] => - [13] => - [14] => 28.84 [15] => 28.84 [16] => 29.57 )
					 [2003] => Array ( [0] => CEREALS [1] => Arròs closca llarg [2] => €/100 kg [3] => 2003 [4] => 28.80 [5] => 28.80 [6] => 28.80 [7] => - [8] => - [9] => - [10] => - [11] => - [12] => - [13] => - [14] => 29.40 [15] => 27.50 [16] => 28.66 )
					 .
					 .
					 )
					 )
					 */
					// calculem el preu del prod. agregat.
					// suma(preu * qFisica) / suma(qFisiques)
					reset($infoagregat);
					$sumaqf=array();
					for ($m=1;$m<=13;$m++)
					{  $sumaqf[$m]=0;
					}
					while (list (, $uncodi) = each ($infoagregat))
					{
						// echo "Inici iteració infoagregat ".date("H:i:s")."\r\n";  
						$descripcio='';
						$qfRes = $CalculsExcel->QFisiquesSumBasePercebuts($CalculsExcel->baseTable, $uncodi, $descripcio);
						$qf[$uncodi]=$qfRes[$uncodi];
						/*
						 Array ( [002100] => Array ( [SUM(MES1)] => 24029 [SUM(MES2)] => 12014 [SUM(MES3)] => 12014 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 13240 [SUM(MES12)] => 12423 [DESCRIPCIO] => [ANUAL] => 124227 ) )
						 */

						for ($m=1;$m<=13;$m++)
						{  if ($m==13) $indexV='ANUAL';
							else $indexV='SUM(MES'.$m.')';
							$sumaqf[$m]+=$qf[$uncodi][$indexV];
						}
						// echo "Fi iteració infoagregat ".date("H:i:s")."\r\n";  
					}

					for ($lany=$anyinici;$lany<=$anyfi;$lany++)
					{
						// echo "Inici iteració any ($lany) ".date("H:i:s")."\r\n";  
						for ($m=1;$m<=13;$m++)
						{
							$sumaprod = 0;
							if ($m==13) $indexV='ANUAL';
							else $indexV='SUM(MES'.$m.')';
							reset($infoagregat);
							while (list (, $uncodi) = each ($infoagregat))
							{
								$sumaprod += $qf[$uncodi][$indexV]*$recorda_ho[$uncodi][$lany][$m+4];
							}
							
							
							// $preusAgr ---> pel CSV
							// $preusAgr1 --> per l'Excel				
							if ( ($lany==$anyfiAbs) and ($m==13) ) $preusAgr[$m]='';  // En l'any en curs no hi ha preu CAT
							elseif ( ($lany==$anyfiAbs) and ($m>$mesfiAbs) ) $preusAgr[$m]='';  // En l'any en curs no hi ha preu en els mesos en curs i següents
							elseif ($sumaqf[$m]==0) $preusAgr[$m]='-';
							else $preusAgr[$m]=number_format(floatval($sumaprod)/floatval($sumaqf[$m]), 10, ',', '');
							if ( ($lany==$anyfiAbs) and ($m==13) ) $preusAgr1[$m]='';  // En l'any en curs no hi ha preu CAT
							elseif ( ($lany==$anyfiAbs) and ($m>=$mesfiAbs) ) $preusAgr1[$m]='';  // En l'any en curs no hi ha preu en els mesos en curs i següents
							elseif ($sumaqf[$m]==0) $preusAgr1[$m]='-';
							else $preusAgr1[$m]=number_format(floatval($sumaprod)/floatval($sumaqf[$m]), 10, '.', '');

						}

						// $preusAgr:  Array ( [1] => 14.68 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 13.94 [6] => 12.92 [7] => 12.54 [8] => 12.85 [9] => 13.36 [10] => 13.47 [11] => 13.97 [12] => 14.29 [13] => 13.24 )
						$infoProd = $preus->infoProd($estructuraSortida[$CodiProd][0]);
						// Array ( [CODIPROD] => 001100 [NOMPROD] => Blat tou o semidur [UNITAT] => eur/100 kg [SUBGRUP] => Blat [GRUP] => CEREALS [FASEINTERCANVI] => De productor a majorista [CONDICIONSCOMERCIALITZACIO] => A granel, s/magatzem agricultor )
						//$output .= $excel->ferCSV(array_merge(array("'".$CodiProd,$infoProd['GRUP'],$CalculsExcel->qFisiquesRed[$CodiProd]['DESCRIPCIO'],$infoProd['UNITAT'],$lany),$preusAgr),true);
						
						$output[]=array_merge(array("'".$CodiProd,$infoProd['GRUP'],$CalculsExcel->qFisiquesRed[$CodiProd]['DESCRIPCIO'],$infoProd['UNITAT'],$lany),$preusAgr);
						$array_x_excel[]=array_merge(array($CodiProd,$infoProd['GRUP'],$CalculsExcel->qFisiquesRed[$CodiProd]['DESCRIPCIO'],$infoProd['UNITAT'],$lany),$preusAgr1);
						// echo "Fi iteració any ($lany) ".date("H:i:s")."\r\n";  
					}

					$recorda_ho=array(); // reset!
					$qf=array(); // reset!
				}
				// echo "Fi iteració estructuraSortida ".date("H:i:s")."\r\n";  
			}
			// echo "Fi loop estructuraSortida ".date("H:i:s")."\r\n";  
			$this->list = $output;
			array_unshift($this->list, array_keys($this->list[0]));
			if ($format == 'csv') {
				return Response::stream($this->callback, 200, $headers);
				die();
			} else {
				
				if ($salaris) $titolSheet='Salaris pagats';
				elseif ($pagatspercebuts == 'pagats') $titolSheet='Preus pagats';
				elseif ($pagatspercebuts == 'percebuts') $titolSheet='Preus percebuts';
				
				
							
				if ($pagatspercebuts=='percebuts')
				{ 
					$titol1 = 'Preus percebuts pel pagès. Catalunya '.$anyinici.'-'.$anyfi.' ';
					$titol2 = "Font: Gabinet Tècnic";
					$titol3 = 'Base 20'.$base;
					$titol4 = "Durant el 2016 s'ha procedit a una millora en el procés d'obtenció de la informació de preus. Per més detall, consulteu la Nota Metodològica d'aquest document.";
					$info6 = "PREUS PERCEBUTS";
				}
				elseif ($pagatspercebuts=='pagats')
				{   
					if (!$salaris)
					{  	  
						$titol1  = 'Preus pagats pel pagès. Catalunya '.$anyinici.'-'.$anyfi.' ';
						$titol2  = "Font: Gabinet Tècnic";
						$titol3  = 'Base 20'.$base;
						$titol4  = "Durant el 2016 s'ha procedit a una millora en el procés d'obtenció de la informació de preus. Per més detall, consulteu la Nota Metodològica d'aquest document.";
						$info6  = "PREUS PAGATS";
					}
					else
					{  	  
						$titol1 = "Salaris pagats per l'empresari agrari. Catalunya ".$anyinici.'-'.$anyfi.' ';
						$titol2 = "Font: Gabinet Tècnic";
						$titol3 = 'Base 20'.$base;
						$titol4 = "Durant el 2016 s'ha procedit a una millora en el procés d'obtenció de la informació de preus. Per més detall, consulteu la Nota Metodològica d'aquest document.";
						$info6 = "SALARIS PAGATS";

					}
				}
				$this->configurarestilsexcel();
				reset($array_x_excel);
				$mes = array();
				$grup_anterior = '';
				$producte_anterior = '';
				$codi_anterior = '';
				$linia_sobre = '0';
				$ferunitats = '0';
				ExcelMaat::create($pagatspercebuts, function($excel) use(
							$titol1,
							$titol2,
							$titol3,
							$titol4,
							$info6,
							$array_x_excel
						)
				{
						$excel->sheet('NOTA METODOLÒGICA', function($sheet) use(
							$titol1,
							$titol2,
							$titol3,
							$titol4,
							$info6,
							$array_x_excel
						){
							$sheet->setStyle($this->general);
							// Set top, right, bottom, left
							$sheet->setPageMargin(array(
								1.50, 1.50, 1.20, 1.50
							));
							$sheet->setWidth(array(
								'A'     =>  2,
								'B'     =>  1.29,
								'C'		=>  5.29
							));
							$sheet->cells('B2:L31', function($cells) {
									$cells->setBorder('thin', 'thin', 'thin', 'thin');  // Set all borders (top, right, bottom, left)
							});
							$objDrawing = new PHPExcel_Worksheet_Drawing;
							$objDrawing->setPath(base_path('www/UI/assets/ppp_imgs/gencat/logo.bmp'));
							$objDrawing->setCoordinates('C3');
							$objDrawing->setWorksheet($sheet);
							$sheet->cells('D3', function($cells) { 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue('Generalitat de Catalunya'); 
							});
							$sheet->cells('D4', function($cells) { 
								$cells->setFont($this->inf1);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue("Departament d'Agricultura"); 
							});
							$sheet->cells('D5', function($cells) { 
								$cells->setFont($this->inf1);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue("Ramaderia, Pesca i Alimentació"); 
							});
							$sheet->cells('D6', function($cells) { 
								$cells->setFont($this->inf2);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue("Secretaria General"); 
							});
							$sheet->cells('D7', function($cells) { 
								$cells->setFont($this->titleFormat4);
								$cells->setAlignment('left');
								$cells->setFontFamily('Arial');
								$cells->setValue("Gabinet Tècnic"); 
							});
							$sheet->cells('C11', function($cells) use($titol1){ 
								$cells->setFont($this->inf1);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($titol1); 
							});
							$sheet->cells('C13', function($cells) { 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue("NOTA METODOLÒGICA"); 
							});
							$info = "Durant l’any 2016 s’ha procedit a una millora en el procés d’obtenció de la informació de preus.";
							$info1 = "Aquesta millora ha consistit en la selecció dels centres d’informació més rellevants";
							$info2="(llotges, grans venedors, grans compradors) i en una definició més acurada i representativa";
							$info3="dels productes estàndards a informar. Aquest fet provoca indefectiblement un petit";
							$info4="trencament de les series estadístiques, la justificació  del qual prové de la millora de la";
							$info5="qualitat informativa assolida.";
							$sheet->cells('C15', function($cells) use($info){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info); 
							});
							$sheet->cells('C16', function($cells) use($info1){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info1); 
							});
							$sheet->cells('C17', function($cells) use($info2){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info2); 
							});
							$sheet->cells('C18', function($cells) use($info3){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info3); 
							});
							$sheet->cells('C19', function($cells) use($info4){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info4); 
							});
							$sheet->cells('C20', function($cells) use($info5){ 
								$cells->setFont($this->inf);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info5); 
							});
							$sheet->cells('C22', function($cells) use($info6){ 
								$cells->setFont($this->titleFormat2);
								$cells->setFontFamily('Arial');
								$cells->setAlignment('left');
								$cells->setValue($info6); 
							});

							//
							$f = 23;
							$c  = 'D';
							$grup_anterior='';
							foreach ($array_x_excel as $linia_array)
							{	
									$grup = $linia_array[1];
									if(($grup != $grup_anterior))
									{
									  $sheet->cells($c.$f, function($cells)  use ($grup){ 
										$cells->setFont($this->titleFormat2);
										$cells->setFontFamily('Arial');
										$cells->setAlignment('left');
										$cells->setValue($grup); 
									  });
									  $f++;
									  if($f == 30){$f = 23;$c='G';}
									}
									$grup_anterior = $grup;
							}
						});
						
						//
						$colNames   = array('Codi','Producte','Any');
						$colNames1  = array('Gen.','Feb.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Oct.','Nov.','Des.','Anual');
						$colBorder  = array('','','','','','','','','','','');
						$colBorder1 = array('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
						$colBorder2 = array('','','','','','','','','','','','','','','','');
						//
						$grup_anterior = '';
						$pag= 1;
						$liniaFinal = 0;
						$this->SheetsArray = array();
						$codi_anterior='';
						$codi='';
						$producte_anterior = '';
						$producte = '';
						$cols = range('A', 'Z');
						foreach ($array_x_excel as $linia_array)  
						{ 
										$codi = $linia_array[0];
										$grup = $linia_array[1];
										$producte = $linia_array[2];
										$unitats = $linia_array[3];
										$any = $linia_array[4];
										$anual = array_pop($linia_array);

										array_shift($linia_array);
										array_shift($linia_array);
										array_shift($linia_array);
										array_shift($linia_array);
										array_shift($linia_array);
										if(($grup != $grup_anterior)) {
											if($liniaFinal == 1) {
													// $cart->writeRow($currentRow,0,$colBorder2,$FormatRatllaDalt);
											}
											$excel->sheet($grup, function($sheet) use(
												$grup,
												$titol1,
												$titol2,
												$titol3,
												$titol4,
												$colNames,
												$colNames1
											){
												$this->SheetsArray[$grup]=$sheet;
												$sheet->setWidth(array(
													'A'     =>  7.29,
													'B'     =>  33,
													'C'		=>  7.29,
													'D'		=>  7.29,
													'E'		=>  7.29,
													'F'		=>  7.29,
													'G'		=>  7.29,
													'H'		=>  7.29,
													'I'		=>  7.29,
													'J'		=>  7.29,
													'K'		=>  7.29,
													'L'		=>  7.29,
													'M'		=>  7.29,
													'N'		=>  7.29,
													'O'		=>  7.29,
													'P'		=>  7.29
												));
												$sheet->cells('A5:P5', function($cells) {
														$cells->setBorder('thin', 'none', 'thin', 'none');  // Set all borders (top, right, bottom, left)
												});
												$sheet->cells('A1', function($cells) use($titol1){ 
													$cells->setFont($this->titleFormat);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue($titol1); 
												});
												$sheet->cells('A2', function($cells) use($grup){ 
													$cells->setFont($this->titleFormat1);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue(ucfirst(strtolower($grup))); 
												});
												$sheet->cells('A3', function($cells) use($titol2){ 
													$cells->setFont($this->titleFormat2);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue($titol2); 
												});
												$sheet->cells('A4', function($cells) use($titol4){ 
													$cells->setFont($this->titleFormat4);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue($titol4); 
												});
												$sheet->cells('O3', function($cells) use($titol3){ 
													$cells->setFont($this->titleFormat3);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue($titol3); 
												});
												
												$ix=0;
												$cols = range('A', 'Z');
												foreach($colNames as $name){
													$sheet->cells($cols[$ix].'5', function($cells) use($name){ 
														$cells->setFont($this->FormatRatllaDaltBold1);
														$cells->setFontFamily('Arial');
														$cells->setAlignment('right');
														$cells->setValue($name); 
													});
													$ix++;
												}
												foreach($colNames1 as $name){
													$sheet->cells($cols[$ix].'5', function($cells) use($name){ 
														$cells->setFont($this->FormatRatllaDaltBold);
														$cells->setFontFamily('Arial');
														$cells->setAlignment('right');
														$cells->setValue($name); 
													});
													$ix++;
												}
												
											});
											$currentRow = 5;
											$primer = true;
											$liniaFinal = 1;
											
										}
										else{
											//$cart->write($currentRow,0,'',$FormatRatllaSenseRes);
											$primer = false;
											
										}
										$grup_anterior = $grup;
										//
										if($codi != $codi_anterior) { 			
											$this->SheetsArray[$grup]->cells('A'.($currentRow+1), function($cells) use ($codi){ 
													$cells->setFont($this->FormatRatllaDalt);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('left');
													$cells->setValue($codi); 
													$cells->setBorder('solid', 'none', 'none', 'none');
												});
										} else {
											// $cart->writeString($currentRow,0,$codi,$FormatRatllaDalt);
										}
										$codi_anterior=$codi;
										if($producte != $producte_anterior){
											$linia_sobre = '0';
											$this->SheetsArray[$grup]->cells('B'.($currentRow+1), function($cells) use ($producte){ 
													$cells->setFont($this->FormatRatllaDalt);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('right');
													$cells->setValue($producte); 
													$cells->setBorder('solid', 'none', 'none', 'none');
												});
											$productes_iguals = '1';
											$ferunitats = '0';
										} else {
											$linia_sobre = '1';
											if ($productes_iguals ==1 && $ferunitats != '1')
											{
												$this->SheetsArray[$grup]->cells('B'.($currentRow+1), function($cells) use ($unitats){ 
													$cells->setFont($this->FormatRatllaSenseRes);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('right');
													$cells->setValue(str_replace('eur','€',$unitats)); 
												});
												$ferunitats ='1';
											}
											else
											{
												//$cart->write($currentRow,1,'',$FormatRatllaSenseRes);
											}
										}
										$producte_anterior=$producte;
										$pag++;
										
										if($linia_sobre == '0') {
											$this->SheetsArray[$grup]->cells('A'.($currentRow+1).':P'.($currentRow+1), function($cells) {
												$cells->setBorder('thin', 'none', 'none', 'none');  // (top, right, bottom, left)
											});
											$this->SheetsArray[$grup]->cells('C'.($currentRow+1), function($cells) use ($any){ 
													$cells->setFont($this->FormatRatllaDalt);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('right');
													$cells->setValue($any); 
												});
											for ($m = 1; $m <= 13; $m++) {
												if (isset($linia_array[($m-1)])) {
													$this->SheetsArray[$grup]->setCellValueByColumnAndRow(3+$m,($currentRow+1).'',$linia_array[($m-1)]);	
												}
											}
										} else {
											$this->SheetsArray[$grup]->cells('C'.($currentRow+1), function($cells) use ($any){ 
													$cells->setFont($this->FormatRatllaDalt);
													$cells->setFontFamily('Arial');
													$cells->setAlignment('right');
													$cells->setValue($any); 
												});
											for ($m = 1; $m <= 13; $m++) {
												if (isset($linia_array[($m-1)])) {
													$this->SheetsArray[$grup]->setCellValueByColumnAndRow(3+$m,($currentRow+1).'',$linia_array[($m-1)]);	
												}
											}
										} 
										
										
										$currentRow++;
							
						}
						if($liniaFinal == 1) {
							//$cart->writeRow($currentRow,0,$colBorder2,$FormatRatllaDalt);
						}
						
						
				})->export('xls');
				die();
			} // format xls
			
		}
	}

    public function excel_indexCompMensAnual($anyinici, $anyfi, $actuals='no',$format) {
		
			if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
			} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/csv'
					,   'Content-Disposition' => 'attachment; filename=indexCompMensAnual_'.$anyinici.'_'.$anyfi.'.'.$format
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
			
				$Inc_Ara= strtolower($actuals[0]); 
				$base = config('ppp.base')-2000;
				$pagatspercebuts='percebuts';
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$preus = new \App\Library\Prices($pagatspercebuts,$anyinici, 1, 8, $base ,DB::connection());
				$limits_anys_preus=$preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
				$anyiniciAbs=$limits_anys_preus['MIN'];
				$mesiniciAbs=$limits_anys_preus['MMIN'];
				$anyfiAbs=$limits_anys_preus['MAX'];
				$mesfiAbs=$limits_anys_preus['MMAX'];
				$any_sessio = $aplicacio->ENCURS_ANY_DADES;
				$mes_sessio = $aplicacio->ENCURS_MES_DADES;
				if ($preus->quants_preus_entrats_App($any_sessio,$mes_sessio,DB::connection(),$base)==0)
				 {  if (($any_sessio>=$anyfiAbs) or ( ($any_sessio=$anyfiAbs) and ($mes_sessio>=$mesfiAbs) ))
					{  $mesfiAbs++;
					   if ($mesfiAbs>=12) { $mesfiAbs=1; $anyfiAbs++; }
					}
				 }
				//
				$excel 			= new \App\Library\ExcelWriter();
				$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
				$CalculsExcel 	= $excelfactory->creaExcel(DB::connection(),$pagatspercebuts,$base);
				
				// 
				$avgCat=array();
				for ($any=$anyinici; $any<=$anyfi; $any++)
				{ 
				  $avgCat[$any] = $CalculsExcel->entradaPreusAnyT($any);
				  if ($base=='05')
				  { unset($avgCat[$any]['113200']); // Segalls no surten en l'índex Base 05
					unset($avgCat[$any]['221000']); // Suro no surten en l'índex Base 05    
				  }
				  /*
					[054210] => Array
					(
						[DESCRIPCIO] => Bledes
						[MES1] => 0
						[MES2] => 0
						[MES3] => 0
						[MES4] => 0
						[MES5] => 0
						[MES6] => 0
						[MES7] => 0
						[MES8] => 0
						[MES9] => 0
						[MES10] => 0
						[MES11] => 0
						[MES12] => 0
						[ANUAL] => 0
					)

				  */	
				  $avgCat_INI = $avgCat[$any];
				  $avgCat[$any] = array();
				  
				  while (list($elcodi, $val) = each($avgCat_INI)) 
					{  //print_r($val);
					   if ($val['MES1']+$val['MES2']+$val['MES3']+$val['MES4']+$val['MES5']+$val['MES6']+
						  $val['MES7']+$val['MES8']+$val['MES9']+$val['MES10']+$val['MES11']+$val['MES12']>0)
							   $avgCat[$any][$elcodi] = $val;
					}
				  

				
				
				  
				}	
			}
			
			/*
				print_r($avgCat[2006]);
				Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
													  [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 
													  [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
						   [002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg 
													   [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 
													   [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
						   [002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 
													   [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
						   [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
														[MES1] => 13.83 [MES2] => 13.37 [MES3] => 12.71 [MES4] => 12.3 [MES5] => 12.3 [MES6] => 12.28 [MES7] => 12.1 [MES8] => 12.52 
													   [MES9] => 13.62 [MES10] => 14.69 [MES11] => 15.67 [MES12] => 15.94 [ANUAL] => 13.28 ) 
							·
							·
							·
			*/
			$qFisiques = $CalculsExcel->qFisiques("basepercebuts".$base, $avgCat[$anyinici]);
			//print_r($qFisiques); die();
			/*
			Array ( [001100] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat tou o semidur [ANUAL] => 234804 [consist] => 001100 [PREUS_ANUAL] => 13.24 ) 
			[002100] => Array ( [SUM(MES1)] => 24029 [SUM(MES2)] => 12014 [SUM(MES3)] => 12014 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 13240 [SUM(MES12)] => 12423 [DESCRIPCIO] => Arròs closca rodó i semillarg [ANUAL] => 124227 [consist] => 002100 [PREUS_ANUAL] => 28.16 ) 
			[002200] => Array ( [SUM(MES1)] => 1100 [SUM(MES2)] => 1100 [SUM(MES3)] => 1100 [SUM(MES4)] => 0 [SUM(MES5)] => 0 [SUM(MES6)] => 0 [SUM(MES7)] => 0 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 0 [SUM(MES11)] => 1100 [SUM(MES12)] => 1100 [DESCRIPCIO] => Arròs closca llarg [ANUAL] => 5500 [consist] => 002200 [PREUS_ANUAL] => 28.13 ) 
			.
			.
			.
			[001000] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat [ANUAL] => 234804 [consist] => Array ( [0] => 001100 ) [PREUS_ANUAL] => 13.24 ) 
			[002000] => Array ( [SUM(MES1)] => 25129 [SUM(MES2)] => 13114 [SUM(MES3)] => 13114 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 14340 [SUM(MES12)] => 13523 [DESCRIPCIO] => Arròs [ANUAL] => 129727 [consist] => Array ( [0] => 002100 [1] => 002200 ) [PREUS_ANUAL] => 28.1587280982 ) 
			[003000] => Array ( [SUM(MES1)] => 14855
			*/
			
			for ($any=$anyinici; $any<=$anyfi; $any++)
			{
			  $preusProductesAnyT[$any] = $CalculsExcel->preusProductesAnyT($avgCat[$any], $qFisiques);     
			}
			/*
			preusProductesAnyT[2006]
			Array ( 
			[001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
			[001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
			[002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
			[002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
			[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.9538354254 [MES12] => 23 [ANUAL] => 23.7532165239 ) 
			.
			.
			.
			.
			.
			.
			)
			*/
			
			$wValor = $CalculsExcel->wValor($qFisiques);
			for ($any=$anyinici; $any<=$anyfi; $any++)
			{ 
				$indexSimpleAnyT                   = $CalculsExcel->indexSimpleAnyT($qFisiques, $preusProductesAnyT[$any]);
				$indexCompostAnyT                  = $CalculsExcel->indexCompostAnyT($indexSimpleAnyT, $wValor);  
				$indexCompostAnyTGrups[$any]       = $CalculsExcel->indexCompostAnyTGrups($indexCompostAnyT, $wValor);
  
			}
			
			
			if (!$Inc_Ara)
			{
				if ($anyfi==$anyfiAbs)  // volem info de l'últim any en curs
				{   
					  if (is_array($indexCompostAnyTGrups[$anyfiAbs]))
					  {  while (list ( $elcodi,) = each ($indexCompostAnyTGrups[$anyfiAbs])) 
						  {    // $indexCompostAnyTGrups[$anyfiAbs]); 
							   // Array ( [000000] => Array ( [DESCRIPCIO] => CEREALS [MES1] => 2.01506001453 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 0 [MES12] => 0 [ANUAL] => 0.156222800368 ) 
							   for ($mesix=$mesfiAbs; $mesix<=12; $mesix++)
							   {  // Des del període actual fins final d'any .. valor en blanc
								  $indexCompostAnyTGrups[$anyfiAbs][$elcodi]['MES'.$mesix]='';
							   }    
							   $indexCompostAnyTGrups[$anyfiAbs][$elcodi]['ANUAL'] ='';   
						  }
						
					  }
				}
			}
			
			for ($mesix=$mesfiAbs; $mesix<=12; $mesix++)
			{
			  //$indexCompostAnyTGrups[$anyfiAbs][$elcodi]
			}
			
			$output='';
			$output .= $excel->ferCSV(array('Producte','Any',
						 'Gen.','Feb.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Oct.','Nov.','Des.','Anual'),true);
			$prods = array_keys($indexCompostAnyTGrups[$anyfi]);
			while (list ( ,$elcodi) = each ($prods)) 
			{  for ($any=$anyinici; $any<=$anyfi; $any++)
			   {  $linia = $indexCompostAnyTGrups[$any][$elcodi];
				  // Array ( [DESCRIPCIO] => CEREALS [MES1] => 100.917653112 [MES2] => 103.173490416 [MES3] => 103.70670268 [MES4] => 104.541804132 [MES5] => 102.782562398 [MES6] => 101.774227125 [MES7] => 94.3149774304 [MES8] => 96.7210858456 [MES9] => 102.488054837 [MES10] => 100.009079289 [MES11] => 101.076955818 [MES12] => 102.411428117 [ANUAL] => 100.000005681 )
				  $desc = array_shift($linia);
				  array_unshift($linia,$any);
				  // Array ( [0] => 2000 [MES1] => 100.917653112 [MES2] => 103.173490416 [MES3] => 103.70670268 [MES4] => 104.541804132 [MES5] => 102.782562398 [MES6] => 101.774227125 [MES7] => 94.3149774304 [MES8] => 96.7210858456 [MES9] => 102.488054837 [MES10] => 100.009079289 [MES11] => 101.076955818 [MES12] => 102.411428117 [ANUAL] => 100.000005681 )
				  array_unshift($linia,$desc);
				  $lalinia = array();
				  $lalinia1 = array();
				  reset($linia);
				  while (list ($index,$valor) = each($linia))
				  {  
					 if (strpos($index,'MES')===false)
					 {  if (strpos($index,'ANUAL')!==false)
						{       if ($valor==0) { $lalinia[]='-'; }
								else { 
									 $lalinia[]=number_format($valor,10,',',''); 
									 $lalinia1[]=number_format($valor,10,'.','');
									
								}
								
						}            
						else 
							 {  
							  $lalinia[]=$valor;
							  $lalinia1[]=$valor;
							 }
					 }
					 else  // valor mensual
					 {  $mes_en_curs = intval(str_replace('MES','',$index));
						
						if ($valor==0.0) {          	      
							   if (($any==$anyfiAbs) and ($mes_en_curs>=$mesfiAbs))
							   { $lalinia[]=''; 
								 $lalinia1[]='';
							   }
							   else 
							   { $lalinia[]='-'; 
								 $lalinia1[]='-';
							   }
						}
						else { 
							  $lalinia[]=number_format($valor,10,',',''); 
							  $lalinia1[]=number_format($valor,10,'.','');
						}
					 }
				  }
				  
				  // Array ( [0] => CEREALS [1] => 2000 [2] => 100.917653112 [3] => 103.173490416 [4] => 103.70670268 [5] => 104.541804132 [6] => 102.782562398 [7] => 101.774227125 [8] => 94.3149774304 [9] => 96.7210858456 [10] => 102.488054837 [11] => 100.009079289 [12] => 101.076955818 [13] => 102.411428117 [14] => 100.000005681 ) 
				  $output .= $excel->ferCSV($lalinia,true); 
				  $array_x_excel[] = $lalinia1;   
			   }
				 
			}
			
			if ($format == 'csv') {
				$theoutput = function($output) 
				{
					  return $output;
				};
				return response($theoutput($output), 200, $headers);
				die();
			} else {
				ExcelMaat::create("Índexs mensuals i anuals compostos", function($excel) use(
							$base,$anyinici,$anyfi,$CalculsExcel,$array_x_excel
						)
				{
					
					$excel->sheet('Índexs simples', function($sheet) use(
							$base,$anyinici,$anyfi,$CalculsExcel,$array_x_excel
						){
							$sheet->setPageMargin(array(
								1.50, 1.50, 1.20, 1.50
							));
							$sheet->setWidth(array(	
							    'A'     =>  20,
								'B'     =>  7.57,
								'C'		=>  5.29,
								'D'		=>  5.29,
								'E'		=>  5.29,
								'F'		=>  5.29,
								'G'		=>  5.29,
								'H'		=>  5.29,
								'I'		=>  5.29,
								'J'		=>  5.29,
								'K'		=>  5.29,
								'L'		=>  5.29,
								'M'		=>  5.29,
								'N'		=>  5.29,
								'O'		=>  5.29,
								'P'		=>  5.29,
								));
							$sheet->setStyle(array(
									'font' => array(
										'family'     => 'Arial'
									),
									
							));
							$titol1 = 'Índex de preus percebuts agraris. Catalunya '.$anyinici.'-'.$anyfi.' ';
							$titol2 = 'Índexs mensuals i anuals. Font: Serveis Territorials i Gabinet Tècnic (DAR)';
							$titol3 = 'Any Base 20'.$base.'=100';
							
							$titol4_1 = "Durant l'any 2016 s'ha procedit a una millora en el \r\nprocés d'obtenció de la informació de preus. Aquesta millora ha consistit en la selecció dels centres d'informació";
							$titol4_2 = "més rellevants (llotges, grans venedors, grans compradors) i en una definició més acurada i representativa dels productes estàndards a informar.";
							$titol4_3 = "Aquest fet provoca indefectiblement un petit trencament de les series estadístiques, la justificació del qual prové de la millora de la qualitat informativa assolida.";
							$sheet->cells('A1', function($cells) use ($titol1){ 
								$cells->setFont(array(
											'size'       => '14',
											'bold'       =>  false
								));
								$cells->setAlignment('left');
								$cells->setValue($titol1); 
							});
							$sheet->cells('A2', function($cells) use ($titol2){ 
								$cells->setFont(array(
											'size'       => '12',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue($titol2); 
							});
							$sheet->cells('M2', function($cells) use ($titol3){ 
								$cells->setFont(array(
											'size'       => '14',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue($titol3); 
							});
							$sheet->cells('A3', function($cells) use ($titol4_1){ 
								$cells->setFont(array(
											'size'       => '8',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue($titol4_1); 
							});
							$sheet->cells('A4', function($cells) use ($titol4_2){ 
								$cells->setFont(array(
											'size'       => '8',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue($titol4_2); 
							});
							$sheet->cells('A4', function($cells) use ($titol4_3){ 
								$cells->setFont(array(
											'size'       => '8',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue($titol4_3); 
							});
							$sheet->cells('A6:O6', function($cells) {
									$cells->setBorder('thin', 'none', 'thin', 'none');  // Set all borders (top, right, bottom, left)
							});
							$sheet->cells('A6', function($cells){ 
								$cells->setFont(array(
											'size'       => '8',
											'bold'       =>  true
								));
								$cells->setAlignment('left');
								$cells->setValue("Producte"); 
							});
							$colNames = array('Any','Gen.','Feb.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Oct.','Nov.','Des.','Anual');	
							$cols = range('A', 'Z');
							$ix=1;
							foreach($colNames as $name){
								$sheet->cells($cols[$ix].'6', function($cells) use($name){ 
									$cells->setFont(array(
											'size'       => '8',
											'bold'       =>  true
									));
									$cells->setFontFamily('Arial');
									$cells->setAlignment('right');
									$cells->setValue($name); 
								});
								$ix++;
							}
							$currentRow = 7;
							$prodAra='';
							$prodAnt='';
							reset($CalculsExcel->valorConsist);
							$producte_anterior = '';
							$linia_sobre = '0';
							 
							$producte_array = $CalculsExcel->valorConsist;
							
							$FormatRatllaDalt_esq="8";
							$FormatRatllaDaltN_esq="8";
							$FormatRatllaDalt_esq_bold="8";
							$FormatRatllaSenseResEsq_N="8";
							$FormatRatllaSenseResEsq="8";
							$FormatRatllaSenseResN="8";	
							$FormatRatllaSenseResN1="8";	
							$FormatRatllaDalt="8";
							$FormatRatllaDalt1="8";
							$FormatRatllaSenseRes="8";
							$FormatRatllaSenseRes1="8";
							$FormatRatllaDaltN="8";
							$FormatRatllaDaltN1="8";
							
							
							$negreta = "si";
							$primer = "no";
							foreach ($array_x_excel as $linia_array) {
									/*
									 *Array ( [0] => CEREALS [1] => 2000 [2] => 100,9 [3] => 103,2 [4] => 103,7 [5] => 104,5 [6] => 102,8 [7] => 101,8 [8] => 94,3 [9] => 96,7 [10] => 102,5 [11] => 100,0 [12] => 101,1 [13] => 102,4 [14] => 100,0 ) 
									 * 
									 * 
									 */
									$producte = array_shift($linia_array);
									$any =  array_shift($linia_array);   	        
									$producteMIN = $CalculsExcel->a_minuscules[$producte];
									if ($producteMIN!='') $producte=$producteMIN;
									if($producte != $producte_anterior){
										 $linia_sobre = '0';	
										 if($currentRow == 4)
										{
											$primer = "si";
											
											if (array_key_exists(strtoupper($producte), $producte_array))
											{
											   $formatAra = $FormatRatllaSenseResEsq_N;
											   $negreta = "si";	
												
											}
											else
											{
											   $formatAra = $FormatRatllaSenseResEsq;
											   $negreta = "no";
														   
											}
											
										} // $currentRow == 4
										else {
											if (array_key_exists(strtoupper($producte), $producte_array))
											{
											   $formatAra = $FormatRatllaDaltN_esq;
											   $negreta = "si";	
								   
											}
											else
											{
											   $formatAra = $FormatRatllaDalt_esq;
											   $negreta = "no";
														   
											}
											if (($producte=='IPPA General') or ($producte=='Prod. Ramaders') or ($producte=='Prod. Agrícoles') or ($producte=='Prod. Forestal'))
											{		          
												$formatAra =  $FormatRatllaDalt_esq_bold;
												$negreta = "si";
											}
										}
										$sheet->cells('A'.($currentRow+1), function($cells) use ($producte){ 
											$cells->setFont(array(
														'size'       => '8',
														'bold'       =>  true
											));
											$cells->setAlignment('left');
											$cells->setValue($producte); 
										});										
										
									} else{
										$linia_sobre = '1';										
									}
									$producte_anterior=$producte;
									//
									//
									if($linia_sobre == '0')
									{
									   
									   if($primer == "si")
										{
											
										   if ($negreta == "si")
										   {
											$FormatRow =  $FormatRatllaSenseResN;			     
											$FormatAny =  $FormatRatllaSenseResN1;			     
										   }
										   else if ($negreta == "no")
										   {
											$FormatRow =  $FormatRatllaSenseRes;
											$FormatAny =  $FormatRatllaSenseRes1;
										   }	
										}
										else
										{
										   if ($negreta == "si")
										   {
											$FormatRow = $FormatRatllaDaltN;			     
											$FormatAny = $FormatRatllaDaltN1;
										   }
										   else if ($negreta == "no")
										   {
											$FormatRow = $FormatRatllaDalt;
											$FormatAny = $FormatRatllaDalt1;
										   }		
										}
									   
									}
									else
									{		        
									   
									   if($primer == "si")
										{
										   if ($negreta == "si")
										   {
											 $FormatRow = $FormatRatllaSenseResN;			     
											 $FormatAny = $FormatRatllaSenseResN1;			     
										   }
										   else if ($negreta == "no")
										   {
											$FormatRow = $FormatRatllaSenseRes;
											$FormatAny = $FormatRatllaSenseRes1;
										   }
										}
										else
										{
										   if ($negreta == "si")
										   {
											 $FormatRow = $FormatRatllaSenseResN;			     
											 $FormatAny = $FormatRatllaSenseResN1;
										   }
										   else if ($negreta == "no")
										   {
											$FormatRow = $FormatRatllaSenseRes;
											$FormatAny = $FormatRatllaSenseRes1;
										   }
										}
										
									} 
									$sheet->cells('B'.($currentRow+1), function($cells) use ($any){ 
											$cells->setFont(array(
														'size'       => '8',
														'bold'       =>  true
											));
											$cells->setAlignment('right');
											$cells->setValue($any); 
										});	
								    //$cart->writeRow($currentRow,2,$linia_array,$FormatRow);
									$cols = range('A', 'Z');
									$ix=2;
									foreach($linia_array as $valor){
										$sheet->cells($cols[$ix].($currentRow+1), function($cells) use($valor){ 
											$cells->setFont(array(
													'size'       => '8',
													'bold'       =>  true
											));
											$cells->setFontFamily('Arial');
											$cells->setAlignment('right');
											$cells->setValue($valor); 
										});
										$ix++;
									}
								    $primer = "no";	
									
									$currentRow++;
									
								
							} // fi foreach $array_x_excel
							
							
						});
				})->export('xls');
				die('');
			}// format xls
			
	}
	
	public function indexCompAnualMobil($anyinici, $anyfi, $actuals='no',$format) {
			if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
			} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/csv'
					,   'Content-Disposition' => 'attachment; filename=indexCompAnualMobil_'.$anyinici.'_'.$anyfi.'.'.$format
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$Inc_Ara= strtolower($actuals[0]); 
				$base = config('ppp.base')-2000;
				$pagatspercebuts='percebuts';
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$preus = new \App\Library\Prices($pagatspercebuts,$anyinici, 1, 8, $base ,DB::connection());
				$limits_anys_preus=$preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
				$anyiniciAbs=$limits_anys_preus['MIN'];
				$mesiniciAbs=$limits_anys_preus['MMIN'];
				$anyfiAbs=$limits_anys_preus['MAX'];
				$mesfiAbs=$limits_anys_preus['MMAX'];
				$any_sessio = $aplicacio->ENCURS_ANY_DADES;
				$mes_sessio = $aplicacio->ENCURS_MES_DADES;
				if ($preus->quants_preus_entrats_App($any_sessio,$mes_sessio,DB::connection(),$base)==0)
				 {  if (($any_sessio>=$anyfiAbs) or ( ($any_sessio=$anyfiAbs) and ($mes_sessio>=$mesfiAbs) ))
					{  $mesfiAbs++;
					   if ($mesfiAbs>=12) { $mesfiAbs=1; $anyfiAbs++; }
					}
				 }
				//
				$excel 			= new \App\Library\ExcelWriter();
				$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
				$CalculsExcel 	= $excelfactory->creaExcel(DB::connection(),$pagatspercebuts,$base);

				$output='';
				
				$avgCat=array();
				for ($any=$anyinici-1; $any<=$anyfi; $any++)
				{
				  $avgCat[$any] = $CalculsExcel->entradaPreusAnyT($any);
				  if ($base=='05')
				  { unset($avgCat[$any]['113200']); // Segalls no surten en l'índex Base 05
					unset($avgCat[$any]['221000']); // Suro no surten en l'índex Base 05    
				  }
				}
				/*
				print_r($avgCat[2006]);
				Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
													  [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 
													  [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
						   [002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg 
													   [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 
													   [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
						   [002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 
													   [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
						   [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
														[MES1] => 13.83 [MES2] => 13.37 [MES3] => 12.71 [MES4] => 12.3 [MES5] => 12.3 [MES6] => 12.28 [MES7] => 12.1 [MES8] => 12.52 
													   [MES9] => 13.62 [MES10] => 14.69 [MES11] => 15.67 [MES12] => 15.94 [ANUAL] => 13.28 ) 
							·
							·
							·
				*/
				$qFisiques = $CalculsExcel->qFisiques("basepercebuts".$base, $avgCat[$anyinici]);
				/*
				Array ( [001100] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat tou o semidur [ANUAL] => 234804 [consist] => 001100 [PREUS_ANUAL] => 13.24 ) 
				[002100] => Array ( [SUM(MES1)] => 24029 [SUM(MES2)] => 12014 [SUM(MES3)] => 12014 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 13240 [SUM(MES12)] => 12423 [DESCRIPCIO] => Arròs closca rodó i semillarg [ANUAL] => 124227 [consist] => 002100 [PREUS_ANUAL] => 28.16 ) 
				[002200] => Array ( [SUM(MES1)] => 1100 [SUM(MES2)] => 1100 [SUM(MES3)] => 1100 [SUM(MES4)] => 0 [SUM(MES5)] => 0 [SUM(MES6)] => 0 [SUM(MES7)] => 0 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 0 [SUM(MES11)] => 1100 [SUM(MES12)] => 1100 [DESCRIPCIO] => Arròs closca llarg [ANUAL] => 5500 [consist] => 002200 [PREUS_ANUAL] => 28.13 ) 
				.
				.
				.
				[001000] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat [ANUAL] => 234804 [consist] => Array ( [0] => 001100 ) [PREUS_ANUAL] => 13.24 ) 
				[002000] => Array ( [SUM(MES1)] => 25129 [SUM(MES2)] => 13114 [SUM(MES3)] => 13114 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 14340 [SUM(MES12)] => 13523 [DESCRIPCIO] => Arròs [ANUAL] => 129727 [consist] => Array ( [0] => 002100 [1] => 002200 ) [PREUS_ANUAL] => 28.1587280982 ) 
				[003000] => Array ( [SUM(MES1)] => 14855
				*/


				for ($any=$anyinici-1; $any<=$anyfi; $any++)
				{ 
				  $preusProductesAnyT[$any] = $CalculsExcel->preusProductesAnyT($avgCat[$any], $qFisiques);
				}
				/*
				preusProductesAnyT[2006]
				Array ( 
				[001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
				[001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
				[002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
				[002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
				[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.9538354254 [MES12] => 23 [ANUAL] => 23.7532165239 ) 
				.
				.
				.
				.
				.
				.
				)
				*/

				$wValor = $CalculsExcel->wValor($qFisiques);
				$indexAnualMobilCompostAnyTOut = array();
				for ($any=$anyinici; $any<=$anyfi; $any++)
				{ 
					  $indexAnualMobilSimpleAnyT = $CalculsExcel->indexAnualMobilSimpleAnyT($preusProductesAnyT[$any], 
																					 $preusProductesAnyT[$any-1],
																					 $qFisiques);																 
					  $indexAnualMobilCompostAnyT = $CalculsExcel->indexAnualMobilCompostAnyT($indexAnualMobilSimpleAnyT, $wValor);
					 /*
					   Array ( [001000] => Array ( [DESCRIPCIO] => Blat 
												[MES1] => 107.945350808 
												[MES2] => 108.891104574 
												[MES3] => 109.640754047 
												[MES4] => 110.444641725 
								.				
								.				
								.				
								.     
								.				
							[000000] => Array ( [DESCRIPCIO] => CEREALS 
												[MES1] => 98.7907253858 
												[MES2] => 99.3855957008 
												[MES3] => 99.9836058098 
					 
					   */
					  
						
					  $indxs=array('DESCRIPCIO','MES1','MES2');  
					  reset($CalculsExcel->valorConsist);
					  while (list ($codiP, $infoP) = each ($CalculsExcel->valorConsist)) 
					  {  // Array ( [name] => CEREALS [consist] => Array ( [0] => 001000 [1] => 002000 [2] => 003000 [3] => 004000 [4] => 005000 [5] => 006000 [6] => 007000 ) ) 
						 // Array ( [name] => LLEGUMINOSES [consist] => Array ( [0] => 011000 [1] => 013000 [2] => 014000 [3] => 016000 ) ) 
						 $nom = $infoP['name'];
						 $elsconsit = $infoP['consist'];
						 
						 $infoIndex = $indexAnualMobilCompostAnyT[$codiP];
						 $desc = ucwords(strtolower($infoIndex['DESCRIPCIO']));
						 $desc = str_replace('Del ','del ',$desc);
						 $desc = str_replace('De ','de ',$desc);
						 $desc = str_replace(' I ',' i ',$desc);
						 if ($desc=='Total') $desc='IPPA General'; 
						 if ($any==2000) { $linia=array($desc,$any,'-','-','-','-','-','-','-','-','-','-','-','100'); }
						 else	   
						 {   $decimals = 10;       
					         /*
							 $linia = array($desc,$any,
										number_format($infoIndex['MES1'],$decimals,'.',''),number_format($infoIndex['MES2'],$decimals,'.',''),
										number_format($infoIndex['MES3'],$decimals,'.',''),number_format($infoIndex['MES4'],$decimals,'.',''),
										number_format($infoIndex['MES5'],$decimals,'.',''),number_format($infoIndex['MES6'],$decimals,'.',''),
										number_format($infoIndex['MES7'],$decimals,'.',''),number_format($infoIndex['MES8'],$decimals,'.',''),
										number_format($infoIndex['MES9'],$decimals,'.',''),number_format($infoIndex['MES10'],$decimals,'.',''),
										number_format($infoIndex['MES11'],$decimals,'.',''),number_format($infoIndex['MES12'],$decimals,'.','')
										);*/
							  $linia = array($desc,$any,
										$infoIndex['MES1'],$infoIndex['MES2'],
										$infoIndex['MES3'],$infoIndex['MES4'],
										$infoIndex['MES5'],$infoIndex['MES6'],
										$infoIndex['MES7'],$infoIndex['MES8'],
										$infoIndex['MES9'],$infoIndex['MES10'],
										$infoIndex['MES11'],$infoIndex['MES12']
										);			                           
										                                       
							  // Array ( [0] => CEREALS [1] => 2001 [2] => 100.2 [3] => 100.2 [4] => 100.1 [5] => 99.9 [6] => 99.9 [7] => 100.0 [8] => 100.7 [9] => 101.3 [10] => 101.2 [11] => 101.2 [12] => 101.2 [13] => 101.3 )
								                                               
										  if ($any==$anyfiAbs)                 
										  {   //echo $any.",".$mesfiAbs; die();
											  for ($mesix=$mesfiAbs;$mesix<=12; $mesix++)
											  {  if ($mesix>$mesfiAbs) $linia[$mesix+1]='';
											  }                                
										  }                                    
							                                                   
										                                       
						  }                                                    
						  $indexAnualMobilCompostAnyTOut[$codiP][$any]=$linia; 
					  }                                                        
				}                                                              
				/*                                                             
				   Array (                                                     
					[000000] => Array (                                        
					 [2000] => Array ( [0] => CEREALS [1] => 2000 [2] => - [3] => - [4] => - [5] => - [6] => - [7] => - [8] => - [9] => - [10] => - [11] => - [12] => - [13] => 100 ) 
					 [2001] => Array ( [0] => CEREALS [1] => 2001 [2] => 100.2 [3] => 100.2 [4] => 100.1 [5] => 99.9 [6] => 99.9 [7] => 100.0 [8] => 100.7 [9] => 101.3 [10] => 101.2 [11] => 101.2 [12] => 101.2 [13] => 101.3 ) 
					 [2002] => Array ( [0] => CEREALS [1] => 2002 [2] => 101.4 [3] => 101.4 [4] => 101.4 [5] => 101.4 [6] => 101.5 [7] => 100.8 [8] => 98.4 [9] => 96.6 [10] => 95.8 [11] => 95.0 [12] => 94.1 [13] => 92.9 ) 
					 [2003] => Array ( [0] => CEREALS [1] => 2003 [2] => 92.4 [3] => 92.1 [4] => 91.8 [5] => 91.7 [6] => 91.6 [7] => 91.6 [8] => 92.1 [9] => 92.6 [10] => 93.1 [11] => 94.7 [12] => 97.1 [13] => 99.6 ) 
					 [2004] => Array ( [0] => CEREALS [1] => 2004 [2] => 100.4 [3] => 101.0 [4] => 101.6 [5] => 101.8 [6] => 101.8 [7] => 102.0 [8] => 102.5 [9] => 102.7 [10] => 102.1 [11] => 100.0 [12] => 96.9 [13] => 94.0 ) 
					 [2005] => Array ( [0] => CEREALS [1] => 2005 [2] => 92.0 [3] => 90.8 [4] => 89.7 [5] => 89.1 [6] => 88.7 [7] => 88.5 [8] => 88.8 [9] => 89.0 [10] => 89.3 [11] => 90.0 [12] => 91.0 [13] => 91.8 ) 
					 [2006] => Array ( [0] => CEREALS [1] => 2006 [2] => 92.5 [3] => 92.9 [4] => 93.2 [5] => 93.2 [6] => 93.3 [7] => 93.2 [8] => 92.3 [9] => 92.2 [10] => 92.9 [11] => 94.7 [12] => 96.4 [13] => 98.1 ) 
					 [2007] => Array ( [0] => CEREALS [1] => 2007 [2] => 98.8 [3] => 99.4 [4] => 100.0 [5] => 100.3 [6] => 100.5 [7] => 101.6 [8] => 107.8 [9] => 112.1 [10] => 118.0 [11] => 124.3 [12] => 128.3 [13] => 132.4 ) ) 
					[010000] => Array ( [2000] => Array ( [0] => LLEGUMINOSES [1] .........
				 */                                                            
				 if (!$Inc_Ara)
				{   
					// Eliminem els valors futurs
					if ($anyfi==$anyfiAbs)  // volem info de l'últim any en curs
					{   
					   while (list ( $elcodi,) = each ($indexAnualMobilCompostAnyTOut)) 
						  {    //$indexAnualMobilCompostAnyTOut[$elcodi][$anyfiAbs] 
							   // Array ( [0] => CEREALS [1] => 2008 [2] => [3] => [4] => [5] => [6] => [7] => [8] => [9] => [10] => [11] => [12] => [13] => )
							   for ($mesix=$mesfiAbs; $mesix<=12; $mesix++)
							   {  // Des del període actual fins final d'any .. valor en blanc
								  $indexAnualMobilCompostAnyTOut[$elcodi][$anyfiAbs][$mesix+1]='';
							   }    
							   $indexCompostAnyTGrups[$anyfiAbs][$elcodi][$anyfiAbs][13]='';  
						  }
					}
				}                                                                      
				if ($format=='csv')
				{				
					$output='';
					$output .= $excel->ferCSV(array('Producte','Any',
								 'Gen.','Feb.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Oct.','Nov.','Des.'),false);
						
					reset($CalculsExcel->valorConsist);
					while (list ($codiP, $infoP) = each ($CalculsExcel->valorConsist)) 
					{  for ($any=$anyinici; $any<=$anyfi; $any++)
						{  $linia = $indexAnualMobilCompostAnyTOut[$codiP][$any];
						   $output .= $excel->ferCSV($linia,true);
						}
					}                                                                                                     
					$theoutput = function($output)                                 
					{                                                              
						  return $output;                                          
					};                                                             
																				   
					return response($theoutput($output), 200, $headers);           
				}   else {
						ExcelMaat::create("Índexs anuals mòbils", function($excel) use(
							$base,$anyinici,$anyfi
						)
						{
							$excel->sheet('Índexs anuals mòbils', function($sheet) use(
							$base,$anyinici,$anyfi
						    ){
								$titol1 = 'Índex de preus percebuts agraris. Catalunya '.$anyinici.'-'.$anyfi.' ';
								$titol2 = 'Índexs anuals mòbils. Font: Serveis Territorials i Gabinet Tècnic (DAR)';
								$titol3 = 'Any Base';
								$titol4 = '20'.$base.'=100';
								$titol5_1 = "Durant l'any 2016 s'ha procedit a una millora en el \r\nprocés d'obtenció de la informació de preus. Aquesta millora ha consistit en la selecció dels centres d'informació";
								$titol5_2 = "més rellevants (llotges, grans venedors, grans compradors) i en una definició més acurada i representativa dels productes estàndards a informar.";
								$titol5_3 = "Aquest fet provoca indefectiblement un petit trencament de les series estadístiques, la justificació del qual prové de la millora de la qualitat informativa assolida.";
								$sheet->cells('A1', function($cells) use ($titol1){ 
									$cells->setFont(array(
												'size'       => '14',
												'bold'       =>  false
									));
									$cells->setFontFamily('Consolas');
									$cells->setAlignment('left');
									$cells->setValue($titol1); 
								});
								$sheet->cells('A2', function($cells) use ($titol2){ 
									$cells->setFont(array(
												'size'       => '14',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol2); 
								});
								$sheet->cells('M2', function($cells) use ($titol3){ 
									$cells->setFont(array(
												'size'       => '14',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol3); 
								});
								$sheet->cells('N2', function($cells) use ($titol4){ 
									$cells->setFont(array(
												'size'       => '14',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol4); 
								});
								$sheet->cells('A3', function($cells) use ($titol5_1){ 
									$cells->setFont(array(
												'size'       => '8',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol5_1); 
								});
								$sheet->cells('A4', function($cells) use ($titol5_2){ 
									$cells->setFont(array(
												'size'       => '8',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol5_2); 
								});
								$sheet->cells('A5', function($cells) use ($titol5_3){ 
									$cells->setFont(array(
												'size'       => '8',
												'bold'       =>  false
									));
									$cells->setAlignment('left');
									$cells->setValue($titol5_3); 
								});
								$colNames = array('Producte','Any',
											'Gen.','Feb.','Mar.','Abr.','Mai.','Jun.','Jul.','Ago.','Set.','Oct.','Nov.','Des.');
								$sheet->row(5, $colNames );	
								
							 });
						})->export('xls');
						die('');				
				} // format xls                                                            
			}  // és sc                                                        
			                                                                   
			                                                                   
			                                                                   
	}  //  servei                                                              
	
	public function exportacio_preus_TOTS($opcio){
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
			} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/plain'
					,   'Content-Disposition' => 'attachment; filename=exportacio_preus_TOTS'.$opcio.'.txt'
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$output="";
				$pagatspercebuts = $opcio;
				$base = config('ppp.base')-2000;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$preus = new \App\Library\Prices($pagatspercebuts,$any, 1, 99, $base ,DB::connection());
				$limits_anys_preus=$preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
				$array_Territoris = array(8=>'Barcelona',
										  17=>'Girona',
										  25=>'Lleida',
										  43=>'Tarragona',
										  99=>'Catalunya');
				$SQL_elsProd = strtoupper("SELECT CODIPROD,NOMPROD  FROM  DESCRIPCIO_PREUS".$pagatspercebuts."  order by CODIPROD");	
				$rows_elsProd = DB::connection()->select($SQL_elsProd);
				if (count($rows_elsProd)==0) { die('No hi ha productes'); }
				$array_prod = array();
				
				foreach($rows_elsProd as $rowOBJ){  
					$rowelscom = ((array) $rowOBJ); 
					$array_prod[$rowelscom['CODIPROD']]= $rowelscom['NOMPROD'];
				}						  
				$ix=0;
				foreach($array_prod as $clau_P => $valor_P)  {
					$territVeure=array(8,17,25,43,99);
					$dadesmes=true;
					$anuals=true;
					if ($anuals) $preusAnuals=$preus->mitjanaAnual($clau_P,2000,$any);
					/*
					Array ( 	[2000] => Array ( [Cat] => 13.2418335718 [8] => 12.5480023375 [17] => 12.408005395 [25] => 13.9031974662 [43] => 14.025 ) 
						[2001] => Array ( [Cat] => 14.6814177356 [8] => 14.5339998202 [17] => 14.3179888978 [25] => 15.0326007811 [43] => 14.093 ) 
						[2002] => Array ( [Cat] => 13.0432292039 [8] => 12.2900002997 [17] => 12.0160000892 [25] => 13.9322021337 [43] => 13.209 ) 
						[2003] => Array ( [Cat] => 13.8579900257 [8] => 13.1750065929 [17] => 12.8800004459 [25] => 14.6339923795 [43] => 14.296 ) 
						[2004] => Array ( [Cat] => 14.0296982164 [8] => 13.2199986514 [17] => 13.2 [25] => 14.804610402 [43] => 14.573 ) 
						[2005] => Array ( [Cat] => 13.394970699 [8] => 13.0350005994 [17] => 13.2490005796 [25] => 13.7413978853 [43] => 13.077 ) 
						[2006] => Array ( [Cat] => 14.1089171394 [8] => 13.5520094998 [17] => 12.8080004905 [25] => 14.835595161 [43] => 15.164 ) )
					*/
					$elsPreusHist = $preus->HistoricPrices2Arr($clau_P,$limits_anys_preus['MIN'],$limits_anys_preus['MAX'],1,$limits_anys_preus['MMAX'],$territVeure,$dadesmes,$preusAnuals,$anuals);
					 /*
						Array ( 
							[0] => Array ( [0] => Any [1] => Mes [2] => Barcelona [3] => Girona [4] => Lleida [5] => Tarragona [6] => Catalunya ) 
							[1] => Array ( [0] => 2000 [1] => 1 [2] => [3] => [4] => 14.72 [5] => 14.48 [6] => 14.683032 ) 
							[2] => Array ( [0] => 2000 [1] => 2 [2] => [3] => [4] => 14.72 [5] => 14.72 [6] => 14.718528 ) 
							[3] => Array ( [0] => 2000 [1] => 3 [2] => [3] => [4] => 14.72 [5] => 14.72 [6] => 14.718528 ) 
							[4] => Array ( [0] => 2000 [1] => 4 [2] => [3] => [4] => 15.03 [5] => 15.03 [6] => 15.028497 ) 
							[5] => Array ( [0] => 2000 [1] => 5 [2] => [3] => [4] => 13.94 [5] => [6] => 13.94 ) 
							[6] => Array ( [0] => 2000 [1] => 6 [2] => [3] => 12.43 [4] => 13.34 [5] => [6] => 12.919429 ) 
							[7] => Array ( [0] => 2000 [1] => 7 [2] => 12.38 [3] => 12.32 [4] => 13.22 [5] => [6] => 12.544684 ) 
							[8] => Array ( [0] => 2000 [1] => 8 [2] => 12.56 [3] => 12.98 [4] => 13.28 [5] => [6] => 12.853962 ) 
							[9] => Array ( [0] => 2000 [1] => 9 [2] => 12.86 [3] => [4] => 13.7 [5] => 13.25 [6] => 13.359013 ) 
							[10] => Array ( [0] => 2000 [1] => 10 [2] => 13.04 [3] => [4] => 13.94 [5] => 13.46 [6] => 13.470598 ) 
							[11] => Array ( [0] => 2000 [1] => 11 [2] => [3] => [4] => 14.12 [5] => 13.63 [6] => 13.970265 ) 
							[12] => Array ( [0] => 2000 [1] => 12 [2] => [3] => [4] => 14.36 [5] => 13.87 [6] => 14.286093 ) 
							[13] => Array ( [0] => 2000 [1] => Anual [2] => [3] => [4] => [5] => [6] => ) 
							[14] => Array ( [0] => 2001 [1] => 1 [2] => [3] => [4] => 14.48 [5] => 13.28 [6] => 14.301072 ) 
							[15] => Array ( [0] => 2001 [1] => 2 [2] => [3] => [4] => 14.3 [5] => 13.37 [6] => 14.089878 )   
							.
							.
							.
					 */ 
					
					$output .= "Any;"."Territori;"."Codi;"."Mes1;"."Mes2;"."Mes3;"."Mes4;"."Mes5;"."Mes6;"."Mes7;"."Mes8;"."Mes9;"."Mes10;"."Mes11;"."Mes12;"."Anual;\r\n";
					 for ($any=$limits_anys_preus['MIN']; $any<=$limits_anys_preus['MAX']; $any++ )
					 {      
						foreach( $array_Territoris as $clau_T => $valor_T )
						{ $output .= $any.";";
						  $output .= str_pad($clau_T,2,'0', STR_PAD_LEFT).";";
						  $output .= "".$clau_P.";";
						  for ($mes=1;$mes<=13;$mes++)
							{ 
								$index = ($any-$limits_anys_preus['MIN'])*13+$mes;
								$preu=0;
								if (isset($elsPreusHist[$index])) { 
										if (isset($elsPreusHist[$index][(array_search($clau_T,$territVeure)+2)])) { 
											$preu=$elsPreusHist[$index][(array_search($clau_T,$territVeure)+2)]; 
										}
								}
								$output .= str_replace('.',',',$preu).";";	
								$ix++;	
							} 
							$output .= "\r\n";  		
						}
					 }	
					
					if ($ix>20000){ break; }
				}
				
				
				
				$theoutput = function($output)  { return $output; };                                                             
				return response($theoutput($output), 200, $headers);         
			}

	}

	
	public function exportacio_ponde_TOTES($opcio){
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
			} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/plain'
					,   'Content-Disposition' => 'attachment; filename=exportacio_ponde_TOTES'.$opcio.'.txt'
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$output="";
				$anymes = $opcio;
				$base = config('ppp.base')-2000;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$pagatspercebuts = 'percebuts';
				$preus = new \App\Library\Prices($pagatspercebuts,$any, 1, 99, $base ,DB::connection());
				$limits_anys_preus=$preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
				$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
				$CalculsExcel 	= $excelfactory->creaExcel(DB::connection(),$pagatspercebuts,$base);
				$avgCat[2000] = $CalculsExcel->entradaPreusAnyT(2000,true);	
				$qFisiques = $CalculsExcel->qFisiques("basepercebuts00", $avgCat[2000]);
				$wValorAll = $CalculsExcel->wValorAll($qFisiques);
				/*
					 Array
					(
						[001000] => Array
							(
								[MES1] => 163130.04
								[MES2] => 107535.28
								[MES3] => 79731.28
								[MES4] => 79731.28
								[MES5] => 27804
								[MES6] => 257769.56
								[MES7] => 1107711.36
								[MES8] => 491270.2
								[MES9] => 251493.8
								[MES10] => 220035.56
								[MES11] => 159462.56
								[MES12] => 163130.04
								[ANUAL] => 3108804.96
							)
							.
							.
							.
						[TOTAL_MES] => Array
								(
									[MES1] => 38743234.737
									[MES2] => 37381255.007
									[MES3] => 27314295.717
									[MES4] => 26286615.487
									[MES5] => 27296368.757
									[MES6] => 27356089.977
									[MES7] => 31317932.047
									[MES8] => 25250105.207
									[MES9] => 31074647.987
									[MES10] => 25717497.597
									[MES11] => 26364586.357
									[MES12] => 25629092.397
								)
						
							[TOTAL_PRODS] => 188


				 */
				$array_anys = array('2000','2001','2002','2003','2004','2005','2006','2007');
				$array_Territoris = array(8=>'Barcelona',
										  17=>'Girona',
										  25=>'Lleida',
										  43=>'Tarragona',
										  99=>'Catalunya');
															  
				$SQL_elProd = strtoupper("SELECT CODIPROD,NOMPROD  FROM  DESCRIPCIO_PREUS".$pagatspercebuts." WHERE actiu".$base." = 's' order by CODIPROD");
				$rows_elProd = DB::connection()->select($SQL_elProd);
				$array_prod = array();
				if (count($rows_elProd)>0) {
					foreach($rows_elProd as $rowOBJ){  
						$rowelscom = ((array) $rowOBJ); 
						$array_prod[$rowelscom['CODIPROD']]= $rowelscom['NOMPROD'];
				   }				
				}
				$output .= ""."Codi;"."Mes1;"."Mes2;"."Mes3;"."Mes4;"."Mes5;"."Mes6;"."Mes7;"."Mes8;"."Mes9;"."Mes10;"."Mes11;"."Mes12;\r\n";
				$totalValor=0;
				foreach($array_prod as $clau_P => $valor_P)    
				{    if (isset($wValorAll[$clau_P])) {
						 $data = $wValorAll[$clau_P];	  
						 $output .=  "".$clau_P.";"; 
						 if (in_array($clau_P,array('211110','211120','211210','211220','211230','211240','211250','211260',
																	  '211280','212110','212210','212220','212230','212240')))
													 $dataANUAL=($data['ANUAL']/1000);
							  elseif (preg_match("/^062/",$clau_P)) $dataANUAL=($data['ANUAL']/10);      
							  else $dataANUAL=$data['ANUAL'];
							  $totalValor+=$dataANUAL;
							  for ($mes=1;$mes<13;$mes++)
								{ 
								  if ($anymes=='m') {
									  if ($wValorAll['TOTAL_MES']['MES'.$mes]!=0) { $valor=$wValorAll[$clau_P]['MES'.$mes]/$wValorAll['TOTAL_MES']['MES'.$mes]*10000; }
									  else { $valor='?'; }
								  }
								  else {
									  if($totalValor!=0) { $valor= $wValorAll[$clau_P]['MES'.$mes]/$totalValor*100; }
									  else { $valor='?'; }
								  }
								  $output .=  str_replace('.',',',$valor).";";		
								} 
							  $output .=  "\r\n"; 		
					}
				}
				
				
				
				
				$theoutput = function($output)  { return $output; };                                                             
				return response($theoutput($output), 200, $headers);  
			}

	}

	public function exportacioQFisiques($format) {
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
		} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/html'
					,   'Content-Disposition' => 'attachment; filename=exportacioQFisiques_'.$format.'.html'
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$base = config('ppp.base')-2000;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$output="";
				$tipusPreusArr = array('percebuts','pagats');
				reset($tipusPreusArr);
				$QFisiques = array();
				while (list(, $tipusPreus) = each($tipusPreusArr)) 
				{   
					// Array ( [1] => percebuts [value] => percebuts [0] => 0 [key] => 0 ) 
					$lesEspecificacionsSQL     = strtoupper("select * from base".$tipusPreus.$base." order by codiprod,coditerr ");
					$rows_lesEspecificacions = DB::connection()->select($lesEspecificacionsSQL);
					$array_prod = array();
					if (count($rows_lesEspecificacions)>0) {  
						foreach($rows_lesEspecificacions as $rowOBJ){  
							$fila = ((array) $rowOBJ); 
							// Array ( [CODIPROD] => 001100 [CODITERR] => 8 [MES1] => 0 [MES2] => 0 [MES3] => 0 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 33369 [MES8] => 20021 [MES9] => 6674 [MES10] => 6674 [MES11] => 0 [MES12] => 0 ) 
							$QFisiques[$fila['CODIPROD'].$fila['CODITERR']] = $fila;
						}
					}
				}
				/*
				 --> $QFisiques
				 Array
				(
					[00110043] => Array
						(
							[CODIPROD] => 001100
							[CODITERR] => 43
							[MES1] => 1823
							[MES2] => 1823
							[MES3] => 1823
							[MES4] => 1823
							[MES5] => 0
							[MES6] => 0
							[MES7] => 0
							[MES8] => 0
							[MES9] => 1823
							[MES10] => 3646
							[MES11] => 3646
							[MES12] => 1823
						)
						.
						.
				*/
				$html = '<table border="0" cellpadding="2" cellspacing="2">
					 <tr><th colspan="14">Quantitats físiques. Base '.$base.'</th></tr>
					 <tr><th>CODI</th><th>TERRITORI</th><th>MES1</th><th>MES2</th><th>MES3</th>' .
							'<th>MES4</th><th>MES5</th><th>MES6</th><th>MES7</th><th>MES8</th><th>MES9</th>' .
							'<th>MES10</th><th>MES11</th><th>MES12</th></tr>'."\r\n";

				while (list($codi_terr, $fila) = each($QFisiques))
				{
				  $html = $html .'<tr><td>'."\r\n";; 
				  $html = $html .$fila['CODIPROD'].'</td><td align="center">'.str_pad($fila['CODITERR'], 2, "0", STR_PAD_LEFT);
								
				  for ($m=1; $m<13; $m++) 
				  {
					$html = $html .'</td><td align="right">' .$fila['MES'.$m]."\r\n";;
				  }		
				  $html = $html .'</td></tr>'."\r\n";;
				}
				$html.="</table>";
				
				$html='<html><body><style type="text/css">
						<!--
						body,td,th {
							font-family: Helvetica, Arial;
							font-size: 11px;
							color: #333333;
						}
						a:link {
							color: #333333;
							text-decoration: none;
						}
						a:visited {
							text-decoration: none;
							color: #333333;
						}
						a:hover {
							text-decoration: none;
							color: #0066FF;
						}
						a:active {
							text-decoration: none;
						}
						-->
						</style>'.$html.'</body></html>';
				
				$output = $html;
				$theoutput = function($output)  { return $output; };                                                             
				return response($theoutput($output), 200, $headers); 			
				
		}
	}
	
	public function exportacioPreusBase($format) {
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
		} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/html'
					,   'Content-Disposition' => 'attachment; filename=exportacioPreusBase_'.$format.'.html'
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$arreglar_preu = function($p)  { return str_replace('.',',',$p); };   
				$base = config('ppp.base')-2000;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$html="";
				$tipusPreusArr = array('percebuts');
				reset($tipusPreusArr);
				$PreusBase = array();

				$ix=0;
				while (list(, $tipusPreus) = each($tipusPreusArr)) 
				{ 
						// Array ( [1] => percebuts [value] => percebuts [0] => 0 [key] => 0 ) 
						$elsPreusBaseSQL     = strtoupper("select * from preus".$tipusPreus." where ANY=".(2000+$base)." order by codiprod,coditerr,mes ");	
						//echo $elsPreusBaseSQL.'<br>';
						$row_elsPreusBaseSQL = DB::connection()->select($elsPreusBaseSQL);
						if (count($row_elsPreusBaseSQL)>0) {  
							foreach($row_elsPreusBaseSQL as $rowOBJ){  
								$fila = ((array) $rowOBJ); 
								//  Array ( [CODIPROD] => 001100 [CODITERR] => 8 [ANY] => 2000 [MES] => 7 [FINPREU] => 12.38 [PREU1] => 0 [PREU2] => 0 [PREU3] => 0 [DATAPREU1] => [DATAPREU2] => [DATAPREU3] => [HORAPREU1] => [HORAPREU2] => [HORAPREU3] => [VALIDAT] => s [AUTOM] => n ) 
								$PreusBase[$fila['CODIPROD'].$fila['CODITERR'].$fila['MES']] = $fila;
								$ix++;
							}
							
						}
				}
				/*
				 --> $PreusBase
				 Array
				(
					[00110087] => Array
						(
							[CODIPROD] => 001100
							[CODITERR] => 8
							[ANY] => 2000
							[MES] => 7
							[FINPREU] => 12.38
							[PREU1] => 0
							[PREU2] => 0
							[PREU3] => 0
							[DATAPREU1] => 
							[DATAPREU2] => 
							[DATAPREU3] => 
							[HORAPREU1] => 
							[HORAPREU2] => 
							[HORAPREU3] => 
							[VALIDAT] => s
							[AUTOM] => n
						)
						.
						.
				*/
				$html = '<table border="0" cellpadding="2" cellspacing="2">
						 <tr><th colspan="14">Preus any base '.$base.'</th></tr>
						 <tr><th>CODI</th><th>TERRITORI</th><th>MES</th><th>PREU</th></tr>'."\r\n";
				while (list($codi_terr, $fila) = each($PreusBase))
				{
				  $html = $html .'<tr><td>'."\r\n";
				  $html = $html .$fila['CODIPROD'].'</td><td align="center">'.str_pad($fila['CODITERR'], 2, "0", STR_PAD_LEFT).
								'</td><td align="right">' .$fila['MES'].
								'</td><td align="right">' .$arreglar_preu($fila['FINPREU']);				
				  $html = $html .'</td></tr>'."\r\n";
				}
				$html.="</table>";
				$html = '<html><body><style type="text/css">
				<!--
				body,td,th {
					font-family: Helvetica, Arial;
					font-size: 11px;
					color: #333333;
				}
				a:link {
					color: #333333;
					text-decoration: none;
				}
				a:visited {
					text-decoration: none;
					color: #333333;
				}
				a:hover {
					text-decoration: none;
					color: #0066FF;
				}
				a:active {
					text-decoration: none;
				}
				-->
				</style>'.$html.'</body></html>';

				$output = $html;
				$theoutput = function($output)  { return $output; };                                                             
				return response($theoutput($output), 200, $headers); 	
				
				
				
		}  // ÉS SC
	}

	public function exportacioPonderValor($format) {
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
		} else {
				$headers = [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
					,   'Content-type'        => 'text/html'
					,   'Content-Disposition' => 'attachment; filename=exportacioPonderValor_'.$format.'.html'
					,   'Expires'             => '0'
					,   'Pragma'              => 'public'
				];
				$base = config('ppp.base')-2000;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				$any = $aplicacio->ENCURS_ANY_DADES;
				$mes = $aplicacio->ENCURS_MES_DADES;
				$tipusPreus = 'percebuts';
				$pagatspercebuts = 'percebuts';
				
				$arreglar_preu = function($p)  { return str_replace('.',',',$p); };   
				
				$html="";
				$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
				$CalculsExcel 	= $excelfactory->creaExcel(DB::connection(),$pagatspercebuts,$base);
				$preus = new \App\Library\Prices($pagatspercebuts,$any, 1, 99, $base ,DB::connection());
				$avgCat['20'+$base] = $CalculsExcel->entradaPreusAnyT(2000+$base,true);
				$qFisiques = $CalculsExcel->qFisiques("basepercebuts".$base, $avgCat['20'+$base]);
				$wValorAll = $CalculsExcel->wValorAll($qFisiques);
				/*  $wValorAll
				Array
				(
					[001000] => Array
						(
							[MES1] => 163130.04
							[MES2] => 107535.28
							[MES3] => 79731.28
							[MES4] => 79731.28
							[MES5] => 27804
							[MES6] => 257769.56
							[MES7] => 1107711.36
							[MES8] => 491270.2
							[MES9] => 251493.8
							[MES10] => 220035.56
							[MES11] => 159462.56
							[MES12] => 163130.04
							[ANUAL] => 3108804.96
						)
					.
					.
					.
					[TOTAL_MES] => Array
							(
								[MES1] => 24693588.537
								[MES2] => 23843581.241
								[MES3] => 22137787.569
								[MES4] => 21650858.74
								[MES5] => 22723599.743
								[MES6] => 25205903.667
								[MES7] => 29201964.7
								[MES8] => 25215886.244
								[MES9] => 31074647.987
								[MES10] => 25717497.597
								[MES11] => 25680123.046
								[MES12] => 24517489.014
							)
			*/
			$lesEspecifSQL     = strtoupper("select * from descripcio_preus".$tipusPreus." where actiu".$base." = 's'  order by CODIPROD ");
			$rows_lesEspeci = DB::connection()->select($lesEspecifSQL);
			$perc_pond_mes_Arr = array();  // ponderacions mensuals
			$pos_mes_Arr = array();  // reng mensuals
			$perc_pond_any_Arr = array();  // ponderacions anuals
			$pos_any_Arr = array();  // reng anual
			$valor_Arr = array();
			$totalValor=0;
			if (count($rows_lesEspeci)>0) {
				foreach($rows_lesEspeci as $rowOBJ){  
					$fila = ((array) $rowOBJ); 
					if (isset($wValorAll[$fila['CODIPROD']])) {
						for ($mes=1;$mes<=12;$mes++)
						{  //echo $fila['CODIPROD'].'//'.$mes.' ('.Prices::BaseZeroTerritoriMesCat($fila['CODIPROD'],$mes,'base'.$tipusPreus.$base,$Conn).')<br>';
						   if ($preus->BaseZeroTerritoriMesCat($fila['CODIPROD'],$mes,'base'.$tipusPreus.$base,DB::connection()) )
						   {
							  $perc_pond_mes_Arr[$fila['CODIPROD']][$mes] = '';
							  $perc_pond_any_Arr[$fila['CODIPROD']][$mes] = '';
							  $wValorAll[$fila['CODIPROD']]['MES'.$mes] = '';
							  
						   }
						   else
						   {
								 if ($wValorAll['TOTAL_MES']['MES'.$mes]!=0) { 
									$pes_mes = $arreglar_preu(number_format(($wValorAll[$fila['CODIPROD']]['MES'.$mes]/$wValorAll['TOTAL_MES']['MES'.$mes]*10000),2,',','')); }
								 else { $pes_mes = '(?)';  }
								 $pos_mes = $preus->get_rang_posicio_mes_conn($fila['CODIPROD'],$mes,DB::connection(),$base);		 
								 $perc_pond_mes_Arr[$fila['CODIPROD']][$mes]=$pes_mes;
								 $pos_mes_Arr[$fila['CODIPROD']][$mes]=$pos_mes;
								 $pos_any= $preus->get_rang_posicio_anual_conn($fila['CODIPROD'],$mes,DB::connection(),$base);
								 $pos_any_Arr[$fila['CODIPROD']][$mes]=$pos_any;
								 if (isset($wValorAll[$fila['CODIPROD']])) {
									 $valor_Arr[$fila['CODIPROD']]['MES'.$mes]=$arreglar_preu(number_format($wValorAll[$fila['CODIPROD']]['MES'.$mes],2,',',''));
									 $totalValor+=$wValorAll[$fila['CODIPROD']]['MES'.$mes];
								 }
						   }
						} // loop mesos
					} // isset especificació en wValorAll
				} // loop especificacions
			} // Hi ha especificacions actius a la base
			$wValorAll['Total']=$totalValor;
			// ja tenim el total de la producció anual .... ara ja podem calcular la ponderació anual
			$rows_lesEspeci = DB::connection()->select($lesEspecifSQL);
			if (count($rows_lesEspeci)>0) {
				foreach($rows_lesEspeci as $rowOBJ){  
						$fila = ((array) $rowOBJ); 
						for ($mes=1;$mes<=12;$mes++) {
							if (isset($wValorAll[$fila['CODIPROD']])) {
								$pes_any = $arreglar_preu(number_format(($wValorAll[$fila['CODIPROD']]['MES'.$mes]/$wValorAll['Total']*10000),2,',',''));
							    $perc_pond_any_Arr[$fila['CODIPROD']][$mes]=$pes_any;
							}
						} // loop mesos
				} // loop especificacions
			}	 // Hi ha especificacions actius a la base			
			$html = '<b>Ponderacions valoratives</b> Base '.$base.'<br>';
			$html = $html .'<table border="0" cellpadding="2" cellspacing="2">';

			reset($perc_pond_mes_Arr);
			while (list($codiprod, $filaPondMes) = each($perc_pond_mes_Arr))
			{ // $filaPondMes: Array ( [1] => 66.061698467 [2] => 45.1003055762 [3] => 36.0159206296 [4] => 36.8259203746 [5] => 12.2357374335 [6] => 102.26554993 [7] => 379.327682702 [8] => 194.825672692 [9] => 80.9321476804 [10] => 85.5586976027 [11] => 62.0957149288 [12] => 66.5361937786 ) 
			  $html =  $html .'<tr><th>'.$codiprod.'</th>
					 <th>gener</th><th>febrer</th><th>març</th><th>abril</th>
					 <th>maig</th><th>juny</th><th>juliol</th><th>agost</th>
					 <th>setembre</th><th>octubre</th><th>nobrembre</th><th>desembre</th>
					 </tr>'."\r\n";
			  for ($mes=1;$mes<=12;$mes++) {
				  if (!isset($valor_Arr[$codiprod]['MES'.$mes])) { 
					$valor_Arr[$codiprod]['MES'.$mes] = '-';
				  }
			  }				  
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  $html = $html .'Valor</td><td>'.$valor_Arr[$codiprod]['MES1'].'</td><td>'.$valor_Arr[$codiprod]['MES2'].'</td><td>'.$valor_Arr[$codiprod]['MES3'].'</td><td>'.$valor_Arr[$codiprod]['MES4'].'</td>
											 <td>'.$valor_Arr[$codiprod]['MES5'].'</td><td>'.$valor_Arr[$codiprod]['MES6'].'</td><td>'.$valor_Arr[$codiprod]['MES7'].'</td><td>'.$valor_Arr[$codiprod]['MES8'].'</td>
											 <td>'.$valor_Arr[$codiprod]['MES9'].'</td><td>'.$valor_Arr[$codiprod]['MES10'].'</td><td>'.$valor_Arr[$codiprod]['MES11'].'</td><td>'.$valor_Arr[$codiprod]['MES12'].'</td>
					 </tr>'."\r\n";
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  $html = $html .'Ponderació mensual</td><td>'.$filaPondMes[1].'</td><td>'.$filaPondMes[2].'</td><td>'.$filaPondMes[3].'</td><td>'.$filaPondMes[4].'</td>
											 <td>'.$filaPondMes[5].'</td><td>'.$filaPondMes[6].'</td><td>'.$filaPondMes[7].'</td><td>'.$filaPondMes[8].'</td>
											 <td>'.$filaPondMes[9].'</td><td>'.$filaPondMes[10].'</td><td>'.$filaPondMes[11].'</td><td>'.$filaPondMes[12].'</td>
					 </tr>'."\r\n";
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  for ($mes=1;$mes<=12;$mes++) {
				  if (!isset($pos_mes_Arr[$codiprod][$mes])) { 
					$pos_mes_Arr[$codiprod][$mes] = '-';
					$pos_any_Arr[$codiprod][$mes] = '-';
				  }
			  }	
			  if (!isset($perc_pond_any_Arr[$codiprod])) {
				  $perc_pond_any_Arr[$codiprod] = array_fill(1, 12, '-');
			  }
			  $html = $html .'Rang mensual</td><td>'.$pos_mes_Arr[$codiprod][1].'</td><td>'.$pos_mes_Arr[$codiprod][2].'</td><td>'.$pos_mes_Arr[$codiprod][3].'</td><td>'.$pos_mes_Arr[$codiprod][4].'</td>
											 <td>'.$pos_mes_Arr[$codiprod][5].'</td><td>'.$pos_mes_Arr[$codiprod][6].'</td><td>'.$pos_mes_Arr[$codiprod][7].'</td><td>'.$pos_mes_Arr[$codiprod][8].'</td>
											 <td>'.$pos_mes_Arr[$codiprod][9].'</td><td>'.$pos_mes_Arr[$codiprod][10].'</td><td>'.$pos_mes_Arr[$codiprod][11].'</td><td>'.$pos_mes_Arr[$codiprod][12].'</td>
					 </tr>'."\r\n";
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  $html = $html .'Ponderació anual</td><td>'.$perc_pond_any_Arr[$codiprod][1].'</td><td>'.$perc_pond_any_Arr[$codiprod][2].'</td><td>'.$perc_pond_any_Arr[$codiprod][3].'</td><td>'.$perc_pond_any_Arr[$codiprod][4].'</td>
											 <td>'.$perc_pond_any_Arr[$codiprod][5].'</td><td>'.$perc_pond_any_Arr[$codiprod][6].'</td><td>'.$perc_pond_any_Arr[$codiprod][7].'</td><td>'.$perc_pond_any_Arr[$codiprod][8].'</td>
											 <td>'.$perc_pond_any_Arr[$codiprod][9].'</td><td>'.$perc_pond_any_Arr[$codiprod][10].'</td><td>'.$perc_pond_any_Arr[$codiprod][11].'</td><td>'.$perc_pond_any_Arr[$codiprod][12].'</td>
					 </tr>'."\r\n";
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  
			  $html = $html .'<tr align="right"><td>'."\r\n";
			  $html = $html .'Rang anual</td><td>'.$pos_any_Arr[$codiprod][1].'</td><td>'.$pos_any_Arr[$codiprod][2].'</td><td>'.$pos_any_Arr[$codiprod][3].'</td><td>'.$pos_any_Arr[$codiprod][4].'</td>
											 <td>'.$pos_any_Arr[$codiprod][5].'</td><td>'.$pos_any_Arr[$codiprod][6].'</td><td>'.$pos_any_Arr[$codiprod][7].'</td><td>'.$pos_any_Arr[$codiprod][8].'</td>
											 <td>'.$pos_any_Arr[$codiprod][9].'</td><td>'.$pos_any_Arr[$codiprod][10].'</td><td>'.$pos_any_Arr[$codiprod][11].'</td><td>'.$pos_any_Arr[$codiprod][12].'</td>
					 </tr>'."\r\n";
			  
			}
			$html.="</table>";
			$html = '<html><body><style type="text/css">
					<!--
					body,td,th {
						font-family: Helvetica, Arial;
						font-size: 11px;
						color: #333333;
					}
					a:link {
						color: #333333;
						text-decoration: none;
					}
					a:visited {
						text-decoration: none;
						color: #333333;
					}
					a:hover {
						text-decoration: none;
						color: #0066FF;
					}
					a:active {
						text-decoration: none;
					}
					-->
					</style>'.$html.'</body></html>';
				
			$output = $html;
			$theoutput = function($output)  { return $output; };                                                             
			return response($theoutput($output), 200, $headers); 
				
		} // ÉS SC
	}
	
	
}                                                                              
                                                                               
 ?>