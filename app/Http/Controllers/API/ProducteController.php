<?php 

namespace App\Http\Controllers\API;

use DateTime;
use JWTAuth;
use Auth;
use APIException;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Input;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

use App\Library\Preus;
use App\Library\Calculs;

use App\Models\Aplicacio;
use App\Models\Territori;
use App\Models\Percebut;
use App\Models\Pagat;
use App\Models\Notapercebut;
use App\Models\Notapagat;

class ProducteController extends APIController
{
	
	public function __construct() {
		//$user = JWTAuth::parseToken()->toUser();
		//$this->user = $user;
		$this->timestart = new DateTime( "now" );
	}
	
	/**
	 * Retorna informació per visualitzar a la pantalla d'inici de gestió dels preus percebuts
	 * 
	 * @param $id codi del producre
	 */
	public function getinforevisio($id)
	{
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$result['informacio']['user'] = $user;
		$rol = $user->rol;
		if ($rol != 'sc') { $result['estat']=false;	}
		else {
			$codiprog = str_pad($id,6,"0",STR_PAD_LEFT);
			$descripcioARR = DB::select('select * from DESCRIPCIO_PREUSPERCEBUTS where CODIPROD = :codiprog', ['codiprog' => $codiprog]);
			if (empty($descripcioARR)) { $result['estat']=false;	}
			else {
				$descripcio = $descripcioARR[0];
				$result['informacio']['infoprod'] = [
						'grup' => $descripcio->GRUP,
						'subgrup' => $descripcio->SUBGRUP,
						'condcomerc' => $descripcio->CONDICIONSCOMERCIALITZACIO,
						'codi' => $descripcio->CODIPROD,
						'especificacio' => $descripcio->NOMPROD,
						'tipus' => $descripcio->TIPUS,
						'gestor' => $descripcio->GESTOR,
						'intercanvi' => $descripcio->FASEINTERCANVI,
						'unitats' => $descripcio->UNITAT,
						'estat' => '',
						'preu' => rand(200,5000)/100
					];
				$infogestorsARR = DB::select('select CODIPROD,CODITERR,MES5  from BASEPERCEBUTS10 where CODIPROD = :codiprog', ['codiprog' => $codiprog]);
				$infogestorsRES= array();
				foreach ($infogestorsARR as $gestorinfo) {
					$infogestorsRES[]=$gestorinfo->CODITERR;
				}
				$result['informacio']['infogestors'] = $infogestorsRES;
				$result['estat']=true;
			}
		}
		
		
		
		return  $this->respond($result);  
	}
	
	public function _obtenirTipus($codiprod) {
		$tipus = 'percebuts';
		$producte = Percebut::where('CODIPROD', $codiprod)->first();
		if (is_null($producte)) {
			$tipus = 'pagats';
			$producte = Pagat::where('CODIPROD', $codiprod)->first();
		}
		if (is_null($producte)) {
			$tipus = '';
		}
		return $tipus;
	}
	
	public function obtenirTipus($codiprod) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		
		$codiprod = str_pad($codiprod, 6, "0", STR_PAD_LEFT);
		$tipus = $this->_obtenirTipus($codiprod);
		$result['informacio'] = array('tipus'=>$tipus, 'dades'=>'');

		return  $this->respond($result);  
	}
	
	public function obtenircomentaris($codiprod,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		$tipus = $this->_obtenirTipus($codiprod);
		$result['informacio']['tipus'] = $tipus;
		$notes = null;
		
		if ($tipus == 'percebuts') { $notesOBJ = new Notapercebut; }
		if ($tipus == 'pagat')     { $notesOBJ = new Notapagat; }
		
		$lesnotes = array();
		
		if ($rol=='st') {
			$notes = $notesOBJ->where('CODIPROD', $codiprod)->where('CODITERR', $user->territori)->where('ANY', $any)->where('MES', $mes)->orderBy('DATACOMENTARI', 'desc')->get(); 
			/*
			$ix=0;
			foreach($notes as $nota)
			{
				$lesnotes[$ix]=$nota;
				$lesnotes[$ix]['INFOPREUSENTRATS'] = nl2br($nota['INFOPREUSENTRATS']);
				$ix++;
			}
			*/
			$result['informacio']['comentaris'][$user->territori] = $notes;
		}
		if ($rol=='sc') {
			
			$notes = array();
			$totsTerrits = $notesOBJ->where('CODIPROD', $codiprod)->where('ANY', $any)->where('MES', $mes)->select('CODITERR')->distinct()->get();
			$result['informacio']['comentaris'] = array();
			foreach($totsTerrits as $unTerrit) {
				$notes = $notesOBJ->where('CODIPROD', $codiprod)->where('CODITERR', $unTerrit['CODITERR'])->where('ANY', $any)->where('MES', $mes)->orderBy('DATACOMENTARI', 'desc')->get();
				$result['informacio']['comentaris'][$unTerrit['CODITERR']]=$notes;
			}
		}
		
		return  $this->respond($result);  
		
	}
	
	public function afegircomentari(Request $request) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		$result['informacio']['path']=$request->path();                 
		$result['informacio']['params']=$request->all();

		$codiprod = $result['informacio']['params']['codiprod'];
		$any = $result['informacio']['params']['any'];
		$mes = $result['informacio']['params']['mes'];
		$comentari = $result['informacio']['params']['comentari'];
		$infoarxiuadj=array();  // info sobre l'arxiu adjunt
		$infoarxiuadj['extension']='';
		$arxiu_adj_mime='';
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		$result['informacio']['territori'] = $user->territori;
		$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
		$result['informacio']['aplicacio'] = $aplicacio;
		$base = $aplicacio->BASE;
		$pagatspercebuts = $this->_obtenirTipus($codiprod);
		$result['informacio']['tipus'] = $pagatspercebuts;
		
		$destins = array();
		if ($rol=='sc') {
			// multidesti
			$paramdestins = $result['informacio']['params']['destins'];
			foreach ($paramdestins as $destiInfo) {
				if (isset($destiInfo['checked'])) {
					if (($destiInfo['checked'])) {
							$destins[] =  $destiInfo['CODITERR'];
					}
				}
			}
			
		} 
		if ($rol=='st') {
			$destins[] =  $user->territori;
		}
		$preus 		= 	new \App\Library\Prices($pagatspercebuts,$any, $mes, $user->territori, $base,DB::connection());
		$unitats=$preus->units($codiprod);	 
		$descpreus=$preus->descPrices($codiprod);
		// Enregistrament dels comentaris i accions amb l'arxiu Upload
		//   en el cas que sigui comentari multidestinatari, no pot haver-hi la possibilitat d'esborrar l'adjunt, no té sentit.
		$result['informacio']['destins'] = $destins;
		
		while (list ($Tkey,$territori) = each ($destins)) {
			$elspreusentrats=$preus->enteredPricesTerritori($codiprod,$territori);
			$infoPreus = "";
			for ($i=1; $i<=$elspreusentrats ['MAXPREUS']; $i++) {
				if (isset($elspreusentrats['PREU'.$i])) {
					if ($elspreusentrats['PREU'.$i]>0) {
						$infoPreus .= $descpreus['DESCPREU'.$i].": <b>".number_format($elspreusentrats['PREU'.$i],2,',','')."</b> ".$unitats.". <br>  ";
					}
				}
			}
			if ($preus->priceTerritori($codiprod,$territori)>0) {
				  $infoPreus .= "Preu final: <b><u>".number_format($preus->priceTerritori($codiprod,$territori),2,',','')."</u></b> "." ".$unitats.". <br> ";
			}
			$datahorapreu=$preus->PriceDateTime($codiprod,$territori);
			if ( (trim($datahorapreu[0])!='') and (trim($datahorapreu[1])!='') )
			{
				$infoPreus .= "Data del preu: <b>".trim($datahorapreu[0])."</b>. <br>";
				$infoPreus .= "Hora del preu: <b>".trim($datahorapreu[1])."</b>. <br>";
			}
			else
			{
			}
			$result['informacio']['infoPreus'] = $infoPreus;
			$seq=0;
			$taula = "notes_preus".$pagatspercebuts; 
			$nouSeqSQL = strtoupper("SELECT max(seq)  as MAX  FROM ".$taula." WHERE CodiProd='".$codiprod."' AND CodiTerr='".$territori."' 
	                AND  ANY='".$any."' AND mes='".$mes."' ");
			$result['informacio']['sql'] = $nouSeqSQL;
			$recs_quinaSeq = DB::connection()->select($nouSeqSQL); 
			$rec_quinaSeq = ((array)$recs_quinaSeq[0]); 
			$result['informacio']['rec_quinaSeq'] = $rec_quinaSeq;
			$seq = $rec_quinaSeq['MAX'];
			if (is_null($seq)) { $seq=1;}
			else { $seq += 1; }
			$qui = strtoupper($rol);
			$noucomSQL = strtoupper("INSERT INTO ".$taula." ".
			   "	  ( CodiProd , CodiTerr , ANY , Mes , seq , qui , Comentari , infoPreusEntrats, DataComentari, extensio, tipus_mime ) ".
			   "     VALUES ( ".
			   " '".$codiprod."', '".$territori."', '".$any."', '".$mes."', '".$seq."', '".$qui ."', '".
					str_replace("'","''",stripslashes($comentari))."', '".$infoPreus."', NOW(), '".$infoarxiuadj['extension']."', '".$arxiu_adj_mime."'  )");  
			$result['informacio']['INSERT'] = $noucomSQL;
			$res_noucom = DB::connection()->select($noucomSQL); 
			$result['informacio']['res_noucom'] = $res_noucom;
			
		}
		
		
		
		
		
		/*
		
		$unitats	=	$preus->units($codiprod);
		$result['informacio']['unitats'] = $unitats;
		$elspreusentrats = $preus->enteredPrices($codiprod);
		$result['informacio']['elspreusentrats'] = $elspreusentrats;
		$descpreus=$preus->descPrices($codiprod);
		$result['informacio']['descpreus'] = $descpreus;
		$infoPreus = "";
		for ($i=1; $i<=$elspreusentrats ['MAXPREUS']; $i++)
		{  $infoPreus .= $descpreus['DESCPREU'.$i].": ".$elspreusentrats['PREU'.$i]." ".$unitats.".   ";
		}
		$result['informacio']['infoPreus'] = $infoPreus;
		$seq = 0;
		$taula = "notes_preus".$pagatspercebuts;
		$quinaSeq = strtoupper("SELECT max(seq) as M  FROM ".$taula."  WHERE CodiProd = '" .$codiprod ."' AND" .
		    " CodiTerr = '" .$user->territori ."' AND ANY = '" .$any ."' AND mes = '" .$mes ."' ");
		$result['informacio']['sql'] = $quinaSeq;
		$rec_quinaSeq = DB::connection()->select($quinaSeq); 
		$fila = ((array)$rec_quinaSeq[0]); 
		$result['informacio']['rec_quinaSeq'] = $fila;
		$seq = $fila['M'];
		if (is_null($seq)) { }
		$result['informacio']['seq'] = $seq;
		$result['informacio']['idsnull'] = is_null($seq);
		*/
		
		return  $this->respond($result);  
	}

	
	public function obtenirpreus($codiprod,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		
		$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
		$result['informacio']['aplicacio'] = $aplicacio;
		$base = $aplicacio->BASE-2000;
		$any = $aplicacio->ENCURS_ANY_DADES;
		$mes = $aplicacio->ENCURS_MES_DADES;
		
		$pagatspercebuts = $this->_obtenirTipus($codiprod);
		$result['informacio']['tipus'] = $pagatspercebuts;
		$preus 		= 	new \App\Library\Prices($pagatspercebuts,$any, $mes, $user->territori, $base,DB::connection());
		$unitats 	= 	$preus->units($codiprod);
		while (list ($key,$TCodi) = each ($preus ->TerritoriesCodes)) {
			if (!$preus->ifBaseZeroTerritori($codiprod,$TCodi)) {
				$result['informacio']["preusEntrats"][$TCodi] = $preus ->enteredPricesTerritori($codiprod,$TCodi);
				$result['informacio']["preusEntrats"][$TCodi]['unitats'] = $unitats;
				$result['informacio']["preusEntrats"][$TCodi]['PreuFinal'] = $preus->priceTerritori($codiprod,$TCodi);
				$result['informacio']["preusEntrats"][$TCodi]['dataultmodifpre'] = $preus->PriceDateTime($codiprod,$TCodi);
				$result['informacio']["preusEntrats"][$TCodi]['metode'] = $preus ->methodPrices($codiprod);
				$result['informacio']["preusEntrats"][$TCodi]['es_automatic'] = $preus->is_priceAuto($codiprod, $TCodi, $any, $mes);
				$result['informacio']["preusEntrats"][$TCodi]['es_validat'] = $preus->priceTerritoriValidat($codiprod,$TCodi);  
			}
			
		}
		
		return  $this->respond($result); 
	}
	
	public function invalidapreu($codiprod,$territori,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		if ($rol == 'sc') {
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$result['informacio']['aplicacio'] = $aplicacio;
			$base = $aplicacio->BASE-2000;
			$any = $aplicacio->ENCURS_ANY_DADES;
			$mes = $aplicacio->ENCURS_MES_DADES;
			$territoriModel = Territori::where('CODITERR',$territori)->first();
			$result['informacio']['estatterritori'] = $territoriModel;
			$pagatspercebuts = $this->_obtenirTipus($codiprod);
		    $result['informacio']['tipus'] = $pagatspercebuts;
			if ($territoriModel->ESTAT != 'revisant') {
				$result['estat']=false;
				$result['informacio']['msg'] = "El territori ".($territoriModel->NOMTERR)." no està en estat de revisió";
			} else {
					$where = " WHERE CodiProd = '".$codiprod."'
														AND CodiTerr  = '".$territori."' 
														AND ANY = '".$any ."'
														AND mes = '".$mes."'
														AND validat = 's'  ";
					
					$affected = DB::update( strtoupper("UPDATE preus".$pagatspercebuts." SET validat='")."n".strtoupper("' ".$where." "));
					$result['estat']=true;
					$result['informacio']['resp_update']=$affected;

			}
		} else {
			$result['estat']=false;
			$result['informacio']['msg'] = "Error de premisos.";
		}
		
		
		
		return  $this->respond($result); 
	}
	
	public function validapreu($codiprod,$territori,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		if ($rol == 'sc') {
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$result['informacio']['aplicacio'] = $aplicacio;
			$base = $aplicacio->BASE-2000;
			$any = $aplicacio->ENCURS_ANY_DADES;
			$mes = $aplicacio->ENCURS_MES_DADES;
			$territoriModel = Territori::where('CODITERR',$territori)->first();
			$result['informacio']['estatterritori'] = $territoriModel;
			if ($territoriModel->ESTAT != 'revisant') {
				$result['estat']=false;
				$result['informacio']['msg'] = "El territori ".($territoriModel->NOMTERR)." no està en estat de revisió";
			} else {
				
				$pagatspercebuts = $this->_obtenirTipus($codiprod);
				$result['informacio']['tipus'] = $pagatspercebuts;
				
				$preus 		= 	new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base,DB::connection());
				
				$esCAT = (strtolower($preus->TipusPreu($codiprod)) == 'cat');
				$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerr($codiprod,true);
				$Codterr = array();
				$ok=false;
				if ($esCAT) {
					while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
					  { 
						$territoriModel = Territori::all();
						$territsRevisantArr=array();
						foreach ($territoriModel as $unTerritori) {
							if ($unTerritori->ESTAT != 'revisant') { $territsRevisantArr[]=$unTerritori->NOMTERR[0]; } 
						}
						if ($TPercent>0)  {	if (in_array($Tlletra,$territsRevisantArr)) {    $Codterr[] = array_search($Tlletra,$preus->TerritorisLletra);  } }
					  }	
					if (count($Codterr)!=count($ponderacionsArr_tmp) and (count($Codterr)>0)) {
							$result['estat']=false;
							$result['informacio']['msg'] = "Preu CAT, actualització a múltiples territoris. No tots estan en estat de revisió";
						}
					else { $ok=true; }
				} else {
		            $Codterr[] = $territori;
					$ok=true;
				}
				if ($ok) {
					if($esCAT)
					{
						$where = " WHERE CodiProd = '".$codiprod."'
														AND ANY = '".$any ."'
														AND mes = '".$mes."'
														AND validat = 'n'  "; 
														//AND (  (  (FinPreu / ".str_replace(',','.',$preufinal).") > 0.99 ) or ( (FinPreu / ".str_replace(',','.',$preufinal).") < 1.01 ) )";
					 }
					else
					{
						$where = " WHERE CodiProd = '".$codiprod."'
														AND CodiTerr  = '".$territori."' 
														AND ANY = '".$any ."'
														AND mes = '".$mes."'
														AND validat = 'n'  ";
					}
					$affected = DB::update( strtoupper("UPDATE preus".$pagatspercebuts." SET validat='")."s".strtoupper("' ".$where." "));
					if ($affected==0) {
						    $result['estat']=false;
							$result['informacio']['msg'] = "No s'ha actualitzat";
					} else {
						$result['estat']=true;
						$result['informacio']['resp_update']=$affected;
					}
				}
				
			}
			
		} else {
			$result['estat']=false;
			$result['informacio']['msg'] = "Error de premisos.";
		}
		
		
		return  $this->respond($result); 
	}
	
	public function eliminapreu($codiprod,$territori,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		if ($rol == 'sc') {
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$result['informacio']['aplicacio'] = $aplicacio;
			$base = $aplicacio->BASE-2000;
			$any = $aplicacio->ENCURS_ANY_DADES;
			$mes = $aplicacio->ENCURS_MES_DADES;
			$territoriModel = Territori::where('CODITERR',$territori)->first();
			$result['informacio']['estatterritori'] = $territoriModel;
			if ($territoriModel->ESTAT != 'revisant') {
				$result['estat']=false;
				$result['informacio']['msg'] = "El territori ".($territoriModel->NOMTERR)." no està en estat de revisió";
			} else {
				
				$pagatspercebuts = $this->_obtenirTipus($codiprod);
				$result['informacio']['tipus'] = $pagatspercebuts;
				
				$preus 		= 	new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base,DB::connection());
				
				$esCAT = (strtolower($preus->TipusPreu($codiprod)) == 'cat');
				$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerr($codiprod,true);
				$Codterr = array();
				$ok=false;
				if ($esCAT) {
					while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
					  { 
						$territoriModel = Territori::all();
						$territsRevisantArr=array();
						foreach ($territoriModel as $unTerritori) {
							if ($unTerritori->ESTAT != 'revisant') { $territsRevisantArr[]=$unTerritori->NOMTERR[0]; } 
						}
						if ($TPercent>0)  {	if (in_array($Tlletra,$territsRevisantArr)) {    $Codterr[] = array_search($Tlletra,$preus->TerritorisLletra);  } }
					  }	
					if (count($Codterr)!=count($ponderacionsArr_tmp) and (count($Codterr)>0)) {
							$result['estat']=false;
							$result['informacio']['msg'] = "Preu CAT, actualització a múltiples territoris. No tots estan en estat de revisió";
						}
					else { $ok=true; }
				} else {
		            $Codterr[] = $territori;
					$ok=true;
				}
				if ($ok) {
					$where = " WHERE CodiProd = '".$codiprod."'
														AND CodiTerr  = '".$territori."' 
														AND ANY = '".$any ."'
														AND mes = '".$mes."'
														AND validat = 'n'  ";
					$affected = DB::delete( strtoupper("DELETE from preus".$pagatspercebuts."  ".$where." "));
					if ($affected==0) {
						    $result['estat']=false;
							$result['informacio']['msg'] = "No s'ha eliminat";
					} else {
						$result['estat']=true;
						$result['informacio']['resp_update']=$affected;
					}
				}
				
			}
			
		} else {
			$result['estat']=false;
			$result['informacio']['msg'] = "Error de premisos.";
		}
		
		
		return  $this->respond($result); 
	}
	
	
	public function autopreu($codiprod,$territori,$any,$mes) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
	    $result['informacio']['codiprod'] = $codiprod;
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$rol = $user->rol;
		if ($rol == 'sc') {
			$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
			$result['informacio']['aplicacio'] = $aplicacio;
			$base = $aplicacio->BASE-2000;
			$any = $aplicacio->ENCURS_ANY_DADES;
			$mes = $aplicacio->ENCURS_MES_DADES;
			$territoriModel = Territori::where('CODITERR',$territori)->first();
			$result['informacio']['estatterritori'] = $territoriModel;
			if ($territoriModel->ESTAT != 'revisant') {
				$result['estat']=false;
				$result['informacio']['msg'] = "El territori ".($territoriModel->NOMTERR)." no està en estat de revisió";
			} else {
				$pagatspercebuts = $this->_obtenirTipus($codiprod);
				$result['informacio']['tipus'] = $pagatspercebuts;
				
				$preus 		= 	new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base,DB::connection());
				
				$sql = "SELECT * FROM ".$preus->descripcioTable."  WHERE CodiProd ='".$codiprod."' ";	
				$rows_elProd = DB::connection()->select(strtoupper($sql)); 
				if (count($rows_elProd)==1) {
						$result['informacio']['res_elProd'] = ((array) $rows_elProd[0]);
						$row_elProd = ((array) $rows_elProd[0]);
						$tipus=$row_elProd['TIPUS'];
						$gestor=$row_elProd['GESTOR'];
						$result['informacio']['gestor'] = $gestor;
						$result['informacio']['tipus'] = $tipus;
						$esauto = false;
						if($tipus == "Prov")
						{
							$gestor_arr = array();
							$gestor_arr = explode(",",$gestor);   // $gestor="G,L ";
							$gestor_codis = array();
							
							for($i=0;$i<count($gestor_arr);$i++)
							{	$gestor_codis[$i+1] = array_search($gestor_arr[$i],$preus->TerritorisLletra);
								
							}

							$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerrMonth($codiprod,$mes);

							if (!array_search($territori,$gestor_codis))
							{	if (is_array($ponderacionsArr_tmp))
								{	
									$territori_lletra = $preus->TerritorisLletra[$territori];
									$percent = $ponderacionsArr_tmp[$territori_lletra];
											if ($percent > 0) $esauto = true;
								}
							}	
						}
						 
						if ($esauto) {
							$Conn = DB::connection();    
							$preuFinal=$preus->price($codiprod);
							if ($preuFinal==0) {
									$result['informacio']['preuFinal'] = $preuFinal;
									$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerr($codiprod,true);	   
									// Array ( [B] => 0 [G] => 0 [L] => 85.204123042 [T] => 14.795876958 ) 
									$ponderacionsArr=array();
									$preusArr=array();
									while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
									{  if ($preus->TerritorisLletra[$territori]!=$Tlletra)
									  { 
										$preuFinalTerr=$preus->priceTerritori($codiprod,array_search($Tlletra,$preus->TerritorisLletra));
										if ($preuFinalTerr>0)
										{
										  $ponderacionsArr[]=$TPercent;
										  $preusArr[]=$preuFinalTerr;
										}
									  }
									}
									$result['informacio']['preusArr'] = $preusArr;
									$preuMitjPond = \App\Library\Calculs::mitjanaPonderada($ponderacionsArr,$preusArr);
									$preuMitjPond = round($preuMitjPond*100)/100.0;
									$numPreus = $preus->maxPrices($codiprod);	   
									$calcpreufinal = $preus->methodPrices($codiprod);
									if ($calcpreufinal=='M') 
									{    
										if ($numPreus>0) $preu[1] = $preuMitjPond;
										if ($numPreus>1) $preu[2] = $preuMitjPond;
										if ($numPreus>2) $preu[3] = $preuMitjPond;
										if ($preus->priceTerritoriValidat($codiprod,$territori)) die();
										if (($preu[1]+$preu[2]+$preu[3])>0)  // hi ha algun preu ....
										{  $noupreu = " INSERT INTO  preus".$pagatspercebuts." 
													   (  codiprod,coditerr,ANY,mes ";
										   for ($i = 1; $i <= $numPreus; $i++) 
												 {  if ($preu[$i]>0) 
													{
														$noupreu .= ", Preu".$i." , DataPreu".$i."  , HoraPreu".$i." ";
													}
												 }						
										   $noupreu .= ",FinPreu,autom ) VALUES ( '".$codiprod."',".$territori.",".$any.",".$mes.", ";						
										   $q=0;
										   $sumapreu=0;
										   for ($i = 1; $i <= $numPreus; $i++) 
												 {  if ($preu[$i]>0) 
													{  $q++;
														$sumapreu += $preu[$i];
														$noupreu .= " ".$preu[$i].",  NOW() ,NOW() ,  ";
													}
												 }
										   $preufinal = $sumapreu;
										   $preufinal = $preufinal / $q;		
										   $noupreu .= " ".$preufinal.",'s' ) ";
										   // INSERT INTO preuspercebuts ( codiprod,coditerr,"ANY",mes , Preu1 , DataPreu1 , HoraPreu1 , Preu2 , DataPreu2 , HoraPreu2 , Preu3 , DataPreu3 , HoraPreu3 ,FinPreu) VALUES ( 001100,25,2007,12, 20, SYSDATE ,SYSDATE , 20, SYSDATE ,SYSDATE , 20, SYSDATE ,SYSDATE , 20 ) 					
										   if ($preufinal>0)
										   {  
									           $res_noupreu = $Conn->select(strtoupper($noupreu));
											   //echo $res_noupreu;
											   if ($res_noupreu === false) die("Error.".".Error.".$noupreu); 
											   $result['informacio']['preuMitjPond'] = $preuMitjPond;
										   }
										   else {
											    $result['estat']=false;
												$result['informacio']['msg'] = "Preu final es zero.";
										   }											   
										   
										 }	else {
											    $result['estat']=false;
												$result['informacio']['msg'] = "Preu final es zero.";
										   }			 
									}
									// 
									if ($calcpreufinal=='S') 
										{   // s'ha de posar la mitjana de cada un dels preus
											$elsPreusExistents = array();
											for ($i = 1; $i <= $numPreus; $i++) 
											{
												$elsPreusExistents['MitjanaPreu'.$i] = 0;
											}
											$QuantsTenenPreu = 0;
											// Array ( [B] => 0 [G] => 0 [L] => 85.204123042 [T] => 14.795876958 ) 
											reset($ponderacionsArr_tmp);
											while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
											{   $NumeroTerr = array_search($Tlletra, $preus->TerritorisLletra);	    	   
												$elsPreusExistents[$NumeroTerr]=$preus->enteredPricesTerritori($codiprod,$NumeroTerr);	    	    
												$sumaTmp = 0;
												for ($i = 1; $i <= $numPreus; $i++) 
												{   if (isset($elsPreusExistents[$NumeroTerr]['PREU'.$i]))
													{
													 $elsPreusExistents['MitjanaPreu'.$i] += $elsPreusExistents[$NumeroTerr]['PREU'.$i];
													 $sumaTmp += $elsPreusExistents[$NumeroTerr]['PREU'.$i];
													}
												}
												if ($sumaTmp>0) $QuantsTenenPreu++;  // Hi ha preu
												
											}
											
											for ($i = 1; $i <= $numPreus; $i++) 
												{  if ($QuantsTenenPreu>0)
												   {
													$elsPreusExistents['MitjanaPreu'.$i] = $elsPreusExistents['MitjanaPreu'.$i]/$QuantsTenenPreu;
												   }
												}
											
												/*
													Array ( [MitjanaPreu1] => 12.5 
															[MitjanaPreu2] => 22.5 
															[8] => Array ( [PREU1] => 10 [PREU2] => 20 [PREU3] => 0 [NECPREUS] => 2 [MAXPREUS] => 2 ) 
															[17] => Array ( [PREU1] => 15 [PREU2] => 25 [PREU3] => 46.3 [NECPREUS] => 2 [MAXPREUS] => 2 ) 
															[25] => Array ( [NECPREUS] => 2 [MAXPREUS] => 2 ) 
															[43] => Array ( [NECPREUS] => 2 [MAXPREUS] => 2 ) )  
												 */
											$noupreu = " INSERT INTO  preus".$pagatspercebuts." 
														   (  codiprod,coditerr,\"ANY\",mes ";
											   for ($i = 1; $i <= $numPreus; $i++) 
													 { 
															$noupreu .= ", Preu".$i." , DataPreu".$i."  , HoraPreu".$i." ";
													 }						
											   $noupreu .= ",FinPreu,autom ) VALUES ( '".$codiprod."',".$territori.",".$any.",".$mes.", ";						
											   $q=0;
											   $sumapreu=0;
											   for ($i = 1; $i <= $numPreus; $i++) 
													 {  
														 $noupreu .= " ".$elsPreusExistents['MitjanaPreu'.$i].",  SYSDATE ,SYSDATE ,  ";
														 $sumapreu += $elsPreusExistents['MitjanaPreu'.$i];
													 }
											   $preufinal = $sumapreu;
											   $preufinal = $preufinal;		
											   $noupreu .= " ".$preufinal.",'s' ) ";
											   if ($preufinal>0)
											   {   $res_noupreu = $Conn->Execute($noupreu);	
												   die($preufinal.'::'.$territori);
											   }
											   else die("Error."."Preu final 0.<br>"); 
											
										}
									
							} // $preuFinal==0
						    else {
												$result['estat']=false;
												$result['informacio']['msg'] = "El preu ja existeix.";
							} // 
						} // $esauto
						else{}
								
				} // codi correcte
				else {
					$result['estat']=false;
					$result['informacio']['msg'] = "Codi incorrecte.";
				}
			
			} // estat revisant
		} // té rol sc
		else {
			$result['estat']=false;
			$result['informacio']['msg'] = "Error de premisos.";
		}
		
		return  $this->respond($result);
		
	}
}                                                                  
                                                                   
?>