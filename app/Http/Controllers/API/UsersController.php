<?php 

namespace App\Http\Controllers\API;

use JWTAuth;
use Auth;
use ExcelMaat;
use APIException;
use DB;
use App\Models\User;
use App\Models\Aplicacio;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;


class UsersController extends APIController
{
	/**
	 * Create a new user account
	 * 
	 * @param Request $request 
	 * @return Response
	 */
	public function register(Request $request)
	{	
		$user = new User;
		$user->register($request);

		return $this->respondCreated();
	}


	/**
	 * Retrieve authentication token
	 * 
	 * @param Request $request 
	 * @return type
	 */
	public function login(Request $request)
	{
		$user = new User;
		$token = $user->login($request);
		$user  = Auth::User();  
		$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();

		return $this->respond([
			'token' => $user->login($request),
			'rol'	=> $user->rol,
			'nom'	=> $user->first_name,
			'cognom'=> $user->last_name,
			'aplicacio'=> $aplicacio
		]);
	}

	/**
	 * Invalidate current token
	 *
	 * @param Request $request
	 */
	public function logout(Request $request)
	{
		$user = new User;
		$user->logout($request);

		return $this->respond();
	}

	/**
	 * Refresh an authentication token
	 * 
	 * @param Request $request 
	 */
	public function refreshToken(Request $request)
	{
		$user = new User;
		
		return $this->respond([
			'token' => $user->refreshToken($request)
		]);
	}

	/**
	 * Get language for current Request
	 * 
	 * @param Request $request 
	 */
	public function getLanguage(Request $request)
	{
		return $this->respond([
			'language' => 'ca'
		]);
	}
	
	/**
	 * Retorna la informació bàsica d'un usuari a partir del seu token
	 * 
	 * @param Request $request 
	 * @return Response
	 */
	public function info(Request $request)
	{	
		$user = JWTAuth::parseToken()->toUser();
		$territori = 0;
		if ($user->rol=='sc') { $territori = 99;  $territorinom='catalunya'; }
		if ($user->rol=='st') { 
			if ($user->last_name=='Barcelona') { $territori = 8;  $territorinom=$user->last_name;}
			if ($user->last_name=='Girona')    { $territori = 17; $territorinom=$user->last_name;}
			if ($user->last_name=='Lleida')    { $territori = 25; $territorinom=$user->last_name;}
			if ($user->last_name=='Tarragona') { $territori = 43; $territorinom=$user->last_name;}
		}
		return $this->respond([
			'nom' 		=> $user->first_name,
			'cognom' 	=> $user->last_name,
			'rol' 		=> $user->rol,
			'territorinum'	=> $territori,
			'territorinom'	=> $territorinom
		]);
	}
	
	/**
	 * Retorna informació de l'aplicació
	 * aquest mètode estaà pensat per ser una eina de test
	 * 
	 * @param Request $request 
	 */
	public function getAppInfo(Request $request)
	{
		return $this->respond([
			'nom' => 'PPP',
			'versio' => "ver 1.0",
			'dades/lògica' => [
			'sftp'=> [
				'host'=> 'laravel.ticnologia.com',
				'user'=> 'laravelticnologia',
				'pwd' => 'TICnolog2017',
				'port'=> 22
				],
			'bbdd'=> [
				'type'=>'MySql',
				'host'=>'laravel.ticnologia.com',
				'user'=> 'myticnolog',
				'pwd' => 'umFJaNuZ',
				'name'=> 'preus'
				],
			'repo'=>'git@bitbucket.org:alberttuatara/apipreus.git'
			],
			'pantalles' => [
				'repo'=>'git@bitbucket.org:alberttuatara/pantallespreus.git',
				'accio'=>'npm run start:hmr'
				],
			'aplicació amb Oracle'	=> [
				'http'=> [
					'host'=> 'http://81.37.212.229',
					'port' => 81,
					'ruta'=> '/preus/entrar.html',
					'users' => [
						['user'=> 'sc@sc.ppp',	'pwd' => 'sc'],
						['user'=> 'b@st.ppp', 'pwd' => 'b' ],['user'=> 'g@st.pppt', 'pwd' => 'g' ],['user'=> 'll@st.ppp', 'pwd' => 'll' ],['user'=> 't@st.ppp', 'pwd' => 't' ]
					]
				],
				'ftp'=> [
					'host'=> 'http://IP pública ADSL/',
					'port' => 81,
					'ruta'=> '/ppp/ppp/',
					'users' => ['user'=> 'abc',	'pwd' => 'abc'],
					'port'=> 211
				],
			],
			'serveis de pàgines' => [
	             "pàgina d'entrada de preus" =>  'http://laravel.ticnologia.com/api/pagina/entrada/2010/percebuts/011000/2016/1',
				 "pàgina de revisió de preus" => 'http://laravel.ticnologia.com/api/pagina/revisio/2010/percebuts/054170/2017/2',
				 "pàgina de consulta d'històrics" => 'http://laravel.ticnologia.com/api/pagina/consulta/2010/percebuts/054170'
			]
		]);
	}
	
	/**
	 * Retorna informació per visualitzar a la pantalla HOME
	 * 
	 * @param Request $request 
	 */
	public function getIniciData()
	{
		return $this->respond([
			'titol' => "API per l'Aplicació Aplicatiu per gestionar l'EPPP a Catalunya"
			
		]);
	}
	

     
	/**
	 * Aquesta funció serveix per fer un TEST de la creació d'un Excel
	 * 
	 * 
	 */
	public function testexcel()
	{
		// https://github.com/Maatwebsite/Laravel-Excel
		ExcelMaat::create('Documents Excel', function($excel) {
			$excel->sheet('Fulla Excel', function($sheet) {
				$sheet->setOrientation('landscape');
			});
		})->export('xls');
		die();
	}
	
	
	/**
	 * Aquesta funció serveix per fer un TEST
	 * 
	 * @param Request $request 
	 */
	public function testpreus()
	{
		
		$is_ok = true;
		return $this->respond(array('is_ok'=>$is_ok));
	}
	
	/**
	 * Method protected by authentication for testing purpose
	 * 
	 * @param Request $request 
	 */
	public function getProtected(Request $request) 
	{
		return $this->respondAccepted();
	}
     
	public function user(Request $request)
	{	
		return $this->respond([
			'user' => User::getAuthenticated($request)
		]);
	}
}    
     
?>