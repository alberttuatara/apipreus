<?php 

namespace App\Http\Controllers\API;

use DateTime;
use DatePeriod;
use DateInterval;
use JWTAuth;
use Auth;
use App;
use APIException;
use DB;
use App\Models\Territori;
use App\Models\Aplicacio;
use App\Library\Preus;
use Illuminate\Http\Request;
use Illuminate\Http\Input;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;
use Response;


class CalculsController extends APIController
{
	
	public function __construct() {
		
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$this->rol = $user->rol;
		$this->timestart = new DateTime( "now" );

			
		$this->callback = function()
			{
				$FH = fopen('php://output', 'w');
				foreach ($this->list as $row) { 
					fputcsv($FH, $row);
				}
				fclose($FH);
			};
		
		
	}
	
	private function preparar() {
		

		
		return true;

		
	}
	
	public function resultatsCalculs($opcio,$any,$format='web',$precisio=false) {
		
		if ($this->rol != 'sc') {
				return response()->json(['error' => 'Not authorized.'],403);
		} else {
			$headers = [
					'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
				,   'Content-type'        => 'text/html'
				,   'Content-Disposition' => 'attachment; filename='.$opcio.'_'.$any.'.html'
				,   'Expires'             => '0'
				,   'Pragma'              => 'public'
			];
			$base = config('ppp.base')-2000;
			$pagatspercebuts = 'percebuts';
			$precis = $precisio;
			$filtre	=	'';
			$titol 	= '';
			$html	= '';
			if  ($opcio=='qFisiques')           		{  $passos=1; }
			if  ($opcio=='preusProductesAny')   		{  $passos=2; }
			if 	($opcio=='indexSimpleAnyT')    			{  $passos=3; }
			if 	($opcio=='indexCompostAnyT')   			{  $passos=4; }
			if  ($opcio=='indexCompostAnyTGrups') 		{  $passos=5; }
			if 	($opcio=='indexAnualMobilSimpleAnyT') 	{  $passos=6; }
			if 	($opcio=='indexAnualMobilCompostAnyT') 	{  $passos=7; }
			if 	($opcio=='acumulatAnyT')               	{  $passos=8; }
			if 	($opcio=='varACiMENanyT')              	{  $passos=9; }
			if 	($opcio=='contribucioAnyT')            	{  $passos=10; }
			if 	($opcio=='taxaVariacioInteranualAnyT') 	{  $passos=11; }
			
			$preus =  new \App\Library\Prices($pagatspercebuts,$any, 1, 99, $base ,DB::connection());
			$limits_anys_preus=$preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
			// Array ( [MAX] => 2008 [MIN] => 2000 [MMIN] => 1 [MMAX] => 1 ) 
			$anydesde = $limits_anys_preus['MIN'];
			$anyfi    = $limits_anys_preus['MAX'];
			$any_fer = $any;
			
			$excelfactory 	= new \App\Library\ExcelFactory(new \App\Library\SimpleFactory());
			$excel 	= $excelfactory->creaExcel(DB::connection(),$pagatspercebuts,$base);
			$avgCat=array();
			for ($any=$anydesde; $any<=$anyfi; $any++)
			{
			  $avgCat[$any] = $excel->entradaPreusAnyT($any);
			  if ($base=='05')
			  { unset($avgCat[$any]['113200']); // Segalls no surten en l'índex Base 05
				unset($avgCat[$any]['221000']); // Suro no surten en l'índex Base 05    
			  }
			  /*
						[054210] => Array
						(
							[DESCRIPCIO] => Bledes
							[MES1] => 0
							[MES2] => 0
							[MES3] => 0
							[MES4] => 0
							[MES5] => 0
							[MES6] => 0
							[MES7] => 0
							[MES8] => 0
							[MES9] => 0
							[MES10] => 0
							[MES11] => 0
							[MES12] => 0
							[ANUAL] => 0
						)

				*/		
				$avgCat_INI = $avgCat[$any];
				$avgCat[$any] = array();
				while (list($elcodi, $val) = each($avgCat_INI)) 
				{  //print_r($val);
				   if ($val['MES1']+$val['MES2']+$val['MES3']+$val['MES4']+$val['MES5']+$val['MES6']+
					  $val['MES7']+$val['MES8']+$val['MES9']+$val['MES10']+$val['MES11']+$val['MES12']>0)
						   $avgCat[$any][$elcodi] = $val;
				}
				
			  
			} // del loop ANYS
			/*
			Array ( [001100] => Array ( [DESCRIPCIO] => Blat tou o semidur 
												  [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 
												  [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
					   [002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg 
												   [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 
												   [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
					   [002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 
												   [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
					   [003300] => Array ( [DESCRIPCIO] => Ordi per a pinso 
													[MES1] => 13.83 [MES2] => 13.37 [MES3] => 12.71 [MES4] => 12.3 [MES5] => 12.3 [MES6] => 12.28 [MES7] => 12.1 [MES8] => 12.52 
												   [MES9] => 13.62 [MES10] => 14.69 [MES11] => 15.67 [MES12] => 15.94 [ANUAL] => 13.28 ) 
						·
						·
						·
			*/
			$qFisiques = $excel->qFisiques("basepercebuts".$base, $avgCat['20'.$base]);  // $avgCat['20'.$base]  --> Preus BASE sense les agrupacions Preus calculats amb tots els decimals.
			//  $qFisiques  ---> Quantitats físiques + preus anuals. Ja conté les agrupacions.
			/*
			Array ( [001100] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat tou o semidur [ANUAL] => 234804 [consist] => 001100 [PREUS_ANUAL] => 13.24 ) 
			[002100] => Array ( [SUM(MES1)] => 24029 [SUM(MES2)] => 12014 [SUM(MES3)] => 12014 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 13240 [SUM(MES12)] => 12423 [DESCRIPCIO] => Arròs closca rodó i semillarg [ANUAL] => 124227 [consist] => 002100 [PREUS_ANUAL] => 28.16 ) 
			[002200] => Array ( [SUM(MES1)] => 1100 [SUM(MES2)] => 1100 [SUM(MES3)] => 1100 [SUM(MES4)] => 0 [SUM(MES5)] => 0 [SUM(MES6)] => 0 [SUM(MES7)] => 0 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 0 [SUM(MES11)] => 1100 [SUM(MES12)] => 1100 [DESCRIPCIO] => Arròs closca llarg [ANUAL] => 5500 [consist] => 002200 [PREUS_ANUAL] => 28.13 ) 
			.
			.
			.
			[001000] => Array ( [SUM(MES1)] => 12321 [SUM(MES2)] => 8122 [SUM(MES3)] => 6022 [SUM(MES4)] => 6022 [SUM(MES5)] => 2100 [SUM(MES6)] => 19469 [SUM(MES7)] => 83664 [SUM(MES8)] => 37105 [SUM(MES9)] => 18995 [SUM(MES10)] => 16619 [SUM(MES11)] => 12044 [SUM(MES12)] => 12321 [DESCRIPCIO] => Blat [ANUAL] => 234804 [consist] => Array ( [0] => 001100 ) [PREUS_ANUAL] => 13.24 ) 
			[002000] => Array ( [SUM(MES1)] => 25129 [SUM(MES2)] => 13114 [SUM(MES3)] => 13114 [SUM(MES4)] => 12014 [SUM(MES5)] => 12014 [SUM(MES6)] => 12014 [SUM(MES7)] => 12014 [SUM(MES8)] => 0 [SUM(MES9)] => 0 [SUM(MES10)] => 2451 [SUM(MES11)] => 14340 [SUM(MES12)] => 13523 [DESCRIPCIO] => Arròs [ANUAL] => 129727 [consist] => Array ( [0] => 002100 [1] => 002200 ) [PREUS_ANUAL] => 28.1587280982 ) 
			[003000] => Array ( [SUM(MES1)] => 14855
			*/
			for ($any=$anydesde ; $any<=$anyfi; $any++)
			{
			  $preusProductesAnyT[$any] = $excel->preusProductesAnyT($avgCat[$any], $qFisiques);
			}
			/*
			preusProductesAnyT2006
			Array ( 
			[001100] => Array ( [DESCRIPCIO] => Blat tou o semidur [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
			[001000] => Array ( [DESCRIPCIO] => Blat [MES1] => 13.96 [MES2] => 13.82 [MES3] => 13.55 [MES4] => 13.42 [MES5] => 13.78 [MES6] => 13.35 [MES7] => 13.09 [MES8] => 13.69 [MES9] => 14.99 [MES10] => 16.31 [MES11] => 17.05 [MES12] => 17.26 [ANUAL] => 14.11 ) 
			[002100] => Array ( [DESCRIPCIO] => Arròs closca rodó i semillarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.95 [MES12] => 23 [ANUAL] => 23.76 ) 
			[002200] => Array ( [DESCRIPCIO] => Arròs closca llarg [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 0 [MES5] => 0 [MES6] => 0 [MES7] => 0 [MES8] => 0 [MES9] => 0 [MES10] => 0 [MES11] => 23 [MES12] => 23 [ANUAL] => 23.6 ) 
			[002000] => Array ( [DESCRIPCIO] => Arròs [MES1] => 24 [MES2] => 24 [MES3] => 24 [MES4] => 24 [MES5] => 24 [MES6] => 24 [MES7] => 24 [MES8] => 0 [MES9] => 0 [MES10] => 22.5 [MES11] => 22.9538354254 [MES12] => 23 [ANUAL] => 23.7532165239 ) 
			.
			.
			.
			.
			.
			.
			)
			*/
			$wValor = $excel->wValor($qFisiques);
			
			if ($passos>=3)
			{
				$indexSimpleAnyT_1  = $excel->indexSimpleAnyT($qFisiques, $preusProductesAnyT[$any_fer-1]);
				$indexSimpleAnyT    = $excel->indexSimpleAnyT($qFisiques, $preusProductesAnyT[$any_fer]);
				if ($passos>=4)
				{   
					$indexCompostAnyT_1 = $excel->indexCompostAnyT($indexSimpleAnyT_1, $wValor);
					$indexCompostAnyT            = $excel->indexCompostAnyT($indexSimpleAnyT, $wValor);		
					if ($passos>=5)
					{
						$indexCompostAnyTGrups       = $excel->indexCompostAnyTGrups($indexCompostAnyT, $wValor);
						if ($passos>=6)
						{
							$indexAnualMobilSimpleAnyT_1 = $excel->indexAnualMobilSimpleAnyT($preusProductesAnyT[$any_fer-1],
																							 $preusProductesAnyT[$any_fer-2], 
																							 $qFisiques);															 
							$indexAnualMobilSimpleAnyT = $excel->indexAnualMobilSimpleAnyT($preusProductesAnyT[$any_fer], 
																						   $preusProductesAnyT[$any_fer-1], 
																						   $qFisiques);
							if ($passos>=7)
							{															   
								$indexAnualMobilCompostAnyT_1 = $excel->indexAnualMobilCompostAnyT($indexAnualMobilSimpleAnyT_1, $wValor);
								$indexAnualMobilCompostAnyT = $excel->indexAnualMobilCompostAnyT($indexAnualMobilSimpleAnyT, $wValor);
								$accumulatAnyT = $excel->accumulatAnyT($wValor, $indexCompostAnyT);
								
								$varACiMENanyT = $excel->varACiMENanyT($accumulatAnyT, $indexCompostAnyT, $indexCompostAnyT_1, 
																	   $indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1);
																	   
								$contribucioAnyT = $excel->contribucioAnyT($indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1, 
																		   $indexAnualMobilSimpleAnyT, $indexAnualMobilSimpleAnyT_1, $wValor);
								
								
								$taxaVariacioInteranualAnyT = $excel->taxaVariacioInteranualAnyT($indexAnualMobilCompostAnyT, $indexAnualMobilCompostAnyT_1, 
																								 $indexAnualMobilSimpleAnyT, $indexAnualMobilSimpleAnyT_1);
							}
						}
					}
				}
			}	
			if ($opcio=='qFisiques')           {  $html = $excel->toHTMLqFisiques($qFisiques); }
			if ($opcio=='preusProductesAny')   {  $html = $excel->toHTML($preusProductesAny[$any_fer]); }
			if 	($opcio=='indexSimpleAnyT')    
				{  $titol = 'Índexs mensuals i anuals simples  '.$any_fer; 
				   if ($precisio) $html = $excel->toHTML($indexSimpleAnyT,'',10);
				   else  $html = $excel->toHTML($indexSimpleAnyT);
				}
			if 	($opcio=='indexCompostAnyT')   
				{  $titol = 'Índexs mensuals i anuals compostos  '.$any_fer;  
				   if ($precisio) $html = $excel->toHTML($indexCompostAnyT,'',10);
				   else  $html = $excel->toHTML($indexCompostAnyT);
				}
			if  ($opcio=='indexCompostAnyTGrups') 
				{ if ($formatOUT=='csv') 
					{ $excel->toCSVbyMonth($indexCompostAnyTGrups);
					}
				  else  { $html = $excel->toHTML($indexCompostAnyTGrups);}
				}
			if 	($opcio=='indexAnualMobilSimpleAnyT') 
				{  $titol = 'Índex anual mòbil simple  '.$any_fer; 
				   if ($precis) $html = $excel->toHTML($indexAnualMobilSimpleAnyT,'',10);
				   else $html = $excel->toHTML($indexAnualMobilSimpleAnyT);
				}
			if 	($opcio=='indexAnualMobilCompostAnyT') 
				{  $titol = 'Índex anual mòbil compost  '.$any_fer;
				   if ($filtre=='') 
					{ if ($precis) $html = $excel->toHTML($indexAnualMobilCompostAnyT,'',10);
					  else $html = $excel->toHTML($indexAnualMobilCompostAnyT);
					}
					else 
					{  if ($precis) $html = $excel->toHTML($indexAnualMobilCompostAnyT,$filtre,10);
					   else $html = $excel->toHTML($indexAnualMobilCompostAnyT,$filtre);
					}
				}
			if 	($opcio=='acumulatAnyT')               
				{  $titol = 'Acumulat compost any '.$any_fer; 
				   if ($precis) $html = $excel->toHTML($accumulatAnyT,'',10);
				   else  $html = $excel->toHTML($accumulatAnyT);
				}
			if 	($opcio=='varACiMENanyT')              
				{  $titol = 'Variació acumulada i mensual any '.$any_fer; 
				   $html = $excel->toHTMLvarACiMENanyT($varACiMENanyT,$precis);
				}
			if 	($opcio=='contribucioAnyT')            
				{  $titol = "Contribució a la millora (+) o empitjorament (-) de l'índex general any ".$any_fer; $excel->sortidaPercent = true; 
				   if ($precis) $html = $excel->toHTML($contribucioAnyT,'',10);
				   else  $html = $excel->toHTML($contribucioAnyT,'',3);
				}
			if 	($opcio=='taxaVariacioInteranualAnyT') 
				{  	$excel->toHTML_format_ZERO_especial = false;  // no volem que marqui els zeros com a "--";
					$titol = 'Taxa de variació interanual any '.$any_fer; 
					 if ($precis) $html = $excel->toHTML($taxaVariacioInteranualAnyT,'',10);
					 else  $html = $excel->toHTML($taxaVariacioInteranualAnyT);
				}
			
			
			
			$html = '<html><body><style type="text/css">
			<!--
			body,td,th {
				font-family: Helvetica, Arial;
				font-size: 11px;
				color: #333333;
			}
			a:link {
				color: #333333;
				text-decoration: none;
			}
			a:visited {
				text-decoration: none;
				color: #333333;
			}
			a:hover {
				text-decoration: none;
				color: #0066FF;
			}
			a:active {
				text-decoration: none;
			}
			-->
			</style>
			<h2>'.$titol.'. Base '.$base.'</h2><br>'.$html.'</body></html>';
			
				
			$output = $html;
			$theoutput = function($output)  { return $output; };                                                             
			return response($theoutput($output), 200, $headers); 
		} // És SC
				
		
	}
	
	
}                                                                              
                                                                               
 ?>                                                                            