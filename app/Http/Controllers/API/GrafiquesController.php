<?php 

namespace App\Http\Controllers\API;

use JWTAuth;
use APIException;
use DB;
use App\Models\Territori;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;


class GrafiquesController extends APIController
{
	

	/**
	 * Retorna la informació per la gràfica 
	 *  Importància relativa del preu durant l'any per territoris.    
     *  Ponderacions quantitatives. Base 2010
	 * 
	 * @param Request $request 
	 * @return Response
	 */
	public function importanciarelativa()
	{	
		return $this->respondAccepted();
	}
	
	

}    
     
 ?>  