<?php 

namespace App\Http\Controllers\API;

use DateTime;
use DatePeriod;
use DateInterval;
use JWTAuth;
use Auth;
use APIException;
use DB;
use App\Models\Territori;
use App\Library\Preus;
use Illuminate\Http\Request;
use Illuminate\Http\Input;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;


class PaginesController extends APIController
{
	
	public function __construct() {
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$this->timestart = new DateTime( "now" );
		
	}
	
	private function a_Float($str) {
		return floatval(str_replace(",",".",$str));
	}
	
	public function status(Request $request) {
		$result = array('estat'=>true, 'informacio'=>array('status'=>'ok'));
		
		$preus = new Preus();
		$result['informacio'] = $preus->is_ok();
		return $this->respond($result);
	}
	
	public function logtime($stepid,&$res) {
		$timenow = new DateTime(  "now"  );
		$diffInSeconds = $timenow->getTimestamp() - $this->timestart->getTimestamp();
		$res['time']['STEP_'.$stepid]=$diffInSeconds;
		
	}
	
	public function TerritoriNom_a_codi($territorinom) {
		$TerritorisNoms = array('8'=>'Barcelona','17'=>'Girona','25'=>'Lleida','43'=>'Tarragona');
		$ret=0;
		foreach($TerritorisNoms as $clave => $valor){    
				if (strtolower($valor) == strtolower($territorinom)) { $ret=$clave; }
		}
		return $ret;
	}
	
	/**
	 * Retorna informació per visualitzar a la pantalla d'inici de gestió dels preus percebuts
	 * 
	 * @param Request $request 
	 */
	public function getProductesLlista(Request $request)
	{
		$res = array();
		
		$user = JWTAuth::parseToken()->toUser();
		
		$res['info']=array();
		$res['info']['rol']=$user->rol;
		$res['info']['select_productes_visualitzar']=array();
		$res['info']['select_productes_visualitzar'][] = array('Tots' => '- Tots -');
		if ($user->rol == 'st') { 
			$res['info']['select_productes_visualitzar'][] = array('CalFerCal' => 'Els que aquest mes cal entrar preu (segons calendari)');
			$res['info']['select_productes_visualitzar'][] = array('CalFerGestors' => 'Els que aquest mes cal entrar preu (segons gestors)');
			$res['info']['select_productes_visualitzar'][] = array('NoCalFerCal' => 'Els que aquest mes no cal entrar preu (segons calendari)');
			$res['info']['select_productes_visualitzar'][] = array('NoCalFerGestor' => 'Els que aquest mes no cal entrar preu (segons gestors)');
		}
		if ($user->rol == 'sc') { 
			$res['info']['select_productes_visualitzar'][] = array('CalFerCal' => 'Els que aquest mes cal entrar preu');
			$res['info']['select_productes_visualitzar'][] = array('NoCalFerCal' => 'Els que aquest mes no cal entrar preu');
		}
		$res['info']['select_productes_visualitzar'][] = array('TenenPreu' =>  'Els que tenen preu');
		$res['info']['select_productes_visualitzar'][] = array('NoTenenPreu' =>  'Els que no tenen preu');
		$res['info']['select_productes_visualitzar'][] = array('TenenPreuNoFinalitzat' =>  'Els que tenen preu però no finalitzat');
		$res['info']['select_productes_visualitzar'][] = array('TenenPreuFinalitzat' =>  'Els que tenen preu finalitzat');
		$res['info']['select_productes_visualitzar'][] = array('AmbComentari' =>  'Els que tenen comentaris');
		$res['info']['select_productes_visualitzar'][] = array('AmbUltimComentariSC' =>  'Els que tenen comentari sense respondre');
		$res['info']['select_productes_visualitzar'][] = array('TenenPreuFinalitzatNoValidat' =>  'Els que tenen preu finalitzat però no validat');
		$res['info']['select_productes_visualitzar'][] = array('TenenPreuFinalitzatValidat' =>  'Els que tenen preu finalitzat i estan validats');

		
		$res['dades']=array();
		
		return $this->respond($res);
	}
	
	public function getProductesData($base,$pagatspercebuts, $any, $mes,$opcio) {
		$res = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		
		$this->logtime(0,$res);
		$res['informacio']["block"]=null;
		$base = $base-2000;
		$res['informacio']['opcio']		= $opcio;
		$res['informacio']['tipuspreus']	= $pagatspercebuts;
		$res['informacio']['base']		= $base;
		$res['informacio']['any']			= $any;
		$res['informacio']['mes']			= $mes;
		$res['informacio']['rol'] 		= $this->user->rol;
		$territori=0;
		if ($this->user->rol == 'st') {
			$territori = $this->TerritoriNom_a_codi($this->user->last_name);
			$res['informacio']['territori'] = $territori;
		}
		
		$MOrdre='A';
		$MOrdreCamp='CodiProd'; // 'MOrdreCamp','CodiProd'
		$quantsProds = 0;
		$estatAplicatiu = 'entrada';  // revisant  historic
		$this->preparePage($base,$pagatspercebuts,'000000',$any, $mes, $territori);
		
		$elsCalFerCal = null;
		$filtreVista = $opcio;
		$filtreOrdre = '';
		$tots = $this->preus->arrayOfAllProducts();  // [1]
		$this->preus->tots = $tots;
		$this->logtime(1,$res);
		if ($this->user->rol == 'st') {
			if ($filtreVista=='CalFerCal')        { $elsCalFerCal       = $this->preus->arrayOfProductsToDo(); } // [2]
			if ($filtreVista=='CalFerGestors')    { $elsCalFerGestors   = $this->preus->arrayOfProductsToDoWithPricesTypes($territori); } // [3]
			if ($filtreVista=='NoCalFerCal')      { $elsCalFerCal       = $this->preus->arrayOfProductsToDo();
													$elsNoCalFerCal     = array_diff_key($tots,$elsCalFerCal);  // [4] = [1]-[2]
												  }
			if ($filtreVista=='NoCalFerGestor')   { $elsNoCalFerGestors = array_diff_key($tots,$this->preus->arrayOfProductsToDoWithPricesTypes($territori));	 } // [5] = [1]-[3]
			if ($filtreVista=='TenenPreu')        { $elsTenenPreu       = $this->preus->TenenPreu($this->preus->arrayOfProductsToDoWithPricesTypes($territori)); } // [6] suconjunt de [3]
			if ($filtreVista=='NoTenenPreu')      { $elsCalFerGestors   = $this->preus->arrayOfProductsToDoWithPricesTypes($territori); 
													$elsNoNoTenenPreu   = array_diff_key($elsCalFerGestors,$this->preus->TenenPreu($elsCalFerGestors));	 } // [7] = [3]-[6]
			if ($filtreVista=='TenenPreuNoFinalitzat')
					{ $elsTenenPreu       = $this->preus->TenenPreu($this->preus->arrayOfProductsToDoWithPricesTypes($territori));
					  $elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzat($elsTenenPreu); } // [8] suconjunt de [6]  										
					
				}
			if ($filtreVista=='TenenPreuFinalitzat')
                                      { $elsTenenPreu       		= $this->preus->TenenPreu($this->preus->arrayOfProductsToDoWithPricesTypes($territori));
                                        $elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzat($elsTenenPreu);
                                        $elsTenenPreuFinalitzat     = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat); } // [9] = [6]-[8]              
			if ($filtreVista=='TenenPreuFinalitzatNoValidat')
			    {   $elsTenenPreu                = $this->preus->TenenPreu($this->preus->arrayOfProductsToDoWithPricesTypes($territori));
                    $elsTenenPreuNoFinalitzat    = $this->preus->TenenPreuNoFinalitzat($elsTenenPreu);
                    $elsTenenPreuFinalitzat      = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat);                    
			        $elsTenenPreuFinalitzatNoValidat = $this->preus->TenenPreuFinalitzatNoValidat($elsTenenPreuFinalitzat);  // [10] suconjunt de [9]  	
				}
			if ($filtreVista=='TenenPreuFinalitzatValidat')		// [11] = [9]-[10]
			{               $elsTenenPreu                = $this->preus->TenenPreu($this->preus->arrayOfProductsToDoWithPricesTypes($territori));
							$elsTenenPreuNoFinalitzat    = $this->preus->TenenPreuNoFinalitzat($elsTenenPreu);
							$elsTenenPreuFinalitzat      = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat);                
							$elsTenenPreuFinalitzatNoValidat = $this->preus->TenenPreuFinalitzatNoValidat($elsTenenPreuFinalitzat);
							$elsTenenPreuFinalitzatValidat     = array_diff_key($elsTenenPreuFinalitzat,$elsTenenPreuFinalitzatNoValidat);
				
			}	
			if ($filtreVista=='AmbComentari') $AmbComentari=$this->preus->arrayOfProductsWithNotes();	
			if ($filtreVista=='AmbUltimComentariSC') $AmbUltimComentariSC=$this->preus->arrayOfProductsWithLastNoteSC();
				
		
		if ($this->user->rol == 'sc') {
			$AmbUltimComentariSC=null;
			if ($filtreVista=='CalFerCal')        { $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat(); } // [2]
			if ($filtreVista=='NoCalFerCal')      { $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
													$elsNoCalFerCal     = array_diff_key($tots,$elsCalFerCal);
												  } // [4] = [1]-[2]
			if ($filtreVista=='TenenPreu')        { $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
													$elsTenenPreu       = $this->preus->TenenPreuCat($elsCalFerCal); } // [6] suconjunt de [2]
			if ($filtreVista=='NoTenenPreu')      { $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
													$elsTenenPreu       = $this->preus->TenenPreuCat($elsCalFerCal);
													$elsNoNoTenenPreu   = array_diff_key($elsCalFerCal,$elsTenenPreu);	 } // [7] = [2]-[6]	
			if ($filtreVista=='TenenPreuNoFinalitzat')
													{ $elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzatCat(); }  // [8]     
			if ($filtreVista=='TenenPreuFinalitzat')
                                      { $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
                                        $elsTenenPreu       = $this->preus->TenenPreuCat($elsCalFerCal);
                                        $elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzatCat();
                                        $elsTenenPreuFinalitzat     = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat); } // [9] = [6]-[8] 
			if ($filtreVista=='TenenPreuFinalitzatNoValidat')
			    {  $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
                   $elsTenenPreu       = $this->preus->TenenPreuCat($elsCalFerCal);
                   $elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzatCat();
                   $elsTenenPreuFinalitzat     = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat);
                   $elsTenenPreuFinalitzatNoValidat = $this->preus->TenenPreuFinalitzatNoValidatCat($elsTenenPreuFinalitzat); }  // [10] subconjunt de [9]
			if ($filtreVista=='TenenPreuFinalitzatValidat')		// [11] = [9]-[10]
				{   $elsCalFerCal       = $this->preus->arrayOfProductsToDoCat();
					$elsTenenPreu       = $this->preus->TenenPreuCat($elsCalFerCal);
					$elsTenenPreuNoFinalitzat   = $this->preus->TenenPreuNoFinalitzatCat();
					$elsTenenPreuFinalitzat     = array_diff_key($elsTenenPreu,$elsTenenPreuNoFinalitzat);
					$elsTenenPreuFinalitzatNoValidat = $this->preus->TenenPreuFinalitzatNoValidatCat($elsTenenPreuFinalitzat);
					$elsTenenPreuFinalitzatValidat     = array_diff_key($elsTenenPreuFinalitzat,$elsTenenPreuFinalitzatNoValidat);
				}     
			if ($filtreVista=='AmbComentari') $AmbComentari=$this->preus->arrayOfProductsWithNotesCat();
			if ($filtreVista=='AmbUltimComentariSC') $AmbUltimComentariSC=$this->preus->arrayOfProductsWithLastNoteSC_Cat();
													               
		}                                                          
		$this->logtime(2,$res);                                    
		$res['informacio']['elsCalFerCal']=$elsCalFerCal;          
		$SQL_elsProds = strtoupper("SELECT *  FROM ".$this->preus->descripcioTable."
                               where actiu".$base." = 's'          
                               ORDER BY ".$this->preus->descripcioTable.".".$MOrdreCamp."  ".(($MOrdre=='A')?'ASC':'DESC')." ");	
		$Conn = DB::connection();                                  
		$rows_elsProds = $Conn->select($SQL_elsProds);             
		$num = count($rows_elsProds);                              
		$llistat_preus=array();                                    
		$elsProductesLlista=array();	                           
		if ($num>0){                                               
			/**                                                    
			* while                                                
			* set -> MapaUSU                                       
			* set -> filtreVista                                   
			* set -> PreusSessio                                   
			* set -> mes                                           
			* set-> territoriNom                                   
			* set -> AscDecCodiProd                                
			* set -> AD                                            
			* set -> llegenda_estat                                
			* set -> llegenda_estat_ST                             
			*/	                                                   
			foreach($rows_elsProds as $rowOBJ){      
                			
				$fila = ((array) $rowOBJ);                         
				 // $fila ===> Array ( [CODIPROD] => 001100 [NOMPROD] => Blat tou o semidur [UNITAT] => eur/100 kg [SUBGRUP] => Blat [GRUP] => CEREALS [FASEINTERCANVI] => De productor a majorista
				//   [CONDICIONSCOMERCIALITZACIO] => A granel, s/magatzem agricultor [NECPREUS] => 3 [MAXPREUS] => 3 [CALCUL] => M [DESCPREU1] => al dia 5 [DESCPREU2] => al dia 15 [DESCPREU3] => al dia 25 [ACTIU] => s 
				//   [PUBLICAR] => s [TIPUS] => Cat [GESTOR] => LL ) 
				$CodiProd = $fila['CODIPROD'];                     
				$max=$fila['MAXPREUS'];                            
				$nec=$fila['NECPREUS'];                            		                                                   
				$calmostrar = true;                                
				if ($this->user->rol == 'st') {                    
			        if ($filtreVista=='CalFerCal')      { $calmostrar = array_key_exists ( $CodiProd, $elsCalFerCal);        }     
					if ($filtreVista=='CalFerGestors')  { $calmostrar = array_key_exists ( $CodiProd, $elsCalFerGestors);    }	
					if ($filtreVista=='NoCalFerCal')    { $calmostrar = array_key_exists ( $CodiProd, $elsNoCalFerCal);      }
					if ($filtreVista=='NoCalFerGestor') { $calmostrar = array_key_exists ( $CodiProd, $elsNoCalFerGestors);  }
					if ($filtreVista=='TenenPreu')      { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreu);        }  
					if ($filtreVista=='NoTenenPreu')    { $calmostrar = array_key_exists ( $CodiProd, $elsNoNoTenenPreu);    }
					if ($filtreVista=='TenenPreuNoFinalitzat') { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuNoFinalitzat);   }
					if ($filtreVista=='TenenPreuFinalitzat')   { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzat);     }
					if ($filtreVista=='TenenPreuFinalitzatNoValidat')  { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzatNoValidat);   }
					if ($filtreVista=='TenenPreuFinalitzatValidat')   { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzatValidat);   }
					if ($filtreVista=='AmbComentari')   
							{ if (is_array($AmbComentari)) $calmostrar = array_key_exists ( $CodiProd, $AmbComentari); 
							  else $calmostrar = false;        
							}
					if ($filtreVista=='AmbUltimComentariSC')      
						{ if (is_array($AmbUltimComentariSC)) $calmostrar = array_key_exists ( $CodiProd, $AmbUltimComentariSC);       
						  else $calmostrar = false;  
						} 		

					
				}                                                  
				if ($this->user->rol == 'sc') {                    
					if ($filtreVista=='CalFerCal')      { $calmostrar = array_key_exists ( $CodiProd, $elsCalFerCal);       }
					if ($filtreVista=='NoCalFerCal')    { $calmostrar = array_key_exists ( $CodiProd, $elsNoCalFerCal);     }
					if ($filtreVista=='TenenPreu')      { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreu);       }  
					if ($filtreVista=='NoTenenPreu')    { $calmostrar = array_key_exists ( $CodiProd, $elsNoNoTenenPreu);   }
					if ($filtreVista=='TenenPreuNoFinalitzat') { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuNoFinalitzat);   }
					if ($filtreVista=='TenenPreuFinalitzat') { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzat);   }
					if ($filtreVista=='TenenPreuFinalitzatNoValidat')  { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzatNoValidat);   }	
					if ($filtreVista=='TenenPreuFinalitzatValidat')    { $calmostrar = array_key_exists ( $CodiProd, $elsTenenPreuFinalitzatValidat);   }
					if ($filtreVista=='AmbComentari')   
								{ if (is_array($AmbComentari)) $calmostrar = array_key_exists ( $CodiProd, $AmbComentari);
								  else $calmostrar = false;       
								}
					if ($filtreVista=='AmbUltimComentariSC')      
								{ if (is_array($AmbUltimComentariSC)) $calmostrar = array_key_exists ( $CodiProd, $AmbUltimComentariSC);   
								  else $calmostrar = false;          
								}
				}                                                  
				                                                   
				if ($calmostrar)                                   
				{   $quantsProds++;                                
					if ($this->user->rol == 'sc')                  
					{                                              
					   $llistat_preus[$CodiProd]  = array('NomProd'=>$fila['NOMPROD'],
														  'Grup'=>$fila['GRUP'],
														  'Subgrup'=>$fila['SUBGRUP'],
														  'tipus'=>$fila['TIPUS'],
														  'gestor'=>$fila['GESTOR'],
														  'PreusTerrits'=>$this->preus->stateAllTerritories($CodiProd, $any, $mes, $pagatspercebuts)
													);             
					                                               
					}                                              
					if ($this->user->rol == 'st') {                
						$llistat_preus[$CodiProd]  = array('NomProd'=>$fila['NOMPROD'],
												 'Grup'=>$fila['GRUP'],
												 'Subgrup'=>$fila['SUBGRUP'],
			                                     'tipus'=>$fila['TIPUS'],
			                                     'gestor'=>$fila['GESTOR'],
		                                         'PreusElTerrit'=>$this->preus->enteredPricesTerritori($CodiProd,$territori));                                     
					}                                              
				}                                                  
			}                                                      
			                                                       
		}              	
		$this->logtime(3,$res);                                    
		$elsCodis_x_llistar = array_keys($llistat_preus);          
		reset($elsCodis_x_llistar);                                
		$ix=0;                                                     
		while (list (, $codi) = each ($elsCodis_x_llistar)) {      
			$info=$llistat_preus[$codi];                           
			$elsProductesLlista[]=$codi;                           
			// SC : $info ---> Array ( [NomProd] => Blat tou o semidur [Grup] => CEREALS [Subgrup] => Blat [PreusTerrits] => Array ( [8] => res [17] => OK [25] => OK [43] => res ) )
			// ST :                                                
			$ix++;                                                 
			if ($this->user->rol == 'sc') {                        
				$unitats 	= 	$this->preus->units($codi);        
				$imatgesEstat='';                                  
			    $EstatCat	=	$this->preus->stateAllTerritories($codi, $any, $mes, $pagatspercebuts);
				// Array ( [8] => res [17] => OK [25] => OK [43] => res )
				$cv 		= 	$this->preus->CoefVariacio($codi, $any, $mes, $pagatspercebuts);
				                                                   
				// mirem si hi ha comentaris sense contestar ...   
				if (is_array($AmbUltimComentariSC)) {              
					                                               
					                                               
				}                                                  
				 // mirem els gestor, i construim l'estat "gràfic" 
				foreach ($this->preus->TerritorisLletra as $idT=>$lletraT) 
				{                                                  
				  $EstatCatGrafic[$idT]=$EstatCat[$idT];           
				                                                   
				  if ($estatAplicatiu=='historic')                 
					{ 	 // qui ha entrat els preus                
						 $qui = $this->preus->priceQui($codi);  // 021030:Array ( [8] => 8 [17] => 17 [25] => 25 ) 
						 if ($qui[$idT]!='') $EstatCatGrafic[$idT]=$EstatCatGrafic[$idT].'_'.$this->preus->TerritorisLletra[$qui[$idT]];
					}	                                           
				  else {                                           
					if (preg_match("/".preg_quote($lletraT, '/')."/i",$llistat_preus[$codi]["gestor"])) { $EstatCatGrafic[$idT]=$EstatCatGrafic[$idT].'G';	}
				  }                                                
				}                                                  
				$imatgesEstat = array($EstatCatGrafic[8],$EstatCatGrafic[17],$EstatCatGrafic[25],$EstatCatGrafic[43]);
				$preufinal=$this->preus->priceCat($codi);          
				if ($preufinal==0.0) { $preufinal=''; $unitats='';	  }
				else { $preufinal.=''; 	  }                        
				if ( ($EstatCat[8]=='res') and ($EstatCat[17]=='res') and ($EstatCat[25]=='res') and ($EstatCat[43]=='res') ) $llistat_preus[$codi]["estillinia"]="_gris";
				else $llistat_preus[$codi]["estillinia"]="";       
				$llistat_preus[$codi]["cv"]=$cv;                   
				$res['informacio']["block"]['LlegendaEstatsSC']=true;
				                                                   
			}                                                      
			if ($this->user->rol == 'st') {      
				$gestor = false;
				if (preg_match("/".preg_quote($this->preus->TerritorisLletra[$territori], '/')."/i",$llistat_preus[$codi]["gestor"])) { $$gestor = true;	}
				$res['informacio']["block"]['BEspai']=true;          
				$territoriPreu = $territori;
				if ($llistat_preus[$codi]["tipus"]=='Prov') {
					if (preg_match("/".preg_quote($this->preus->TerritorisLletra[$territori], '/')."/i",trim($llistat_preus[$codi]["gestor"])))
					{
					  if ($this->preus->priceTerritoriValidat($codi,$territori)) $botons = 'v';
					  else $botons = $this->preus->estat($codi);
					  //if ($codi=='054061') echo $botons;
					  $preufinal=$this->preus->price($codi,$territoriPreu);
					}
					else { $botons = '-'; $preufinal=0; }
					
				}
				if ($llistat_preus[$codi]["tipus"]=='Cat') 
				{
					$elgestor = $llistat_preus[$codi]["gestor"];
					$elgestorCodi = array_search($elgestor,$this->preus->TerritorisLletra);
					if ($elgestorCodi==$territori)
					{
						 $territs = explode(",", $this->preus->TerritsDonenPreuUnProdUnMes($codi,$mes));
						 
						 if (count($territs)>0) // hi ha territs que donen preu
						 {   if ($this->preus->priceTerritoriValidat($codi,$territs[0])) $botons = 'v';
							 else $botons = $this->preus->estat($codi,$territs[0]);  // Cat
							 //if ($codi=='011000') echo '('.$territs[0].'-'.$botons.')';
							 $territoriPreu = $territs[0];
							 $preufinal=$this->preus->price($codi,$territoriPreu);
						 }  
						 else  { $botons = '-'; $preufinal=0.0; }
					} else { $botons = '-'; $preufinal=0.0; }
					
				}
				//
				$unitats = $this->preus->units($codi);
				if ($preufinal==0.0) {  $preufinal=''; $unitats='';  }
				$imatgesEstat='';
				// (¿?) falta acabar
				$imatgesEstat = array('-','-','-','-');
				for ($i = 1; $i<=strlen($botons); $i++)  {
					
				}
				// Estil de la línia gris o vermell
				if ($llistat_preus[$codi]["tipus"]=='Prov')
				{ 
				  if ($botons=='-') $llistat_preus[$codi]["estillinia"]="_gris";
				  elseif (preg_match("/".preg_quote($this->preus->TerritorisLletra[$territori], '/')."/i",trim($llistat_preus[$codi]["gestor"])))
					{ $llistat_preus[$codi]["estillinia"]=""; }
				  else  $llistat_preus[$codi]["estillinia"]="_gris";
				  //if ($codi=='052110') echo $preus->TerritorisLletra[$territori].'::'.trim($llistat_preus[$codi]["gestor"]).'<br>';
				}
				else // Cat
				{   		    
					if ($this->preus->TerritorisLletra[$territori]==trim($llistat_preus[$codi]["gestor"])) 	
					{  
					   if (trim($this->preus->TerritsDonenPreuUnProdUnMes($codi,$mes))!='') $llistat_preus[$codi]["estillinia"]="";
					   else $llistat_preus[$codi]["estillinia"]="_gris";
					}
					else $llistat_preus[$codi]["estillinia"]="_gris";
				}
				
			}                                                                                                        
			$llistat_preus[$codi]["imgsEstats"]=$imatgesEstat;     
			$llistat_preus[$codi]["preufinal"]=$preufinal;         
			$llistat_preus[$codi]["unitats"]=str_replace('dotzenes','dotz.',strtolower($unitats));
			$llistat_preus[$codi]["NomProd"]=str_replace(' ','&nbsp;',$info['NomProd']);
			$llistat_preus[$codi]["GrupProd"]=str_replace(' ','&nbsp;',$info['Grup']);
			$llistat_preus[$codi]["SubgrupProd"]=str_replace(' ','&nbsp;',$info['Subgrup']);
			$llistat_preus[$codi]["estatProd"]=$info;  // (¿?) era així $info[3];
		}                                                          
		$this->logtime(4,$res);                                    
		// $elsProductesLlista: Array ( [0] => 001100 [1] => 002100 [2] => 002200 [3] => 003300 [4] => 003400 [5] => 004000 [6] => 005000 [7] => 006000 [8] => 007000 [9] => 011000 [10] => 013000 [11] => 014000 [12] => 016000 [13] => 021010 [14] => 021020 [15] => 021030 [16] => 021040 [17] => 023000 [18] => 033000 [19] => 041000 [20] => 042000 [21] => 051110 [22] => 051120 [23] => 051170 [24] => 051180 [25] => 051212 [26] =>
		// $llistat_preus: Array ( [001100] => Array ( [NomProd] => Blat tou o semidur [Grup] => CEREALS [Subgrup] => Blat [PreusTerrits] => Array ( [8] => res [17] => OK [25] => OK [43] => res ) [estillinia] => [cv] => 11.46% [imgsEstats] =>  [preufinal] => 17.12 [unitats] => €/100 kg [GrupProd] => CEREALS [SubgrupProd] => Blat [estatProd] => ) [002100] => Array ( [NomProd] => Arròs closca
		if ($filtreOrdre=='CV') usort($elsProductesLlista, "ordre_x_cv");
		                                                           
		reset($elsProductesLlista);                                
		/**                                                        
		* while2                                                   
		* set -> CodiProd                                          
		* set -> NomProd                                           
		* set -> GrupProd                                          
		* set -> SubgrupProd                                       
		* set-> imgsEstats                                         
		* set -> preufinal                                         
		* set -> unitats                                           
		* set -> cv                                                
		* set -> TabNum                                            
		* set -> estillinia                                        
		* set -> UNPRODUCTE                                        
		*/                                                         
		$res['informacio']["block"]['UNPRODUCTE']=array();         
		while (list (, $codi) = each ($elsProductesLlista))        
		{                                                          
			$info=$llistat_preus[$codi];                           
			$resArr = array();                                     
			$resArr['CodiProd']	=	$codi;                         
			$resArr['NomProd']	=	$llistat_preus[$codi]["NomProd"];
			$resArr["GrupProd"]	=	$llistat_preus[$codi]["GrupProd"];
			$resArr["SubgrupProd"]	=	$llistat_preus[$codi]["SubgrupProd"];
			$resArr["imgsEstats"]	=	$llistat_preus[$codi]["imgsEstats"];
			//$resArr["preufinal"]	=	(floatval($llistat_preus[$codi]["preufinal"])==0)?(''):($this->preus->number_format_O($llistat_preus[$codi]["preufinal"],2,',',''));
			$resArr["preufinal"]	=	(floatval($llistat_preus[$codi]["preufinal"]));
			$resArr["unitats"]	=	str_replace('eur','€',$llistat_preus[$codi]["unitats"]);
			// $resArr["cv"]	=	(strval($llistat_preus[$codi]["cv"])=='')?(''):($this->preus->number_format_O($llistat_preus[$codi]["cv"],2,',',''));
			if (isset($llistat_preus[$codi]["cv"]))  { $resArr["cv"]	=	(floatval($llistat_preus[$codi]["cv"])); }
			else { $resArr["cv"]	=	''; }
			                                                       
			if ($this->user->rol == 'st')                          
			{ $resArr["TabNum"]	=	'entrada_preus';		       
			  $resArr["tipusPreu"]	=	$llistat_preus[$codi]["tipus"];
			                                                       
			}                                                      
			if ($this->user->rol == 'sc')                          
			{  $resArr["TabNum"]	=	'revisio_preus';           
			   $resArr["tipusPreu"]	=	$llistat_preus[$codi]["tipus"];
			                                                       
			}                                                      
			$resArr["estillinia"]	=	$llistat_preus[$codi]["estillinia"];
			                                                       
			$res['informacio']["block"]['UNPRODUCTE'][]=$resArr;   
		}                                                          
		$this->logtime(5,$res);                                    
		$res['estat'] = true;                                      
		$res['informacio']['llistat_preus'] = $elsProductesLlista; 
		return $this->respond($res);                               
	}                                                              
	                                                               
	public function page_prepare($codiprod,$descripcioTable) {     
		// Això NO hauria d'estar en el controller  --> cap a la classe Preus
		$sql = strtoupper("SELECT *  FROM ".$descripcioTable."  WHERE CodiProd ='".$codiprod."' ");	
        $Conn = DB::connection();                                  
		$rows = $Conn->select($sql);     	
		$num = count($rows);                                       
		$row_elProd = array();                                     
		if ($num>0) {                                              
			$row_elProd = ((array)$rows[0]); ;                     
			//$row_elProd['DESCPREU1'] = 'DESCPREU1';                
			//$row_elProd['DESCPREU2'] = 'DESCPREU2';                
			//$row_elProd['DESCPREU3'] = 'DESCPREU3';                
		}                                                          
		return $row_elProd;                                        
		                                                           
	}                                                              
	                                                               
	public function preparePage($base,$pagatspercebuts,$codiprod,$any, $mes, $territori) {
			$this->preus = new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base ,DB::connection());
	}                                                              
	                                                               
	public function page_infoComentaris($codiprod,$territori,$PagatsPercebuts,$any,$mes) {
		// Això NO hauria d'estar en el controller  --> cap a la classe Preus
		$sql = strtoupper("SELECT * FROM notes_preus".$PagatsPercebuts." where   
                         CodiProd ='".$codiprod."'  AND            
						 CodiTerr ='".$territori."'  AND           
                         ANY ='".$any."'  AND                      
                         Mes ='".$mes."'                           
						 ORDER BY seq ASC ");	                   
        $Conn = DB::connection();                                  
		$elscomm_Arr = $Conn->select($sql);                        
		return $elscomm_Arr;                                       
	}                                                              
	                                                               
	/**                                                            
	 *                                                             
	 *  http://laravel.ticnologia.com/api/pagina/revisio/2010/percebuts/054170/2017/2
	 *                                                             
	 * @param Request $request                                     
	 * @return Response                                            
	 */                                                            
	public function revisio($base,$pagatspercebuts,$codiprod, $any,$mes)
	{	                                                           
		                                                           
		$any		= intval($any);                                
		$mes 		= intval($mes);         
		$base 		= $base-2000;
		// ========  (extret de core/progs/pag2.php) ==========    
		$anyant = $any-1;                                          
		$anymesant = ($mes>1)?($any):($anyant);                    
		$mesant = ($mes>1)?($mes-1):(12);  
		
		// ======== ===========================================    
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['informacio']["block"]=array();
		$preus = new \App\Library\Prices($pagatspercebuts,$any, $mes, 99, $base,DB::connection());
		$limits = $preus->limits_anys_preus($pagatspercebuts,DB::connection(),$base);
		                                                           
		if ($preus->limits_no_correctes($any,$mes)) { $result['informacio']['estat']="Error en la selecció ANY i MES."; }
		else {                                                     
			$result['informacio']['limits']=$limits;               
			$result['informacio']["historic_anyant"]=($any-1);     
			$result['informacio']["historic_any"]=$any;            
			$result['informacio']["historic_mesant"]=$mes;         
			$result['informacio']["historic_mes"]=$mes;  
			$result['informacio']["anyant"]=$anyant;
			$result['informacio']["anymesant"]=$anymesant;
			$result['informacio']["mesant"]=$mesant;
			$result['informacio']["mesnom"]=$preus->mesos[$mes];
			$result['informacio']["mesantnom"]=$preus->mesos[$mesant];
			
			$elspreusTerrits = $preus->priceTerr($codiprod);       
			$elsPercentsQ	 = $preus->baseCalPercentageAllTerr($codiprod,true);
			$elsPercentsAntQ = $preus->baseCalPercentageAllTerrMonth($codiprod,$mesant,true);
			$elspreusTerritsMAnt = $preus->pricesYearBefore($codiprod); 
	        $elspreusTerritsAAnt = $preus->pricesMonthBefore($codiprod); 
			$totalPercentCat=0;                                    
			reset($preus->TerritorisNoms);                         
			// Les quantitats físiques                             
			$elspesos=$preus ->baseCalAllTerr($codiprod);  
			if (isset($elspesos['Barcelona'])) { $result['informacio']["pes8"] =$elspesos['Barcelona']; }
			if (isset($elspesos['Girona'])) { $result['informacio']["pes17"]=$elspesos['Girona'];    }
			if (isset($elspesos['Lleida'])) { $result['informacio']["pes25"]=$elspesos['Lleida'];    }
			if (isset($elspesos['Tarragona'])) { $result['informacio']["pes43"]=$elspesos['Tarragona']; }
			$nopreucat = false; // la posarem a true quan decidim no calcular el preuCAT .. falten preus dels Territoris per exemple.
			$quantsTerritsHiHaAct  = 0;  // Quants territoris donen preu aquest mes
			$quantsTerritsHiHaAnt  = 0;  // Quants territoris donen preu aquest el mes anterior
			$quantsPreusActEntrat  = 0;  // Quants territoris han donat preu aquest mes	 
			$quantsPreusAntEntrat  = 0;  // Quants territoris han donat preu el mateix mes de l'any anterior
			$quantsPreusMAntEntrat = 0;  // Quants territoris han donat preu aquest mes anterior
			                                                       
			$percArr=array();       
			$result['informacio']["preusEntrats"]=array();			
			while (list ($Tkey, $TNom) = each ($preus->TerritorisNoms)) 	{
				$FilaPonderaTerr = array();                        
				$calMesActual = !$preus->ifBaseZeroTerritori($codiprod,$Tkey);
				$calMesAnterior = !$preus->ifBaseZeroTerritori($codiprod,$Tkey,$mesant);
				                                                   
				if ($calMesActual or $calMesAnterior) {            
					$EstatCat=$preus->stateAllTerritories($codiprod, $any, $mes, $pagatspercebuts);
					$FilaPonderaTerr["imatgesEstat_".$Tkey]=$EstatCat[$Tkey].'.gif';
					                                               
					if ($calMesActual)   $quantsTerritsHiHaAct++;  
					if ($calMesAnterior) $quantsTerritsHiHaAnt++;  
					if (!isset($elspreusTerrits[$Tkey])) {  $elspreusTerrits[$Tkey]=0.0; }
					if ( ($elspreusTerrits[$Tkey]>0.0) and ($calMesActual) )     $quantsPreusActEntrat++;
					if (!isset($elspreusTerritsAAnt[$Tkey])) {  $elspreusTerritsAAnt[$Tkey]=0.0; }
					if ( ($elspreusTerritsAAnt[$Tkey]>0.0) and ($calMesAnterior) ) $quantsPreusAntEntrat++;
					if (!isset($elspreusTerritsMAnt[$Tkey])) {  $elspreusTerritsMAnt[$Tkey]=0.0; }
					if ( ($elspreusTerritsMAnt[$Tkey]>0.0) and ($calMesActual) ) $quantsPreusMAntEntrat++;
					                                               
					// Pesos                                       
						// Pes mes actual i mateix mes, any anterior
						if ($calMesActual) {                       
							$FilaPonderaTerr["percP".$Tkey] = $preus->number_format_O(($elsPercentsQ[$preus->TerritorisLletra[$Tkey]]),3,',',''); }
						else { $FilaPonderaTerr["percP".$Tkey] = '--'; }
						// Pes mes anterior                        
						if ($calMesAnterior) {                     
							$FilaPonderaTerr["percPA".$Tkey] = $preus->number_format_O(($elsPercentsAntQ[$preus->TerritorisLletra[$Tkey]]),3,',',''); }
						else { $FilaPonderaTerr["percPA".$Tkey] = '--'; } 					
					// Preus                                       
						// Preus mes i any actual                  
						if (($calMesActual) and ($any>=$limits['MIN']))  {
							if ($elspreusTerrits[$Tkey]>0.0) { $FilaPonderaTerr["preu".$Tkey] = $preus->number_format_O(($elspreusTerrits[$Tkey]),3,',',''); }
							else { $FilaPonderaTerr["preu".$Tkey] = '?'; }
						}                                          
						else { $FilaPonderaTerr["preu".$Tkey] = '--'; }							
						// Preus mes anterior			 			 		
						if (($calMesAnterior) and ($any>=$limits['MIN'])) {
						  if ($elspreusTerritsAAnt[$Tkey]>0.0) {  $FilaPonderaTerr["preuA".$Tkey] = $preus->number_format_O(($elspreusTerritsAAnt[$Tkey]),3,',',''); }
						  else { $FilaPonderaTerr["preuA".$Tkey] = '?'; }
						 }			                               
						else { $FilaPonderaTerr["preuA".$Tkey] = '--'; }
						// Preus mateix mes i any anterior	       
						if (($calMesActual) and ($anyant>=$limits['MIN'])) {
							if ($elspreusTerritsMAnt[$Tkey]>0.0) {  $FilaPonderaTerr["preuM".$Tkey] = $preus->number_format_O(($elspreusTerritsMAnt[$Tkey]),3,',',''); }
							else { $FilaPonderaTerr["preuM".$Tkey] = '?'; }
						}                                          
						else { $FilaPonderaTerr["preuM".$Tkey] = '--'; }
                                                                   
						if ($calMesActual) {  
							if (!isset($percArr[$Tkey])) {  $percArr[$Tkey]=array(); }			
							$percArr[$Tkey]['percA']=null;
							$percArr[$Tkey]['percM']=null;
							if (floatval($elspreusTerrits[$Tkey])!=0.0) {
								                                   
								// Mes anterior                    
								if ($elspreusTerritsAAnt[$Tkey]>0.0) {
								   $variacioPerc = ($elspreusTerrits[$Tkey]-$elspreusTerritsAAnt[$Tkey])*100/$elspreusTerritsAAnt[$Tkey];
								   $FilaPonderaTerr["percA".$Tkey] = $preus->number_format_O(($variacioPerc),2,',','');
								   $percArr[$Tkey]['percA']=$variacioPerc;
								 }                                 
								 else {                            
									 if ($calMesAnterior) {  $FilaPonderaTerr["percA".$Tkey] = "?"; }
									 else { $FilaPonderaTerr["percA".$Tkey] = "--"; }
								 }                                 
								// Mateix mes i any anterior       
								if ($elspreusTerritsMAnt[$Tkey]!=0.0) { 
									$variacioPerc = ($elspreusTerrits[$Tkey]-$elspreusTerritsMAnt[$Tkey])*100.0/$elspreusTerritsMAnt[$Tkey];
									$FilaPonderaTerr["percM".$Tkey] = $preus->number_format_O(($variacioPerc),2,',',''); 
									$percArr[$Tkey]['percM']=$variacioPerc;
								}                                  
								else {                             
									if ($calMesActual) {  $FilaPonderaTerr["percM".$Tkey] = "?"; }
									else { $FilaPonderaTerr["percM".$Tkey] = '--'; }
								}                                  
								//$totalPercentCat+=$elsPercentsQ[$Tkey];							 
							} else {                               
									$FilaPonderaTerr["percA".$Tkey] = "?"; 
									$FilaPonderaTerr["percM".$Tkey] = "?";
									$nopreucat = true;             
							}                                      
						} else {                                   
						   $FilaPonderaTerr["percA".$Tkey] = "--"; 
						   $FilaPonderaTerr["percM".$Tkey] = "--"; 
						 }
						
						$result['informacio']["FilaPondera".$Tkey][] = $FilaPonderaTerr;
						$result['informacio']["preusEntrats"][$Tkey] = $preus ->enteredPricesTerritori($codiprod,$Tkey);
						$result['informacio']["preusEntrats"][$Tkey]['PreuFinal'] = $preus->priceTerritori($codiprod,$Tkey);
						$result['informacio']["preusEntrats"][$Tkey]['dataultmodifpre'] = $preus->PriceDateTime($codiprod,$Tkey);
						$result['informacio']["preusEntrats"][$Tkey]['es_automatic'] = $preus->is_priceAuto($codiprod, $Tkey, $any, $mes);
						$result['informacio']["preusEntrats"][$Tkey]['es_validat'] = $preus->priceTerritoriValidat($codiprod,$Tkey);  
						
						
				}                                                  
				                                                   
			} 
			//
			$result['informacio']["block"]["AlertaFaltaPreuHistoric"] = false;			
			if ( 
				  ($quantsTerritsHiHaAnt!=$quantsPreusAntEntrat) or 
				  ($quantsTerritsHiHaAct!=$quantsPreusMAntEntrat) ) { $result['informacio']["block"]["AlertaFaltaPreuHistoric"] = true; }
			  if ($quantsTerritsHiHaAct!=$quantsPreusActEntrat) $faltenpPreusxEntrar=true;	      
			  else $faltenpPreusxEntrar=false;
			$result['informacio']["block"]["Bloc_CV"] = array();
			$hihapreuMesActual = $preus->baseCalAllTerrAny($codiprod,$mes);
			// Array ( [Barcelona] => 39 [Girona] => 30 [Lleida] => 0 [Tarragona] => 14 )
			$hihapreuMesAnterior = $preus->baseCalAllTerrAny($codiprod,$mesant);
			// Array ( [Barcelona] => 39 [Girona] => 30 [Lleida] => 0 [Tarragona] => 14 )
			if (is_array($hihapreuMesActual)) $calMesActual = (array_sum($hihapreuMesActual)>0);
			else $calMesActual = false;
			if (is_array($hihapreuMesAnterior)) $calMesAnterior = (array_sum($hihapreuMesAnterior)>0);
			else $calMesAnterior = false;
			$result['informacio']["block"]["GraficaVariacio"] = false;
			$result['informacio']["block"]["GraficaVariacio23D"] = false;
			$result['informacio']["block"]["ObjecteGraficaVariacio"] = false;
			if ($quantsTerritsHiHaAct>0) { $result['informacio']["block"]["GraficaVariacio"] = true;	}
			if ($quantsTerritsHiHaAct>0) { $result['informacio']["block"]["GraficaVariacio23D"] = true;	}
			if ($preus->quants_preus_entrats_HiHa($codiprod,$pagatspercebuts,DB::connection(),$base)>0)  { $result['informacio']["block"]["ObjecteGraficaVariacio"] = true;	}
			//ObjecteGraficaVariacio
			// %pes Catalunya és sempre 100%
			if ($quantsTerritsHiHaAct>0) $result['informacio']['block']["Bloc_CV"]["percPCat"]=$preus->number_format_O((100),3,',','');  // 100%
			else $result['informacio']['block']["Bloc_CV"]["percPCat"]='--';	   
			if ($quantsTerritsHiHaAnt>0) $result['informacio']['block']["Bloc_CV"]["percPACat"]=$preus->number_format_O((100),3,',','');  // 100%
			else $result['informacio']['block']["Bloc_CV"]["percPACat"]='--';
			// Preu actual Cat  
			$preuCatAct = $preus->priceCat($codiprod);	  
			if ($quantsTerritsHiHaAct==0) $result['informacio']['block']["Bloc_CV"]["preuCat"]='--';
			elseif (!$faltenpPreusxEntrar) $result['informacio']['block']["Bloc_CV"]["preuCat"]=$preus->number_format_O(($preuCatAct),3,',','');
			else $result['informacio']['block']["Bloc_CV"]["preuCat"]='?';
			// Preu mes anterior Cat 
			$preuCatMesAnt=0;	
			if (($quantsTerritsHiHaAnt!=$quantsPreusAntEntrat)) $result['informacio']['block']["Bloc_CV"]["preuACat"]='?'; // falten preus
			else
			{ $preuCatMesAnt = $preus->priceCatMonthBefore($codiprod);
				if ($preuCatMesAnt==0) $result['informacio']['block']["Bloc_CV"]["preuACat"]='--';
				else $result['informacio']['block']["Bloc_CV"]["preuACat"]=$preus->number_format_O(($preuCatMesAnt),3,',','');
			}

			// Preu mes actual i any anterior Cat  
			if (($quantsTerritsHiHaAct!=$quantsPreusMAntEntrat)) $result['informacio']['block']["Bloc_CV"]["preuMCat"]='?'; // falten preus
			else
			{
				$preuCatAnyAnt = $preus->priceCatYearBefore($codiprod);
				if ($quantsTerritsHiHaAct==0) $result['informacio']['block']["Bloc_CV"]["preuMCat"]='--';
				else
				  if (($anyant>=$limits['MIN'])) $result['informacio']['block']["Bloc_CV"]["preuMCat"]=$preus->number_format_O($preuCatAnyAnt ,3,',','');
				  else $result['informacio']['block']["Bloc_CV"]["preuMCat"]='--';
			}
			if ($quantsTerritsHiHaAct>0) {
				if (!$nopreucat) {
					 if ($preuCatMesAnt==0)  { $result['informacio']['block']["Bloc_CV"]["percACat"]='--'; }
					 else
					 { 
						if ($preuCatAct>0) { $result['informacio']['block']["Bloc_CV"]["percACat"]=$preus->number_format_O(($preuCatAct-$preuCatMesAnt)*100/$preuCatMesAnt,2,',',''); }
						else $result['informacio']['block']["Bloc_CV"]["percACat"]='?';
					 }
				}
				else $result['informacio']['block']["Bloc_CV"]["percACat"]='?'; 
			}
			else $result['informacio']['block']["Bloc_CV"]["percACat"]='--';
			if ($preuCatAnyAnt>0) {
			  if (!$nopreucat) { 
				if ($quantsTerritsHiHaAct>0) { $result['informacio']['block']["Bloc_CV"]["percMCat"]=$preus->number_format_O(($preuCatAct-$preuCatAnyAnt)*100/$preuCatAnyAnt,2,',',''); }
				else { $result['informacio']['block']["Bloc_CV"]["percMCat"]='--'; }
			  }	
			  else { $result['informacio']['block']["Bloc_CV"]["percMCat"]='?'; }
			} 
			else { $result['informacio']['block']["Bloc_CV"]["percMCat"]='--'; }
			//
			$metode= $preus ->methodPrices($codiprod);
			$result['informacio']["calcpreufinal"]=(($metode=='M')?'Mitjana':'Suma');
			
			// Coeficient de variació
			if ($quantsTerritsHiHaAct+$quantsTerritsHiHaAnt>0) 
			{ 
				$cv1 = ($preus->CoefVariacio($codiprod, $any, $mes, $pagatspercebuts));
				$cv2 = ($preus->CoefVariacio($codiprod, $anymesant, $mesant, $pagatspercebuts));
				$cv3 = ($preus->CoefVariacio($codiprod, $anyant, $mes, $pagatspercebuts));
				$cv1 = str_replace('%','',$cv1);
				$cv2 = str_replace('%','',$cv2);
				$cv3 = str_replace('%','',$cv3);
				//echo $cv1."/".$cv2."/".$cv3."/";
				if ($calMesActual) 
				{
				   if ($quantsPreusActEntrat>0) $result['informacio']['block']["Bloc_CV"]["cv1"]=$cv1;
				   else $result['informacio']['block']["Bloc_CV"]["cv1"]='?';
				} 
				else $result['informacio']['block']["Bloc_CV"]["cv1"]='--';
				if (($calMesAnterior) and ($anyant>=$limits['MIN'])) 
				{ $result['informacio']['block']["Bloc_CV"]["cv2"]=$cv2;
				  }
				else $result['informacio']['block']["Bloc_CV"]["cv2"]='--';
				if ($calMesActual) 
				{ if ($anyant>=$limits	['MIN']) $result['informacio']['block']["Bloc_CV"]["cv3"]=$cv3;
				  else $result['informacio']['block']["Bloc_CV"]["cv3"]='--';
				}
				else $result['informacio']['block']["Bloc_CV"]["cv3"]='--';
			
			}
			else
			{
				$result['informacio']['block']["Bloc_CV"]["cv1"]='--';
				$result['informacio']['block']["Bloc_CV"]["cv2"]='--';
				$result['informacio']['block']["Bloc_CV"]["cv3"]='--';
			}
			// (!!) Aquest codi està repetit a "pàgina-entrada", es podria posar en una funció
			$filtre=array();
			for ($m=1; $m<=12; $m++) $filtre[$m] = $preus->baseCalPercentageAllTerrMonth($codiprod,$m);
			$result['informacio']["evolucio_preus"] = array();
			$result['informacio']["evolucio_preus"]['filtre']=$filtre;
			$result['informacio']["evolucio_preus"]['HistPrices'] = array('xAxisValues'=>array(),'data'=>array());
			$arrayPrices = $preus->HistoricPrices_ambFiltre($codiprod,$filtre);
			$elsTerrs = array_keys($arrayPrices); 
			$T = $preus->TerritorisNoms;
			$info=array();
			foreach ($elsTerrs as $elTerr) {
					$info[$elTerr]=array('label'=>$T[$elTerr],'data'=>array_fill(0, 13, null));
			}
			$pastDate = mktime(0, 0, 0, $mes,   1,   $any-1);
			$start = new DateTime(date('Y-m-d',mktime(0, 0, 0, $mes,   1,   $any-1)));
			$interval = new DateInterval('P1M');  // 1 mes
			$end = new DateTime(date('Y-m-d',mktime(0, 0, 0, $mes,   2,   $any)));
			$period = new DatePeriod($start, $interval, $end);
			$ix=0;
			foreach ($period as $dt) {
				$y = $dt->format('Y');
				$m = $dt->format('m');
				$result['informacio']["evolucio_preus"]['HistPrices']['xAxisValues'][]=$dt->format('Y/m');
				// Territoris
				foreach ($elsTerrs as $elTerr) {
					if (isset($arrayPrices[$elTerr][intval($y)][intval($m)])) { $info[$elTerr]['data'][$ix]=floatval($arrayPrices[$elTerr][intval($y)][intval($m)]); }
				}
				$ix++;
			}
			$result['informacio']["evolucio_preus"]['HistPrices']['data']=array();
			foreach ($info as $infodetall) {
					$result['informacio']["evolucio_preus"]['HistPrices']['data'][]=$infodetall;
			}
			// comparativa_variacions
			$result['informacio']["block"]["comparativa_variacions"] = array();
			$Etiqueta1 = $preus->mesos[$mes]." de ".$any;     // actual
			$Etiqueta2 = 'Respecte '.$preus->mesos[$mesant]." de ".$anymesant;  // fa un mes
			$Etiqueta3 = 'Respecte '.$preus->mesos[$mes]." de ".$anyant;     // fa un any
			$result['informacio']["block"]["comparativa_variacions"]['etiquetes']=array($Etiqueta2,$Etiqueta3);
			$result['informacio']["block"]["comparativa_variacions"]['data']=array();
			while (list ($Tkey, $TNom) = each ($preus->TerritorisNoms)) 	{                      
				$calMesActual = !$preus->ifBaseZeroTerritori($codiprod,$Tkey);
				$calMesAnterior = !$preus->ifBaseZeroTerritori($codiprod,$Tkey,$mesant);                                          
				if ($calMesActual) {
					$dades = array("coditerr"=>$Tkey,"label"=>$TNom, "data"=>array(),"color"=>'');
					$dades["data"]=array($percArr[$Tkey]['percA'],$percArr[$Tkey]['percM']);
					$result['informacio']["block"]["comparativa_variacions"]['dades'][]=$dades;
				}
			}
			
			
			
			
			
			
			$result['estat']="OK";                                 
		}                                                          
		$result['informacio']["ponderacions_quantitatives"] = array();
		$result['informacio']["ponderacions_quantitatives"]['chart_data'] = array();
		$T = $preus->TerritorisNoms;
		while (list ($Tkey, $TNom) = each ($T)) {	
			$elspesos=$preus->baseCalHistoricMesTerrit($codiprod,$Tkey);
			if (is_array($elspesos)) 
			  { $queHiHa = array_sum($elspesos);
				if ($queHiHa>0)  // no són tots zeros
				{ $elspesosxgrafica = array_merge(array($TNom),$elspesos);
				  // Array ( [0] => Barcelona [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 0 [7] => 50 [8] => 29.99 [9] => 10 [10] => 10 [11] => 0 [12] => 0 ) 
				  $infoPondercionsTerritori = array(
					'data'=>(array_slice($elspesosxgrafica, 1)),
					'label'=>($elspesosxgrafica[0]));
				  $result['informacio']["ponderacions_quantitatives"]['chart_data'][]	= $infoPondercionsTerritori;
				}
			  }
		}
		
		
		return $this->respond($result);                            
		                                                           
	}                                                              
	                                                               
	                                                               
   	/**                                                            
	 *                                                             
	 *  http://laravel.ticnologia.com/api/pagina/entrada/2010/percebuts/054170/2017/2
	 *                                                             
	 * @param Request $request                                     
	 * @return Response                                            
	 */                                                            
	public function entrada($base,$pagatspercebuts,$codiprod, $any,$mes)
	{	                                                           
		$debug = false;                                            
		if ($debug) { $result['debug']=array(); }                  
		                                                           
		$opcio = "entrada_preus";                                  
		// User / session data                                     
		$_SESSION = array();                                       
		$_SESSION['EsSc']				=false;  // ST connectat amb permisos especials
	// $_SESSION['PagatsPercebuts'] 	= 'percebuts';             
		// $_SESSION['perfil']    			= 'ST';                    
		$_SESSION['estatAplicatiu'] 	= 'entrada';  //  entrada ; revisant ;                     
		                                                           
	// $PagatsPercebuts = $_SESSION['PagatsPercebuts'];            
		$PagatsPercebuts = $pagatspercebuts;                       
		$territori       = intval($this->user->territori);         
		$perfil          = strtoupper($this->user->rol);                    
		if ($perfil=='SC') { $territori = 99; }                    
		$estatAplicatiu  = $_SESSION['estatAplicatiu'];            
	    $base			 = $base-2000;                      
		                                                           
		                                                           
		// Input data                                              
		$_REQUEST = array();                                       
		$_REQUEST['ple'] = 'si';                                   
		$_REQUEST['elsnumeros'] = '';                              
		$_REQUEST['elspunts'] = '';                                
		$_REQUEST['dadesmes'] = '';                                
		$_REQUEST['anuals'] = '';                                  
		// ===================================================================
		/*                                                         
		    .........  $paramsGrafica (per l'històric)             
		*/                                                         
		                                                           
		$elsnumeros = ($_REQUEST['elsnumeros']);                   
	    $elspunts  	= ($_REQUEST['elspunts']);                     
		$dadesmes  	= trim($_REQUEST['dadesmes']);                 
		$anuals  	= trim($_REQUEST['anuals']);                   
		$ple  = (strtolower($_REQUEST['ple'])=='si');              
		if ( ($dadesmes=='') and ($anuals=='') ) $dadesmes=1;      
		                                                           
		$codiAnt = '002100';                                       
		$codiSeg = '003200';                                       
		                                                           
		// ===================================================================
		                                                           
		                                                           
		                                                           
		                                                           
		                                                           
		$territoriUsuari=$territori;  // lin 63 pag2.php           
		                                                           
		$any		= intval($any);                                
		$mes 		= intval($mes);                                
		// ========  (extret de core/progs/pag2.php) ==========    
		$anyant = $any-1;                                          
		$anymesant = ($mes>1)?($any):($anyant);                    
		$mesant = ($mes>1)?($mes-1):(12);                          
		                                                           
		// ======== ===========================================    
		                                                           
		$result = array('estat'=>false, 'informacio'=>array());    
		$preus = new \App\Library\Prices($pagatspercebuts,$any, $mes, $territori, $base,DB::connection());
		
		if (is_null($this->user)) {  $result['informacio']['user']=null; }
		else { $result['informacio']['user']=$this->user;   }
		                                                           
		$infoCodi = $this->page_prepare($codiprod,$preus->descripcioTable);  // 
		if (empty($infoCodi)) {                                    
			$nomproducte = '';                                     
			$subgrup     = '';                                     
			$grup        = '';                                     
		} else {                                                   
		/*                                                         
		    [CODIPROD] => 011000                                   
			[NOMPROD] => Mongetes seques                           
			[UNITAT] => eur/100 kg                                 
			[SUBGRUP] => Mongetes seques                           
			[GRUP] => LLEGUMINOSES                                 
			[FASEINTERCANVI] => De productor a majorista           
			[CONDICIONSCOMERCIALITZACIO] => A granel, s/magatzem agricultor
			[NECPREUS] => 3                                        
			[MAXPREUS] => 3                                        
			[CALCUL] => M                                          
			(¿?) [DESCPREU1] => DESCPREU1                          
			(¿?) [DESCPREU2] => DESCPREU2                          
			(¿?) [DESCPREU3] => DESCPREU3                          
			[PUBLICAR] => s                                        
			[TIPUS] => Cat                                         
			[GESTOR] => B                                          
			[ACTIU00] => s                                         
			[ACTIU05] => s                                         
			[MAPA] => s                                            
			[ACTIU10] => s                                         
		*/                                                         
			$nomproducte	= $infoCodi['NOMPROD'];                
			$unitats		= $infoCodi['UNITAT'];                 
			$intercanvi		= $infoCodi['FASEINTERCANVI'];         
			$condcomer		= $infoCodi['CONDICIONSCOMERCIALITZACIO'];
			$subgrup		= $infoCodi['SUBGRUP'];                
			$grup			= $infoCodi['GRUP'];                   
			$tipus			= $infoCodi['TIPUS'];                  
			$gestor			= $infoCodi['GESTOR'];                 
		}                                                          
                                                                   
		$elspesos = $preus->baseCalHistoricMesPerc($codiprod);     
		                                                           
		// El comentaris                                           
		$elscomm_Arr = $this->page_infoComentaris($codiprod,$territori,$PagatsPercebuts,$any,$mes);
		                                                           
		                                                           
		$result['informacio']["codiprod"]	=	$codiprod;         
		$result['informacio']["nomproducte"]	=	$nomproducte;  
		$result['informacio']["nomproducte_js"]	=	addslashes($nomproducte);
		$result['informacio']["condcomer"]	=	$condcomer;        
		$result['informacio']["intercanvi"]	=	$intercanvi;       
		$result['informacio']["unitats"]	=	str_replace("eur","€",$unitats);
                                                                   
		if ($perfil == 'SC') {                                     
			                                                       
		}                                                          
		                                                           
		$result['informacio']["subgrup"]	=	$subgrup;          
		$result['informacio']["grup"]		=	$grup;             
		$nomproducteAnt=$preus->nom($codiAnt);                     
		$nomproducteSeg=$preus->nom($codiSeg);                     
		                                                           
		                                                           
		// ==========================================================================================
		// ============== Pantalla entrada ==========================================================
		// ==========================================================================================
                                                                   
		// if ($preus->quants_preus_entrats_HiHa($codiprod,$PagatsPercebuts,$Conn,$base)>0)  $t->parse("ObjecteGraficaVariacio","ObjecteGraficaVariacio",false);
		                                                           
		$result['informacio']["historic_anyant"]=($any-1);         
		$result['informacio']["historic_any"]=$any;                
		$result['informacio']["historic_mesant"]=$mes;             
		$result['informacio']["historic_mes"]=$mes;                
		$result['informacio']["TerritorisNoms"] = $preus->TerritorisNoms;
                                                                   
		if ($preus->ifBaseZeroTerritori($codiprod,$territori) or (array_search($territoriUsuari,$preus->BuscaGestor($codiprod))===false))
		{      // no s'ha d'entar el preu                          
			   $result['informacio']["block"]["NoEntraPreu"]=array();
			   $result['informacio']["block"]["NoEntraPreu"][]=array("display_sino"=>"none");  	
	   }                                                           
		else {                                                     
			$result['informacio']["block"]["SiEntraPreu"]=array(); 
		    $result['informacio']["block"]["SiEntraPreu"][]=array("display_sino"=>"block");  
		}			                                               
		$result['informacio']["TerrNom"] = $preus->TerritorisNoms[$territori];
		$result['informacio']["classemes".$mes] = " class='fila-detall1' ";
		$result['informacio']["territori"] = $territori;           
		                                                           
		                                                           
		                                                           
		$preusEntrats = $preus ->enteredPrices($codiprod);  // Array ( [PREU1] => 95 [PREU2] => 95 [PREU3] => 95 [QUI] => 25 [NECPREUS] => 3 [MAXPREUS] => 3 )
		if ($debug) { $result['debug']['preusEntrats'] = $preusEntrats;  }
		for ($m = 1; $m <= $preusEntrats['MAXPREUS']; $m++)        
		{                                                          
			$result['informacio']["block"]["Preu".$m]=array();     
			if (isset($preusEntrats['PREU'.$m])) { $elvalor = $preusEntrats['PREU'.$m]; }
			else { $elvalor = 0; }			
			if ($elvalor==0) {  $result['informacio']["block"]["Preu".$m]["preuValor".$m] = ''; }
			else {  $result['informacio']["block"]["Preu".$m]["preuValor".$m] = $preus->number_format_O($elvalor,2,',',''); }
			$result['informacio']["block"]["Preu".$m]["PreuDesc".$m] = $infoCodi['DESCPREU'.$m]; // (¿?) té valor actualment?, mirant el codi sembla que no
		}                                                          
		$elspesos = $preus->baseCalHistoricMesPerc($codiprod);     
		// Comparatives		                                       
		for ($m = 1; $m <= 12; $m++)                               
		{   $valor= ($elspesos[$m]>0)?($preus->number_format_O($elspesos[$m],2,',','').'%'):('--');
				  $result['informacio']["p".$m]=$valor;            
		}                                                          
		                                                           
		//                                                         
		$elspesosPercent=$preus ->baseCalPercentageAllTerr($codiprod);
		$elspesos=$preus ->baseCalAllTerr($codiprod);              
		if (!is_array($elspesos)) $elspesos=array();               
		if (is_array($elspesosPercent))   // Array ( [B] => 0 [G] => 0 [L] => 85.2 [T] => 14.79 )  
		{ 	reset($elspesosPercent); }                             
		else { $elspesosPercent = array('B'=>0,'G'=>0,'L'=>0,'T'=>0);   }
		                                                           
		$paramsGrafica = "codi=123";                               
		while (list ($key, $val) = each ($elspesosPercent)) {      
		  if ($val>0) { $result['informacio']["i".$key] = $preus->number_format_O($val,2,',','')."%";	 }
		  else { $result['informacio']["i".$key] = " - ";	 }     
          $paramsGrafica .= "&i".$key."=".	$val;	               
		}                                                          
		$paramsGrafica = "codi=123";                               
		while (list ($key, $val) = each ($elspesos)) {             
			  $valNum=array_search($key,$preus->TerritorisNoms);   
			  $valLletra = $preus ->TerritorisLletra[$valNum];     
			  $paramsGrafica .= "&v".$valLletra."=".	$val;	   
			}	                                                   
		if ($ple==1) { $paramsGrafica .= "&ple=si"; }	           
		$paramsGrafica .= "&descacar=".$preus->TerritorisNoms[$territori];	
		$result['informacio']["paramspastelentrada"] = $paramsGrafica;
		// Els 13 mesos                                            
		$elmes = $mes;                                             
		for ($m = 1; $m <= 13; $m++)                               
		{                                                          
		   $result['informacio']["any".$m] = ($m==(13-$mes+1))?$any:''; 
		   $result['informacio']["cm".$m]  = $preus ->mesosAvr[$elmes];  	   
		   $elmes+=1;                                              
		   if ($elmes >12) { $elmes=1; }                           
		}                                                          
		// Els preus i variacions dels 13 mesos                    
		$cp=array();                                               
		$vp=array();                                               
		$any_ix=$anyant;                                           
		$mes_ix = $mes-1;                                          
		for ($m = 0; $m <= 12; $m++)   // m==0 --> mateix mes, any anterior
		{    $mes_ix++;                                            
			 if ($mes_ix>12) {  $mes_ix=1; $any_ix++;}		       
			 $elpreu = $preus->priceDate($codiprod, $any_ix,$mes_ix) ;
			 $cp[$m+1]=$elpreu;                                    
			                                                       
		}                                                          
		for ($m = 0; $m <= 11; $m++)   // m==0 --> mateix mes, any anterior
		{                                                          
		  if (floatval($cp[$m+1])+0.0>0)                           
		  {                                                        
			                                                       
			  $vp[$m+1] = ($cp[13]-$cp[$m+1])/$cp[$m+1]*100;	       
			  $result['informacio']["cp".($m+1)]  = $cp[$m+1];
			  if ($cp[13]=='') { $result['informacio']["vp".($m+1)]  = ''; }
			  else                                                 
			  { $vp_mostrar = $preus->number_format_O(abs($vp[$m+1]),2,',','');
				if ($vp[$m+1]<0) { $vp_mostrar = '-'.$vp_mostrar;  } // no hi ha espai!!!!
				$result['informacio']["vp".($m+1)]  = $vp_mostrar; 
			  }                                                    
			                                                       
		  }                                                        
		  else  // no hi ha preu                                   
		  {                                                        
			 $cp[$m+1]="";                                   
			 $vp[$m+1]="";                                   
			 $result['informacio']["cp".($m+1)]  = ($cp[$m+1]);    
			 $result['informacio']["vp".($m+1)]  = ($vp[$m+1]);    
		  }                                                        
		}                                                          
		$vp[13]=" · ";                                             
		if ($preus ->ifBaseZeroTerritori($codiprod,$territori)) $result['informacio']["cp13"]  = '--';
		else {                                                     
		  if (trim($cp[13])=='') $result['informacio']["cp13"]  = ' ';
		  else $result['informacio']["cp13"]  = $preus->number_format_O($cp[13],2,',','');
		}                                                          
		if ($cp[13]!='') $result['informacio']["preuFinal"]  = $preus->number_format_O($cp[13],2,',','');
		else  $result['informacio']["preuFinal"]  = '';            
		$result['informacio']["preuInicial"]  = $preus->number_format_O($cp[13],2,',',''); // variable per "recordar el preu actual
		//                                                         
		// Gràfica comparativa amb els altres territs i amb el total de Cat	
		$elspreusTerritsMAnt = $preus->pricesYearBefore($codiprod); 
		$elspreusTerritsAAnt = $preus->pricesMonthBefore($codiprod);
		while (list ($Tkey, $TNom) = each ($preus->TerritorisNoms)) 	
		{                                                          
			 if (!$preus->ifBaseZeroTerritori($codiprod,$Tkey))    
			 {		                                               
				 $result['informacio']["preuA".$Tkey]  = $preus->number_format_O(($elspreusTerritsAAnt[$Tkey]),2,',','');			 
				 $result['informacio']["preuM".$Tkey]  = $preus->number_format_O(($elspreusTerritsMAnt[$Tkey]),2,',','');											  
			}                                                      
		}                                                          
		$result['informacio']["preu".$territori]  = $preus->number_format_O(($cp[13]),2,',',''); 	 
		$preuCatMesAnt = $preus->priceCatMonthBefore($codiprod);   
		$result['informacio']["preuACat"]  = $preus->number_format_O(($preuCatMesAnt),2,',','');
		$preuCatAnyAnt = $preus->priceCatYearBefore($codiprod);    
		$result['informacio']["preuMCat"]  = $preus->number_format_O($preuCatAnyAnt ,2,',','');
		                                                           
		 //  --------------                                        
		$preuValidat=$preus->priceTerritoriValidat($codiprod,$territori);
		                                                           
		                                                           
		if ($preus->priceTerritoriValidat($codiprod,$territori) )  // (¿?) no cal tornar a cridar la funció, ho tenim a la var
		{                                                          
		   $result['informacio']["block"]["PreuValidat"]=array();  
		   if ($_SESSION['EsSc']) // ST connectat amb permisos especials
			 {	$result['informacio']["block"]["PreuValidat"]["img_alert"]='<img src="images/alerta.gif" alt="Alerta."  width="36" height="36" hspace="0" vspace="0" border="0"  />';
				$result['informacio']["block"]["BotoActualitzaPreu"]=array();			
				$result['informacio']["preuvalidat"]='false';			
			 }                                                     
		   else $result['informacio']["preuvalidat"]='true';       
		}                                                          
		elseif ($preus->is_priceAuto($codiprod, $territori, $any, $mes))
		{                                                          
		   $result['informacio']["preuautomatic"]="<br>automatic"; 
		   $result['informacio']["preuvalidat"]='false';           
		}                                                          
		elseif ($preus->espotmodif_preu($codiprod,$territoriUsuari,$perfil))
		{  // Mirem si el territor que entra les dades, ja sigui perquè és gestor o perquè és la provincia que li toca entrar el preu, està en mode "entrar dades".
		   $result['informacio']["block"]["BotoActualitzaPreu"]=array(); 
		   $result['informacio']["preuvalidat"]='false';           
		                                                           
		}                                                          
		elseif ($preus->estatAplicatiu($territoriUsuari,$perfil)=='validat')	
		{  if ($_SESSION['EsSc']) // ST connectat amb permisos especials, introduint un preu passat inexistent. Si el preu ja hi fos, seria validat i no hauria arribat aquí.
		   {	$result['informacio']["block"]["PreuValidat"]["img_alert"]='<img src="images/alerta.gif" alt="Alerta."  width="36" height="36" hspace="0" vspace="0" border="0"  />';
				$result['informacio']["block"]["BotoActualitzaPreu"]=array();			
				$result['informacio']["preuvalidat"]='false';			
		   }                                                       
		   else                                                    
		   {  $result['informacio']["block"]["PreuValidat"]=array();
			  $result['informacio']["preuvalidat"]='true';         
		   }                                                       
		}                                                          
		else                                                       
		{                                                          
		   $result['informacio']["preuvalidat"]='false';           
		}                                                          
		                                                           
		//                                                         
		$metode= $preus->methodPrices($codiprod);                  
		$result['informacio']["calcpreufinal"]=($metode=='M')?'Mitjana':'Suma';
		$datahorapreu=$preus->PriceDateTime($codiprod,$territori); 
		$result['informacio']["dataultmodifpreu"]=$datahorapreu[0]." ".$datahorapreu[1];  // (¿?) cal revisar el format
		                                                           
		// Gràfics                                                 
		$rndGrafica=rand(1000,2000);  // (¿?) per cache, potser ja no cal
		$result['informacio']["rndGrafica"] = $rndGrafica;	       
		                                                           
		// $esBestiarPerVida = $preus->esBestiarPerVida($codiprod);
		//$get_ponderacio_anual = $preus->get_ponderacio_anual($codiprod,$mes);
		//var_dump($get_ponderacio_anual);                         
		//$get_ponderacio_mensual = $preus->get_ponderacio_mensual($codiprod,$mes);7
		if (($PagatsPercebuts=='percebuts') and (!$preus->esBestiarPerVida($codiprod)) 
			and ($preus->get_ponderacio_anual($codiprod,$mes))     
			and ($preus->get_ponderacio_mensual($codiprod,$mes))   
			)                                                      
		{                                                          
			                                                       
			  // $CalculsExcel = new Excel($Conn,'percebuts',$base);
			  // $avgCat[2000] = $CalculsExcel->entradaPreusAnyT(2000,true);		
			  $result['informacio']["block"]["ImportanciaRelativa"]=array();
			                                                       
			                                                       
		      $pes_mes = $preus->get_ponderacio_mensual($codiprod,$mes);
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_impmes"]=$preus->number_format_O($pes_mes,2,',','');				
				//Posicio mensual                                  
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_posmes"]=$preus->get_rang_posicio_mes($codiprod,$mes);
				                                                   
			  //Pes any                                            
              $pes_any = $preus->get_ponderacio_anual($codiprod,$mes);
			  //echo $pes_any;                                     
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_impany"]=$preus->number_format_O($pes_any,2,',','');
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_totalmes"]=$preus->get_rang_total_productes_mes($mes);
			                                                       
			  //Posició anual                                      
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_posany"]=$preus->get_rang_posicio_anual($codiprod,$mes);
			  $result['informacio']["block"]["ImportanciaRelativa"]["pes_totalany"]=$preus->get_rang_quantitat_productes_anual();
				                                                   
			                                                       
		}                                                          
		  
        // Importancia relativa
		// Mesos <--> Ponderacions quantitatives de la base per territori
		/*
		[
			{data: [44,8,80,33,2,55,13,8,44,5,null,38], label: 'Barcelona'}
		  ];
		*/
		// (???) Aquests càlculs es repeteixen en ENTRADA i REVISIÓ, es pot reaprofitar el codi
		$result['informacio']["ponderacions_quantitatives"] = array();
		$result['informacio']["ponderacions_quantitatives"]['chart_data'] = array();
		$T = $preus->TerritorisNoms;
		while (list ($Tkey, $TNom) = each ($T)) {	
			$elspesos=$preus->baseCalHistoricMesTerrit($codiprod,$Tkey);
			if (is_array($elspesos)) 
			  { $queHiHa = array_sum($elspesos);
				if ($queHiHa>0)  // no són tots zeros
				{ $elspesosxgrafica = array_merge(array($TNom),$elspesos);
				  // Array ( [0] => Barcelona [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 0 [7] => 50 [8] => 29.99 [9] => 10 [10] => 10 [11] => 0 [12] => 0 ) 
				  $infoPondercionsTerritori = array(
					'data'=>(array_slice($elspesosxgrafica, 1)),
					'label'=>($elspesosxgrafica[0]));
				  $result['informacio']["ponderacions_quantitatives"]['chart_data'][]	= $infoPondercionsTerritori;
				}
			  }
		}
		// Historic de preus
		$filtre=array();
		for ($m=1; $m<=12; $m++) $filtre[$m] = $preus->baseCalPercentageAllTerrMonth($codiprod,$m);
		$result['informacio']["evolucio_preus"] = array();
		$result['informacio']["evolucio_preus"]['filtre']=$filtre;
		$result['informacio']["evolucio_preus"]['HistPrices'] = array('xAxisValues'=>array(),'data'=>array());
		$arrayPrices = $preus->HistoricPrices_ambFiltre($codiprod,$filtre);
		$elsTerrs = array_keys($arrayPrices); 
		$T = $preus->TerritorisNoms;
		$info=array();
		foreach ($elsTerrs as $elTerr) {
					$info[$elTerr]=array('label'=>$T[$elTerr],'data'=>array_fill(0, 13, null));
		}
		$pastDate = mktime(0, 0, 0, $mes,   1,   $any-1);
		$start = new DateTime(date('Y-m-d',mktime(0, 0, 0, $mes,   1,   $any-1)));
		$interval = new DateInterval('P1M');  // 1 mes
		$end = new DateTime(date('Y-m-d',mktime(0, 0, 0, $mes,   2,   $any)));
		$period = new DatePeriod($start, $interval, $end);
		$ix=0;
		foreach ($period as $dt) {
			$y = $dt->format('Y');
			$m = $dt->format('m');
			$result['informacio']["evolucio_preus"]['HistPrices']['xAxisValues'][]=$dt->format('m/Y');
			// Territoris
			foreach ($elsTerrs as $elTerr) {
				if (isset($arrayPrices[$elTerr][intval($y)][intval($m)])) { $info[$elTerr]['data'][$ix]=floatval($arrayPrices[$elTerr][intval($y)][intval($m)]); }
			}
			$ix++;
		}
		$result['informacio']["evolucio_preus"]['HistPrices']['data']=array();
		foreach ($info as $infodetall) {
			$result['informacio']["evolucio_preus"]['HistPrices']['data'][]=$infodetall;
		}
		//$result['informacio']["evolucio_preus"]['arrayPrices']=$arrayPrices;
		
		// Els comentaris                                          
		reset ($elscomm_Arr);                                      
		$primercom=true;                                           
		$ultimQuiComentari = "";                                   
		$qCom=0;                                                   
		$quantsComs=0;                                             
		if (count($elscomm_Arr)>0) { $result['informacio']["block"]["HiHaComentaris"]=array(); }
		while (list ($key, $infoComm) = each ($elscomm_Arr))       
		{                                                          
		    $qCom++;	   	                                       
			$QuiComentari = $infoComm->QUI;                        
			/*                                                     
			 $t->set("dataComentari",                              
				  substr(trim($infoComm[2]),8,2)."/".              
				  $preus->mesosAvr[intval(substr(trim($infoComm[2]),5,2))]."/".
				  substr(trim($infoComm[2]),0,4).' '.              
				  substr(trim($infoComm[2]),-8));                  
			*/                                                     
			if ($QuiComentari=='SC') $quantsComs=0;                
			else	$quantsComs++;                                 
			$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]  = array();
			$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["dataComentari"] = $infoComm->DATACOMENTARI;
			$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["quiComentari"] = (($QuiComentari=='ST')?('<strong>'):('<i>')).$QuiComentari.(($QuiComentari=='ST')?('</strong>'):('</i>'));	   
			$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["Comentari"]=stripslashes(nl2br($infoComm->COMENTARI));
			if (trim($infoComm->INFOPREUSENTRATS)!='') {           
					$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["infoPreusEntrats"]=nl2br($infoComm->INFOPREUSENTRATS);
			}                                                      
			else {                                                 
				$result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["infoPreusEntrats"]='No hi havia preu en el moment del comentari.';	 
			}                                                      
			$possiblearxiuadj = $codiprod."_".$territori."_".$any."_".$mes."_".$infoComm->SEQ."_".$QuiComentari.".".$infoComm->EXTENSIO."";
 $rutaadjuntscom = ''; // (***)                                    
			if (file_exists($rutaadjuntscom."/".$possiblearxiuadj)) {
			   // adjunt  --> 001100_25_2007_1_1_ST.gif            
			   $result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["ArxiuAdjunt"] = array();
			   $result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["ArxiuAdjunt"]["arxiu"]     = $possiblearxiuadj;	   
			   $result['informacio']["block"]["HiHaComentaris"][($qCom-1)]["ArxiuAdjunt"]["arxiulink"] = $rutaadjuntscom."/".$possiblearxiuadj;	   
		   }	                                                   
			else {   }                                             
			                                                       
			                                                       
			                                                       
			$primercom=false;                                      
		}                                                          
		// ===============================================================================================
		$result['informacio']["codiprodAnt"]    = $codiAnt;        
		$result['informacio']["nomproducteAnt"] = str_replace("'","´",$nomproducteAnt);
		$result['informacio']["codiprodSeg"]    = $codiSeg;        
		$result['informacio']["nomproducteSeg"] = str_replace("'","´",$nomproducteSeg);
		// ===============================================================================================
		                                                           
		$result['estat']="OK";                                     
		                                                           
		return $this->respond($result);                            
		                                                           
	}                                                              
     
    public function entrada_preu(Request $request) {
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['estat']['path']=$request->path();                 
		$result['estat']['params']=$request->all();  
	    
		$estatOK = true;
		$msg = '';
		
		$base			= $result['estat']['params']['base'];
		$elcodi			= $result['estat']['params']['elcodi'];
		$any			= $result['estat']['params']['any'];
		$mes			= $result['estat']['params']['mes'];
		$preu1=0;
		$preu2=0;
		$preu3=0;
		$preu1			= PaginesController::a_Float($result['estat']['params']['preu1']);
		$preu1			= round($preu1*100)/100.0;
		if (isset($result['estat']['params']['preu2'])) {
			$preu2			= PaginesController::a_Float($result['estat']['params']['preu2']);
			$preu2			= round($preu2*100)/100.0;
		}
		if (isset($result['estat']['params']['preu3'])) {
			$preu3			= PaginesController::a_Float($result['estat']['params']['preu3']);
			$preu3			= round($preu3*100)/100.0;
		}
		$preufinal				= PaginesController::a_Float($result['estat']['params']['preufinal']);
		$territori				= $result['estat']['params']['territori'];
		$pagatspercebuts		= $result['estat']['params']['tipuspreus'];
		$calcpreufinal			= $result['estat']['params']['calcpreufinal'];  // !!! s'ha de recuperar de la BBDD
		
		$EsSc=false;
		
		$preu[1] = $preu1;
		$preu[2] = $preu2;
		$preu[3] = $preu3;
		if ($calcpreufinal == '') {  $estatOK = false; $msg = ''; }
		$user = $this->user;
		if ($user->territori != $territori) {   $estatOK = false; $msg = 'territori erroni';  }
		$steps = '(1)';
		if ($estatOK) {
			$steps .= '(2)';
			$preus = new \App\Library\Prices($pagatspercebuts,$any,$mes,$territori,($base-2000),DB::connection());
			$quantsPreus = $preus->maxPrices($elcodi);		
			$result['informacio']['quantsPreus'] = $quantsPreus;			
			//Comprovar gestor
			$gestors = array();
			$gestors = $preus->BuscaGestor($elcodi);
			$esGestor = in_array($territori,$gestors);
			if($esGestor) {
				$steps .= '(es gestor)';
				//Mira si el territori conectat és gestor d'aquest producte sino surt
				$esCAT = (strtolower($preus->TipusPreu($elcodi)) == 'cat');
					
				$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerr($elcodi,true);	 
				//print_r($ponderacionsArr_tmp);	
				// Array ( [B] => 46.9879518072 [G] => 36.1445783133 [L] => 0 [T] => 16.8674698795 ) 
				//Comprova el tipus de producte Cat o prov
				if ($esCAT)	
				{
				 while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
				  { 
					if ($TPercent>0)	$Codterr[] = array_search($Tlletra,$preus->TerritorisLletra);
				   }
				} else {
					$steps .= '(es st)';
					$Codterr = array();
					$Codterr[] = $territori;
					//Si el tipus és prov cal borrar els preus automàtics
					$ponderacionsArr_tmp = $preus->baseCalPercentageAllTerr($elcodi,true);	  
						// Array ( [B] => 46.9879518072 [G] => 36.1445783133 [L] => 0 [T] => 16.8674698795 ) 
						while (list ($Tlletra,$TPercent) = each ($ponderacionsArr_tmp))
						{ 
							$steps .= '('.$Tlletra.')';
							if (($preus->TerritorisLletra[$territori]!=$Tlletra) and ($TPercent>0))
							  { $TCodi = array_search($Tlletra,$preus->TerritorisLletra);
								if ($preus->is_priceAuto($elcodi, $TCodi, $any, $mes))
								{  // Preu entrat automàtic
								   $forapreu = strtoupper("DELETE preus".$pagatspercebuts.
										 " WHERE CodiProd ='".$elcodi."' AND  CodiTerr ='".$TCodi."' AND ANY ='".$any."' AND Mes ='".$mes."' ");
									$Conn = DB::connection();                                  
									$rows_elsProds = $Conn->select($forapreu);  
								   //echo "Preu esborrat. ";
								}
							  }
						}
				}
				if ($preus->priceTerritoriValidat($elcodi,$territori))
				   {   
					
					if ($EsSc) // ST connectat amb permisos especials
					   {
					   }
					   else {
						   {  $estatOK = false; $msg = 'Preu ja validat'; }
					   }
				   }
				
			}
			

		}
		if ($estatOK) {
			$steps .= '(3)';
			//if ($preus->is_priceAuto($CodiProd, $territori, $any, $mes)) die('Preu Automàtic.');
			//Esborrar preus abans de tot per després fer INSERTS i no UPDATES. Ho fem per tots els territoris necessaris
			$Total = count($Codterr); 
			for ($i=0;$i<$Total;$i++)
			{
			 $forapreu = strtoupper("DELETE FROM preus".$pagatspercebuts." WHERE CodiProd ='".$elcodi."' AND  CodiTerr ='".$Codterr[$i]."' AND ANY ='".$any."' AND Mes ='".$mes."	' ");
			 $Conn = DB::connection();                                  
			 $rows_elsProds = $Conn->select($forapreu);  
			}
			
			//Es mira quins preus estan inserits i depenen d'això es canvi el valor dels parametres i del preufinal
			$q=0;
			$sumapreu=0;
			$datahora = array();
			$PreusZero = True;
			$preuFinal = 0;
			$noupreu = 'INSERT INTO  preus'.$pagatspercebuts."( codiprod, mes, ANY, ";
			for ($i = 1; $i <= $quantsPreus; $i++) 
			 {    //echo $preu[$i].',';
			      $preuI = floatval($preu[$i]);
				  $noupreu .= "Preu".$i.", DataPreu".$i.", HoraPreu".$i.", "; 
				  if ($preu[$i]>0) 
				  { $datahora[$i] = " NOW() ";
				    $q++;
					$PreusZero = False;
				  }
				  else
				  { $datahora[$i] = " null ";
				    
				  }
				  $sumapreu += $preu[$i];
				  
				  //$noupreu .= ",  Preu".$i."  =".$preuI.", DataPreu".$i."  = ".$datahora.", HoraPreu".$i."  = ".$datahora." ";   
			 }
			 $steps .= '(4).('.$preuFinal.')('.($PreusZero?'si':'n').').';
			if ($preuFinal==0 and (!$PreusZero))
			{    $steps .= '(4.1)';
				$preuFinal = $sumapreu;
				if (strtolower($calcpreufinal)=='mitjana')  {  $preuFinal = $preuFinal / $q; }			   
			 }	  
			if ($PreusZero) 
			{
				 for ($i = 1; $i <= $quantsPreus; $i++) 
					{$preu[$i] = $preuFinal;
					 $datahora[$i] = " NOW() ";
					}
			}
			//echo $preuFinal;
			 $noupreu .= "FinPreu, coditerr,qui) VALUES ('".$elcodi."',".$mes.", ".$any.", ";
			 for ($i = 1; $i <= $quantsPreus; $i++) 
				 {
					$noupreu .= $preu[$i].", ".$datahora[$i].", ".$datahora[$i].", ";
									 
				 }
			 $noupreu.= $preuFinal;	
			 $steps .= '(5)';
			 for ($i=0;$i<$Total;$i++)
			 {  
				$steps .= '(5.'.$i.':'.$preuFinal.')';
				$res_noupreu=false;
				$noupreuinsert= $noupreu.", ".$Codterr[$i].", ".$territori.")";
				if ($preuFinal>0) $res_noupreu = $Conn->select(strtoupper($noupreuinsert));
				$result['informacio']['noupreuinsert'] = strtoupper($noupreuinsert);
				$result['informacio']['result'] = $res_noupreu;
				/*
				if ($res_noupreu===false) die('Error: '.$noupreuinsert);
				$nr = $Conn->Affected_Rows();*/
							 
					if ($EsSc)
					{ $marcaPreu = "UPDATE preus".$pagatspercebuts." set Alerta = 's' WHERE CodiProd ='".$CodiProd."' AND  CodiTerr ='".$Codterr[$i]."' AND ANY ='".$any."' AND Mes ='".$mes."' ";
					  $res_marcaPreu = $Conn->Execute($marcaPreu);
					}  			 
			}
			$result['informacio']['priceTerritoriPeriode']=$preus->priceTerritoriPeriode($elcodi,$territori,$mes,$any);
			$result['informacio']['enteredPricesTerritori']=$preus->enteredPricesTerritori($elcodi,$territori);
			
		}
		$result['informacio']['steps'] = $steps;
		$result['informacio']['msg'] = $msg;
		$result['estat']['OK']=$estatOK;
		return $this->respond($result);     
	}

	
	/**                                                            
	 *                                                             
	 *  http://laravel.ticnologia.com/api/pagina/consulta/2010/percebuts/054170
	 *                                                             
	 * @param Request $request                                     
	 * @return Response                                            
	 */                                                            
	public function consulta($base,$pagatspercebuts,$codiprod)     
	{                                                              
		//$base = $base-2000;                                      
		$_GET = array();                                           
		$_GET['elcodixhistoric']='';                               
		$_SESSION = array();                                       
		$_SESSION['base']=10;                                      
		$_SESSION['any']=2017;                                     
		$_SESSION['mes']=1;                                        
		$_SESSION['territori'] = 8;                                
		$_SESSION['perfil'] = 'SC';                                
		$_REQUEST = array();                                       
		$_REQUEST['elsnumeros'] =                                  
		$_REQUEST['elspunts'] =                                    
		$_REQUEST['dadesmes'] = 1;                                 
		$_REQUEST['anuals'] = 1;                                   
		$_REQUEST['ple'] = 'si';                                   
		$_REQUEST['Veure8'] = 'si';                                
		$_REQUEST['Veure17'] = 'si';                               
		$_REQUEST['Veure25'] = 'si';                               
		$_REQUEST['Veure43'] = 'si';                               
		$_REQUEST['Veure99'] = 'si';                               
		$_REQUEST['adaptarMaxMin'] = 'si';                         
		// sel_prods=051115&ultimabase=10&                         
		// base=10&elcodixhistoric=051115&mesinici=2&anyinici=2016&mesfi=2&
		// anyfi=2017&Veure8=si&Veure17=si&Veure25=si&Veure43=si&Veure99=si&dadesmes=1&anuals=1
		                                                           
		$territVeure = array();                                    
	                                                               
		foreach ($_REQUEST as $clau=>$valor)                       
		{ 	                                                       
			if(preg_match('/Veure/i', $clau))                      
				{   $cterrA = sscanf($clau,"Veure%d");             
					 $cterr = $cterrA[0];                          
					 if (strtolower($_REQUEST['Veure'.$cterr])=='si') 
						 { $territVeure[] = $cterr;                
							//$paramsGrafica .= "&Veure".$cterr."=si";
						 }                                         
				}                                                  
		}                                                          
		// Array ( [0] => 8 [1] => 17 [2] => 99 [3] => 99 )        
		// La primera vegada que es carrega la pàgina, veure tot els territoris	
		$adaptarMaxMin =false;                                     
		if (count($territVeure)==0)                                
		{ while (list ($Tkey,$TCodi) = each ($preus ->TerritoriesCodes)) 
		  {  $territVeure[] = $TCodi;                              
			  $paramsGrafica .= "&Veure".$TCodi."=si";             
		  }                                                        
		  $adaptarMaxMin =true;  // La primera vegada ...          
		  $_REQUEST['elspunts']=1;                                 
		}                                                          
		else                                                       
		{  // No és la 1a vegada                                   
		   $adaptarMaxMin = (strtolower($_REQUEST['adaptarMaxMin'])=='si');	
		   $dadesmes=1;	                                           
		}                                                          
		//if ($adaptarMaxMin) $paramsGrafica .= "&adaptarMaxMin=si";
		                                                           
		$debug = false;                                            
		if ($debug) { $result['debug']=array(); }                  
		                                                           
		$opcio="historic";                                         
	$paramsGrafica=''; // (!!)                                     
	$anyinici = 2016;                                              
	$anyfi = 2016;                                                 
	$mesinici = 1;                                                 
	$mesfi=12;                                                     
		                                                           
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['informacio']['block']=array();                    
		                                                           
		$PagatsPercebuts = $pagatspercebuts;                       
		$elsnumeros  = ($_REQUEST['elsnumeros']);                  
		$elspunts  = ($_REQUEST['elspunts']);                      
		$ple  = (strtolower($_REQUEST['ple'])=='si');              
		if ($ple) $paramsGrafica .= "&ple=si";			           
		$dadesmes  = trim($_REQUEST['dadesmes']);                  
		$anuals  = trim($_REQUEST['anuals']);                      
		if ( ($dadesmes=='') and ($anuals=='') ) $dadesmes=1;      
		                                                           
		$any = intval($_SESSION['any']);                           
        $mes = intval($_SESSION['mes']);                           
		$territori = intval($_SESSION['territori']);               
		$perfil = $_SESSION['perfil'];                             
		if ($perfil=='SC') { $territori = 99; }                    
		$base=$_SESSION['base'];                                   
		                                                           
		$elcodi = ($_GET['elcodixhistoric']!='')?($_GET['elcodixhistoric']):($codiprod);
		                                                           
		$this->preparePage($base,$pagatspercebuts,$codiprod,$any, $mes, $territori);
		                                                           
		                                                           
		$CalculsExcel = new \App\Library\Excel(DB::connection(),$pagatspercebuts,$base);
		if (($this->preus->es_agregacio($CalculsExcel,$elcodi))) $es_una_agregacio=true;
		else $es_una_agregacio=false;                              
		$result['informacio']['es_agregacio']=$es_una_agregacio;   
		if ($es_una_agregacio)                                     
		 {                                                         
		   $elsPreusAgr = $this->preus->pricesAgrAll($elcodi,$preus->setAgregacio($elcodi,$CalculsExcel));
		                                                           
		   //print_r($elsPreusAgr);                                
		   /*                                                      
					Array (                                        
				[2000]                                             
					=> Array ( 	[8] => Array ( [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 0 [7] => 12.38 [8] => 12.56 [9] => 12.86 [10] => 13.04 [11] => 0 [12] => 0 [2000] => 0 ) 
								[17] => Array ( [1] => 0 [2] => 0 [3] => 0 [4] => 0 [5] => 0 [6] => 12.43 [7] => 12.32 [8] => 12.98 [9] => 0 [10] => 0 [11] => 0 [12] => 0 [2000] => 0 ) 
								[25] => Array ( [1] => 14.72 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 13.94 [6] => 13.34 [7] => 13.22 [8] => 13.28 [9] => 13.7 [10] => 13.94 [11] => 14.12 [12] => 14.36 [2000] => 14.36 ) 
								[43] => Array ( [1] => 14.48 [2] => 14.72 [3] => 14.72 [4] => 15.03 [5] => 0 [6] => 0 [7] => 0 [8] => 0 [9] => 13.25 [10] => 13.46 [11] => 13.63 [12] => 13.87 [2000] => 13.87 ) ) 
				[2001] => Array ( [8] =>                           
                                                                   
			   */	                                               
		 }                                                         
		$result['informacio']["elcodixhistoric"]=$elcodi;          
		if ($es_una_agregacio)                                     
		 {                                                         
		    $elcodis_de_Agr		=	$this->preus->setAgregacio($elcodi,$CalculsExcel);   // agafem un codi de l'agregació per agafar les dades de la BD
		    $elcodi_a_buscar	=	$elcodis_de_Agr[0];            
		    $elnomdelprod		=	$CalculsExcel->qFisiquesRed[$elcodi]['DESCRIPCIO'];	   
		 }                                                         
		else $elcodi_a_buscar	=	$elcodi;                       
		$SQL_elProd = strtoupper("SELECT *  FROM ".$this->preus->descripcioTable."  WHERE CodiProd ='".$elcodi_a_buscar."' ");
		$rows = DB::connection()->select($SQL_elProd);             
		$num = count($rows);                                       
		                                                           
		if ($num>0) { 	                                           
			 $row_elProd = ((array)$rows[0]);                      
			 // $row_elProd); Array ( [CODIPROD] => 703101 [NOMPROD] => Blat [UNITAT] => eur/100 kg [SUBGRUP] => Pinsos simples [GRUP] => ALIMENTS DEL BESTIAR [FASEINTERCANVI] => De venedor a comprador [CONDICIONSCOMERCIALITZACIO] => A granel, preu en destí (explotació comprador), de qualitat mitjana i sense IVA [NECPREUS] => 1 [MAXPREUS] => 1 [CALCUL] => M [DESCPREU1] => Preu [DESCPREU2] => [DESCPREU3] => [ACTIU] => s [PUBLICAR] => s [TIPUS] => Cat [GESTOR] => L ) 
			 $result['informacio']["elcodiprod"]=$elcodi;          
			 $result['informacio']["elgrup"]=$row_elProd['GRUP'];  
			 $result['informacio']["lescondcomer"]=$row_elProd['CONDICIONSCOMERCIALITZACIO'];
			 $result['informacio']["laintercanvi"]=$row_elProd['FASEINTERCANVI'];
			 $result['informacio']["lesunitats"]=str_replace("eur","€",$row_elProd['UNITAT']);
		}		                                                   
		else $elnomdelprod='';                                     
		if ($elcodi=='') $result['informacio']["elnomproducte"]=$elnomdelprod;			  
		elseif ($es_una_agregacio) $result['informacio']["elnomproducte"]=$elnomdelprod;
		else $result['informacio']["elnomproducte"]=$row_elProd['NOMPROD'];
		                                                           
		 // la llista de tots els productes                        
		 // Els prods de la BD                                     
		$SQL_elsProds = strtoupper("SELECT *  FROM ".$this->preus->descripcioTable." WHERE actiu".$base." ='s' ORDER BY CodiProd   ");	
		$res_elsProds = DB::connection()->select($SQL_elsProds);   
		$grupAct='';                                               
		$grupAnt='';                                               
		$subgrupAct='';                                            
		$subgrupAnt='';                                            
		$num = count($res_elsProds);                               
		if ($num>0) { 	                                           
			$result['informacio']['block']['UnProductedeTots']=array();
			foreach($res_elsProds as $rowOBJ){                     
					$rowelsProds = ((array) $rowOBJ);              
					                                               
					if (!is_null($this->preus->pertany_agregacio($CalculsExcel,$rowelsProds['CODIPROD']))) $Codi_llista_es_agregacio=true;
					else $Codi_llista_es_agregacio=false;          
					$grupAct = $rowelsProds['GRUP'];               
					$subgrupAct = $rowelsProds['SUBGRUP'];         
					$fer = true;                                   
					if ($grupAct!=$grupAnt)                        
					   {                                           
						$bloc=array();                             
					    $bloc["elcodi"]='';                        
					    $bloc["elnom"]=$rowelsProds['GRUP'];       
					    $bloc["css_sel_prods"]="gris";  // gris    
					    //echo $rowelsProds['GRUP']."<br>";        
                        $result['informacio']['block']['UnProductedeTots'][]=$bloc;
					   }                                           
					if ($subgrupAct!=$subgrupAnt) {                
						 if ($subgrupAct==$rowelsProds['NOMPROD']){
							$bloc=array();                         
							$bloc["elcodi"]=$rowelsProds['CODIPROD']; 
					        $bloc["elnom"]="&nbsp;&nbsp;".$rowelsProds['SUBGRUP']; 
					        $bloc["css_sel_prods"]=""; //          
					        //echo "&nbsp;&nbsp;".$rowelsProds['SUBGRUP']."<br>";
                            $result['informacio']['block']['UnProductedeTots'][]=$bloc;
                            $fer = false;                          
						 }                                         
						 else                                      
						 {                                         
							$bloc=array();                         
							if ($Codi_llista_es_agregacio)         
						    {                                      
						   	  $bloc["elcodi"]=$preus->pertany_agregacio($CalculsExcel,$rowelsProds['CODIPROD']); 
					          $bloc["elnom"]="*&nbsp;&nbsp;".$rowelsProds['SUBGRUP']; 
					          $bloc["css_sel_prods"]="";           
						    }                                      
						    else                                   
						    {                                      
						   	  $bloc["elcodi"]='';                  
					          $bloc["elnom"]="&nbsp;&nbsp;".$rowelsProds['SUBGRUP']; 
					          $bloc["css_sel_prods"]="";           
						    }                                      
					        //echo "&nbsp;&nbsp;".$rowelsProds['SUBGRUP']."<br>";
                            $result['informacio']['block']['UnProductedeTots'][]=$bloc;
						   }                                       
					}                                              
					if ($fer)                                      
					   {                                           
					    $bloc=array();                             
						$bloc["elcodi"]	=	$rowelsProds['CODIPROD']; 
					    $bloc["elnom"]	=	"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rowelsProds['NOMPROD']; 
					    $bloc["css_sel_prods"]	=	""; //         
					    //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rowelsProds['NOMPROD']."<br>";
                        $result['informacio']['block']['UnProductedeTots'][]=$bloc;
					   }	                                       
					$grupAnt=$grupAct;                             
					$subgrupAnt=$subgrupAct;		               
			}					                                   
		}                                                          
		                                                           
		//                                                         
		$limits = $this->preus->limits_anys_preus($PagatsPercebuts,DB::connection(),$base);  // (¿?)  la connexió cal?
		// Array ( [MAX] => 2009 [MIN] => 2005 [MMIN] => 1 [MMAX] => 1 )
		$result['informacio']["datamaxmes"] = $limits['MMAX'];     
		$result['informacio']["datamaxany"] = $limits['MAX'];      
		// selecció Any                                            
		 for ($aa=$limits['MIN'];$aa<=$limits['MAX'];$aa++)        
		 {                                                         
			if (!isset($result['informacio']['block']['AnysSelect2'])) {
				$result['informacio']['block']['AnysSelect2']=array();
			}                                                      
			$bloc=array();                                         
			$bloc["anysel"]=$aa;                                   
			$result['informacio']['block']['AnysSelect2']=$bloc;   
		 }                                                         
		 for ($aa=$limits['MIN'];$aa<=$limits['MAX'];$aa++)        
		 {                                                         
			if (!isset($result['informacio']['block']['AnysSelect1'])) {
				$result['informacio']['block']['AnysSelect1']=array();
			}                                                      
			$bloc=array();                                         
			$bloc["anysel"]=$aa;                                   
			$result['informacio']['block']['AnysSelect1']=$bloc;   
		 }                                                         
		// Preus anuals                                            
		                                                           
		$preusAnuals=$this->preus->mitjanaAnual($elcodi,$anyinici,$anyfi);
		/*                                                         
		Array ( [2005] => Array ( [Cat] => 127.887580952 [8] => 120.15933162 [17] => 130.19 [25] => -- [43] => 205 ) 
				  [2006] => Array ( [Cat] => 134.762952381 [8] => 126.06311054 [17] => 143.32 [25] => -- [43] => 205 ) ) 
		*/		                                                   
		if ($anyinici<2000) $elsPreusHist = "<br><span class='color_vermell boldMes'>No hi ha dades </span>";
		else if ( ($anyfi<$anyinici) or ( ($anyfi==$anyinici) and ($mesfi<$mesinici) ) )  $elsPreusHist = "<br><span class='color_vermell boldMes'>No hi ha dades </span>";
		else                                                       
		 {  if (!$es_una_agregacio)                                
			   {                                                   
				  $elsPreusHist = $this->preus->HistoricPrices3ToHTML($elcodi,$anyinici,$anyfi,$mesinici,$mesfi,$territVeure,$dadesmes,$preusAnuals,$anuals);
				                                                   
			   }                                                   
			else {                                                 
			   $elsPreusHist = $this->preus->HistoricAgregatPricesToHTML($elsPreusAgr,$anyinici,$anyfi,$mesinici,$mesfi,$territVeure,$dadesmes,$anuals);
			}                                                      
				                                                   
		 }	                                                       
		$result['informacio']["anyinici"]=$anyinici;               
		$result['informacio']["anyfi"]=$anyfi;                     
		$result['informacio']["mesinici"]=$mesinici;               
		$result['informacio']["mesfi"]=$mesfi;                     
		$result['informacio']["rand"]=rand(1,9999);                
		 foreach ($territVeure as $cterrit)                        
		   {                                                       
			$result['informacio']["checked".$cterrit]='checked'; 	   
			$result['informacio']['block']["VeureHist".$cterrit]=array();
			                                                       
		   }                                                       
		if ($dadesmes==1) $result['informacio']["checkeddadesmes"]='checked';   
		else $result['informacio']["checkeddadesmes"]='';          
		if ($anuals==1) $result['informacio']["checkedanuals"]='checked';   
		else $result['informacio']["checkedanuals"]='';            
		if ($elsnumeros==1) $result['informacio']["checkedelsnumeros"]='checked';   
		else $result['informacio']["checkedelsnumeros"]='';        
		if ($elspunts==1) $result['informacio']["checkedelspunts"]='checked';   
		else $result['informacio']["checkedelspunts"]='';          
		if ($ple==1) $result['informacio']["checkedple"]='checked';   
		else $result['informacio']["checkedple"]='';               
		if ($adaptarMaxMin==1) $result['informacio']["checkedadaptarMaxMin"]='checked';   
		else $result['informacio']["checkedadaptarMaxMin"]='';     
                                                                   
		foreach ($this->preus->TerritoriesCodes as $cterrit)       
		{   if (in_array($cterrit,$territVeure)) { $result['informacio']["Veure".$cterrit]='si'; 	 }
		 else {  $result['informacio']["Veure".$cterrit]='no'; 	}  
		}	                                                       
		if (in_array(99,$territVeure)) { $result['informacio']["Veure99"]='si'; 	 }
		else {  $result['informacio']["Veure99"]='no'; 	}          
		$result['informacio']["dadesmes"]=$dadesmes;               
		$result['informacio']["anuals"]=$anuals;                   
		$result['informacio']["taulaHistPeus"]=$elsPreusHist;      
		                                                           
		$result['estat']="OK";                                     
		                                                           
		return $this->respond($result);                            
	}                                                              
	                                                               
	public function consulta_refresc(Request $request) {           
		                                                           
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['estat']['path']=$request->path();                 
		$result['estat']['params']=$request->all();                
		                                        
		                                              
		                           
		$anyiniciseleccionat = $result['estat']['params']['anyiniciseleccionat'];
		$mesiniciseleccionat = $result['estat']['params']['mesiniciseleccionat'];
		$anyfiseleccionat = $result['estat']['params']['anyfiseleccionat'];
		$mesfiseleccionat = $result['estat']['params']['mesfiseleccionat'];
		$anuals = $result['estat']['params']['anuals'];            
		$mensuals = $result['estat']['params']['mensuals'];        
		$territoris = $result['estat']['params']['territoris'];  
		$base = $result['estat']['params']['base']-2000;  	
		$pagatspercebuts = $result['estat']['params']['tipuspreus'];  		
		$elcodi = $result['estat']['params']['elcodi'];
		                                                           
		/*                                                         
		{                                                          
		  "territoris": [8,17,25,43,99],                           
		  "mensuals": true,                                        
		  "anuals":   true,                                        
		  "anyiniciseleccionat": 2010,                             
		  "mesiniciseleccionat": 1,                                
		  "anyfiseleccionat":    2014,                             
		  "mesfiseleccionat":    12                                
		}                                                          
		*/                                                         
		                                                           
		                                                           
		$elsPreusHist=array();                                     
		$elsPreusHist['territorisCodis'] = array(                  
			8,17,25,43,99                                          
		);                                                         
		$elsPreusHist['territorisNoms'] = array(                   
			"Barcelona","Girona","Lleida","Tarragona","Catalunya"  
		);                                                         
		$elsPreusHist['valors'] = array(                           
			array("any" => 2016, "mes" => 1, "PreusTerritoris" => array("46,00","46,00","46,00"," -- ","46,00")),
			array("any" => 2016, "mes" => 2, "PreusTerritoris" => array("47,00","58,00","58,00","66,00","58,03")),
			array("any" => 2016, "mes" => 3, "PreusTerritoris" => array("46,00","60,00","60,00","69,00","59,96"))
		);                                                         
		$this->preparePage($base,$pagatspercebuts,$elcodi,$anyiniciseleccionat, $mesiniciseleccionat, 8);
		$preusAnuals=   $this->preus->mitjanaAnual($elcodi,$anyiniciseleccionat,$anyfiseleccionat);
		$elsPreusHist = $this->preus->HistoricPrices3ToHTML($elcodi,$anyiniciseleccionat,$anyfiseleccionat,$mesiniciseleccionat,$mesfiseleccionat,
								$territoris,$mensuals,$preusAnuals,$anuals);
		$result['informacio']["taulaHistPeus"]=$elsPreusHist;      
                                                    
		                                                           
		return $result;                                            
		                                                           
	}                                                              

    public function consulta_arbreCodis	(Request $request) {   
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['estat']['path']=$request->path();                 
		$result['estat']['params']=$request->all();  
		$base = $result['estat']['params']['base'];  
		$pagatspercebuts = $result['estat']['params']['tipuspreus']; 		
		$result['informacio']=array();
  
		$this->preparePage($base,$pagatspercebuts,'001100',2000, 1, 8);
		$SQL_elsProds = strtoupper("SELECT *  FROM ".$this->preus->descripcioTable." WHERE actiu".($base-2000)." ='s' ORDER BY CodiProd   ");	
		$Conn = DB::connection();                                  
		$ROWSelsProds = $Conn->select($SQL_elsProds);  
		$result['informacio']['rows'] = $ROWSelsProds;	
		$num = count($ROWSelsProds);       
		$grupAct='';
		$grupAnt='';
		$subgrupAct='';
		$subgrupAnt=''; 
		if ($num>0) { 	    
			$result['informacio']['block'] = array();
			$result['informacio']['block']['UnProductedeTots']=array();
			$CalculsExcel = new \App\Library\Excel(DB::connection(),$pagatspercebuts,$base);
			foreach($ROWSelsProds as $rowOBJ){                     
					$rowelsProds = ((array) $rowOBJ);              
					                                               
					if (!is_null($this->preus->pertany_agregacio($CalculsExcel,$rowelsProds['CODIPROD']))) $Codi_llista_es_agregacio=true;
					else $Codi_llista_es_agregacio=false;          
					$grupAct = $rowelsProds['GRUP'];               
					$subgrupAct = $rowelsProds['SUBGRUP'];         
					$fer = true;                                   
					if ($grupAct!=$grupAnt)                        
					   {                                           
						$bloc=array();                             
					    $bloc["elcodi"]='';                        
					    $bloc["elnom"]=$rowelsProds['GRUP'];    
                        $bloc["tipus"]  = 'grup';						
					    $bloc["css_sel_prods"]="gris";  // gris    
					    //echo $rowelsProds['GRUP']."<br>";        
                        $result['informacio']['block']['UnProductedeTots'][]=$bloc;
					   }                                           
					if ($subgrupAct!=$subgrupAnt) {                
						 if ($subgrupAct==$rowelsProds['NOMPROD']){
							$bloc=array();                         
							$bloc["elcodi"]=$rowelsProds['CODIPROD']; 
					        $bloc["elnom"] = $rowelsProds['SUBGRUP']; 
							$bloc["tipus"]  = 'subgrup';
					        $bloc["css_sel_prods"]=""; //          
					        //echo "--".$rowelsProds['SUBGRUP']."<br>";
                            $result['informacio']['block']['UnProductedeTots'][]=$bloc;
                            $fer = false;                          
						 }                                         
						 else                                      
						 {                                         
							$bloc=array();                         
							if ($Codi_llista_es_agregacio)         
						    {                                      
						   	  $bloc["elcodi"]=$preus->pertany_agregacio($CalculsExcel,$rowelsProds['CODIPROD']); 
					          $bloc["elnom"]= $rowelsProds['SUBGRUP']; 
							  $bloc["tipus"]  = 'subgrup';
					          $bloc["css_sel_prods"]="";           
						    }                                      
						    else                                   
						    {                                      
						   	  $bloc["elcodi"]='';                  
					          $bloc["elnom"]= $rowelsProds['SUBGRUP']; 
							  $bloc["tipus"]  = 'subgrup';
					          $bloc["css_sel_prods"]="";           
						    }                                      
					        //echo "--".$rowelsProds['SUBGRUP']."<br>";
                            $result['informacio']['block']['UnProductedeTots'][]=$bloc;
						   }                                       
					}                                              
					if ($fer)                                      
					   {                                           
					    $bloc=array();                             
						$bloc["elcodi"]	=	$rowelsProds['CODIPROD']; 
						$bloc["tipus"]  = 'codiproducte';
					    $bloc["elnom"]	=	$rowelsProds['NOMPROD']; 
					    $bloc["css_sel_prods"]	=	""; //         
					    //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rowelsProds['NOMPROD']."<br>";
                        $result['informacio']['block']['UnProductedeTots'][]=$bloc;
					   }	                                       
					$grupAnt=$grupAct;                             
					$subgrupAnt=$subgrupAct;		               
			}					                                   
		}
		
		return $result;  		
	}

}                                                                  
                                                                   
 ?>