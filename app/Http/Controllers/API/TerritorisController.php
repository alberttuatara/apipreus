<?php 

namespace App\Http\Controllers\API;

use DateTime;
use JWTAuth;
use APIException;
use DB;
use App\Models\Territori;
use App\Models\Aplicacio;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Illuminate\Support\Facades\Log;


class TerritorisController extends APIController
{
	
	public function __construct() {
		$user = JWTAuth::parseToken()->toUser();
		$this->user = $user;
		$this->timestart = new DateTime( "now" );
	}
	
	
	/**
	 * Retorna la informació bàsica de cada un dels territoris
	 * 
	 * @param Request $request 
	 * @return Response
	 */
	public function info(Request $request)
	{	
		$user = JWTAuth::parseToken()->toUser();
		$rol = $user->rol;
		if ($rol=='sc') { $territoris = Territori::all(); }
		if ($rol=='st') { $territoris = Territori::where('CODITERR',$user->territori)->get(); }
		
		return $this->respond([
			'info' => $territoris,
			'rol' => $rol
		]);
	}

	/**
	 * Retorna la informació bàsica d'un terriori
	 * 
	 * @param Request $request 
	 * @return Response
	 */
	public function infoUn($coditerr)
	{	
		$user = JWTAuth::parseToken()->toUser();
		$rol = $user->rol;
		if ($rol=='sc') { $territoris = Territori::all(); }
		if ($rol=='st') { $territoris = Territori::where('CODITERR',$user->territori)->get(); }
		
		return $this->respond([
			'info' => $territori
		]);
	}

	
	public function estatpreusUn($coditerr) {
		$result = array('estat'=>false, 'time'=>array(), 'informacio'=>array());
		
		$user = JWTAuth::parseToken()->toUser();
		$rol = $user->rol;
		if ($rol=='sc') { 
			$territoris = Territori::all(); 
			$result['informacio']['estatterritori'] = $territoris;
		}
		if ($rol=='st') { 
			if ($user->territori != $coditerr) {
				$result['estat']=false;
				$result['informacio']['msg']="El territori no és correcte";
			} else {
				
				$territoris = Territori::where('CODITERR',$user->territori)->first();
				$result['informacio']['estatterritori'] = $territoris;
				
				//$result['informacio']['user'] = $user;
				$aplicacio = Aplicacio::where('IDAPP', 'ppp')->first();
				// $result['informacio']['aplicacio'] = $aplicacio;
				$preus = new \App\Library\Prices('percebuts',$aplicacio->ENCURS_ANY_DADES,$aplicacio->ENCURS_MES_DADES,$user->territori,($aplicacio->BASE-2000),DB::connection());
				$elsCalFer = $preus->arrayOfProductsToDo();
				$elsFets = $preus->arrayOfProductsDone();
				$result['informacio']['percebutsperfer'] = count($elsCalFer);
				$result['informacio']['percebutsfets'] = count($elsFets);  
				$preus = new \App\Library\Prices('pagats',$aplicacio->ENCURS_ANY_DADES,$aplicacio->ENCURS_MES_DADES,$user->territori,($aplicacio->BASE-2000),DB::connection());
				$elsCalFer=$preus->arrayOfProductsToDo();
				$elsFets = $preus->arrayOfProductsDone();
				$result['informacio']['pagatsperfer'] = count($elsCalFer);
				$result['informacio']['pagatsfets'] = count($elsFets);
				$result['estat']=true;
			}
		}
		
		return $result;  
		
	}
	
	
	public function actualitzarestatpreusUn(Request $request) {
		
		// informa de "preus entrats"
		
		$result = array('estat'=>false, 'informacio'=>array());    
		$result['informacio']['path']=$request->path();                 
		$result['informacio']['params']=$request->all();  
	    
		$estatOK = false;
		$msg = '';
		
		$user = JWTAuth::parseToken()->toUser();
		$rol = $user->rol;
		// si és ST, només pot actualtzar el seu territori, i fer el pas de "entrada" a "revisant"
		if ($rol=='st') {
			$territori = $user->territori;
			$territori = Territori::where('CODITERR',$territori)->where('ESTAT',"entrada")->first();
			$territori->ESTAT = 'revisant';
		}
		if ($rol=='sc') {
			$territoriParm =  $result['informacio']['params']['territori'];
			$estatact =  $result['informacio']['params']['estatact'];  // actualment això no està informat correctament
			$estatseg =  $result['informacio']['params']['estatseg'];
			if (! (
						(($estatact=='entrada') and ($estatseg=='revisant'))  or
						(($estatact=='revisant') and ($estatseg=='entrada'))  or
						(($estatact=='revisant') and ($estatseg=='validat'))  or
						(($estatact=='validat') and ($estatseg=='revisant'))  
						) ) {
				$estatOK = false;
				$msg= "Canvi d'estat no permès: ".$estatact." --> ".$estatseg;
			} else {
				$territori = Territori::where('CODITERR',$territoriParm)->where('ESTAT',$estatact)->first();
				if ( !empty ( $territori ) ) {
					$territori->ESTAT = $estatseg;
				}
			}
		}
		if ( !empty ( $territori ) ) {
			$saveresult = $territori->save();
			$result['informacio']['saveresult'] = $saveresult;
			$estatOK = true;
		} else {
			$estatOK = false;
			$msg= "El Territori no s'ha trobat.";
		}
		if ($rol=='sc') { $territoris = Territori::all(); }
		if ($rol=='st') { $territoris = Territori::where('CODITERR',$user->territori)->get(); }
		$result['informacio']['territoris']=$territoris;   
		$result['informacio']['msg']=$msg;   
		$result['estat'] = $estatOK;
		return $result;
		
	}
	

	

}    
     
 ?>  